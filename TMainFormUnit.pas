unit TMainFormUnit;

interface

//------------------------------------------------------------------------------
//  Player Frames
//    TPlayerFrameUnit      Player Frame
//    TMixerFrameUnit       Mixer Frame
//    TLogFrameUnit         Log Frame
//    TVuFrameUnit          Volume Meter Frame
//    DetailFrameUnit       Artist, Album Details
//    TMediaInfoFrameUnit   Media Info Frame
//    TStateFrameUnit       State Frame
//
//  Player Units:
//    TMediaStringsUnit     Application Resources
//    TBaseObjectUnit       Base Object for most classes with references
//    TPlayerDeskBandUnit   Deskband Manager
//    TPlayerKeyboardUnit   KeyBoard and Keyboard Hook Manager
//
//    TBaseObjectUnit       Base Object and Reference Object
//    TPmaExceptionUnit
//------------------------------------------------------------------------------
//  Media Library:
//    TMediaFactoryUnit               Media Factory
//    TMediaUtilsUnit                 Media Uitilites
//    TMediaInfoUnit                  Media Information
//    TMediaPlayerUnit                Media Player
//    TMediaTaggerUnit                Media Tags
//    TMediaStorageUnit               Media Save and Load            (SAVE)
//    TMediaBaseUnit                  Media Base Class               (SAVE)
//      TMediaContainerUnit           Media Folder
//        TMediaLinkUnit              Media Folder
//          TMediaVideoLinkUnit       Media Video Link
//          TMediaArtistLinkUnit      Media Artist Link
//          TMediaPlayListLinkUnit    Media PlayList Link
//        TMediaDescUnit              Media Desc & Pictures          (SAVE)
//          TMediaArtistUnit          Media Artist
//          TMediaAlbumUnit           Media Album                    (SAVE)
//        TMediaPlayListUnit          Media PlayList                 (SAVE)
//      TMediaItemUnit                Media Item, Base Class         (SAVE)
//        TMediaMovieUnit             Media Movie                    (SAVE)
//        TMediaTrackUnit             Media Track
//      TMediaPropUnit                Media Properties               (SAVE)
//      TMediaDataBaseUnit            Media Dabatase
//    TMediaFileUnit                  Media Fole, File, Folder
//
//  Media Threads:
//    TMediaTestThreadUnit            Media Refresh Thread
//    TMediaTagsThreadUnit            Media Refresh Tags Thread
//    TMediaSoundAsThreadUnit         Media Refresh SoundAs Thread
//    TMediaMonThreadUnit             Media Monitoring Thread
//
//------------------------------------------------------------------------------
//  Components Aware of the Media Library
//    TBassPlayerUnit       Bass Player         (Component)
//    TMediaListViewUnit    ListView            (Component) TPmaListViewUnit
//    TMediaTreeViewUnit    TreeView            (Component) TGenTreeViewUnit
//      
//------------------------------------------------------------------------------
//  Components
//    TGenAppPropUnit       Application Prop    (Component)
//    TBkgThreadQueueUnit   Background Queue    (Component)
//    TPmaLogUnit           Logging             (Component)
//    TGenRemoteUnit        Remote Control      (Component)
//    TWmMsgFactoryUnit,    Message Factory     (Component)
//    TMasterMixerUnit;     Master Mixer        (Component)
//    TgenSliderUnit        Slider Control      (Component)
//    TGenButtonUnit        Button              (Component)
//    TGenListViewUnit      ListView            (Component)
//    TGenScrollBarUnit     Scroll Bar          (Component)
//    TGenMemoUnit          Memo                (Component)
//    TPmaListViewUnit      ListView            (Component)
//    TGenTreeViewUnit      TreeView            (Component)
//    TGenLightUnit         Light               (Component)
//    TGenProgressBarUnit   Progress Bar        (Component)
//    TGenFileSystemUnit    File System         (Component)
//    TGenSplitterUnit      Splitter Bar        (Component)
//    TGenHintManagerUnit   Hint Manager        (COmponent)
//    TGenPopupMenuUnit     Popup Menu          (Component)
//    TGenVuMeterUnit       Volume Meter        (Component)
//      TVuMeterUnit        Base Component
//      TVuBaseUnit         Base Volume Meter Class
//      TVuBarUnit          Bar and Bands Vu Class
//      TVuAnalyzerUnit     Analyzer Vu Class
//      TVuFlerpUnit        Flerp Vu Class
//      TVuAteroidsUnit     Asteroids Vu Class
//      TVuBassSolUnit      Bass Sol Component Class
//      TVuGraphicsUnit     Graphics used by Vu Meter
//
//------------------------------------------------------------------------------
//  Old Explorer Rect Windows
//    TGenRectWinUnit     Base Rect Window Class
//    TItemListWinUnit    Item List RectWin Class
//    TClockWinUnit       Clock RectWin Class
//    TSaverWinUnit       Screen Saver RectWin Class
//    TVideoInfoWinUnit   Video Info RectWin Class
//    TWoferWinUnit       Wofer RectWin Class
//    TEqWinUnit          Equalizer RectWin Class
//    TTrackInfoWinUnit   Track Info RectWin Class
//    TCurTrackWinUnit    CurTrack RectWin Class
//    TVuWinUnit          Vu RectWin Class
//
//------------------------------------------------------------------------------
//  Color Pick Dialog (PmaComp):
//    TGenColorPickUnit
//      TGenHsvUnit               HSV Object
//      THsvBaseFrameUnit         Hsv Base Frame
//      THsvHueFrameUnit          Hue Frame
//      THsvSatFrameUnit          Saturation Frame
//      THsvValueFrameUnit        Value Frame
//      THsvHueSatFrameUnit       Hue / Saturation Frame
//      THsvHueCircleFrameUnit    Hue Circle
//      THsvSatValFrameUnit       Saturation / Value Frame
//
//------------------------------------------------------------------------------
//  Font Pick Dialog (PmaComp):
//    TGenPickFontUnit            Pick a Font
//
//------------------------------------------------------------------------------
//  Generic Modules (PmaComp)
//    TGenStrUnit,              String Functions
//    TGenGraphicsUnit,         Graphic Functions
//    TGenHsvUnit,              HSV Object
//    TGenIniFileUnit           IniFiles Functions
//    TGenGraphicsUnit          Graphic Functions
//    TPmaClassesUnit           Class Handling
//------------------------------------------------------------------------------
//  Generic Modules (others)
//    TGenObjectUnit      TGenObject TGenObjectList
//    TGenLogUnit         Log Object
//    TGenRtfFileUnit     TextFile Class
//    TGenShellUnit       Shell Functions
//    TGenOpenDialogUnit  Open Dialog
//    TGenPickFolderUnit  Pick a Folder
//    TGenTimerUnit       TGenTimer Class
//    TGenTimeUnit        Time Functions
//    TGenHintUnit
//
//  Graphics Units;
//    TGeomCurveUnit      Curve
//    TGraphicsMathUnit   Graphic Matematics
//    TGraphicsPrimUnit   Graphic Primitives
//    TGeomUtilsUnit      General geometric functions Matrixes etc
//
//------------------------------------------------------------------------------
//  Old Things, still used:
//    TGenClassesUnit     Class Management
//    TGenClassesDlgUnit  Class Management Dialog
//------------------------------------------------------------------------------

//{$DEFINE LOGRESIZE} // Uncomment if Resizing should be logged

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, ComCtrls, ImgList, CommCtrl, StdCtrls, Menus, Contnrs,

  TPlayerDeskBandUnit,    // Player Deskband Manager
  TPlayerKeyboardUnit,    // Player Keyboard Manager

  TWmMsgFactoryUnit,      // Message Factory  (Component)
  TGenRemoteUnit,         // Remote Control   (Component)
  TBassPlayerUnit,        // Bass Player      (Component)
  TMasterMixerUnit,       // Master Mixer     (Component)
  TGenButtonUnit,         // Button           (Component)
  TGenSliderUnit,         // Slider           (Component)
  TPmaListViewUnit,       // ListView         (Component)
  TGenTreeViewUnit,       // TreeView         (Component)
  TGenFileSystemUnit,     // File System      (Component)
  TGenProgressBarUnit,    // Progress Bar     (Component)
  TMediaListViewUnit,     // Media ListView   (Component)
  TMediaTreeViewUnit,     // Media TreeView   (Component)
  TPmaLogUnit,            // Log Object       (Component)
  TGenPopupMenuUnit,      // Popup Menu       (Component)
  TGenSplitterUnit,       // Gen Splitter     (Component)
  TBkgThreadQueueUnit,    // Background Queue (Component)
  TMediaPlayerUnit,       // Media Player     (Component)
  TMediaTaggerUnit,       // Media Tagger     (Component)
  TGenHintManagerUnit,    // Hint Manager     (Component)

  TMediaBaseUnit,         // Media Base       (Media Db)
  TMediaArtistLinkUnit,   // Artist Links     (Media Db)
  TMediaPlayListLinkUnit, // PlayList Links   (Media Db)
  TMediaVideoLinkUnit,    // Video Links      (Media Db)
  TMediaPlayListUnit,     // Media Playlist   (Media Db)
  TMediaTrackUnit,        // Media Track      (Media Db)
  TMediaFactoryUnit,      // Media Factory    (Media Db)

  TPlayerFrameUnit,       // Player           (Frame)
  TLogFrameUnit,          // Log              (Frame)
  TMixerFrameUnit,        // Mixer            (Frame)
  TVuFrameUnit,           // Volume Meter     (Frame)
  DetailFrameUnit,        // Artist/Album     (Frame)
  TMediaInfoFrameUnit,    // Media Info       (Frame)
  TStateFrameUnit,        // State Info       (Frame)

  TGenAppPropUnit;        // Application Properties

//------------------------------------------------------------------------------
// Messages
//------------------------------------------------------------------------------
const
  //----------------------------------------------------------------------------
  //  MSG_APP_... (10) Application Messages                              MSG_ID
  //----------------------------------------------------------------------------
  // Escape Background Processes
  //  wParam : 0
  //  lParam : 0
  //  Owner  : App will tell Background Queue to Terminate all Threads
  //  Sender : Keyboard Hook

  MSG_APP_ESCAPE = WM_USER + 10; // User Want to Escape a Background Process

  MSG_APP_OPEN_TEXTFILE = WM_USER + 11; // Not Used

  //----------------------------------------------------------------------------
  // Select in ListView the Track currently Playing if Tracking is on
  //  wParam : 0
  //  lParam : 0
  //  Owner  : Application Sets Container in TreeView, and Track in ListView
  //  Sender : Application when Tracking is Turned On

  MSG_APP_SELECT_CURRENT_TRACK = WM_USER + 12;

  //----------------------------------------------------------------------------
  // Resize Left Panel
  //  wParam : 0
  //  lParam : 0
  //  Owner  : Application will resize Left Panel
  //  Sender : App when Splitter is moved or a Left Panel Window is On/Off

  MSG_APP_RESIZE_LEFTPANEL = WM_USER + 13;

  //----------------------------------------------------------------------------
  // Resize Right Panel
  //  wParam : 0
  //  lParam : 0
  //  Owner  : Application will resize Right Panel
  //  Sender : App when Splitter is moved or a Right Panel Window is On/Off
  //           or resized

  MSG_APP_RESIZE_RIGHTPANEL = WM_USER + 14;

  //----------------------------------------------------------------------------
  // Handle Tracking On/Off
  //  wParam : Message Command
  //  lParam : On/Off (Boolean)
  //  Owner  : Application
  //  Sender : Tracking Button in Player Frame

  MSG_APP_TRACKING = WM_USER + 15;

//------------------------------------------------------------------------------
//  Main Form
//------------------------------------------------------------------------------
type
  TPlayerMainForm = class(TForm)
    ImageList: TImageList;
    ListViewPopUp: TPopupMenu;
    MainPanel: TPanel;
    LeftPanel: TPanel;
    LeftSplitter: TSplitter;
    RightPanel: TPanel;
    TextMemo: TMemo;
    VuFrame: TVuFrame;
    Remote: TGenRemote;
    MsgPump: TWmMsgFactory;
    MasterMixer: TMasterMixer;
    BassPlayer: TBassPlayer;
    LogFrame: TLogFrame;
    FileSystem: TGenFileSystem;
    TheLog: TPmaLog;
    MediaDb: TMediaFactory;
    LineView: TMediaListView;
    TreeView: TMediaTreeView;
    TreeMenu: TGenPopupMenu;
    ArtistFrame: TDetailFrame;
    AlbumFrame: TDetailFrame;
    HighSplitter: TGenSplitter;
    LowSplitter: TGenSplitter;
    BkgThreadQueue: TBkgThreadQueue;
    MediaInfoFrame: TMediaInfoFrame;
    MediaPlayer: TMediaPlayer;
    MediaTagger: TMediaTagger;
    MixerFrame: TMixerFrame;
    AppProp1: TAppProp;
    GenHintManager1: TGenHintManager;
    StateFrame: TStateFrame;
    PlayerFrame: TPlayerFrame;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure LeftSplitterCanResize(Sender: TObject; var NewSize: Integer;
      var Accept: Boolean);
    procedure LeftPanelResize(Sender: TObject);
    procedure RightPanelResize(Sender: TObject);
    procedure ListViewPopUpPopup(Sender: TObject);
    //procedure TreeMenuPopup(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCanResize(Sender: TObject; var NewWidth,
      NewHeight: Integer; var Resize: Boolean);
    procedure LineViewSelect(Sender: TObject; Item: TPmaListItem;
      Selected: Boolean);
    procedure TreeViewSelect(Sender: TObject; Node: TGenTreeNode;
      Selected: Boolean);
    procedure MainLog(sLine: String);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure LogOutPut(sLine: String);
    procedure MediaDbPlayTrack(PathName: String);
     procedure MediaDbTrackInfo(const TrackFile: String;
      out BitRate: Cardinal; out Length: Integer);
    procedure HighSplitterMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure HighSplitterMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure HighSplitterMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure LowSplitterMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure LowSplitterMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure LowSplitterMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure TreeViewPopupMenu(Sender: TObject; Index: Integer;
      PopupMenu: TGenPopupMenu);
    procedure MediaDbMediaDestroyed(Media: TMediaBase);
    procedure FormShow(Sender: TObject);
  private
    objState : integer;

    //--------------------------------------------------------------------------
    // Extra Application Properties
    //--------------------------------------------------------------------------

    objTracking     : TGenAppPropBool;    // Tracking On/Off
    objLeftSplitter : TGenAppPropInt;     // Left Splitter Position

    objTimer : TTimer;

    //--------------------------------------------------------------------------
    // Subscribers
    //--------------------------------------------------------------------------

    objSubscriber : TWmMsgSubscriber; // Me as a Subscriber

  protected

    procedure OnPmaException (Sender: TObject; E: Exception);

    procedure StartUp;
    procedure ShutDown;

    // Initiate All Extra Application Properties

    procedure InitAppProps;

    procedure OnMediaLoad(Sender : TObject);

    //--------------------------------------------------------------------------
    // Textfile Editing
    //--------------------------------------------------------------------------
  private

    objTextFileEdit : boolean;       // TextFile Editing is On
    //objTextFile     : TDocTextFile;  // TextFile beeing edited

    //--------------------------------------------------------------------------
    // High Splitter
    //--------------------------------------------------------------------------

    objHighSplitterDown : boolean;
    objHighSplitterLast : integer;

    objLowSplitterDown : boolean;
    objLowSplitterLast : integer;

    //--------------------------------------------------------------------------
    // Application Window State (used for Telling Deskband)
    //--------------------------------------------------------------------------

    procedure OnMyMinimize   (Sender: TObject);
    procedure OnMyRestore    (Sender: TObject);
    procedure OnMyActivate   (Sender: TObject);
    procedure OnMyDeActivate (Sender: TObject);

    //--------------------------------------------------------------------------
    // Device Messages for Refreshing Media
    //--------------------------------------------------------------------------

    // Manage Device Change Message from Windows

    procedure WMDeviceChange(Var Msg : TMessage); message WM_DEVICECHANGE;

    //--------------------------------------------------------------------------
    // My Message Processing
    //--------------------------------------------------------------------------

    procedure InitMyMsgProcess;
    procedure ProcessMyMsg(var Msg : TMsg);

    procedure OnShowClasses(Sender : TObject);

    //--------------------------------------------------------------------------
    // Textfile Editing
    //--------------------------------------------------------------------------

    //procedure TextFileInitiate;
    //procedure TextFileStartup(const pTextFile : TObject);
    //procedure TextFileShutDown;

    //--------------------------------------------------------------------------
    // Player Message Management
    //--------------------------------------------------------------------------

    //--------------------------------------------------------------------------
    // Media Db
    //--------------------------------------------------------------------------

    //procedure OnRemoveLink (Sender : TObject);

    procedure OnMediaRebuild          (Sender : TObject);
    procedure OnMediaMonitor          (Sender : TObject);
    procedure OnTerminateAllProcesses (Sender : TObject);
    procedure OnNewMediaVideoLink     (Sender : TObject);
    procedure OnNewMediaMusicLink     (Sender : TObject);
    procedure OnNewMediaPlayListLink  (Sender : TObject);

    //--------------------------------------------------------------------------
    // LineView
    //--------------------------------------------------------------------------

    procedure DisplayHint(Sender: TObject);
  public
end;

  procedure MyLog(Line:string);

var
  PlayerMainForm: TPlayerMainForm;

//------------------------------------------------------------------------------
//
//                              IMPLEMENTATION
//
//------------------------------------------------------------------------------
implementation

uses
  Math,
  StrUtils,
  MMSystem,
  TMyHintUnit,

  TGenStrUnit,        // Strings
  TPmaFormUtils,      // Form Utils
  TGenGraphicsUnit,   // Graphic Function
  TPmaTimerUnit,      // Timer Object
  TGenPickFontUnit,   // Pick Font
  TGenColorPickUnit,  // Pick COlor
  TGenPickFolderUnit, // Pick Folder
  TBaseObjectUnit,
  
  TMediaUtilsUnit,    // Media Utils
  TMediaContainerUnit,// Media Container
  TMediaArtistUnit,   // Media Artist

  TMediaInfoUnit,     // Media Info
  TMyExceptionDlgUnit,
  TPmaExceptionUnit,
  TPmaClassesUnit;    // Classes

{$R *.dfm}

const
  BRD = 2;

  StateOpening = 0;
  StateRunning = 1;
  StateClosing = 2;

//------------------------------------------------------------------------------
//  Free Function For Logging
//------------------------------------------------------------------------------
procedure MyLog(Line:string);
begin
  if Assigned(PmaLog) then
    PmaLog.Log(Line);
end;
//------------------------------------------------------------------------------
//
//                                OPEING PLAYER
//
//------------------------------------------------------------------------------
//  Form Create
//------------------------------------------------------------------------------
procedure TPlayerMainForm.FormCreate(Sender: TObject);
begin
  objState := StateOpening;

  Application.Title := 'Player';
  self.Caption      := 'Player';

  MainLog('');
  TheLog.LogMem('Opening ' + Self.Caption + ' (' + self.ClassName + ')');

  Application.OnException := OnPmaException;

  SetMainThreadId(Windows.GetCurrentThreadId);
  MainLog('  Main Thread Id ' + INtToStr(Windows.GetCurrentThreadId));

  // Application Title and Main Form Captopn MUST be 'Player' for the DeskBand
  // can find the Window

  Application.OnHint := DisplayHint;

  //----------------------------------------------------------------------------
  // Startup Application Properties, and Add the Extra for this App
  //----------------------------------------------------------------------------

  App.StartUp;
  App.pAutoSave := true;
  
  InitAppProps;

  //----------------------------------------------------------------------------
  // Startup All Generic Components
  //----------------------------------------------------------------------------

  MainLog('');
  MainLog('StartUp Components:');

  HintManager.Startup;
  TheMediaPlayer.Startup;
  TreeMenu.StartUp;
  Remote.StartUp;
  MasterMixer.StartUp;

  //----------------------------------------------------------------------------
  // Initiate Form's own Message Handler
  //----------------------------------------------------------------------------

  InitMyMsgProcess;

  //----------------------------------------------------------------------------
  // Setup TextFile Editing
  //----------------------------------------------------------------------------

  //TextFileInitiate;

  //----------------------------------------------------------------------------
  // Startup All Frames
  //----------------------------------------------------------------------------

  MainLog('');
  MainLog('StartUp Frames:');

  ArtistFrame.pArtist := true;
  AlbumFrame.pArtist  := false;

  StartUpAllFrames(self, true);

  //----------------------------------------------------------------------------
  // Startup Bass Player
  //----------------------------------------------------------------------------

  ThePlayer.pMsgIdPlayNext := MSG_MP_PLAY_NEXT;
  ThePlayer.StartUp;

  //----------------------------------------------------------------------------
  // Startup Main Form's own UI
  //----------------------------------------------------------------------------

  LeftPanel.Width := objLeftSplitter.pInt;
  self.LeftPanelResize(nil);

  HighSplitter.StartUp;
  LowSplitter.StartUp;

  self.RightPanelResize(nil);

  //----------------------------------------------------------------------------
  // Setup LineView
  //----------------------------------------------------------------------------

  LineView.Startup(MSG_MDB_MEDIA_CHANGED);

  //----------------------------------------------------------------------------
  // Startup Media Factory
  //----------------------------------------------------------------------------

  TheMedia.StartUp;

  //----------------------------------------------------------------------------
  // Start Player Keyboard Manager
  //----------------------------------------------------------------------------

  TPlayerKeyBoard.StartUp;

  //----------------------------------------------------------------------------
  // Startup Deskband Object
  //----------------------------------------------------------------------------

  TPlayerDeskBand.StartUp;

  // Manage Minimized, Restored etc. for the Deskband

  Application.OnMinimize   := OnMyMinimize;
  Application.OnRestore    := OnMyRestore;
  Application.OnActivate   := OnMyActivate;
  Application.OnDeActivate := OnMyDeactivate;

  //----------------------------------------------------------------------------
  // Start Running
  //----------------------------------------------------------------------------

  objState := StateRunning;

  TheLog.Log('');
  TheLog.LogMem('Opened');

  PostMsg(MSG_APP_TRACKING, WP_CMD_CHANGED, DWORD(objTracking.pBool));

  TheLog.Log(StringOfChar('-',31) + ' Player Initiated ' +
             StringOfChar('-',31));

  StateFrame.pMediaState := msLoading;
end;
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
procedure TPlayerMainForm.StartUp;
begin

end;
//------------------------------------------------------------------------------
//  Initiate Extra Application Properties
//------------------------------------------------------------------------------
procedure TPlayerMainForm.InitAppProps;
begin
  // Set Default Size and Position of Main Window

  if Equals(App.pMainRect, DefaultMainRect) then
    begin
      App.SetNewMainRect( Rect(50,50,
        Screen.DesktopWidth - 50, Screen.DesktopHeight - 50));
    end;

  // Add Left Splitter Position as Integer Property

  objLeftSplitter := App.CreatePropInt(prfAppPref,'LeftSplitter', 240);

  // Add Tracking On/Off as Boolean, with a Message Command

  objTracking := App.CreatePropBool(prfAppPref,'Tracking', true);

end;
//------------------------------------------------------------------------------
//
//                               EXCEPTION HANDLING
//
//------------------------------------------------------------------------------

procedure TPlayerMainForm.OnPmaException (Sender: TObject; E: Exception);
begin

  LogDirect('FATAL: ' +
      'The Player has encountered an Error, and it has to close.' + #13#10 +
      E.Message);

  if (objState = StateRunning) then
    begin
      Application.ShowException(E);
      Application.Terminate;
    end;
end;
//------------------------------------------------------------------------------
//
//                                CLOSING PLAYER
//
//------------------------------------------------------------------------------
procedure TPlayerMainForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  // Fade Out Player Volume to avoid clicks (0.5 s)

  self.SetFocus;
  
  BassPlayer.FadeOut;

  Action := caFree;

  ShutDown;
end;
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
procedure TPlayerMainForm.ShutDown;
begin
  objState := StateClosing;

  StateFrame.pMediaState := msClosing;

  TheLog.Log(StringOfChar('-',32) + ' Player Closing ' +
             StringOfChar('-',32));
             
  TheLog.LogMem('Closing');

  //----------------------------------------------------------------------------
  // ShutDown all things in the right order
  //----------------------------------------------------------------------------

  TheMedia.StopAllThings;      // Background Threads

  //----------------------------------------------------------------------------
  // ShutDown all Generic Components
  //----------------------------------------------------------------------------

  MainLog('');
  MainLog('ShutDown Components:');

  TPlayerDeskBand.ShutDown;    // Deskband
  TPlayerKeyBoard.ShutDown;    // Keyboard

  HintManager.ShutDown;
  Remote.ShutDown;
  MasterMixer.ShutDown;
  TreeMenu.ShutDown;
  TheMediaPlayer.ShutDown;

  //----------------------------------------------------------------------------
  // Log Message Factory before closing all Subscribers
  //----------------------------------------------------------------------------

  MsgFactory.LogSubscribers;

  //----------------------------------------------------------------------------
  // CLoase All Subscribtions
  //----------------------------------------------------------------------------

  MsgFactory.DeSubscribe(objSubscriber);
  objSubscriber := nil;

  //----------------------------------------------------------------------------
  // Shutdown TreeView and ListView to release references to Media
  //----------------------------------------------------------------------------

  MainLog('');
  MainLog('ShutDown MediaViews');

  try
    TreeView.ShutDown;
  except
    ON E:EXCEPTION DO
      begin
        MainLog('FATAL: TreeView.ShutDown');
        MyExceptionDlgShow('TreeView ShutDown: ' + E.Message);
      end;
  end;

  try
    LineView.ShutDown;
  except
    ON E:EXCEPTION DO
      begin
        MainLog('FATAL: LineView.ShutDown: ' + E.Message);
        MyExceptionDlgShow('LineView ShutDown: ' + E.Message);
      end;
  end;

  //----------------------------------------------------------------------------
  // ShutDown all Frames
  //----------------------------------------------------------------------------

  TheLog.Log('');
  TheLog.Log('Closing Frames');
  try
    StartUpAllFrames(self, false);
  except
    on E: Exception do
      MainLog('FATAL: ' + self.ClassName + '.FormDestroy Close Frames');
  end;

  HighSplitter.ShutDown;
  LowSplitter.ShutDown;

  //----------------------------------------------------------------------------
  // Save Main Window Position
  //----------------------------------------------------------------------------

  objLeftSplitter.pInt := LeftPanel.Width;

  //----------------------------------------------------------------------------
  // Save and ShutDown Application Properties
  //----------------------------------------------------------------------------

  App.ShutDown;


  TheLog.Log('');
  TheLog.Log('BassPlayer ShutDown:');
  TheLog.LogMem(' ');
  BassPlayer.ShutDown;
  TheLog.LogMem(' ');

  //----------------------------------------------------------------------------
  // Log Message Pump, All Should be Closed now
  //----------------------------------------------------------------------------

  MsgFactory.LogIt;

  //----------------------------------------------------------------------------
  // ShutDown Media Factory
  //----------------------------------------------------------------------------

  TheMedia.ShutDown;
  TheLog.LogMem(' ');

  //----------------------------------------------------------------------------
  // Log FileSystem
  //----------------------------------------------------------------------------

  FileSystem.LogIt;

  TBaseObject.LogIt;

  //----------------------------------------------------------------------------
  // ShutDown all Classes
  //----------------------------------------------------------------------------

  ClassFactory.SaveandClose;
end;
//------------------------------------------------------------------------------
//  Destroy Form
//------------------------------------------------------------------------------
procedure TPlayerMainForm.FormDestroy(Sender: TObject);
begin

  TheLog.Log('');
  TheLog.LogMem('MainForm Closed');
end;
//------------------------------------------------------------------------------
//
//                                 RUNNING PLAYER
//
//------------------------------------------------------------------------------
//  Forward the Hint to State Frame
//------------------------------------------------------------------------------
procedure TPlayerMainForm.DisplayHint(Sender: TObject);
begin
  StateFrame.SetHint(Application.Hint);
end;
//------------------------------------------------------------------------------
//  Startup My Messages Processing
//------------------------------------------------------------------------------
procedure TPlayerMainForm.InitMyMsgProcess;
begin
  // Set me up as a Subscriber

  objSubscriber := MsgFactory.Subscribe(self.ClassName, ProcessMyMsg);

  //----------------------------------------------------------------------------
  // Add Messages to Subscribe on
  //----------------------------------------------------------------------------

  // Resize Left Panel

  objSubscriber.AddMessage(MSG_APP_RESIZE_LEFTPANEL);

  // Resize Right Panel

  objSubscriber.AddMessage(MSG_APP_RESIZE_RIGHTPANEL);

  // Receive Tracking On/Off

  objSubscriber.AddMessage(MSG_APP_TRACKING);

  objSubscriber.AddMessage(MSG_APP_SELECT_CURRENT_TRACK);
  objSubscriber.AddMessage(MSG_MDB_PLAYLIST_CHG);

  // Player Messages

  objSubscriber.AddMessage(MSG_BASS_TRACK_START);

  // Media Db Messages

  objSubscriber.AddMessage(MSG_MDB_MEDIA_LOADED);   // Media Db Was Loaded

  // Escape

  objSubscriber.AddMessage(MSG_APP_ESCAPE);

  // Message for Starting and Stopping Progress Bar

  //objSubscriber.AddMessage(MSG_MDB_SHOW_PROGRESS);

  // Ui Color

  objSubscriber.AddMessage(MSG_UI_BACKCOLOR);
  objSubscriber.AddMessage(MSG_MDB_MEDIA_LOG);

  // Remote Controller

  objSubscriber.AddMessage(MSG_REMOTE_CMD);
end;
//------------------------------------------------------------------------------
//  Process My Messages
//------------------------------------------------------------------------------
procedure TPlayerMainForm.ProcessMyMsg(var Msg : TMsg);
begin
  case Msg.message of

    //--------------------------------------------------------------------------
    // User Interface
    //--------------------------------------------------------------------------

    MSG_APP_ESCAPE            : BkgQueue.TerminatAllProcesses;
    MSG_APP_RESIZE_LEFTPANEL  : LeftPanelResize (self);
    MSG_APP_RESIZE_RIGHTPANEL : RightPanelResize(self);

    //--------------------------------------------------------------------------
    // Media Db Messages
    //--------------------------------------------------------------------------

    MSG_MDB_MEDIA_LOADED    :
      begin
        objSubscriber.RemMessage(MSG_MDB_MEDIA_LOADED);

        TheLog.LogMem('Media Db Init');

        // Startup TreeView now that the Media is Loade

        TreeView.RefreshId := MSG_MDB_MEDIA_CHANGED;
        TreeView.StartUp;

        // Select Last Folder in TReeView

        TreeView.SelectByPath(TheMediaPlayer.pContainerPath);

        // Tell DeskBand the app is Activated

        TheDeskBand.SetAppWindowState(awsActive);

        // Tell TaskBand to Send all Media Playlists

        TheDeskBand.SendPlayLists;

        StateFrame.pMediaState := msRunning;
      end;

    //MSG_MDB_SHOW_PROGRESS  : OnMsgProgress(Msg); // Media Background Progress

    MSG_MDB_MEDIA_LOG :
      begin
        case Msg.wParam of

          mlBkgOpened :
            begin
              if (Msg.lParam <> 0) then
                MainLog('Thread Opened ' + TClass(Msg.lParam).ClassName)
              else
                MainLog('Thread Opened');
            end;

          mlBkgClosed :
            begin
              if (Msg.lParam <> 0) then
                MainLog('Thread Closed ' + TClass(Msg.lParam).ClassName)
              else
                MainLog('Thread Closed');
            end;

          mlBkgTime  : MainLog('Thread Time   ' + SecondsToStr(Msg.lParam));
          mlBkgCount : MainLog('Media Tested  ' + IntToStr(Msg.lParam));

          mlBkgTags   :
            begin
              if (Msg.lParam <> 0) then
               LogToFile('TagErr ' + TMediaBase(Msg.lParam).pPath);

              //TMediaTrack(Msg.lParam).LogTags;
            end;
          mlBkgV1Tags  :
            begin
              LogToFile('Id3v1 Tags: ' + TMediaBase(Msg.lParam).pPath);
            end;

        else
          MainLog('Media Log: wParam ' + IntToStr(Msg.wParam) +
                            ' lParam ' + IntToStr(Msg.lParam));
        end;
      end;

    MSG_MDB_PLAYLIST_CHG :
      begin
        // A Playlist has been Added, Changed, requeried etc.
        // Tell Deskband about it

        TheDeskBand.SendPlayLists;
      end;

    //--------------------------------------------------------------------------
    //
    //--------------------------------------------------------------------------

    MSG_APP_SELECT_CURRENT_TRACK : if Assigned(TheMediaPlayer) then
      begin
        // Select Current Playing Track

        if Assigned(TheMediaPlayer.pCurTrack) then
          begin
            // The Current Container can be an Artist, Album or a Playlist

            if Assigned(TheMediaPlayer.pContainer) then
              begin
                // If a Playlist, then select it, else select Track Parent

                if (TheMediaPlayer.pContainer is TMediaPlayList) then
                  TreeView.SelectByMedia(TheMediaPlayer.pContainer)
                else
                  TreeView.SelectByMedia(TheMediaPlayer.pCurTrack.pParent);

                LineView.SelectMedia(TheMediaPlayer.pCurTrack);

                // Show Media Information

                PostMsg(MSG_MIF_SHOW_MEDIAINFO, 0,
                          DWORD(TheMediaPlayer.pCurTrack));

                // Show Track Info (Artist and Album)

                PostMsg(MSG_MDL_SHOW_MEDIA_TRACK_INFO, 0,
                          DWORD(TheMediaPlayer.pCurTrack));
              end;
          end;
      end;

    MSG_APP_TRACKING :
      begin
        // Im owner of the Tracking Property

        if Msg.WParam = WP_CMD_SET then
          begin
            objTracking.pBool := Dw2Bool(Msg.LParam);
            PostMsg(MSG_APP_TRACKING, WP_CMD_CHANGED, DWORD(objTracking.pBool));
          end
        else if Msg.WParam = WP_CMD_TOGGLE then
          begin
            objTracking.Toggle;
            PostMsg(MSG_APP_TRACKING, WP_CMD_CHANGED, DWORD(objTracking.pBool));
          end
        else if Msg.WParam = WP_CMD_UPDATE then
          PostMsg(MSG_APP_TRACKING, WP_CMD_CHANGED, DWORD(objTracking.pBool));

        // If Tracking was turned on, Select Current Track

        if (Msg.WParam = WP_CMD_CHANGED) and objTracking.pBool then
          PostMsg(MSG_APP_SELECT_CURRENT_TRACK, 0, 0);
      end;

    //--------------------------------------------------------------------------
    // LineView
    //--------------------------------------------------------------------------

    //MSG_LISTVIEW_OPEN_TEXTFILE : OnLineViewOpenTextFile(Msg);

    //--------------------------------------------------------------------------
    // Player Commands
    //--------------------------------------------------------------------------

    MSG_BASS_TRACK_START :
      begin
        // If Tracking is on, Make Track Selected

        if objTracking.pBool then
          PostMessage(Application.Handle, MSG_APP_SELECT_CURRENT_TRACK, 0, 0);

        // Set Trackinfo text in State Bar

      end;

    //--------------------------------------------------------------------------
    // UI
    //--------------------------------------------------------------------------

    MSG_UI_BACKCOLOR :
      begin
        self.Color := TColor(Msg.lParam);
        self.Invalidate;
      end;

    //--------------------------------------------------------------------------
    // Remote Controller
    //--------------------------------------------------------------------------

    MSG_REMOTE_CMD :
      begin
        case Msg.wParam of
          VK_MEDIA_PLAY_PAUSE  : PostMsg(MSG_BASS_TRACK_PLAYPAUSE, 0, 0);
          MY_MEDIA_FORWARD     : PostMsg(MSG_BASS_FORWARD, 0, 0);
          MY_MEDIA_BACKWARD    : PostMsg(MSG_BASS_REWIND, 0, 0);

          MY_MEDIA_CH_UP       : MainLog('Remote: Channel Up');
          MY_MEDIA_CH_DOWN     : MainLog('Remote: Channel Down');
          MY_MEDIA_GUIDE       : MainLog('Remote: Guide');

          MY_MEDIA_INFO        : MainLog('Remote: Information');
          MY_MEDIA_MENU        : MainLog('Remote: Menu');
          MY_MEDIA_LIVETV      : MainLog('Remote: Live Tv');
          MY_MEDIA_RECORDEDTV  : MainLog('Remote: Recorded Tv');
          MY_MEDIA_TELETEXT    : MainLog('Remote: Tele Text');

          MY_MEDIA_RED         : MainLog('Remote: Red');
          MY_MEDIA_GREEN       : MainLog('Remote: Green');
          MY_MEDIA_YELLOW      : MainLog('Remote: Yellow');
          MY_MEDIA_BLUE        : MainLog('Remote: Blue');
        end;
      end;
  end;
end;
//------------------------------------------------------------------------------
//  Some Media Was Destoyed
//------------------------------------------------------------------------------
procedure TPlayerMainForm.MediaDbMediaDestroyed(Media: TMediaBase);
begin
  if Assigned(Media) and (Media is TMediaBase) then
    begin
      //MainLog('Destoryed ' + Media.pPath);

      // Tell Media Player

      //TheMediaPlayer.MediaDestroyed(Media);

      // Tell Line View

      //LineView.MediaDestroyed(Media);

      // Tell Tree View

      TreeView.MediaDestroyed(Media);

    end;
end;
//------------------------------------------------------------------------------
//
//                           APPLICATION WINDOW STATE
//
//------------------------------------------------------------------------------
// Application Window is Minimized
//------------------------------------------------------------------------------
procedure TPlayerMainForm.OnMyMinimize (Sender: TObject);
begin
  inherited;
  TheDeskBand.SetAppWindowState(awsMinimized);
end;
//------------------------------------------------------------------------------
// Application Window is Restored
//------------------------------------------------------------------------------
procedure TPlayerMainForm.OnMyRestore  (Sender: TObject);
begin
  inherited;
  TheDeskBand.SetAppWindowState(awsRestored);
end;
//------------------------------------------------------------------------------
// Application Window is Activated
//------------------------------------------------------------------------------
procedure TPlayerMainForm.OnMyActivate (Sender: TObject);
begin
  inherited;
  TheDeskBand.SetAppWindowState(awsActive);
end;
//------------------------------------------------------------------------------
// Application Window is DeActivated
//------------------------------------------------------------------------------
procedure TPlayerMainForm.OnMyDeActivate (Sender: TObject);
begin
  inherited;
  TheDeskBand.SetAppWindowState(awsInactive);
end;
//------------------------------------------------------------------------------
//
//                                  MEDIA DB
//
//------------------------------------------------------------------------------
//  CallBack for Media Player used when it needs to Play a Track
//------------------------------------------------------------------------------
procedure TPlayerMainForm.MediaDbPlayTrack(PathName: String);
begin
  BassPlayer.Play(PathName);
end;
//------------------------------------------------------------------------------
//  Callback used when Media Db need Track information
//------------------------------------------------------------------------------
procedure TPlayerMainForm.MediaDbTrackInfo(const TrackFile: String;
  out BitRate: Cardinal; out Length: Integer);
var
  Res : DWORD;
  Len : double;
begin
  TBassPlayer.GetTrackData(TrackFile, Res, Len, BitRate);
  Length := round(Len);
end;
//------------------------------------------------------------------------------
// Event: Some Device has changed
//------------------------------------------------------------------------------
Procedure TPlayerMainForm.WMDeviceChange(Var Msg: TMessage);
Const
  DBT_DEVICEARRIVAL           = $8000; // A device has been inserted and is now available
  DBT_DEVICEQUERYREMOVE       = $800;  // Permission to remove a device is requested
  DBT_DEVICEQUERYREMOVEFAILED = $8002; // Request to remove a device has been canceled
  DBT_DEVICEREMOVEPENDING     = $8003; // Device is about to be removed
  DBT_DEVICEREMOVECOMPLETE    = $8004; // Device has been removed
  DBT_DEVICETYPESPECIFIC      = $8005; // Device Specific Event
  DBT_CONFIGCHANGED           = $008;  // Current configuration has changed

  DBTF_Media        = $0001;
  DBT_DevTyp_Volume = $0002;

begin

  // If Drive Arrived or was Removed we trigger a refresh of my Computer Node
  // Then we tell TreeView and FileView to refresh also

  if (Msg.WParam = DBT_DEVICEARRIVAL)then
    begin
      //MainLog('Device Arrived');
      PostMsg(MSG_MDB_MEDIA_ARRIVED,0,0);
    end;

  if (Msg.WParam = DBT_DEVICEREMOVECOMPLETE) then
    begin
      //MainLog('Device Removed');
      PostMsg(MSG_MDB_MEDIA_REMOVED,0,0);
    end;
End;
//------------------------------------------------------------------------------
//
//                                   TREE VIEW
//
//------------------------------------------------------------------------------
//  User Selected a new Tree Node
//------------------------------------------------------------------------------
procedure TPlayerMainForm.TreeViewSelect
    (Sender: TObject; Node: TGenTreeNode; Selected: Boolean);
begin
  if (objState = StateRunning) and Assigned(Node) and Selected then
    begin
      // Update the ListView

      LineView.SetContent(TreeView.pMedia, TreeView.pPath);
    end;
end;
//------------------------------------------------------------------------------
//
//                                 TREEVIEW MENUS
//
//------------------------------------------------------------------------------
//  Build TReeView Popup Menu
//------------------------------------------------------------------------------
procedure TPlayerMainForm.TreeViewPopupMenu(
              Sender: TObject; Index: Integer; PopupMenu: TGenPopupMenu);
var
  pPref : TGenMenuItem;
  pMenu : TGenMenuItem;
begin
  // Menu is already cleared and set by TMediaTreeView

  // Add Media Background things

  pPref := TGenMenuItem.Create(PopupMenu);
  pPref.Caption := 'Media Library ';
  PopupMenu.Items.Add(pPref);

    pMenu := TGenMenuItem.Create(PopupMenu);
    pMenu.Caption := 'Rebuild MDb ';
    pMenu.OnClick := OnMediaRebuild;
    pPref.Add(pMenu);

    pMenu := TGenMenuItem.Create(PopupMenu);
    pMenu.Caption := 'Monitor ';
    pMenu.OnClick := OnMediaMonitor;
    pMenu.Checked := TheMedia.pMediaMonitor;
    pPref.Add(pMenu);

    pMenu := TGenMenuItem.Create(PopupMenu);
    pMenu.Caption := 'Stop All Background Processes ';
    pMenu.OnClick := OnTerminateAllProcesses;
    pMenu.Enabled := (BkgQueue.pCount > 0);
    pPref.Add(pMenu);

  // Add New SubMenu if nothing is Selected

  if (not Assigned(TreeView.Selected)) then
    begin
      pPref := TGenMenuItem.Create(PopupMenu);
      pPref.Caption := 'New ';
      PopupMenu.Items.Add(pPref);

      pMenu := TGenMenuItem.Create(PopupMenu);
      pMenu.Caption := 'Video Link';
      pMenu.OnClick := OnNewMediaVideoLink;
      pPref.Add(pMenu);

      pMenu := TGenMenuItem.Create(PopupMenu);
      pMenu.Caption := 'Music Link';
      pMenu.OnClick := OnNewMediaMusicLink;
      pPref.Add(pMenu);

      pMenu := TGenMenuItem.Create(PopupMenu);
      pMenu.Caption := 'PlayList Link';
      pMenu.OnClick := OnNewMediaPlayListLink;
      pPref.Add(pMenu);
    end;

  pPref := TGenMenuItem.Create(PopupMenu);
  pPref.Caption := 'Preferences  ';
  PopupMenu.Items.Add(pPref);

    // Add Application Preferences

    App.AddPrefMenu(PopupMenu, pPref, true, true);

    // Add Show Classes

    pMenu := TGenMenuItem.Create(PopupMenu);
    pMenu.Caption := 'Show Classes';
    pMenu.OnClick := OnShowClasses;
    pPref.Add(pMenu);
end;
//------------------------------------------------------------------------------
//  Create a New Video Link
//------------------------------------------------------------------------------
procedure TPlayerMainForm.OnMediaRebuild(Sender: TObject);
begin
  TheMedia.RebuildMediaDb;
end;
//------------------------------------------------------------------------------
//  Create a New Video Link
//------------------------------------------------------------------------------
procedure TPlayerMainForm.OnMediaMonitor(Sender: TObject);
begin
  TheMedia.pMediaMonitor := not TheMedia.pMediaMonitor;
end;
//------------------------------------------------------------------------------
//  Create a New Video Link
//------------------------------------------------------------------------------
procedure TPlayerMainForm.OnTerminateAllProcesses(Sender: TObject);
begin
  BkgQueue.TerminatAllProcesses;
  TheMedia.pMediaMonitor := false;
end;
//------------------------------------------------------------------------------
//  Create a New Video Link
//------------------------------------------------------------------------------
procedure TPlayerMainForm.OnNewMediaVideoLink(Sender: TObject);
var
  Path : string;
  Link : TMediaBase;
begin
  Path := TGenPickFolder.PickFolder('', 'Pick Video Folder');
  if (length(Path) > 0) then
    begin
      MainLog('Add Music Link: ' + Path);

      // Lock Media Db and Create an Video Link as a Child

      if MediaBeginWrite(TheMedia.pMDB) then
        begin
          Link := TMediaVideoLink.CreateBase(TheMedia.pMDB, Path);
          TheMedia.pMDB.EndWrite;

          // Make the Link Visible, and then Select It

          TreeView.RefreshNodes;
          TreeView.SelectByMedia(Link);
        end;
    end;
end;
//------------------------------------------------------------------------------
//  Create a New Music Link
//------------------------------------------------------------------------------
procedure TPlayerMainForm.OnNewMediaMusicLink(Sender: TObject);
var
  Path : string;
  Link : TMediaBase;
begin
  Path := TGenPickFolder.PickFolder('', 'Pick Music Folder');
  if (length(Path) > 0) and
     SysUtils.DirectoryExists(Path) then
    begin
      MainLog('Add Music Link: ' + Path);

      // Lock Media Db and Create an Artist Link as a Child

      if MediaBeginWrite(TheMedia.pMDB) then
        begin
          Link := TMediaArtistLink.CreateBase(TheMedia.pMDB, Path);
          TheMedia.pMDB.EndWrite;

          // Make the Link Visible, and then Select It

          TreeView.RefreshNodes;
          TreeView.SelectByMedia(Link);

          // Start a Background Refresh on all Media

          PostMsg(MSG_MDB_MEDIA_ARRIVED, 0, 0);
        end;
    end;
end;
//------------------------------------------------------------------------------
//  Create a New Album
//------------------------------------------------------------------------------
procedure TPlayerMainForm.OnNewMediaPlayListLink(Sender: TObject);
var
  Path : string;
  Link : TMediaBase;
begin
  Path := TGenPickFolder.PickFolder('', 'Pick PlayList Folder');
  if (length(Path) > 0) then
    begin
      MainLog('Add PlayList Link: ' + Path);

      // Lock Media Db and Create an PlayList Link as a Child

      if MediaBeginWrite(TheMedia.pMDB) then
        begin
          Link := TMediaPlayListLink.CreateBase(TheMedia.pMDB, Path);
          TheMedia.pMDB.EndWrite;

          // Make the Link Visible, and then Select It

          TreeView.RefreshNodes; 
          TreeView.SelectByMedia(Link);
        end;
    end;
end;
//------------------------------------------------------------------------------
//  Toggle Preferences on / off
//------------------------------------------------------------------------------
procedure TPlayerMainForm.OnShowClasses(Sender : TObject);
begin
 //  TGenClasses.ShowClasses;
end;
//------------------------------------------------------------------------------
//
//                                   LINE VIEW
//
//------------------------------------------------------------------------------
//  User Selected a ListItem
//------------------------------------------------------------------------------
procedure TPlayerMainForm.LineViewSelect(Sender: TObject; Item: TPmaListItem;
  Selected: Boolean);
var
  Media    : TMediaBase;
begin
  // ShutDown Any Opened Textfile

  //TextFileShutDown;

  if Assigned(Item) and Assigned(Item.Data) and (Item.Data is TMediaBase) then
    begin
      Media := Item.Data as TMediaBase;

      // Do a Validated Refresh of Media

      MediaRefresh(self.ClassType, Media, true);

      // Show Media Information

      PostMsg(MSG_MIF_SHOW_MEDIAINFO, 0, DWORD(Media));

      // Show Media Track Info (Artist Album)

      PostMsg(MSG_MDL_SHOW_MEDIA_TRACK_INFO, 0, DWORD(Media));
    end;
end;
//------------------------------------------------------------------------------
//
//                               LINEVIEW MENUS
//
//------------------------------------------------------------------------------
//  The ListView Popup Menu is Opened   TODO
//------------------------------------------------------------------------------
procedure TPlayerMainForm.ListViewPopUpPopup(Sender: TObject);
begin
  // Clear All Menu Items

  ListViewPopUp.Items.Clear;
end;
//------------------------------------------------------------------------------
//  Open a Text File
//------------------------------------------------------------------------------
(*procedure TPlayerMainForm.OnLineViewOpenTextFile(var Msg : TMsg);
begin
  //TheLog.Log('Open Textfile ' + CurListItem.pPath);
  //TextFileStartup(CurListItem);
end;*)
//------------------------------------------------------------------------------
//
//                               TEXTFILE EDITING
//
//------------------------------------------------------------------------------
//  Startup TextFile editing
//------------------------------------------------------------------------------
(*procedure TPlayerMainForm.TextFileInitiate;
begin
  objTextFileEdit        := false;
  //objTextFile            := nil;
  TextMemo.WordWrap      := true;
  TextMemo.WantTabs      := false;
  TextMemo.WantReturns   := true;
  TextMemo.ScrollBars    := ssVertical;
  TextMemo.ReadOnly      := false;
  TextMemo.HideSelection := false;
  TextMemo.Visible       := false;
end;*)
//------------------------------------------------------------------------------
//  Startup TextFile editing
//------------------------------------------------------------------------------
(*procedure TPlayerMainForm.TextFileStartup(const pTextFile : TObject);
begin
  TextFileShutDown;
  
  if (pTextFile <> nil) and (pTextFile IS TDocTextFile) then
    begin
      objTextFileEdit := true;
      objTextFile     := pTextFile as TDocTextFile;

      RightPanelResize(nil);

      TextMemo.Lines.LoadFromFile(objTextFile.pPath);

      TextMemo.WantReturns   := true;
      TextMemo.WordWrap      := true;
    end;
end; *)
//------------------------------------------------------------------------------
//  ShutDown TextFile editing
//------------------------------------------------------------------------------
(*procedure TPlayerMainForm.TextFileShutDown;
begin
  
  if objTextFileEdit then
    begin
      objTextFileEdit := false;

      if TheItems.IsItemRecursive(objTextFile) then
        begin
          TheLog.Log('Text File Saved ' + objTextFile.pPath);

        end;

      RightPanelResize(nil);
    end; 
end; *)
//------------------------------------------------------------------------------
//
//                                    LOGGING
//
//------------------------------------------------------------------------------
//  Log
//------------------------------------------------------------------------------
procedure TPlayerMainForm.MainLog(sLine: String);
begin
  TheLog.Log(sLine);
end;
//------------------------------------------------------------------------------
//  Callback for Logging to Log Window
//------------------------------------------------------------------------------
procedure TPlayerMainForm.LogOutPut(sLine: String);
begin
  LogFrame.LogOutPut(sLine);
end;
//------------------------------------------------------------------------------
//
//                                   RESIZING
//
//------------------------------------------------------------------------------
//  Form was resized
//------------------------------------------------------------------------------
procedure TPlayerMainForm.FormResize(Sender: TObject);
var
  Hgt : integer;
begin
{$IFDEF LOGRESIZE}
  TheLog.Log('');
  TheLog.Log('TMainForm.FormResize');
{$ENDIF}

  if (objState = StateRunning) then
  begin
  // Make sure the Size is within Limits

  if (self.Height < (100 + PlayerFrame.GetHeight +
      LogFrame.pHeight + MixerFrame.pHeight + VuFrame.pHeight)) then
    self.Height := (100 + PlayerFrame.GetHeight +
      LogFrame.pHeight + MixerFrame.pHeight + VuFrame.pHeight);

  Hgt := 0;

  // Set the Size and position of State Frame
  
  Inc(Hgt, StateFrame.GetHeight);
  SetCtrlSize(StateFrame, 0,
    self.ClientHeight - Hgt, self.ClientWidth, StateFrame.GetHeight);


  // Set the Size and position of Player Frame

  Inc(Hgt, TPlayerFrame.GetHeight);
  SetCtrlSize(PlayerFrame, 0,
    self.ClientHeight - Hgt, self.ClientWidth, TPlayerFrame.GetHeight);

  // Set the Size of the Main Panel

  SetCtrlSize(MainPanel, 0, 0, self.ClientWidth, self.ClientHeight - Hgt);
  end;
end;
//------------------------------------------------------------------------------
//  Left Panel has Resized
//------------------------------------------------------------------------------
procedure TPlayerMainForm.FormCanResize(Sender: TObject; var NewWidth,
  NewHeight: Integer; var Resize: Boolean);
begin
  if (NewWidth < PlayerFrame.GetMinWidth) then
    begin
      NewWidth := PlayerFrame.GetMinWidth;
      Resize := false;
    end;

  if (NewHeight < (100 + PlayerFrame.GetHeight +
        LogFrame.pHeight + MixerFrame.pHeight + VuFrame.pMinHeight)) then
    begin
      NewHeight := 100 + PlayerFrame.GetHeight +
        LogFrame.pHeight + MixerFrame.pHeight + VuFrame.pMinHeight;
      Resize := false;
    end;
end;
//------------------------------------------------------------------------------
//  Make Sure the Left Splitter is within Margin
//------------------------------------------------------------------------------
procedure TPlayerMainForm.LeftSplitterCanResize(Sender: TObject;
  var NewSize: Integer; var Accept: Boolean);
const
  RightMin = 200; // Right Margin in Prixels
begin
  if (NewSize < MixerFrame.pMinWidth) then
    begin
      NewSize := MixerFrame.pMinWidth;
      Accept := false;
    end
  else if (NewSize > (self.Width - RightMin)) then
    begin
      NewSize := self.Width - RightMin;
      Accept := false;
    end
end;
//------------------------------------------------------------------------------
//  Left Panel has Resized
//------------------------------------------------------------------------------
procedure TPlayerMainForm.LeftPanelResize(Sender: TObject);
begin
{$IFDEF LOGRESIZE}
  TheLog.Log('');
  TheLog.Log('  TMainForm.LeftPanelResize Begin');
{$ENDIF}

  // Make Sure the Window is Heigh enough

  if (self.Height < (100 + PlayerFrame.GetHeight +
      LogFrame.pHeight + MixerFrame.pHeight + VuFrame.pHeight)) then
    self.Height := (100 + PlayerFrame.GetHeight +
      LogFrame.pHeight + MixerFrame.pHeight + VuFrame.pHeight);

  // Set Log Frame Size

  SetCtrlSize(LogFrame, BRD, LeftPanel.Height - LogFrame.pHeight,
       LeftPanel.Width - BRD, LogFrame.pHeight);

  // Set Mixer Frame

  SetCtrlSize(MixerFrame, BRD, LogFrame.Top - MixerFrame.pHeight - BRD,
        LeftPanel.Width - BRD, MixerFrame.pHeight);

  // Set Volume Meter Frame

  SetCtrlSize(VuFrame, BRD, MixerFrame.Top - VuFrame.pHeight - BRD,
        LeftPanel.Width - BRD, VuFrame.pHeight);

  SetCtrlSize(TreeView, BRD, BRD, LeftPanel.Width - BRD, VuFrame.Top - BRD * 2);

{$IFDEF LOGRESIZE}
  TheLog.Log('  TMainForm.LeftPanelResize End');
{$ENDIF}
end;
//------------------------------------------------------------------------------
//  Right Panel has Resized
//------------------------------------------------------------------------------
procedure TPlayerMainForm.RightPanelResize(Sender: TObject);
begin
{$IFDEF LOGRESIZE}
  TheLog.Log('');
  TheLog.Log('  TMainForm.RightPanelResize Begin');
{$ENDIF}

  LineView.Left   := 2;
  LineView.Width  := RightPanel.Width - BRD - 2;
  LineView.Top    := BRD;

  // Set Album Frame Size

  if AlbumFrame.pVisible then
    SetCtrlSize(AlbumFrame,
      LineView.Left,
      RightPanel.Height - AlbumFrame.pVisHeight,
      LineView.Width,
      AlbumFrame.pVisHeight)
  else
    SetCtrlSize(AlbumFrame,
      LineView.Left,
      RightPanel.Height - AlbumFrame.pMinHeight,
      LineView.Width,
      AlbumFrame.pMinHeight);

  // Set Artist Frame Size

  if ArtistFrame.pVisible then
    SetCtrlSize(ArtistFrame,
      LineView.Left,
      AlbumFrame.Top - ArtistFrame.pVisHeight - BRD*2,
      LineView.Width,
      ArtistFrame.pVisHeight)
  else
    SetCtrlSize(ArtistFrame,
      LineView.Left,
      AlbumFrame.Top - ArtistFrame.pMinHeight - BRD*2,
      LineView.Width,
      ArtistFrame.pMinHeight);

  // Set Media Info Frame Size

  if MediaInfoFrame.pVisible then
    SetCtrlSize(MediaInfoFrame,
      LineView.Left,
      ArtistFrame.Top - MediaInfoFrame.pVisHeight - BRD*2,
      LineView.Width,
      MediaInfoFrame.pVisHeight)
  else
    SetCtrlSize(MediaInfoFrame,
      LineView.Left,
      ArtistFrame.Top - MediaInfoFrame.pMinHeight - BRD*2,
      LineView.Width,
      MediaInfoFrame.pMinHeight);

  if objTextFileEdit then
    begin
      SetCtrlSize(LineView, 2, BRD, RightPanel.Width - BRD - 2,
                  2 * RightPanel.Height div 3);

      if not TextMemo.Visible then TextMemo.Visible := true;

      SetCtrlSize(TextMemo, LineView.Left,
          LineView.Top + LineView.Height + BRD,
          LineView.Width,
          RightPanel.Top + RightPanel.Height - TextMemo.Top);
    end
  else
    begin
      if TextMemo.Visible then TextMemo.Visible := false;

      SetCtrlSize(LineView,
        LineView.Left,
        LineView.Top,
        LineView.Width,
        MediaInfoFrame.Top - LineView.Top - BRD*2);
    end;

  // High Splitter between LineView and Artist

  HighSplitter.Left   := LineView.Left;
  HighSplitter.Width  := LineView.Width;
  HighSplitter.Top    := MediaInfoFrame.Top + MediaInfoFrame.Height;
  HighSplitter.Height := BRD * 2;

  // Low Splitter between ArtistFrame and AlbumFrame

  LowSplitter.Left   := LineView.Left;
  LowSplitter.Width  := LineView.Width;
  LowSplitter.Top    := ArtistFrame.Top + ArtistFrame.Height;
  LowSplitter.Height := BRD * 2;

{$IFDEF LOGRESIZE}
  TheLog.Log('  TMainForm.RightPanelResize End');
{$ENDIF}
end;
//------------------------------------------------------------------------------
//  High Splitter Mouse Down
//------------------------------------------------------------------------------
procedure TPlayerMainForm.HighSplitterMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  if ArtistFrame.pVisible then
    begin
      //MainLog('High Down');
      objHighSplitterDown := true;
      objHighSplitterLast := HighSplitter.Top + Y;
    end;
end;
//------------------------------------------------------------------------------
//  High Splitter Mouse Move
//------------------------------------------------------------------------------
procedure TPlayerMainForm.HighSplitterMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
var
  dy : integer;
begin
  if objHighSplitterDown then
    begin
      dy := HighSplitter.Top + Y - objHighSplitterLast;
      if (abs(dy) <> 0) then
        begin
          //MainLog('High Move ' + IntToStr(dy));

          ArtistFrame.pVisHeight := ArtistFrame.pVisHeight - dy;

          objHighSplitterLast := HighSplitter.Top + Y;

          self.RightPanelResize(nil);
        end;
    end;
end;
//------------------------------------------------------------------------------
//  High Splitter Mouse Up
//------------------------------------------------------------------------------
procedure TPlayerMainForm.HighSplitterMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  objHighSplitterDown := false;
end;
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
procedure TPlayerMainForm.LowSplitterMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  if AlbumFrame.pVisible then
    begin
      //MainLog('High Down');
      objLowSplitterDown := true;
      objLowSplitterLast := LowSplitter.Top + Y;
    end;
end;
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
procedure TPlayerMainForm.LowSplitterMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
var
  dy : integer;
begin
  if objLowSplitterDown then
    begin
      dy := LowSplitter.Top + Y - objLowSplitterLast;
      if (abs(dy) <> 0) then
        begin
          //MainLog('Low Move ' + IntToStr(dy));

          AlbumFrame.pVisHeight := AlbumFrame.pVisHeight - dy;

          objLowSplitterLast := LowSplitter.Top + Y;

          self.RightPanelResize(nil);
        end;
    end;
end;
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
procedure TPlayerMainForm.LowSplitterMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  objLowSplitterDown := false;
end;
//------------------------------------------------------------------------------
//                              ININTIALIZATION
//------------------------------------------------------------------------------
procedure TPlayerMainForm.FormShow(Sender: TObject);
begin
  // Load Media if Not Already Loaded

  if not TheMedia.pLoaded then
    begin
      objTimer := TTimer.Create(nil);
      objTimer.Interval := 100;
      objTimer.OnTimer  := OnMediaLoad;
      objTimer.Enabled  := true;
      TheLog.Log('Show Form');
    end;
end;
//------------------------------------------------------------------------------
//                              ININTIALIZATION
//------------------------------------------------------------------------------
procedure TPlayerMainForm.OnMediaLoad(Sender : TObject);
begin
  objTimer.Enabled := false;
  objTimer.Free;
  PostMsg(MSG_MDB_LOAD_MEDIA,0,0);
end;

initialization
  TPmaClassFactory.RegClass(TPlayerMainForm,'TMainFormUnit');
end.

