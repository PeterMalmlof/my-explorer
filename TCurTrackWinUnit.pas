unit TCurTrackWinUnit;

interface
uses
  Windows, ExtCtrls, Graphics, Messages,

  TGenRectWinUnit,    // Base RectWin Class
  TGenAppPropUnit,    // Application Properties
  TTreeItemBaseUnit;

//------------------------------------------------------------------------------
//  MESSAGE IDS
//------------------------------------------------------------------------------
const
  WM_TRACK_ENDED = WM_USER + 219; // A new Item was Selected

//------------------------------------------------------------------------------
//  Object
//------------------------------------------------------------------------------
type TCurTrackWin = class(TGenRectWin)
  protected
    objTimer  : TTimer;
    objItem   : TTreeItemBase;
    objPos    : single;
    objLen    : single;
    objPaused : boolean;

    objDestRect : TRect; // Temp
    objSrcRect  : TRect;

    function  GetUid: string; override;

    procedure SetRect    (const Value : TRect);         override;
    procedure SetItem    (const Value : TTreeItemBase); override;
    procedure SetState   (const Value : integer);       override;

    procedure OnClock (Sender: TObject);

    class function GetDefaultRect: TRect; override;
  public
    constructor Create(const C : TCanvas); override;

    destructor  Destroy; override;

    procedure RefreshUi; override;
    procedure Paint;     override;

    function GetBackBitmap: TBitmap; override;

    procedure Refresh;
end;

implementation

uses
  SysUtils,
  Math,
  Types,
  Forms,

  TGenLogUnit,        // Log Object
  TGenStrUnit,        // String Functions
  TBassPlayerUnit,    // Bass Player Object

  TMediaPlayerResUnit,// Resource Strings
  TDocTrackUnit,      // Audio Track
  
  TGenClassesUnit;    // Class Management

//------------------------------------------------------------------------------
//  Constructor
//------------------------------------------------------------------------------
constructor TCurTrackWin.Create(const C : TCanvas);
begin
  inherited Create(C);

  objItem := nil;

  objTimer          := TTimer.Create(nil);
  objTimer.OnTimer  := OnClock;
  objTimer.Interval := 1000;
  objTimer.Enabled  := false;
end;
//------------------------------------------------------------------------------
//  Destructor
//------------------------------------------------------------------------------
destructor TCurTrackWin.Destroy;
begin
  objTimer.Free;

  inherited;
end;
//------------------------------------------------------------------------------
// Get Default Window Position, must be overridden by all Windows
//------------------------------------------------------------------------------
function TCurTrackWin.GetUid:string;
begin
  result := 'CurTrackWin';
end;
//------------------------------------------------------------------------------
// Get Default Window Position, must be overridden by all Windows
//------------------------------------------------------------------------------
class function TCurTrackWin.GetDefaultRect: TRect;
begin
  result := Rect(
    Screen.Width div 2,
    20,
    (Screen.Width div 2) + 80,
    80);
end;
//------------------------------------------------------------------------------
//  Set State (Override for special management)
//------------------------------------------------------------------------------
procedure TCurTrackWin.SetState(const Value : integer);
begin
  if (Value = StatePlayAudio) then
    begin
      objEnabled       := true;
      objVisible       := true;
      objTimer.Enabled := false;
    end
  else
    begin
      objEnabled       := false;
      objVisible       := false;
      objTimer.Enabled := false;
    end;
end;
//------------------------------------------------------------------------------
//  Set Rect
//------------------------------------------------------------------------------
procedure TCurTrackWin.SetItem (const Value : TTreeItemBase);
begin
  if (Value <> nil) and (Value Is TDocTrack) then
    begin
      objItem := Value;

      if (objItem <> nil) then Refresh;
    end
  else
    begin
      objItem := nil;
    end;
end;
//------------------------------------------------------------------------------
//  Refresh User Inter
//------------------------------------------------------------------------------
procedure TCurTrackWin.SetRect (const Value : TRect);
begin
  objRect.Left   := Value.Left;
  objRect.Top    := Value.Top;

  RefreshUi;
end;
//------------------------------------------------------------------------------
//  Refresh
//------------------------------------------------------------------------------
procedure TCurTrackWin.Refresh;
begin
  if (objItem <> nil) then
    begin
      objPos    := ThePlayer.pTrackPos;
      objLen    := ThePlayer.pTrackLength;
      objPaused := ThePlayer.pPaused;
      Paint;
    end;
end;
//------------------------------------------------------------------------------
//  Set Rect
//------------------------------------------------------------------------------
procedure TCurTrackWin.OnClock (Sender: TObject);
begin
  if objEnabled then
    begin
      if ThePlayer.pTrackEnded then
        begin
          PostMessage(Application.Handle, WM_TRACK_ENDED, 0, 0);
        end
      else
        begin
          objPos    := ThePlayer.pTrackPos;
          objLen    := ThePlayer.pTrackLength;
          objPaused := ThePlayer.pPaused;
          self.Paint;
        end;
    end;
end;
//------------------------------------------------------------------------------
//  Refresh Ui
//------------------------------------------------------------------------------
procedure TCurTrackWin.RefreshUi;
const
  MaxText = 'XX:XX / XX:XX ';
var
  S : TSize;
begin
  S := objCanvas.TextExtent(MaxText + resPaused);
  objRect.Right  := objRect.Left + S.cx + 6;
  objRect.Bottom := objRect.Top  + S.cy + 4;

  objMainRect.pRect := objRect;

  // The Drawing Bitmap must be the same size as Window

  objDrawBitmap.Width  := objRect.Right - objRect.Left;
  objDrawBitmap.Height := objRect.Bottom - objRect.Top;

  // Destination Rect is 1 less in all directions

  objDestRect := Rect (objRect.Left  + 1, objRect.Top    + 1,
                       objRect.Right - 1, objRect.Bottom - 1);

  // Source Rect is the Same Size as Destination Rect but on Drawing Bitmap

  objSrcRect := Rect(1, 1, objDrawBitmap.Width - 1, objDrawBitmap.Height - 1);

  // Set same Font Size size as Screen
  
  objDrawBitmap.canvas.Font       := objCanvas.Font;
  objDrawBitmap.Canvas.Font.Color := objForeColor;
end;
//------------------------------------------------------------------------------
//  Paint
//------------------------------------------------------------------------------
procedure TCurTrackWin.Paint;
const
  cDelim = ' / ';
var
  sTmp : string;
begin
  inherited;

  if objVisible and (objItem <> nil) then
    begin
      // Get the Text

      sTmp := ToTime(round(objPos)) + cDelim + ToTime(round(objLen));

      if objPaused then sTmp := sTmp + ' ' + resPaused;

      // Draw the Background Color first without Pen

      objDrawBitmap.Canvas.Brush.Color := objBackColor;
      objDrawBitmap.Canvas.Brush.Style := bsSolid;
      objDrawBitmap.Canvas.Pen.Style   := psClear;

      objDrawBitmap.Canvas.Rectangle(
        Rect(0, 0, objDrawBitmap.Width, objDrawBitmap.Height));

      // Draw Current Position and Lenght

      objDrawBitmap.Canvas.TextOut(1, 1, sTmp);

      // Copy Draw Bitmap to Screen

      objCanvas.CopyRect(objDestRect, objDrawBitmap.Canvas, objSrcRect);
    end;
end;
//------------------------------------------------------------------------------
//  Return The Draw Bitmap
//------------------------------------------------------------------------------
function TCurTrackWin.GetBackBitmap: TBitmap;
begin
  result := objDrawBitmap;
end;
//------------------------------------------------------------------------------
//                                 INITALIZE
//------------------------------------------------------------------------------
initialization
  FindClass(TCurTrackWin);
end.

