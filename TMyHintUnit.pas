unit TMyHintUnit;

interface

uses
  Windows, Controls, Classes, Graphics, Messages, Forms;

//------------------------------------------------------------------------------
// THintWindow
//------------------------------------------------------------------------------
type
  TMyHintWindow = class(THintWindow)
    constructor Create(AOwner: TComponent); override;

    destructor Destroy; override;
  private
    FActivating : Boolean;
    FRect       : TRect;

    //FBitmap : TBitmap;
  public
    procedure ActivateHint(R: TRect; const AHint: string); override;

    class procedure SetIcon(const Icon : TIcon);

    class procedure SetHintProperties(
              const Font      : TFont;
              const FontColor : TColor;
              const ForeColor : TColor;
              const BackColor : TColor);

  protected
    procedure Paint; override;
  published
    property Caption;
  end;

  {...}

implementation

var AppIcon : TIcon = nil;

var
  HintFont      : TFont;    // Font
  HintFontColor : TColor;   // Font Color
  HintForeColor : TColor;   // Foreground Color
  HintBackColor : TColor;   // Bacground COlor


//------------------------------------------------------------------------------
// Set Hint Properties
//------------------------------------------------------------------------------
class procedure TMyHintWindow.SetHintProperties(
              const Font      : TFont;
              const FontColor : TColor;
              const ForeColor : TColor;
              const BackColor : TColor);
begin
  HintFont      := Font;
  HintFontColor := FontColor;
  HintForeColor := ForeColor;
  HintBackColor := BackColor;
end;

class procedure TMyHintWindow.SetIcon(const Icon : TIcon);
begin
  AppIcon := Icon;
end;

constructor TMyHintWindow.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);

  {
   Hier k�nnen beliebige Schrift Eigenschaften gesetzt
   werden.

   Here you can set custom Font Properties:
   }

  if Assigned(HintFont) then
    begin
      Canvas.Font := HintFont;
    end
  else
    with Canvas.Font do
      begin
        Name := 'Verdana';
        Style := Style + [fsItalic];
        Color := HintFontColor;
      end;

  //if Assigned(AppIcon) then
  //  begin
  //    FBitmap := TBitmap.Create;
  //    FBitmap.Assign(AppIcon);
  //  end;

  self.DoubleBuffered := true;
end;

destructor TMyHintWindow.Destroy;
begin
  //if Assigned(FBitmap) then
  //  FBitmap.Free;

  inherited;
end;

procedure TMyHintWindow.Paint;
var
  R: TRect;
  //bmp: TBitmap;

begin
  R := ClientRect;
  Inc(R.Left, 2);
  Inc(R.Top, 2);

  {*******************************************************
   Der folgende Code ist ein Beispiel wie man die Paint
   Prozedur nutzen kann um einen benutzerdefinierten Hint
   zu erzeugen.

   The folowing Code ist an example how to create a custom
   Hint Object. :
   }

  //bmp := TBitmap.Create;
  //bmp.LoadfromFile('D:\hint.bmp');

  with Canvas do
  begin
    Brush.Style := bsSolid;
    Brush.Color := HintForeColor;
    Pen.Color   := HintForeColor;
    
    Rectangle(0, 0, 18, R.Bottom + 1);


    //Draw(2,(R.Bottom div 2) - (bmp.Height div 2), bmp);
  end;

  if Assigned(AppIcon) then
    begin
      //Canvas.d
      Canvas.Draw(2,(R.Bottom div 2) - (AppIcon.Height div 2), AppIcon);
    end;

  //bmp.Free;

  //Beliebige HintFarbe
  //custom Hint Color
  Color := HintBackColor;

  Canvas.Brush.Style := bsClear;

  DrawTextEx(Canvas.Handle, PAnsiChar(Caption), -1, FRect, DT_LEFT, nil);

  //Canvas.TextOut(20, (R.Bottom div 2) - (Canvas.Textheight(Caption) div 2), Caption);

end;

procedure TMyHintWindow.ActivateHint(R: TRect; const AHint: string);
begin
  FActivating := True;

  try
    Caption := AHint;

    // Calculate the Text Rect

    FRect := Rect(0,0,0,0);
    DrawTextEx(Canvas.Handle, PAnsiChar(Caption), -1, FRect,
      DT_LEFT or DT_CALCRECT, nil);

    // Get Cursor Position and use that

    R.Left := Mouse.CursorPos.X - (20 + FRect.Right) div 2;
    R.Top  := Mouse.CursorPos.Y - FRect.Bottom - 14;
    
    // Set the "Height" Property of the Hint

    R.Bottom := R.Top + FRect.Bottom + 4;

    // Set the "Width" Property of the Hint

    R.Right := R.Left + 20 + FRect.Right + 4;

    Inc(FRect.Left, 20);
    Inc(FRect.Right, 20);

    UpdateBoundsRect(R);

    // Make Sure its stays inside Screen
    
    if (R.Bottom > Screen.DesktopHeight) then
      R.Top := Screen.DesktopHeight - Height;

    if (R.Top < Screen.DesktopTop) then
      R.Top := Screen.DesktopTop;

    if ((R.Left + Width) > Screen.DesktopWidth) then
      R.Left := Screen.DesktopWidth - Width;

    if (R.Left < Screen.DesktopLeft) then
      R.Left := Screen.DesktopLeft;

    SetWindowPos(Handle, HWND_TOPMOST, R.Left, R.Top, Width, Height,
      SWP_SHOWWINDOW or SWP_NOACTIVATE);

    //Invalidate;
  finally
    FActivating := False;
  end;
end;

end.
