unit TLogFrameUnit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, 
  Dialogs, StdCtrls, ExtCtrls,

  TGenAppPropUnit,  // App Properties
  TGenButtonUnit,   // Button
  TGenMemoUnit,     // Memo
  TGenLightUnit,    // Light

  TWmMsgFactoryUnit,  // Message Factory
  TMediaUtilsUnit;    // Media Alive Message

//------------------------------------------------------------------------------
// Log Frame
//------------------------------------------------------------------------------
type
  TLogFrame = class(TFrame)
    WinVisible: TGenButton;
    WinLabel: TLabel;
    LogMemo: TGenMemo;
    procedure FrameResize(Sender: TObject);
    procedure WinVisibleClick(Sender: TObject);

  private
    objState   : integer;           // Frame State
    objHeight  : integer;           // Height when Visible
    objVisible : TGenAppPropBool;   // True if Visible

  protected

    function  GetHeight    : integer;
    function  GetMinHeight : integer;

    // Log to LogFile

    procedure Log(const Line : string);

    // Frame Message Process

    procedure WndProc(var Message: TMessage); override;

  public
    constructor Create(AOwner: TComponent); override;

    procedure   LogOutPut(const sLine : string);

    procedure   StartUp;
    procedure   ShutDown;

    property    pHeight    : integer read GetHeight;
    property    pMinHeight : integer read GetMinHeight;

    // Reintroduced Properties

    property TabOrder;
    property TabStop;
  end;

implementation

{$R *.dfm}

uses
  Math,

  TPmaLogUnit,
  TGenGraphicsUnit,   // Graphic Functions
  TPmaFormUtils,      // Form Utils
  TMainFormUnit,      // Parent Form
  TPmaClassesUnit;    // Classes

const
  BRD       = 4;    // Border Size
  BTNSIZE   = 12;   // Button Size
  DEFHEIGHT = 140;  // Default Window Height

  prefWindow  = 'LogWindow';
  prefVisible = 'On';

  StateOpening = 0;  // Frame is Opening
  StateRunning = 1;  // Frame is running
  StateClosing = 2;  // Frame is Closing

resourcestring
  resHintFrame     = 'Log Window';
  resHintFrameDis  = 'Log Window Hidden';
  resHintVisible   = 'Click to Hide Log Window';
  resHintInVisible = 'Click to Show Log Window';

//------------------------------------------------------------------------------
//  Frame Message Pump
//------------------------------------------------------------------------------
procedure TLogFrame.WndProc(var Message: TMessage);
begin
  case Message.Msg of
    MSG_STARTUP :
      begin
        if BOOLEAN(Message.LParam) then
          self.StartUp
        else
          self.ShutDown;
      end;
  else
    inherited;
  end;
end;
//------------------------------------------------------------------------------
//  Create
//------------------------------------------------------------------------------
constructor TLogFrame.Create(AOwner: TComponent);
begin
  inherited;
  objState := StateOpening;

  // Set default Height

  objHeight := DEFHEIGHT;

  // Make sure some settings is ok

  self.AutoSize              := false;
  self.AutoScroll            := false;
  self.VertScrollBar.Visible := false;
  self.HorzScrollBar.Visible := false;

  objVisible := nil;
  
  LogMemo.WordWrap := false;
end;
//------------------------------------------------------------------------------
//  StartUp
//------------------------------------------------------------------------------
procedure TLogFrame.StartUp;
begin
  Log('  ' + self.ClassName + '.StartUp (' + self.Name + ')');

  objState := StateRunning;

  // Read Window Properties

  objVisible := App.CreatePropBool(prefWindow, prefVisible, true);

  WinVisible.Checked := objVisible.pBool;

  LogMemo.StartUp;

  // Set Hint

  if objVisible.pBool then
    begin
      self.Hint       := resHintFrame;
      WinVisible.Hint := resHintVisible;
    end
  else
    begin
      self.Hint       := resHintFrameDis;
      WinVisible.Hint := resHintInVisible;
    end;
end;
//------------------------------------------------------------------------------
//  ShutDown
//------------------------------------------------------------------------------
procedure TLogFrame.ShutDown;
begin
  Log('  ' + self.ClassName + '.ShutDown (' + self.Name + ')');

  objState := StateClosing;

  // ShutDown all Components

  StartupAllComps(self, false);
end;
//------------------------------------------------------------------------------
//  Log
//------------------------------------------------------------------------------
procedure TLogFrame.Log(const Line : string);
begin
  if Assigned(PmaLog) then
    PmaLog.Log(Line);
end;
//------------------------------------------------------------------------------
//  Callback for Appending a Log Line to LogWindow
//------------------------------------------------------------------------------
procedure TLogFrame.LogOutPut(const sLine : string);
begin
  if (objState = StateRunning) then
    begin
      self.LogMemo.Append(sLine);
    end;
end;
//------------------------------------------------------------------------------
//  Resize
//------------------------------------------------------------------------------
procedure TLogFrame.FrameResize(Sender: TObject);
var
  LabelHgt : integer;
  Pos : integer;
begin

  // Get the Height of Current Label in pixels (must be 2 + CheckBox Height)

  LabelHgt := Max(WinLabel.Height, BTNSIZE + 2);

  // Set Label Text

  SetCtrlPos(WinLabel,
      BTNSIZE + BRD*2, (LabelHgt - WinLabel.Height) div 2);

  // Set Checkbox position

  Pos := (LabelHgt - BTNSIZE + 1)  div 2;
  SetCtrlSize(WinVisible, Pos, Pos, BTNSIZE, BTNSIZE);

  if Assigned(objVisible) and objVisible.pBool then
    begin
      if (objHeight <> self.Height) then self.Height := objHeight;

      LogMemo.Visible := true;

      SetCtrlSize(LogMemo, 2,
        + LabelHgt,
        self.ClientWidth - 2,
        self.ClientHeight - LabelHgt - 2);
    end
  else
    begin
      if (objHeight <> self.pMinHeight) then self.Height := self.pMinHeight;

      LogMemo.Visible := false;
    end;
end;
//------------------------------------------------------------------------------
//  Get real Height
//------------------------------------------------------------------------------
function  TLogFrame.GetHeight: integer;
begin
  if Assigned(objVisible) and objVisible.pBool then
    result := objHeight
  else
    result := self.pMinHeight;
end;
//------------------------------------------------------------------------------
//  Return Minimum Height
//------------------------------------------------------------------------------
function TLogFrame.GetMinHeight: integer;
begin
  result := BRD + WinLabel.Height + 2;
end;
//------------------------------------------------------------------------------
//  User Clicked on Visible Button
//------------------------------------------------------------------------------
procedure TLogFrame.WinVisibleClick(Sender: TObject);
begin
  if (objState = StateRunning) then
    begin
      objVisible.Toggle;

      WinVisible.Checked := objVisible.pBool;

      PostMessage(Application.Handle, MSG_APP_RESIZE_LEFTPANEL, 0,0);

      if objVisible.pBool then
        begin
          self.Hint       := resHintFrame;
          WinVisible.Hint := resHintVisible;
        end
      else
        begin
          self.Hint       := resHintFrameDis;
          WinVisible.Hint := resHintInVisible;
        end
    end;
end;
//------------------------------------------------------------------------------
//                                 INITALIZE
//------------------------------------------------------------------------------
initialization
  TPmaClassFactory.RegClass(TLogFrame);
end.
