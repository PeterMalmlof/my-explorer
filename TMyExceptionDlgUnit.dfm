object MyExceptionDlg: TMyExceptionDlg
  Left = 865
  Top = 828
  BorderIcons = []
  BorderStyle = bsDialog
  Caption = 'MyExceptionDlg'
  ClientHeight = 145
  ClientWidth = 188
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -15
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  OnPaint = FormPaint
  PixelsPerInch = 96
  TextHeight = 16
  object Text1: TLabel
    Left = 64
    Top = 8
    Width = 33
    Height = 16
    Caption = 'Text1'
    WordWrap = True
  end
  object Text2: TLabel
    Left = 16
    Top = 40
    Width = 33
    Height = 16
    Caption = 'Text2'
    WordWrap = True
  end
  object Text3: TLabel
    Left = 16
    Top = 72
    Width = 33
    Height = 16
    Caption = 'Text3'
  end
  object BtnClose: TButton
    Left = 56
    Top = 112
    Width = 75
    Height = 25
    Caption = 'OK'
    TabOrder = 0
    OnClick = BtnCloseClick
  end
end
