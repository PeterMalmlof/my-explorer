object StateFrame: TStateFrame
  Left = 0
  Top = 0
  Width = 302
  Height = 27
  TabOrder = 0
  OnResize = FrameResize
  object State: TLabel
    Left = 32
    Top = 8
    Width = 25
    Height = 13
    Caption = 'State'
  end
  object BkgLight: TGenLight
    Left = 8
    Top = 8
    Width = 12
    Height = 12
    Border = False
    ColorFore = clRed
    ColorHigh = 8454143
    CmdId = -1
    Interval = 200
  end
  object MediaLibInfo: TLabel
    Left = 72
    Top = 8
    Width = 61
    Height = 13
    Caption = 'MediaLibInfo'
  end
  object TrackInfo: TLabel
    Left = 160
    Top = 6
    Width = 46
    Height = 13
    Caption = 'TrackInfo'
  end
  object Progress: TGenProgressBar
    Left = 232
    Top = 7
    Width = 65
    Height = 12
    CmdId = -1
    Vertical = False
    ValueMax = 100
    ValueCur = 33
    ValueMin = 0
    ColorFore = 4259584
    ColorHigh = 8454143
    TabOrder = 0
    OnDblClick = ProgressDblClick
  end
end
