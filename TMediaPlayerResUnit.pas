unit TMediaPlayerResUnit;

interface

resourcestring
  //----------------------------------------------------------------------------
  // Generic Strings
  //----------------------------------------------------------------------------

  resName    = 'Name';
  resDelete  = 'Delete';
  resAdd     = 'Add';
  resNew     = 'New';
  resDesc    = 'Description';
  resFolder  = 'Folder';       
  resUndef   = 'Undefined';
  resDefault = 'Default';
  resYes     = 'Yes';
  resNo      = 'No';


  //----------------------------------------------------------------------------
  // TDocItem Strings
  //----------------------------------------------------------------------------

  resModified = 'Modified';
  resCreated  = 'Created';
  resType     = 'Type';
  resSize     = 'Size';

  //----------------------------------------------------------------------------
  // TDocFolder Strings
  //----------------------------------------------------------------------------

  resDocFolderType = 'Folder';

  //----------------------------------------------------------------------------
  // TDocVideo Strings
  //----------------------------------------------------------------------------

  resDocVideoType = 'Video';

  //----------------------------------------------------------------------------
  // TDocTrack Strings
  //----------------------------------------------------------------------------

  resDocTrackType    = 'Audio';
  resDocTrackArtist  = 'Artist';
  resDocTrackYear    = 'Year';
  resDocTrackAlbum   = 'Album';
  resDocTrackTrack   = 'Track';
  resDocTrackNo      = 'No';
  resDocTrackGenre   = 'Genre';
  resDocTrackRating  = 'Rating';
  resDocTrackBitrate = 'Bitrate';
  resDocTrackPos     = 'Pos';
  resDocTrackLength  = 'Length';
  resDocTrackKbS     = 'kb/s';
  resDocTrackPaused  = '(Paused)';

  //----------------------------------------------------------------------------
  // TDocPlayList Strings
  //----------------------------------------------------------------------------

  resPlayListType   = 'Playlist';
  resPlayListTracks = 'Tracks';
  resPlayListSize   = 'Holds';

  //----------------------------------------------------------------------------
  // TDocFile Strings
  //----------------------------------------------------------------------------

  resDocFileType = 'File';

  //----------------------------------------------------------------------------
  // TDocTextFile Strings
  //----------------------------------------------------------------------------

  resDocTextFileType = 'Text File';

  //----------------------------------------------------------------------------
  // TItemListWin Strings
  //----------------------------------------------------------------------------

  resNoContent = 'No Content in Folder';

  //----------------------------------------------------------------------------
  // TEqWin Strings
  //----------------------------------------------------------------------------

  resEqVol = 'Vol';
  resEqOn  = 'Use Equalizer';
  resEqSet = 'Change Equalizer Setting';

  //----------------------------------------------------------------------------
  // TCurTrackWin Strings
  //----------------------------------------------------------------------------

  resPaused = '(paused)';

implementation

uses
  TPmaClassesUnit;

//------------------------------------------------------------------------------
//                              ININTIALIZATION
//------------------------------------------------------------------------------
initialization
  TPmaClassFactory.RegModule('TMediaPlayerResUnit');
end.
