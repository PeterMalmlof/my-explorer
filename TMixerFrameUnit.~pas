unit TMixerFrameUnit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, 
  Dialogs, StdCtrls, ExtCtrls,

  TGenAppPropUnit,    // Application Properties       (Component)
  TWmMsgFactoryUnit,  // Message Factory              (Component)
  TGenSliderUnit,     // Volume and Equalizer Sliders (Component)
  TMasterMixerUnit,   // Master Mixer                 (Component)
  TGenButtonUnit;     // Buttons                      (Component)

//------------------------------------------------------------------------------
// Mixer Frame
//------------------------------------------------------------------------------
type
  TMixerFrame = class(TFrame)
    MasterVolume : TGenSlider;
    PlayerVolume : TGenSlider;
    Eq1: TGenSlider;
    Eq2: TGenSlider;
    Eq3: TGenSlider;
    Eq4: TGenSlider;
    Eq5: TGenSlider;
    Eq6: TGenSlider;
    Eq7: TGenSlider;
    WinLabel: TLabel;
    WinVisible: TGenButton;

    procedure FrameResize          (Sender: TObject);
    procedure WinVisibleClick      (Sender: TObject);
    procedure MasterVolumeDblClick (Sender: TObject);
    procedure EqEnableClick        (Sender: TObject);

  private
    objState   : integer;          // Frame State
    objHeight  : integer;          // Frame Height when Visible
    objVisible : TGenAppPropBool;  // Frame is Visible

  protected

    procedure Subscribe(const Value : boolean);

    function  GetHeight: integer;
    procedure SetHeight(const Value : integer);

    function  GetMinHeight: integer;
    function  GetMinWidth : integer;

    // Log to LogFile

    procedure Log(const Line : string);

    // Frame Message Process

    procedure WndProc(var Message: TMessage); override;
  public
    constructor Create(AOwner: TComponent); override;

    procedure StartUp;
    procedure ShutDown;

    property pHeight    : integer read GetHeight     write SetHeight;
    property pMinHeight : integer read GetMinHeight;
    property pMinWidth  : integer read GetMinWidth;

    // Reintroduced Properties

    property TabOrder;
    property TabStop;
  end;

implementation

{$R *.dfm}
uses
  Math,

  TPmaLogUnit,       // Logging
  TBassPlayerUnit,   // Bass Player
  TGenGraphicsUnit,  // GRaphics
  TPmaFormUtils,     // Form Utils
  TMainFormUnit,     // Parent Form
  TPmaClassesUnit;   // Classes

const
  BRD       =   4;
  BTNSIZE   =  12;
  VOLSIZE   =  24; // Height and Width of all Buttons
  DEFHEIGHT = 110; // Default Window Height

  prefWindow  = 'MixerFrame';
  prefVisible = 'On';

  StateOpening = 0; // Frame is opening up
  StateRunning = 1; // Frame is Running
  StateClosing = 2; // Frame is Closing

resourcestring
  resHintFrame        = 'Mixer Control';
  resHintFrameDis     = 'Mixer Control Hidden';
  resHintMasterVolume = 'Master Volume (DblClick to Mute)';
  resHintPlayerVolume = 'Player Volume';
  resHintEq1          = 'Equalizer Band Level 80Hz';
  resHintEq2          = 'Equalizer Band Level 200Hz';
  resHintEq3          = 'Equalizer Band Level 450Hz';
  resHintEq4          = 'Equalizer Band Level 1kHz';
  resHintEq5          = 'Equalizer Band Level 2kHz';
  resHintEq6          = 'Equalizer Band Level 4kHz';
  resHintEq7          = 'Equalizer Band Level 8kHz';
  resHintVisible      = 'Click to Hide Master Mixer';
  resHintInVisible    = 'Click to Show Master Mixer';

//------------------------------------------------------------------------------
//  Frame Message Pump
//------------------------------------------------------------------------------
procedure TMixerFrame.WndProc(var Message: TMessage);
begin
  case Message.Msg of
    MSG_STARTUP :
      begin
        if BOOLEAN(Message.LParam) then
          self.StartUp
        else
          self.ShutDown;
      end;
  else
    inherited;
  end;
end;
//------------------------------------------------------------------------------
//  Create
//------------------------------------------------------------------------------
constructor TMixerFrame.Create(AOwner: TComponent);
begin
  inherited;
  objState  := StateOpening;
  objHeight := DEFHEIGHT;

  // Make sure some settings is ok

  self.AutoSize              := false;
  self.AutoScroll            := false;
  self.VertScrollBar.Visible := false;
  self.HorzScrollBar.Visible := false;

  // Read Window Properties

  objVisible := nil;

end;
//------------------------------------------------------------------------------
//  Startup
//------------------------------------------------------------------------------
procedure TMixerFrame.Startup;
begin
  Log('StartUp ' + self.ClassName + '.' + self.Name);

  // Read Window Properties

  objVisible := App.CreatePropBool(prefWindow, prefVisible, true);
  WinVisible.Checked := objVisible.pBool;

  Subscribe(objVisible.pBool);

  // Set Hint Text on all Controls

  self.Hint := resHintFrame;

  MasterVolume.Hint := resHintMasterVolume;
  PlayerVolume.Hint := resHintPlayerVolume;

  Eq1.Hint := resHintEq1;
  Eq2.Hint := resHintEq2;
  Eq3.Hint := resHintEq3;
  Eq4.Hint := resHintEq4;
  Eq5.Hint := resHintEq5;
  Eq6.Hint := resHintEq6;
  Eq7.Hint := resHintEq7;

  objState := StateRunning;
end;
//------------------------------------------------------------------------------
//  ShutDown
//------------------------------------------------------------------------------
procedure TMixerFrame.ShutDown;
begin
  Log('ShutDown ' + self.ClassName + '.' + self.Name);

  objState := StateClosing;

  // ShutDown all Components

  StartupAllComps(self, false);
end;
//------------------------------------------------------------------------------
//  Log
//------------------------------------------------------------------------------
procedure TMixerFrame.Log(const Line : string);
begin
  if Assigned(PmaLog) then
    PmaLog.Log(Line);
end;
//------------------------------------------------------------------------------
//  Subscribe or not
//------------------------------------------------------------------------------
procedure TMixerFrame.Subscribe(const Value : boolean);
begin
  if Value then
    begin
      // Setup the Message Pump Proc

      MasterVolume.CmdId := MSG_MIX_MASTER_VOLUME;
      PlayerVolume.CmdId := MSG_BASS_VOLUME;

      Eq1.CmdId := MSG_BASS_EQ1_POS;
      Eq2.CmdId := MSG_BASS_EQ2_POS;
      Eq3.CmdId := MSG_BASS_EQ3_POS;
      Eq4.CmdId := MSG_BASS_EQ4_POS;
      Eq5.CmdId := MSG_BASS_EQ5_POS;
      Eq6.CmdId := MSG_BASS_EQ6_POS;
      Eq7.CmdId := MSG_BASS_EQ7_POS;

      // We Need new Values for all these Controls

      PostMsg(MSG_MIX_MASTER_VOLUME, WP_CMD_UPDATE,0);
      PostMsg(MSG_BASS_VOLUME, WP_CMD_UPDATE,0);

      PostMsg(MSG_BASS_EQ1_POS, WP_CMD_UPDATE,0);
      PostMsg(MSG_BASS_EQ2_POS, WP_CMD_UPDATE,0);
      PostMsg(MSG_BASS_EQ3_POS, WP_CMD_UPDATE,0);
      PostMsg(MSG_BASS_EQ4_POS, WP_CMD_UPDATE,0);
      PostMsg(MSG_BASS_EQ5_POS, WP_CMD_UPDATE,0);
      PostMsg(MSG_BASS_EQ6_POS, WP_CMD_UPDATE,0);
      PostMsg(MSG_BASS_EQ7_POS, WP_CMD_UPDATE,0);

      self.Hint       := resHintFrame;
      WinVisible.Hint := resHintVisible;
    end
  else
    begin
      // ShutDown All Slider Controls

      MasterVolume.ShutDown;
      PlayerVolume.ShutDown;

      Eq1.ShutDown;
      Eq2.ShutDown;
      Eq3.ShutDown;
      Eq4.ShutDown;
      Eq5.ShutDown;
      Eq6.ShutDown;
      Eq7.ShutDown;

      self.Hint       := resHintFrameDis;
      WinVisible.Hint := resHintInVisible;
    end;
end;
//------------------------------------------------------------------------------
//  User or App Notifies about
//------------------------------------------------------------------------------
procedure TMixerFrame.WinVisibleClick(Sender: TObject);
begin
  if (objState = StateRunning) then
    begin
      objVisible.Toggle;

      WinVisible.Checked := objVisible.pBool;

      // Turn On/Off Subscribtions

      Subscribe(objVisible.pBool);

      // Tell the Main Form to Resize Left Panel

      PostMessage(Application.Handle, MSG_APP_RESIZE_LEFTPANEL, 0,0);
    end;
end;
//------------------------------------------------------------------------------
//  User Toggles Master Mute On/Off
//------------------------------------------------------------------------------
procedure TMixerFrame.MasterVolumeDblClick(Sender: TObject);
begin
  PostMessage(Application.Handle,
    MSG_MIX_MASTER_MUTE, WP_CMD_TOGGLE, 0);
end;
//------------------------------------------------------------------------------
//  Resize
//------------------------------------------------------------------------------
procedure TMixerFrame.FrameResize(Sender: TObject);
var
  LabelHgt : integer;
  Pos : integer;
  M, L : integer;
begin
  // Get the Height of Current Label in pixels (must be 2 + CheckBox Height)

  LabelHgt := Max(WinLabel.Height, BTNSIZE + 2);

  // Set Label Text

  WinLabel.Left := BTNSIZE + BRD*2;
  WinLabel.Top  := (LabelHgt - WinLabel.Height) div 2;

  // Set Checkbox position

  Pos := (LabelHgt - BTNSIZE + 1)  div 2;
  SetCtrlSize(WinVisible, Pos, Pos, BTNSIZE, BTNSIZE);

  if Assigned(objVisible) and objVisible.pBool then
    begin
      if (objHeight <> self.Height) then self.Height := objHeight;

      MasterVolume.Visible := true;
      MasterVolume.Left    := BRD;
      MasterVolume.Width   := VOLSIZE;
      MasterVolume.Top     := LabelHgt;
      MasterVolume.Height  := self.ClientHeight - MasterVolume.Top + BRD;

      PlayerVolume.Visible := true;
      PlayerVolume.Left    := BRD * 2 + VOLSIZE;
      PlayerVolume.Width   := VOLSIZE;
      PlayerVolume.Top     := MasterVolume.Top;
      PlayerVolume.Height  := MasterVolume.Height;

      M :=  PlayerVolume.Left + VOLSIZE +
            (self.ClientWidth - BRD * 2 - VOLSIZE * 2) div 2;
      L := M - round(VOLSIZE * 3.5);

      Eq1.Visible := true;
      Eq1.Left    := L;
      Eq1.Width   := VOLSIZE;
      Eq1.Top     := MasterVolume.Top;
      Eq1.Height  := MasterVolume.Height;
      Inc(L, VOLSIZE);

      Eq2.Visible := true;
      Eq2.Left    := L;
      Eq2.Width   := VOLSIZE;
      Eq2.Top     := MasterVolume.Top;
      Eq2.Height  := MasterVolume.Height;
      Inc(L, VOLSIZE);

      Eq3.Visible := true;
      Eq3.Left    := L;
      Eq3.Width   := VOLSIZE;
      Eq3.Top     := MasterVolume.Top;
      Eq3.Height  := MasterVolume.Height;
      Inc(L, VOLSIZE);

      Eq4.Visible := true;
      Eq4.Left    := L;
      Eq4.Width   := VOLSIZE;
      Eq4.Top     := MasterVolume.Top;
      Eq4.Height  := MasterVolume.Height;
      Inc(L, VOLSIZE);

      Eq5.Visible := true;
      Eq5.Left    := L;
      Eq5.Width   := VOLSIZE;
      Eq5.Top     := MasterVolume.Top;
      Eq5.Height  := MasterVolume.Height;
      Inc(L, VOLSIZE);

      Eq6.Visible := true;
      Eq6.Left    := L;
      Eq6.Width   := VOLSIZE;
      Eq6.Top     := MasterVolume.Top;
      Eq6.Height  := MasterVolume.Height;
      Inc(L, VOLSIZE);

      Eq7.Visible := true;
      Eq7.Left    := L;
      Eq7.Width   := VOLSIZE;
      Eq7.Top     := MasterVolume.Top;
      Eq7.Height  := MasterVolume.Height;
      //Inc(L, VOLSIZE);

    end
  else
    begin
      if (objHeight <> self.pMinHeight) then self.Height := self.pMinHeight;

      MasterVolume.Visible := false;
      PlayerVolume.Visible := false;

      Eq1.Visible := false;
      Eq2.Visible := false;
      Eq3.Visible := false;
      Eq4.Visible := false;
      Eq5.Visible := false;
      Eq6.Visible := false;
      Eq7.Visible := false;
    end;
end;
//------------------------------------------------------------------------------
//  Get Height
//------------------------------------------------------------------------------
function  TMixerFrame.GetHeight: integer;
begin
  if Assigned(objVisible) and objVisible.pBool then
    result := objHeight
  else
    result := self.pMinHeight;
end;
//------------------------------------------------------------------------------
//                                 INITALIZE
//------------------------------------------------------------------------------
procedure TMixerFrame.SetHeight(const Value : integer);
begin
  self.Height := Max(GetMinHeight, Value);
end;
//------------------------------------------------------------------------------
//                                 INITALIZE
//------------------------------------------------------------------------------
function TMixerFrame.GetMinHeight: integer;
begin
  result := BRD + WinLabel.Height + 2;
end;
//------------------------------------------------------------------------------
//                                 INITALIZE
//------------------------------------------------------------------------------
function TMixerFrame.GetMinWidth: integer;
begin
  result := VOLSIZE * 9 + BRD * 4;
end;
//------------------------------------------------------------------------------
//  User Toggles the Equalizer On/Off
//------------------------------------------------------------------------------
procedure TMixerFrame.EqEnableClick(Sender: TObject);
begin
  PostMsg(MSG_BASS_EQ_ENABLE, WP_CMD_TOGGLE, DWORD(0));
end;
//------------------------------------------------------------------------------
//                                     INIT
//------------------------------------------------------------------------------
initialization
  TPmaClassFactory.RegClass(TMixerFrame);
end.
