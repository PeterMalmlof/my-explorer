unit TEqWinUnit;

interface
uses
  Windows, ExtCtrls, Graphics, Types, Menus,

  TGenRectWinUnit,    // Base RectWin Class
  TBassPlayerUnit,    // Bass Player Object
  TGenAppPropUnit,    // Application Properties
  TGenTimeUnit;       // Time Functions

//------------------------------------------------------------------------------
//  Object
//------------------------------------------------------------------------------
type TEqWin = class(TGenRectWin)
  protected
    objEditMode : boolean;
    objIndex    : integer;
    objVolume   : single;

    objEqOn : TGenAppPropBool;
    objEq1  : TGenAppPropDouble;
    objEq2  : TGenAppPropDouble;
    objEq3  : TGenAppPropDouble;
    objEq4  : TGenAppPropDouble;
    objEq5  : TGenAppPropDouble;
    objEq6  : TGenAppPropDouble;
    objEq7  : TGenAppPropDouble;

    objSrcRect    : TRect;
    objDestRect   : TRect;
    objTextHeight : integer;
    objSmallWidth : integer;
    objContrWidth : single;

    function  GetUid: string; override;

    procedure SetRect     (const Value : TRect);   override;
    procedure SetState    (const Value : integer); override;
    procedure SetIndex    (const Value : integer);
    procedure SetEditMode (const Value : boolean);

    function GetVisible: boolean; override;

    class function GetDefaultRect: TRect; override;

    class function IsSizeable: boolean; override;
  public
    constructor Create (const C : TCanvas); reintroduce;
    destructor  Destroy; override;

    procedure SetGain(const bPos : boolean);

    procedure Save;      override;
    procedure RefreshUi; override;
    procedure Paint;     override;

    procedure AddMenues  (const MainMenu : TPopupMenu);
    procedure OnEqToggle (Sender : TObject);
    procedure OnEqMode   (Sender : TObject);

    function GetBackBitmap: TBitmap; override;

    property pEditMode : boolean read objEditMode write SetEditMode;
    property pIndex    : integer read objIndex    write SetIndex;

end;

implementation

uses
  SysUtils,
  Math,
  Forms,

  TGenMenuUnit,       // TGenMenu Class
  TGenStrUnit,        // String Functions
  //TGenAudioUnit,      // Audio Function
  TGenLogUnit,        // Log Object

  TMediaPlayerResUnit,// Resource Strings
  
  TGenClassesUnit;    // Class Management

const
  propOn    = 'On';
  propBand1 = 'Band1';
  propBand2 = 'Band2';
  propBand3 = 'Band3';
  propBand4 = 'Band4';
  propBand5 = 'Band5';
  propBand6 = 'Band6';
  propBand7 = 'Band7';

//------------------------------------------------------------------------------
//  Constructor
//------------------------------------------------------------------------------
constructor TEqWin.Create(const C : TCanvas);
begin

  inherited Create(C);

  objIndex    := PlayerFx4Id;
  objEditMode := false;

  objDrawBitmap.Canvas.Font.Name   := C.Font.Name;
  objDrawBitmap.Canvas.Font.Height := 30;

  // Load All Equalizer Settings

  //ThePlayer.pBandWidth := PlayerFxWdt;

  objEqOn := TGenAppPropBool.Create(
        GetUid,propOn, true) as TGenAppPropBool;
  objEqOn.ForceRead;
  //ThePlayer.pEqualizer := objEqOn.pBool;

  objEq1 := TGenAppPropDouble.Create(
            GetUid, propBand1, 0.0) as TGenAppPropDouble;
  objEq1.ForceRead;
  //ThePlayer.SetEqualBandGain(PlayerFx1Id, objEq1.pDouble);

  objEq2 := TGenAppPropDouble.Create(
            GetUid, propBand2, 0.0) as TGenAppPropDouble;
  objEq2.ForceRead;
  //ThePlayer.SetEqualBandGain(PlayerFx2Id, objEq2.pDouble);

  objEq3 := TGenAppPropDouble.Create(
            GetUid, propBand3, 0.0) as TGenAppPropDouble;
  objEq3.ForceRead;
  //ThePlayer.SetEqualBandGain(PlayerFx3Id, objEq3.pDouble);

  objEq4 := TGenAppPropDouble.Create(
            GetUid, propBand4, 0.0) as TGenAppPropDouble;
  objEq4.ForceRead;
  //ThePlayer.SetEqualBandGain(PlayerFx4Id, objEq4.pDouble);

  objEq5 := TGenAppPropDouble.Create(
            GetUid, propBand5, 0.0) as TGenAppPropDouble;
  objEq5.ForceRead;
  //ThePlayer.SetEqualBandGain(PlayerFx5Id, objEq5.pDouble);

  objEq6 := TGenAppPropDouble.Create(
            GetUid, propBand6, 0.0) as TGenAppPropDouble;
  objEq6.ForceRead;
  //ThePlayer.SetEqualBandGain(PlayerFx6Id, objEq6.pDouble);

  objEq7 := TGenAppPropDouble.Create(
            GetUid, propBand7, 0.0) as TGenAppPropDouble;
  objEq7.ForceRead;
  //ThePlayer.SetEqualBandGain(PlayerFx7Id, objEq7.pDouble);

end;
//------------------------------------------------------------------------------
//  Destructor
//------------------------------------------------------------------------------
destructor TEqWin.Destroy;
begin

  objEqOn.Free;
  objEq1.Free;
  objEq2.Free;
  objEq3.Free;
  objEq4.Free;
  objEq5.Free;
  objEq6.Free;
  objEq7.Free;

  inherited;
end;
//------------------------------------------------------------------------------
// Get Default Window Position, must be overridden by all Windows
//------------------------------------------------------------------------------
function TEqWin.GetUid:string;
begin
  result := 'EqWin';
end;
//------------------------------------------------------------------------------
// Get Default Window Position, must be overridden by all Windows
//------------------------------------------------------------------------------
class function TEqWin.GetDefaultRect: TRect;
begin
  result := Rect(
    (Screen.Width div 2) + 10,
    (Screen.Height div 2) + 10,
    Screen.Width - (Screen.Width div 8),
    2 * (Screen.Height div 3) - 10);
end;
//------------------------------------------------------------------------------
// Return true if Window can be resized
//------------------------------------------------------------------------------
class function TEqWin.IsSizeable: boolean;
begin
  result := true;
end;
//------------------------------------------------------------------------------
//  Set State (Override for special management)
//------------------------------------------------------------------------------
procedure TEqWin.SetState(const Value : integer);
begin
      objEnabled := false;
      objVisible := false;
  (*
  if (Value = StatePlayAudio) then
    begin
      objEnabled := true;
      objVisible := true;
    end
  else
    begin
      objEnabled := false;
      objVisible := false;
    end; *)
end;
//------------------------------------------------------------------------------
//  Menu Handler
//------------------------------------------------------------------------------
procedure TEqWin.SetEditMode (const Value : boolean);
begin
  objEditMode := Value;
  objVisible  := objEditMode;
  objEnabled  := objEditMode;
end;
//------------------------------------------------------------------------------
// Return true if Window is Visible
//------------------------------------------------------------------------------
function TEqWin.GetVisible: boolean;
begin
  result := objEditMode;
end;
//------------------------------------------------------------------------------
//  Save All Settings
//------------------------------------------------------------------------------
procedure TEqWin.Save;
begin
  inherited;
  
  // Remember current Equalizer Settings

  //objEqOn.pBool  := ThePlayer.pEqualizer;
  objEqOn.ForceWrite;

  //objEq1.pDouble := ThePlayer.GetEqualBandGain(PlayerFx1Id);
  objEq1.ForceWrite;

  //objEq2.pDouble := ThePlayer.GetEqualBandGain(PlayerFx2Id);
  objEq2.ForceWrite;

  //objEq3.pDouble := ThePlayer.GetEqualBandGain(PlayerFx3Id);
  objEq3.ForceWrite;

  //objEq4.pDouble := ThePlayer.GetEqualBandGain(PlayerFx4Id);
  objEq4.ForceWrite;

  //objEq5.pDouble := ThePlayer.GetEqualBandGain(PlayerFx5Id);
  objEq5.ForceWrite;

  //objEq6.pDouble := ThePlayer.GetEqualBandGain(PlayerFx6Id);
  objEq6.ForceWrite;

  //objEq7.pDouble := ThePlayer.GetEqualBandGain(PlayerFx7Id);
  objEq7.ForceWrite;

end; 
//------------------------------------------------------------------------------
//  Set Enabled On/Off
//------------------------------------------------------------------------------
procedure TEqWin.SetIndex (const Value : integer);
begin
  objIndex := Value;

  // Turn it around

  if objIndex < PlayerFx1Id then objIndex := PlayerFx7Id;
  if objIndex > PlayerFx7Id then objIndex := PlayerFx1Id;
end;
//------------------------------------------------------------------------------
//  Set Equalizer Band
//------------------------------------------------------------------------------
procedure TEqWin.SetGain(const bPos : boolean);
begin
(*  if (objIndex >= PlayerFx1Id) and (objIndex <= PlayerFx7Id) then
    begin
      if bPos then
        begin
          if (ThePlayer.GetEqualBandGain(objIndex) < PLAYEREQMAX) then
            ThePlayer.SetEqualBandGain(objIndex,
                  ThePlayer.GetEqualBandGain(objIndex) + PLAYEREQMAX/20);

          TheLog.Log('Inc Gain ' + ToStr(ThePlayer.GetEqualBandGain(objIndex)));
          objDirty := true;
        end
      else
        begin
          if (ThePlayer.GetEqualBandGain(objIndex) > -PLAYEREQMAX) then
            ThePlayer.SetEqualBandGain(objIndex,
                  ThePlayer.GetEqualBandGain(objIndex) - PLAYEREQMAX/20);
          TheLog.Log('Dec Gain ' + ToStr(ThePlayer.GetEqualBandGain(objIndex)));
          objDirty := true;
        end;
    end; *)
end;
//------------------------------------------------------------------------------
//  Set Rect
//------------------------------------------------------------------------------
procedure TEqWin.SetRect(const Value : TRect);
begin
  inherited;

  RefreshUi;
end;
//------------------------------------------------------------------------------
//  Set Rect
//------------------------------------------------------------------------------
procedure TEqWin.RefreshUi;
begin
  inherited;

  // Calculate Temp data for drawing

  // The Drawing Bitmap must be the same size as Window

  objDrawBitmap.Width  := objRect.Right - objRect.Left;
  objDrawBitmap.Height := objRect.Bottom - objRect.Top;

  // Destination Rect

  objDestRect := Rect(objRect.Left  + 1, objRect.Top    + 1,
                      objRect.Right - 1, objRect.Bottom - 1);

  objSrcRect := Rect(1,1, objDrawBitmap.Width - 1, objDrawBitmap.Height - 1);

  // Text Height

  objTextHeight := objDrawBitmap.Canvas.TextHeight('X') + 2;

  objSmallWidth := round((objSrcRect.Right - objSrcRect.Left) / 24);

  // Width of each Controler

  objContrWidth :=
    (objSrcRect.Right - objSrcRect.Left - (objSmallWidth * 2)) / 8;
end;
//------------------------------------------------------------------------------
//  Paint
//------------------------------------------------------------------------------
procedure TEqWin.Paint;
var
  I : integer;
  CtrlRect : TRect;
  LongRect : TRect;
  CtrlMidX : integer;
  Value    : single;
  ValuePos : integer;

  sTmp     : string;
  TxtSize  : TSize;
begin
  inherited;

  if objVisible then
    begin
      // Draw Equalizer Main Frame with Background Color

      objDrawBitmap.Canvas.Brush.Color := objBackColor;
      objDrawBitmap.Canvas.Pen.Color   := objForeColor;
      objDrawBitmap.Canvas.Font.Color  := objForeColor;
      objDrawBitmap.Canvas.Pen.Style := psClear;

      objDrawBitmap.Canvas.Rectangle(
        Rect(0, 0, objDrawBitmap.Width, objDrawBitmap.Height)); 

      objDrawBitmap.Canvas.Pen.Style := psSolid;

      // Paint all Controls (8)

      for I := 0 to 7 do
        begin
          //--------------------------------------------------------------------
          // Get the Rect for this Vu Control (Not the Text)
          //--------------------------------------------------------------------

          CtrlRect.Left   := objSrcRect.Left + objSmallWidth +
                              round(I * objContrWidth);
          CtrlRect.Right  := round(CtrlRect.Left + objContrWidth);
          CtrlRect.Top    := objSrcRect.Top + 2;
          CtrlRect.Bottom := objSrcRect.Bottom - objTextHeight;

          // Get the Middle X of COntrol

          CtrlMidX := CtrlRect.Left + (CtrlRect.Right - CtrlRect.Left) div 2;

          //--------------------------------------------------------------------
          // Calculate, and draw Long Rectangle
          //--------------------------------------------------------------------

          LongRect.Left   := CtrlMidX        -  6;
          LongRect.Right  := CtrlMidX        +  6;
          LongRect.Top    := CtrlRect.Top    +  5;
          LongRect.Bottom := CtrlRect.Bottom - 10;
          (*
          if (I > 0) and ThePlayer.pEqualizer then
            begin
              if objEditMode and (I = objIndex) then
                objDrawBitmap.Canvas.Brush.Color := objForeColor
              else
                objDrawBitmap.Canvas.Brush.Color := objHighColor;
            end
          else  *)
            objDrawBitmap.Canvas.Brush.Color := objHighColor;

          objDrawBitmap.Canvas.RoundRect(
            LongRect.Left, LongRect.Top, LongRect.Right, LongRect.Bottom, 9, 9);

          objDrawBitmap.Canvas.MoveTo(CtrlMidX - 6,
            LongRect.Top + (LongRect.Bottom - LongRect.Top) div 2);
          objDrawBitmap.Canvas.LineTo(CtrlMidX + 6,
            LongRect.Top + (LongRect.Bottom - LongRect.Top) div 2);

          //--------------------------------------------------------------------
          // Draw Slider Knob at Value
          //--------------------------------------------------------------------

          LongRect.Left   := CtrlMidX - 15; // Slider Knob Width
          LongRect.Right  := CtrlMidX + 15;

          // Get the Volume Value or Equalizer Value (0 .. +/-1.0)

          //if (I = 0) then
          //  Value := TGenAudioUnit.GetMasterVolume
          //else
          //  Value := 0.5 + (ThePlayer.GetEqualBandGain(I)/(PLAYEREQMAX*2));

          ValuePos := LongRect.Bottom -
                        round((LongRect.Bottom - LongRect.Top) * Value);

          LongRect.Top    := ValuePos - 9; // Slider Knob Position
          LongRect.Bottom := ValuePos + 9;

          if (I = 0) then
            objDrawBitmap.Canvas.Brush.Color := RGB(0,170,170)
          //else if ThePlayer.pEqualizer then
          //  objDrawBitmap.Canvas.Brush.Color := RGB(0,170,170)
          else
            objDrawBitmap.Canvas.Brush.Color := objHighColor;

          objDrawBitmap.Canvas.RoundRect(
            LongRect.Left, LongRect.Top, LongRect.Right, LongRect.Bottom, 9, 9);

          //--------------------------------------------------------------------
          // Draw Text below
          //--------------------------------------------------------------------

          sTmp := '';
          if (I = 0) then
            sTmp := resEqVol;
          //else //if ThePlayer.pEqualizer then
          //  sTmp := FreqToStr(round(ThePlayer.GetEqualBandFreq(I)),'');

          if (length(sTmp) > 0) then
            begin
              objDrawBitmap.Canvas.Brush.Color := objBackColor;

              TxtSize := objDrawBitmap.Canvas.TextExtent(sTmp);

              objDrawBitmap.Canvas.TextOut(CtrlMidX - TxtSize.cx div 2,
                objSrcRect.Bottom - TxtSize.cy - 2, sTmp);
            end;
        end;

      // Copy Bitmap to Screen

      objCanvas.CopyRect(objDestRect, objDrawBitmap.Canvas, objSrcRect);
    end;
end;
//------------------------------------------------------------------------------
//  Return The Draw Bitmap
//------------------------------------------------------------------------------
function TEqWin.GetBackBitmap: TBitmap;
begin
  result := objDrawBitmap;
end;
//------------------------------------------------------------------------------
//  Add Menues
//------------------------------------------------------------------------------
procedure TEqWin.AddMenues (const MainMenu : TPopupMenu);
var
  pMenu : TGenMenu;
begin
  pMenu := TGenMenu.Create(MainMenu);
  pMenu.Caption := resEqOn;
  //pMenu.Checked := ThePlayer.pEqualizer;
  pMenu.OnClick := OnEqToggle;
  MainMenu.Items.Add(pMenu);

  pMenu := TGenMenu.Create(MainMenu);
  pMenu.Caption := resEqSet;
  pMenu.Checked := objEditMode;
  pMenu.OnClick := OnEqMode;
  MainMenu.Items.Add(pMenu);
end;
//------------------------------------------------------------------------------
//  Menu Handler
//------------------------------------------------------------------------------
procedure TEqWin.OnEqToggle (Sender : TObject);
begin
  if (Sender is TMenuItem) then
    begin
      //ThePlayer.pEqualizer := not ThePlayer.pEqualizer;
      objDirty := true;
    end;
end;
//------------------------------------------------------------------------------
//  Menu Handler
//------------------------------------------------------------------------------
procedure TEqWin.OnEqMode (Sender : TObject);
begin
  if (Sender is TMenuItem) then
    begin
      self.pEditMode := not self.pEditMode;

    end;
end;
//------------------------------------------------------------------------------
//                                 INITALIZE
//------------------------------------------------------------------------------
initialization

  RegClass(TEqWin);
end.

