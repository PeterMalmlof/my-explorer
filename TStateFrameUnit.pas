unit TStateFrameUnit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, 
  Dialogs, StdCtrls, Math,

  TWmMsgFactoryUnit,  // Message Factory              (Component)
  TMediaBaseUnit, ExtCtrls, TGenLightUnit, TGenProgressBarUnit;     // Media Base Class

type
  TStateFrame = class(TFrame)
    State: TLabel;
    BkgLight: TGenLight;
    Progress: TGenProgressBar;
    MediaLibInfo: TLabel;
    TrackInfo: TLabel;
    procedure FrameResize(Sender: TObject);
    procedure Log(const Line : string);
    procedure ProgressDblClick(Sender: TObject);
  private
    objState      : integer;           // Frame State
    objSubscriber : TWmMsgSubscriber;  // Subscriber
    objMediaState : integer;           // Media Library State

  protected

    // Message Process called by Message Pump

    procedure MsgProc(var Msg : TMsg);

    // Frame Message Process

    procedure WndProc(var Message: TMessage); override;

    procedure SetMediaState(const Value : integer);

    procedure SetTrackInfo;
  public
    constructor Create(AOwner: TComponent);override;

    procedure StartUp;
    procedure ShutDown;

    procedure SetHint(const Value : string);

    procedure SetMediaLibInfo;

    // Get Current Hight of Frame

    function GetHeight   : integer;

    // Get Minimum Hight (when minimized) of Frame

    class function GetMinWidth : integer;

    property pMediaState : integer read objMediaState write SetMediaState;
    
    // Reintroduced Properties

    property TabOrder;
    property TabStop;

  end;
const
  msLoading = 0;
  msRunning = 1;
  msClosing = 2;

implementation

{$R *.dfm}
uses
  TPmaLogUnit,
  TBkgThreadQueueUnit,  // Background Queue (Component)
  TGenGraphicsUnit,     // Graphic Functions
  TMediaUtilsUnit,      // Media Utilities
  TMediaArtistUnit,
  TMediaAlbumUNit,
  TMediaTrackUnit,
  TMediaMovieUnit,
  TMediaPlayLIstUNit,
  TMediaPlayerUNit,
  TBassPlayerUNit,

  TPmaClassesUnit;
const
  BRD = 1;  // Border between Buttons

  StateOpening = 0; // Frame is opening up
  StateRunning = 1; // Frame is Running
  StateClosing = 2; // Frame is Closing

//------------------------------------------------------------------------------
//  Frame Message Pump (Used only for StartUp and ShutDown
//------------------------------------------------------------------------------
procedure TStateFrame.WndProc(var Message: TMessage);
begin
  if (Message.Msg = MSG_STARTUP) then
    begin
      if BOOLEAN(Message.LParam) then
        self.StartUp
      else
        self.ShutDown;
    end
  else
    inherited;
end;
//------------------------------------------------------------------------------
//  Create
//------------------------------------------------------------------------------
constructor TStateFrame.Create(AOwner: TComponent);
begin
  inherited;

  objState := StateOpening;

  // Make Sure some settings are correct

  self.AutoScroll            := false;
  self.AutoSize              := false;
  self.VertScrollBar.Visible := false;
  self.HorzScrollBar.Visible := false;
  self.ParentColor           := true;
  self.ParentShowHint        := true;
  self.ParentFont            := true;

  Progress.Visible := false;
end;
//------------------------------------------------------------------------------
//  Set Media State
//------------------------------------------------------------------------------
procedure TStateFrame.SetMediaState(const Value : integer);
begin
  objMediaState := Value;
  case objMediaState of
    msLoading : State.Caption := 'Loading Media';
    msRunning : State.Caption := 'Media Loaded';
    msClosing : State.Caption := 'Closing Player';
  end;
end;
//------------------------------------------------------------------------------
//  Get Height of Player FRame
//------------------------------------------------------------------------------
procedure TStateFrame.StartUp;
begin
  Log('  ' + self.ClassName + '.StartUp (' + self.Name + ')');

  BkgLight.CmdId := MSG_MDB_PROCESS_ALIVE;

  // Setup the Message Pump Processing

  if Assigned(MsgFactory) then
    begin
      objSubscriber := MsgFactory.Subscribe(self.ClassName, MsgProc);

      // Message for Starting and Stopping Progress Bar

      objSubscriber.AddMessage(MSG_MDB_SHOW_PROGRESS);

      // Update Track INfo when new Track started Playing

      objSubscriber.AddMessage(MSG_BASS_TRACK_START);

      // Update Media Count when Media is Loaded or refreshed

      objSubscriber.AddMessage(MSG_MDB_MEDIA_LOADED);
      objSubscriber.AddMessage(MSG_MDB_BKG_REFRESHED);
    end;

  // Set Hints on Frame and all Controls

  self.Hint         := 'Player State';
  State.Hint        := 'Current Hint Text';
  TrackInfo.Caption := '';
  TrackInfo.Hint    := 'Current Track or Movie';

  BkgLight.Hint := 'Background Process Running';

  MediaLibInfo.Caption := 'Media Library Loading';

  // Designer Notes:
  //  ParentColor should be set to true on all Child Controls
  //  ShowParentHint should be set to true on all Child Controls
  //  Set TabOrder and Set TabStop = true on all Buttons
  //  Transparent should be set on all TGenButton
  //  Set OverType of otAll on all TGenButton
  //  No Extra Images on Buttons, except Play, Mute, Shuffle and Tracking
  //

  objState := StateRunning;
end;
//------------------------------------------------------------------------------
//  ShutDown
//------------------------------------------------------------------------------
procedure TStateFrame.ShutDown;
begin
  Log('  ' + self.ClassName + '.ShutDown (' + self.Name + ')');

  MediaLibInfo.Caption := 'Media Library Closing';

  objState := StateClosing;

  // Unsubscribe all Messages in Message Pump

  if Assigned(objSubscriber) then
    MsgFactory.DeSubscribe(objSubscriber);

  // ShutDown all Components

  StartupAllComps(self, false);
end;
//------------------------------------------------------------------------------
//  Log
//------------------------------------------------------------------------------
procedure TStateFrame.Log(const Line : string);
begin
  if Assigned(PmaLog) then
    PmaLog.Log(Line);
end;
//------------------------------------------------------------------------------
//  Message Pump Processing
//------------------------------------------------------------------------------
procedure TStateFrame.MsgProc(var Msg : TMsg);
begin
 if (objState = StateRunning) then
    begin
      // Manage Player State Changes

      case Msg.message of

        MSG_MDB_MEDIA_LOADED :
          begin
            objSubscriber.RemMessage(MSG_MDB_MEDIA_LOADED);

            self.SetMediaLibInfo;
          end;

        MSG_MDB_BKG_REFRESHED :
          begin
            self.SetMediaLibInfo;
          end;

        MSG_MDB_SHOW_PROGRESS  :
        begin
          if (Msg.wParam = WP_CMD_USED) then
            begin
              if BOOLEAN(Msg.lParam) then
                begin
                  // Set the Progress Bar Visible and Resize

                  Progress.Visible     := true;
                  Progress.ValueCur    := 0;
                  MediaLibInfo.Visible := false;

                  // Tell the Progress Bar to start Acting on this Message Id

                  Progress.CmdId := MSG_MDB_SHOW_PROGRESS;
                end
              else
                begin
                  // Tell Progress Bar to stop Acting, and make it invisible

                  Progress.CmdId       := -1;
                  Progress.Visible     := false;
                  MediaLibInfo.Visible := true;
                end;
          end;
        end;

        MSG_BASS_TRACK_START : SetTrackInfo;
          
      end;{case msg}
  end;{running}
end;
//------------------------------------------------------------------------------
//  Set Track Information
//------------------------------------------------------------------------------
procedure TStateFrame.SetTrackInfo;
begin
  if Assigned(TheMediaPlayer) then
    begin
      TrackInfo.Caption := TheMediaPlayer.pTrackInfo;
    end;
end;
//------------------------------------------------------------------------------
//  Set Hint
//------------------------------------------------------------------------------
procedure TStateFrame.SetHint(const Value : string);
begin
  if objMediaState = msRunning then
    State.Caption := Value;
end;
//------------------------------------------------------------------------------
//  Set Count
//------------------------------------------------------------------------------
procedure TStateFrame.SetMediaLibInfo;
begin 
  MediaLibInfo.Caption :=
      IntToStr(TMediaArtist.GetDebugCount)    + ' Artists, ' +
      IntToStr(TMediaAlbum.GetDebugCount)     + ' Albums, ' +
      IntToStr(TMediaTrack.GetDebugCount)     + ' Tracks, ' +
      IntToStr(TMediaMovie.GetDebugCount)     + ' Movies, ' +
      IntToStr(TMediaPlayLIst.GetDebugCount)  + ' Playlists';
end;
//------------------------------------------------------------------------------
//  Resize Frame
//------------------------------------------------------------------------------
procedure TStateFrame.FrameResize(Sender: TObject);
begin
  BkgLight.Left := 3;
  BkgLight.Top  := (self.ClientHeight - BkgLight.Height) div 2;

  State.Left := BkgLight.Left + BkgLight.Width + 2 + BRD;


  State.Top  := (self.ClientHeight - State.Height) div 2;

  Progress.Left   := self.ClientWidth div 4;
  Progress.Width  := self.ClientWidth - Progress.Left - 5;
  Progress.Top    := 2;
  Progress.Height := self.ClientHeight - 4;

  MediaLibInfo.Left := Progress.Left;
  MediaLibInfo.Top  := State.Top;

  TrackInfo.Left := 2 * self.ClientWidth div 3;
  TrackInfo.Top  := State.Top;

end;
//------------------------------------------------------------------------------
//  Get Height of Player Frame
//------------------------------------------------------------------------------
function TStateFrame.GetHeight: integer;
begin
  result := BRD * 2 + Max(State.Height, BkgLight.Height);
end;
//------------------------------------------------------------------------------
//  Get Height of Player Frame
//------------------------------------------------------------------------------
class function TStateFrame.GetMinWidth: integer;
begin
  result := 400;
end;
//------------------------------------------------------------------------------
//                                        END
//------------------------------------------------------------------------------
procedure TStateFrame.ProgressDblClick(Sender: TObject);
begin
  // Stop All On-Going Background Threads

  BkgQueue.TerminatAllProcesses;
end;
//------------------------------------------------------------------------------
//                              ININTIALIZATION
//------------------------------------------------------------------------------
initialization
  TPmaClassFactory.RegClass(TStateFrame);
end.
