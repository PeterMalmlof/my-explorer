unit TFullScreenUnit;
//------------------------------------------------------------------------------
//  Modules
//    TExplorerAppUnit    Application Properties
//    TMediaPlayerResUnit Application Resources
//
//  Item Class:
//    TTreeItemBaseUnit   Item Base Class
//
//    TProgListUnit       Program List Class
//    TProgGrpUnit        Program Group Class
//    TProgItemUnit       Program Item Class
//
//    TExtListUnit        Extension List CLass
//    TExtItemUnit        Extension Item
//
//    TVerbListUnit       Verb List Class
//    TVerbItemUnit       Verb Item Class
//
//    TDocListUnit        Document List Class
//    TDocGrpUnit         Document Group Class
//    TDocItemUnit        Document Item Class
//    TDocFolderUnit      Document Folder Class
//    TDocPlayListUnit    Document Play List Class
//    TDocVideoUnit       Document Video Class
//    TDocTrackUnit       Document Audio Class
//    TDocPlTrackUnit     Document Play List Track Class
//    TDocFileUnit        Document File Class
//
//  Item Lists:
//    TItemListBaseUnit   Item List Base Class
//    TItemListUnit       TheItems List
//
//  Volume Meters
//    TVuBaseUnit         Base Volume Meter Class
//    TVuBarUnit          Bar and Bands Vu Class
//    TVuAnalyzerUnit     Analyzer Vu Class
//    TVu3dUnit           3D Vu Class
//    TVuFlerpUnit        Flerp Vu Class
//    TVuAteroidsUnit     Asteroids Vu Class
//    TVuBassSolUnit      Bass Sol Component Class
//    TVuBassCalcUnit     Bass Calculator Component Class
//
//  Rect WIndows
//    TGenRectWinUnit     Base Rect Window Class
//    TItemListWinUnit    Item List RectWin Class
//    TClockWinUnit       Clock RectWin Class
//    TSaverWinUnit       Screen Saver RectWin Class
//    TVideoInfoWinUnit   Video Info RectWin Class
//    TWoferWinUnit       Wofer RectWin Class
//    TEqWinUnit          Equalizer RectWin Class
//    TTrackInfoWinUnit   Track Info RectWin Class
//    TCurTrackWinUnit    CurTrack RectWin Class
//    TVuWinUnit          Vu RectWin Class
//
//  Generic Modules
//    TGenLogUnit         Log Object
//    TGenStrUnit         String Functions
//    TGenAppPropUnit     Application Properties
//    TGenClassesUnit     Class Management
//    TGenTextFileUnit    TextFile Class
//    TGenGraphicsUnit    Graphic Functions
//    TGenShellUnit       Shell Functions
//    TGenOpenDialogUnit  Open Dialog
//    TGenPickFolderUnit  Pick a Folder
//    MyFileTimeUnit      TFileTime Functions
//    TGenTimerUnit       TGenTimer Class
//    TBassPlayerUnit     Bass Player Object
//    TGeomCurveUnit      Curve
//    TGenMenuUnit        Generic Menus
//    TGraphicsMathUnit   Graphic Matematics
//    TGraphicsPrimUnit   Graphic Primitives
//    TGenTimeUnit        Time Functions
//    TGenAudioUnit       Audio Functions
//    TGenRemoteUnit      TGenRemote Class
//------------------------------------------------------------------------------
interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Contnrs,  ExtCtrls, ImgList, Menus,

  TGenTimeUnit,       // Time Functions
  TGenRemoteUnit,     // TGenRemote Class
  TGenAppPropUnit,    // App Properties
  
  TBassPlayerUnit,    // Bass Player Object
  TTreeItemBaseUnit,  // Base Item Class
  TVuBaseUnit,        // Volume Meter Base Class

  TGenRectWinUnit,    // Base RectWin Class
  TItemListWinUnit,   // Item List Window
  TSaverWinUnit,      // ScreenSaves Window
  TClockWinUnit,      // Clock Window
  TVideoInfoWinUnit,  // File Information Window
  TEqWinUnit,         // Equalizer Window
  TWoferWinUnit,      // Wofer Window
  TTrackInfoWinUnit,  // Track Information WIndow
  TCurTrackWinUnit,   // Current Track Window
  TVuWinUnit;         // Volume Meter Window

type
  TFullScreen = class(TForm)
    TypeImages : TImageList;
    MyMenu     : TPopupMenu;
    Wofers     : TImageList;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormDblClick(Sender: TObject);
    procedure FormPaint(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MyMenuPopup(Sender: TObject);
    procedure FormMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure FormMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure FormMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
  private

    //--------------------------------------------------------------------------
    // Application State
    //--------------------------------------------------------------------------

    objState : integer;

    objClockOn : TGenAppPropBool;
    objInfoOn  : TGenAppPropBool;

    //--------------------------------------------------------------------------
    // User Interface Colors
    //--------------------------------------------------------------------------

    objForeColor : TColor;   // Foreground Color
    objHighColor : TColor;   // Selected Text Background Color
    objBackColor : TColor;   // Background Color

    objMouseTimer : TTimer;  // Used for diabling cursor

    //--------------------------------------------------------------------------
    // Remote Control
    //--------------------------------------------------------------------------

    objRemote : TGenRemote; // Remote Controler

    //--------------------------------------------------------------------------
    // Keyboard Control
    //--------------------------------------------------------------------------

    objLastKeyTick : Cardinal;  // Last Key event Tick
    objLastKeyCode : WORD;      // Last Key Code
    objCurKeyCode  : WORD;      // Current Key Code

    //--------------------------------------------------------------------------
    // Window Manager
    //--------------------------------------------------------------------------

    //objWinList : TGenRectWinList; // List of all Windows

    objWinEdit    : boolean;      // True if Window Edit Mode

    //--------------------------------------------------------------------------
    // Generic Windows
    //--------------------------------------------------------------------------

    objItemListWin : TItemListWin;    // Item List Window
    objSaverWin : TSaverWin;          // Screen Saver Window
    objClockWin : TClockWin;          // Clock Window
    objFileInfoWin : TVideoInfoWin;   // Generic Info Window

    //--------------------------------------------------------------------------
    // Audio Playing Windows
    //--------------------------------------------------------------------------

    objTrackInfoWin : TTrackInfoWin;  // Track Info Window
    //objTrackCurWin  : TCurTrackWin;   // Current Track Window
    objEqWin        : TEqWin;         // Equalizer and Volume Window
    objVuWin        : TVuWin;         // Volume Meter Window
    objWoferLeft    : TWoferWin;      // Left Wofer Window
    objWoferRight   : TWoferWin;      // Right Wofer Window

    //--------------------------------------------------------------------------
    // Application State Methods
    //--------------------------------------------------------------------------

    // Change State of Application

    procedure SetState(const State : integer);

    // Set Ui Properties of Windows after Text Height adjustment

    procedure SetUiProperties;

    // Handle Posted messages

    procedure MyOnMessage (var Msg: TMsg; var Handled: Boolean);

    // Disable Mouse Event

    procedure OnMouseTimer (Sender: TObject);

    // Play Selected Video or Audio

    procedure PlayCurrent;

    //--------------------------------------------------------------------------
    // Remote Control
    //--------------------------------------------------------------------------

    // Handle Remote Controler Messages

    procedure OnRemoteMsg (var Msg : TMessage); message WM_INPUT;

    // Remote Command Handler

    procedure OnRemoteCmd(const Code : integer);

    //--------------------------------------------------------------------------
    // Screen Saver Management
    //--------------------------------------------------------------------------

    function  ResetScreenSaver:boolean;
    procedure ResetMouseTimer;

    //--------------------------------------------------------------------------
    // Audio Playing
    //--------------------------------------------------------------------------

    procedure PlayCurTrack;

    //--------------------------------------------------------------------------
    // Other Menu Commands
    //--------------------------------------------------------------------------

    procedure MenuShow;                        // Show  Popup Menu

    procedure MenuShuffle  (Sender : TObject); // Toggle Shuffle
    procedure MenuClockOn  (Sender : TObject); // Toggle Clock
    procedure MenuIncSize  (Sender : TObject); // Inc Text Height
    procedure MenuDecSize  (Sender : TObject); // Dec Text Height
    procedure MenuExit     (Sender : TObject); // Exit Application
  public
end;

var
  FullScreen: TFullScreen;

implementation

{$R *.dfm}

uses
  StrUtils,
  Math,
  MMSystem,

  TGenObjectUnit,     // Gen TObject and TGen ObjectList
  TGenStrUnit,        // String Functions
  TGenGraphicsUnit,   // Graphic Functions
  TGenTextFileUnit,   // Textfile Class
  MyFileTimeUnit,     // TFileTime Function
  TGenShellUnit,      // Shell Functions
  TGenAudioUnit,      // Audio Function
  TGenMenuUnit,       // TGenMenu Class

  TItemListUnit,      // All Items
  TProgItemUnit,
  TDocVideoUnit,      // Video File
  TDocTrackUnit,      // Audio Track

  TVuAnalyzerUnit,
  TVuBarUnit,
  TVu3dUnit,
  TVuFlerpUnit,
  TVuAteroidsUnit,

  TExplorerAppUnit,
  TMediaPlayerResUnit,

  TGenLogUnit,        // Log Object
  TGenClassesUnit;    // Class Management


const
  MouseTime = 10000; // 10 Seconds

//------------------------------------------------------------------------------
//
//                         WATCH IT - ITS THE MESSAGE PUMP
//
//------------------------------------------------------------------------------
procedure TFullScreen.MyOnMessage (var Msg: TMsg; var Handled: Boolean);
begin

  case Msg.message of

    // Remote Input, Command Id is in wParam and lParam

    MSG_REMOTE_INPUT :
      begin
        // TheLog.Log('Remote Id1 ' + IntToStr(Msg.wParam) +
        //                 ' Id2 ' + IntToStr(Msg.lParam));

        self.OnRemoteCmd(Msg.wParam);
        Handled := true;
      end;

    // A New Item was Selected, Tell all Windows about this

    SELECT_ITEM :
      begin
        // TheLog.Log('New Item Selected');

        TheWinList.pItem := objItemListWin.pCurItem;
        self.Invalidate;
        Handled := true;
      end;

    // An Audio Track has ended

    WM_TRACK_ENDED :
      begin
        // TheLog.Log('Track Ended');

        ResetScreenSaver;
        if TheApp.pShuffle.pBool then
          begin
            objItemListWin.SelectRandom;
            PlayCurrent;
          end
        else
          begin
            objItemListWin.SelectNext;
            PlayCurrent;
          end;
        Handled := true;
      end;

    // Screen Saver has triggered
    
    WM_SAVER_TIME :
      begin
        if (not objWinEdit) then
          begin
            self.SetState(StateSaver);
            self.Invalidate;
          end
        else
          begin
            ResetScreenSaver;
          end;

        Handled := true;
      end;

    // ScreenSaver wants to repaint it all

    WM_SAVER_REPAINT :
      begin
        self.Invalidate;
        Handled := true;
      end;
  end;
end;
//------------------------------------------------------------------------------
//
//                 WATCH IT - ITS THE REMOTE CONTROLER MESSAGE PUMP
//
//------------------------------------------------------------------------------
procedure TFullScreen.OnRemoteMsg (var Msg : TMessage);
begin
  // Just Forward it to Remote Controler Object

  if Assigned(objRemote) then
    objRemote.OnMess(Msg);
end;
//------------------------------------------------------------------------------
//
//                               CREATE & DESTROY
//
//------------------------------------------------------------------------------
//  Create
//------------------------------------------------------------------------------
procedure TFullScreen.FormCreate(Sender: TObject);
var
  sTmp : string;
begin
  Randomize;

  objState := StateUndef;

  objMouseTimer := TTimer.Create(nil);
  objMouseTimer.Interval := MouseTime;
  objMouseTimer.OnTimer  := OnMouseTimer;
  objMouseTimer.Enabled  := true;

  //----------------------------------------------------------------------------
  // Startup Logging
  //----------------------------------------------------------------------------

  TGenLog.StartUp(nil);

  //----------------------------------------------------------------------------
  // Initiate The Classes
  //----------------------------------------------------------------------------
  
  sTmp := ExDelim(GetFolderName(Application.ExeName));
  TGenClasses.AddSearchFolder(sTmp);
  TGenClasses.AddSearchFolder(ExDelim(GetFolderName(sTmp)) + '\TGenUnits');

  //----------------------------------------------------------------------------
  // Create The Window List
  //----------------------------------------------------------------------------

  TGenRectWinList.Startup(self);

  //----------------------------------------------------------------------------
  // Create TheApp
  //----------------------------------------------------------------------------

  TExplorerApp.StartUp;

  objInfoOn := TGenAppPropBool.Create(
        'Preferences','InfoOn', true) as TGenAppPropBool;
  objInfoOn.ForceRead;

  objClockOn := TGenAppPropBool.Create(
        'Preferences','ClockOn', true) as TGenAppPropBool;
  objClockOn.ForceRead;

  //----------------------------------------------------------------------------
  // Set Basic User Interface Properties
  //----------------------------------------------------------------------------

  // Avoid Flickering

  self.DoubleBuffered := true;

  // Set Full Screen

  with self do
    begin
      BorderStyle := bsNone;
      FormStyle   := fsStayOnTop;
      Left        := 0;
      Top         := 0;
      Height      := Screen.Height;
      Width       := Screen.Width;
      Color       := RGB(0,0,20);
    end;

  objForeColor := RGB(0, 240, 240);
  objHighColor := RGB(0,  70,  70);
  objBackColor := RGB(0,  30,  30);

  self.Font.Name   := 'Verdana';
  self.Font.Color  := objForeColor;
  self.Font.Height := Screen.Height div TheApp.pTextHeight.pInt;
  self.Color       := objBackColor;

  //----------------------------------------------------------------------------
  // Create All Basic Windows
  //----------------------------------------------------------------------------

  objItemListWin := TItemListWin.Create(self.Canvas, TypeImages);
  TheWinList.Add(objItemListWin);

  objClockWin := TClockWin.Create(self.Canvas);
  TheWinList.Add(objClockWin);

  objSaverWin := TSaverWin.Create(self.Canvas);
  TheWinList.Add(objSaverWin);

  objFileInfoWin := TVideoInfoWin.Create(self.Canvas);
  TheWinList.Add(objFileInfoWin);

  //----------------------------------------------------------------------------
  // Create All Player Windows
  //----------------------------------------------------------------------------

  TBassPlayer.StartUp;

  objTrackInfoWin := TTrackInfoWin.Create(self.Canvas);
  TheWinList.Add(objTrackInfoWin);

  //objTrackCurWin := TCurTrackWin.Create(self.Canvas);
  //TheWinList.Add(objTrackCurWin);

  objEqWin := TEqWin.Create(self.Canvas);
  TheWinList.Add(objEqWin);

  Wofers.BkColor      := objBackColor;
  Wofers.DrawingStyle := dsNormal;
  Wofers.ImageType    := itImage;

  objWoferLeft := TWoferWin.Create(self.Canvas, Wofers, true);
  TheWinList.Add(objWoferLeft);

  objWoferRight := TWoferWin.Create(self.Canvas, Wofers, false);
  TheWinList.Add(objWoferRight);

  objVuWin := TVuWin.Create(self.Canvas, self, objWoferLeft, objWoferRight);
  TheWinList.Add(objVuWin);

  //----------------------------------------------------------------------------
  // Create Remote Controler and Set it up
  //----------------------------------------------------------------------------

  objRemote := TGenRemote.Create;
  Application.OnMessage := MyOnMessage;

  objLastKeyCode := 0;
  objCurKeyCode  := 0;

  //----------------------------------------------------------------------------
  // Open Application
  //----------------------------------------------------------------------------

  objWinEdit := false;
  SetState(StateOpening);
  
  TheLog.Log('-------------------- MediaPlayer Initiated --------------------');

  //----------------------------------------------------------------------------
  // Start Application
  //----------------------------------------------------------------------------

  SetState(StateView);
end;
//------------------------------------------------------------------------------
//  Turn Off/On things acording to State
//------------------------------------------------------------------------------
procedure TFullScreen.SetState(const State : integer);
begin
  if State <> objState then
    begin
      case State of
        //----------------------------------------------------------------------
        //  Application is Openeing
        //----------------------------------------------------------------------
        StateOpening :
          begin
            // Setup All Things

            TGenMenu.PopupMenuSetup(MyMenu);
            SetUiProperties;
          end;

        StateSaver :
          begin
            self.Color := clBlack;

            TheWinList.pState := State;
            objClockWin.pEnabled := objClockOn.pBool;

            Cursor := crNone;
            objMouseTimer.Enabled := true;
          end;

        StateView, StatePlayAudio :
          begin
            self.Color := objBackColor;

            TheWinList.pState := State;
            objClockWin.pEnabled := objClockOn.pBool;

            Cursor := crDefault;
            objMouseTimer.Enabled := true;
          end;

        //----------------------------------------------------------------------
        // Application Is Closing
        //----------------------------------------------------------------------
        StateClosing :
          begin
            TheWinList.pState := State;
          end;

      end;

      // Set the New State
      
      objState := State;

      // We need to Repaint after each State Change to reflect Changes
      
            TheWinList.RefreshUi;
      self.Invalidate;
    end;
end;
//------------------------------------------------------------------------------
//  Calculate All Paint Settings and Borders
//------------------------------------------------------------------------------
procedure TFullScreen.SetUiProperties;
begin
  TheLog.Log('SetUiProperties');

  // Set Text Properties

  self.Font.Height := Screen.Height div TheApp.pTextHeight.pInt;
  self.Color       := objBackColor;

  TheWinList.pBackColor := objBackColor;
  TheWinList.pForeColor := objForeColor;
  TheWinList.pHighColor := objHighColor;

  // Refresh all Windows User Interface

  TheWinList.RefreshUi;

  self.Invalidate;
end;
//------------------------------------------------------------------------------
//  Destroy
//------------------------------------------------------------------------------
procedure TFullScreen.FormDestroy(Sender: TObject);
begin
  TheLog.Log('--------------------- MediaPlayer Closed ----------------------');

  SetState(StateClosing);

  //----------------------------------------------------------------------------
  // Save all Windows data
  //----------------------------------------------------------------------------

  TheWinList.Save;

  objInfoOn.ForceWrite;
  objClockOn.ForceWrite;

  //----------------------------------------------------------------------------
  // Close Player
  //----------------------------------------------------------------------------

  TBassPlayer.ShutDown;

  //----------------------------------------------------------------------------
  // Free Remote Controler
  //----------------------------------------------------------------------------

  objRemote.Free;

  //----------------------------------------------------------------------------
  // Free all Windows
  //----------------------------------------------------------------------------

  TGenRectWinList.ShutDown;

  //----------------------------------------------------------------------------
  // Save and ShutDown Application Properties
  //----------------------------------------------------------------------------

  TExplorerApp.ShutDown;

  //----------------------------------------------------------------------------
  // ShutDown all Classes
  //----------------------------------------------------------------------------

  TGenClasses.WriteModules;
  TGenClasses.WriteClassHier(TObject);
  TGenClasses.ShutDown;
  TGenObject.LogIt;

  //----------------------------------------------------------------------------
  // Shutdown and Flush Log
  //----------------------------------------------------------------------------

  TGenLog.ShutDown;
end;
//------------------------------------------------------------------------------
//  Double Click
//------------------------------------------------------------------------------
procedure TFullScreen.FormDblClick(Sender: TObject);
begin
  if (objState = StateSaver) then
    begin
      SetState(StateView);
    end
  else
    begin
      objWinEdit := not objWinEdit;
      TheWinList.pSelected := objWinEdit;
      if not objWinEdit then
        Cursor := crDefault
      else
        objMouseTimer.Enabled := false;

      self.Invalidate;
    end;
end;
//------------------------------------------------------------------------------
//  Reset the Screen Saver Timer
//------------------------------------------------------------------------------
function TFullScreen.ResetScreenSaver:boolean;
begin
  // Return true if Saver is on

  result := (objState = StateSaver);

  if (objState = StateSaver) then
    begin
      // Bring Back View State

      SetState(StateView);
    end
  else
    begin
      // Tell Saver to start timing over again

      objSaverWin.Reset;
    end;
end;
//------------------------------------------------------------------------------
//
//                                  MENU MANAGEMENT
//
//------------------------------------------------------------------------------
//  Setup all Menus
//------------------------------------------------------------------------------
procedure TFullScreen.MenuShow;
var
  Rows, X, Y : integer;
  TextSize   : TSize;
begin
  if (objState = StatePlayAudio) then
    begin
      Rows := 13;
      TextSize := self.Canvas.TextExtent('XXXXXXXXXXXXXXXXXXXXXXX');
    end
  else
    begin
      Rows := 4;
      TextSize := self.Canvas.TextExtent('XXXXXXXXXXXX');
    end;

  X := (Screen.Width - TextSize.cx) div 2;
  Y := round(Screen.Height - (1.5 * Rows * TextSize.cy)) div 2;

  TheLog.Log('X ' + IntToStr(X) + ' Y ' + IntToStr(Y));
  MyMenu.PopUp (X, Y);
end;
//------------------------------------------------------------------------------
//  Add all Menues
//------------------------------------------------------------------------------
procedure TFullScreen.MyMenuPopup(Sender: TObject);
var
  pMenu : TGenMenu;
begin
  MyMenu.Items.Clear;

  TGenMenu.StartUp(
     RGB(0,170,170),
     objBackColor,
     self.Font.Name,
     objForeColor,
     self.Font.Height);

  pMenu := TGenMenu.Create(MyMenu);
  pMenu.Caption := 'Back';
  MyMenu.Items.Add(pMenu);

  if (objState = StatePlayAudio) then
    begin

      pMenu := TGenMenu.Create(MyMenu);
      pMenu.Caption := '-';
      MyMenu.Items.Add(pMenu);

      pMenu := TGenMenu.Create(MyMenu);
      pMenu.Caption := 'Shuffle';
      pMenu.Checked := TheApp.pShuffle.pBool;
      pMenu.OnClick := MenuShuffle;
      MyMenu.Items.Add(pMenu);

      pMenu := TGenMenu.Create(MyMenu);
      pMenu.Caption := '-';
      MyMenu.Items.Add(pMenu);

      objEqWin.AddMenues(MyMenu);

      //------------------------------------------------------------------------
      // Volume Meter
      //------------------------------------------------------------------------

      pMenu := TGenMenu.Create(MyMenu);
      pMenu.Caption := '-';
      MyMenu.Items.Add(pMenu);

      objVuWin.AddMenues(MyMenu);

      pMenu := TGenMenu.Create(MyMenu);
      pMenu.Caption := '-';
      MyMenu.Items.Add(pMenu);
    end;

  pMenu := TGenMenu.Create(MyMenu);
  pMenu.Caption := 'Increase Text';
  pMenu.OnClick := MenuIncSize;
  MyMenu.Items.Add(pMenu);

  pMenu := TGenMenu.Create(MyMenu);
  pMenu.Caption := 'Decrease Text';
  pMenu.OnClick := MenuDecSize;
  MyMenu.Items.Add(pMenu);

      pMenu := TGenMenu.Create(MyMenu);
      pMenu.Caption := 'Clock';
      pMenu.Checked := objClockOn.pBool;
      pMenu.OnClick := MenuClockOn;
      MyMenu.Items.Add(pMenu);

  pMenu := TGenMenu.Create(MyMenu);
  pMenu.Caption := 'Exit';
  pMenu.OnClick := MenuExit;
  MyMenu.Items.Add(pMenu);
end;
//------------------------------------------------------------------------------
//  Menu
//------------------------------------------------------------------------------
procedure TFullScreen.MenuShuffle(Sender : TObject);
begin
  TheApp.pShuffle.Toggle;
end;
//------------------------------------------------------------------------------
//  Menu
//------------------------------------------------------------------------------
procedure TFullScreen.MenuClockOn(Sender : TObject);
begin
  objClockOn.Toggle;
  objClockWin.pEnabled := objClockOn.pBool;
  self.Invalidate;
end;
//------------------------------------------------------------------------------
//  Menu
//------------------------------------------------------------------------------
procedure TFullScreen.MenuIncSize(Sender : TObject);
begin
  if (TheApp.pTextHeight.pInt > 10) then
    begin
      TheApp.pTextHeight.pInt := TheApp.pTextHeight.pInt - 2;
      self.SetUiProperties;
    end;
end;
//------------------------------------------------------------------------------
//  Menu
//------------------------------------------------------------------------------
procedure TFullScreen.MenuDecSize(Sender : TObject);
begin
  if (TheApp.pTextHeight.pInt < 30) then
    begin
      TheApp.pTextHeight.pInt := TheApp.pTextHeight.pInt + 2;
      self.SetUiProperties;
    end;
end;
//------------------------------------------------------------------------------
//  Menu
//------------------------------------------------------------------------------
procedure TFullScreen.MenuExit(Sender : TObject);
begin
  self.Close;
end;
//------------------------------------------------------------------------------
//
//                              HANDLE REMOTE MESSAGES
//
//------------------------------------------------------------------------------
//  Handle Any Command from The Remote Controler
//------------------------------------------------------------------------------
procedure TFullScreen.OnRemoteCmd(const Code : integer);
var
  bList : boolean;
begin
  bList := false;

  if ResetScreenSaver then EXIT;

  // TheLog.Log('Remote: ' + IntToStr(COde));

  case Code of

    RC_INFO :
      begin
        objInfoOn.Toggle;
        objFileInfoWin.pEnabled  := objInfoOn.pBool;
        objTrackInfoWin.pEnabled := objInfoOn.pBool;
        TheWinList.RefreshUi;
        self.Invalidate;
      end;
    
    RC_STOP : ; // Remote will give VK_MEDIA_STOP Virtual Key COde

    RC_PAUSE :
      begin
        ThePlayer.pPaused := not ThePlayer.pPaused;
      end;

    RC_PLAY :
      begin
        if (ThePlayer.pState = PlayerStatusIdle) then
          begin
            PlayCurTrack;
            TheWinList.RefreshUi;
          end
        else if (ThePlayer.pState = PlayerStatusPlaying) then
          ThePlayer.pPaused := true
        else if (ThePlayer.pState = PlayerStatusPaused) then
          ThePlayer.pPaused := false
      end;

    RC_NEXT : ; // Virtual Key Code VK_MEDIA_NEXT_TRACK

    RC_FORWARD :
      begin
        if (objState = StatePlayAudio) and
            TheApp.pShuffle.pBool then
          begin
            objItemListWin.SelectRandom;
            self.PlayCurTrack;
          end
        else
          begin
            objItemListWin.SelectNext;
            self.PlayCurTrack;
          end;
      end;

    RC_PREVIOUS : ; // Virtual Key Code VK_MEDIA_PREV_TRACK
    
    RC_BACKWARD :
      begin
        if (objState = StatePlayAudio) and
            TheApp.pShuffle.pBool then
          begin
            objItemListWin.SelectRandom;
            self.PlayCurTrack;
          end
        else
          begin
            objItemListWin.SelectPrev;
            self.PlayCurTrack;
          end;
      end; 

    RC_CH_UP   : 
      begin
        objItemListWin.SelectPrevPage;
        bList := true;
      end;

    RC_CH_DOWN :
      begin
        objItemListWin.SelectNextPage;
        bList := true;
      end;

    RC_MENU : MenuShow;

  end;

  if bList and 
     (objState = StatePlayAudio) then
    begin
      objItemListWin.pVisible := true;
      TheWinList.RefreshUi;
      self.Invalidate;
      objMouseTimer.Enabled := true;
      ResetMouseTimer;
    end;
end;
//------------------------------------------------------------------------------
//
//                               HANDLE KEY MESSAGES
//
//------------------------------------------------------------------------------
//  Key Press (Up, Down)
//------------------------------------------------------------------------------
procedure TFullScreen.FormKeyPress(Sender: TObject; var Key: Char);
begin
  if ResetScreenSaver then EXIT;

  if (Key = Chr(VK_ESCAPE)) then
    begin
      self.Close;
    end;
end;
//------------------------------------------------------------------------------
//  Key Down
//------------------------------------------------------------------------------
procedure TFullScreen.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  tDelta  : Cardinal;
  bSingle : boolean;
  bList   : boolean;
begin
  if ResetScreenSaver then EXIT;

  bList := false;

  // Some Keyes are two Keys after another with a very short interval
  // This is Managed by Windows, not the remote, so its actually zero
  // Delta Time (We use 5 mS to be safe) Aouto Repeat is about 30mS

  tDelta := Windows.GetTickCount - objLastKeyTick;

  if (tDelta > 5) then
    begin
      objLastKeyCode := 0;
      objCurKeyCode  := Key;
      bSingle := true;
    end
  else
    begin
      objLastKeyCode := objCurKeyCode;
      objCurKeyCode  := Key;
      bSingle := false;
    end;

  objLastKeyTick := Windows.GetTickCount;

  if bSingle then
    begin
      // TheLog.Log('Key ' + IntToStr(Key));

      case Key of
        VK_UP :
          begin
            if objEqWin.pEditMode then
              objEqWin.SetGain(true)
            else if objVuWin.pEditMode then
              objVuWin.pDataY := 1
            else
              begin
                objItemListWin.SelectPrev;
                bList := true;
              end;
          end;

        VK_DOWN : 
          begin
            if objEqWin.pEditMode then
              objEqWin.SetGain(false)
            else if objVuWin.pEditMode then
              objVuWin.pDataY := -1
            else
              begin
                objItemListWin.SelectNext;
                bList := true;
              end;
          end;

        VK_BROWSER_BACK :
          begin
            if objEqWin.pEditMode then
              objEqWin.pEditMode := false
            else if objVuWin.pEditMode then
              objVuWin.pEditMode := false
            else
              begin
                objItemListWin.SelectParent;
                bList := true;
              end;
          end;

        VK_LEFT :
          begin
            if objEqWin.pEditMode then
              objEqWin.pIndex := objEqWin.pIndex - 1
            else if objVuWin.pEditMode then
              objVuWin.pDataX := +1
            else
              begin
                objItemListWin.SelectParent;
                bList := true;
              end;
          end;

        VK_RIGHT :
          begin
            if objEqWin.pEditMode then
              objEqWin.pIndex := objEqWin.pIndex + 1
            else if objVuWin.pEditMode then
              objVuWin.pDataX := -1
            else
              begin
                objItemListWin.SelectChild;
                bList := true;
              end;
          end;

        VK_RETURN :
          begin
            self.PlayCurrent;
            bList := true;
          end;

        //----------------------------------------------------------------------
        // Media Virtual Key Codes
        //----------------------------------------------------------------------

        VK_MEDIA_STOP :
          begin
            ThePlayer.Stop;
            SetState(StateView);
            bList := true;
          end;

        VK_MEDIA_PLAY_PAUSE :
          begin
            if (ThePlayer.pState = PlayerStatusIdle) then
              PlayCurTrack
            else if (ThePlayer.pState = PlayerStatusPlaying) then
              ThePlayer.pPaused := true
            else if (ThePlayer.pState = PlayerStatusPaused) then
              ThePlayer.pPaused := false
          end;

        VK_MEDIA_NEXT_TRACK :
          begin
            if (objState = StatePlayAudio) and
                TheApp.pShuffle.pBool then
              begin
                objItemListWin.SelectRandom;
                self.PlayCurTrack;
              end
            else
              begin
                objItemListWin.SelectNext;
                self.PlayCurTrack;
              end;
          end;

        VK_MEDIA_PREV_TRACK :
          begin
            if (objState = StatePlayAudio) and
                TheApp.pShuffle.pBool then
              begin
                objItemListWin.SelectRandom;
                self.PlayCurTrack;
              end
            else
              begin
                objItemListWin.SelectPrev;
                self.PlayCurTrack;
              end;
          end;
      end;
    end;

  if bList and
     (objState = StatePlayAudio) then
    begin
      objItemListWin.pVisible := true;
      TheWinList.RefreshUi;
      self.Invalidate;
      objMouseTimer.Enabled := true;
      ResetMouseTimer;
    end;
end;
//------------------------------------------------------------------------------
//
//                                  BASIC METHODS
//
//------------------------------------------------------------------------------
//  Play this with Default Player
//------------------------------------------------------------------------------
procedure TFullScreen.PlayCurrent;
const
  cPlay = 'play';
var
  pItem : TTreeItemBase;
begin
  TheLog.Log('PlayCurrent');
  pItem := objItemListWin.pCurItem;
  if (pItem <> nil) then
    begin
      // If an Audio, play it inhouse

      if (pItem IS TDocTrack) then
        begin
          PlayCurTrack;
        end
      else
        begin
          // If we are Playing an Audio Track, stop it and go to View State

          if (objState = StatePlayAudio) then
            begin
              ThePlayer.Stop;
              SetState(StateView);
            end;

          // Execute it outside application

          pItem.Execute;
        end
    end
  else
    begin
      self.Invalidate;
    end;
end;
//------------------------------------------------------------------------------
//
//                                   PAINT FORM
//
//------------------------------------------------------------------------------
//  Paint
//------------------------------------------------------------------------------
procedure TFullScreen.FormPaint(Sender: TObject);
begin
  TheWinList.Paint;
end;
//------------------------------------------------------------------------------
//
//                            AUDIO PLAYER MANAGEMENT
//
//------------------------------------------------------------------------------
//  Play Next Audio Track Randomly
//------------------------------------------------------------------------------
procedure TFullScreen.PlayCurTrack;
var
  pItem : TTreeItemBase;
begin
  pItem := objItemListWin.pCurItem;
  if (pItem <> nil) then
    begin
      SetState(StatePlayAudio);

      pItem.Execute;

      objVuWin.Reset;
    end;
end;
//------------------------------------------------------------------------------
//
//                             MOUSE MANAGEMENT
//
//------------------------------------------------------------------------------
//  Mouse Timer Event: Turn off Cursor and disable timer
//------------------------------------------------------------------------------
procedure TFullScreen.OnMouseTimer (Sender: TObject);
begin
  self.Cursor           := crNone;
  objMouseTimer.Enabled := false;

  if (objState = StatePlayAudio) then
    begin
      objItemListWin.pVisible := false;
      TheWinList.RefreshUi;
      self.Invalidate;
    end;
end;
//------------------------------------------------------------------------------
//  Mouse Down Event: Let Window List Handle this
//------------------------------------------------------------------------------
procedure TFullScreen.FormMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  TheWinList.SetMouseDown(Point(X,Y));
end;
//------------------------------------------------------------------------------
//  Mouse Move Event: If Edit Mode, let WIndow List handle this
//------------------------------------------------------------------------------
procedure TFullScreen.FormMouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
begin
  if objWinEdit then
    begin
      // Repaint all if a Window was Moved

      if TheWinList.SetMouseMove(Point(X,Y)) then
        begin
          self.Invalidate;
          TheWinList.RefreshUi;
        end;
    end
  else
    begin
      if (Cursor = crNone) then
        begin
          Cursor := crDefault;
          objItemListWin.pVisible := true;
          TheWinList.RefreshUi;
          self.Invalidate;
          ResetMouseTimer;
        end;
        
      if (not objWinEdit) then
        begin
          // Reset Mouse Timer by turning it off and on
          
          objMouseTimer.Enabled := false;
          objMouseTimer.Enabled := true;
        end;
    end;
end;
//------------------------------------------------------------------------------
//  Mouse Up Event: Let Window List handle this
//------------------------------------------------------------------------------
procedure TFullScreen.FormMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  TheWinList.SetMouseUp;
end;
//------------------------------------------------------------------------------
//  Reset Mouse Timer
//------------------------------------------------------------------------------
procedure TFullScreen.ResetMouseTimer;
begin
  if objMouseTimer.Enabled then
    begin
      objMouseTimer.Enabled := false;
      objMouseTimer.Enabled := true;
    end;
end;
//------------------------------------------------------------------------------
//                              ININTIALIZATION
//------------------------------------------------------------------------------
initialization
  RegClass(TFullScreen);
end.
