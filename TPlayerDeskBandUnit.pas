unit TPlayerDeskBandUnit;

interface
uses
  Windows,  Forms, Graphics, Classes, Contnrs, Controls, Menus, Messages,
  StrUtils, SysUtils,

  TWmMsgFactoryUnit,  // Message Factory
  TMediaBaseUnit;     // Media Base

//------------------------------------------------------------------------------
//  MSG_DB_... (500) DeskBand Messages                                   MSG_ID
//------------------------------------------------------------------------------
const
  // Command From Player Deskband
  //  wParam : DeskBand HWND
  //  lParam : Command from DeskBand
  //    1 : Play Previous
  //    2 : Play / Pause
  //    3 : Play Next
  //    4 : Minimize / Restore Toggle
  //    5 : Close Application
  //  Owner  : Application
  //  Sender : DeskBand

  MSG_DB_DESKBAND_CMD = WM_USER + 501;

  //----------------------------------------------------------------------------
  // Player State Send to Player Deskband
  //  wParam : Application HWND
  //  lParam : Player State
  //    0 Player is Opening
  //    1 Player is Idle
  //    2 Player is Playing a Track
  //    3 Player Has Paused Playing a Track
  //    4 Player is Closing Down
  //  Owner  : DeskBand
  //  Sender : Application

  MSG_DB_PLAYER_STATE = WM_USER + 502;

  //----------------------------------------------------------------------------
  // Text Message Sent to DeskBand
  //  wParam : String Type
  //    1 : Player Application full PathName
  //    2 : Current Track Information
  //    10.. Playlists Full PathNames Index (+1) in Player Application
  //  lParam : Player State
  //  Owner  : DeskBand
  //  Sender : Application

  MSG_DB_APP_STRING = WM_USER + 503;

  //----------------------------------------------------------------------------
  // Application Windows State Sent To Deskband
  //  wParam : Application HWND
  //  lParam : Application Window State
  //    1 : Minimized
  //    2 : Restored
  //    3 : Closing
  //    4 : Active
  //    5 : Inactive
  //  Owner  : DeskBand
  //  Sender : Application

  MSG_DB_APPWIN_STATE = WM_USER + 504;

  awsMinimized = 1;
  awsRestored  = 2;
  awsClosing   = 3;
  awsActive    = 4;
  awsInactive  = 5;

//------------------------------------------------------------------------------
//  DeskBand Class
//------------------------------------------------------------------------------
type TPlayerDeskBand = Class(TObject)
  private
    objDeskBandHwnd : HWND;             // DeskBand Window
    objPlayLists    : TStringList;      // Playlist Sent to Deskband
    objSubscriber   : TWmMsgSubscriber; // Message Subscriber
    objMinimized    : boolean;
  protected

    // Subscriber Message Process

    procedure MsgProcess(var Msg : TMsg);

    // Set Deskband Hwnd

    procedure SetDeskBandWnd(const DeskBand : HWND);

    // Post a Message to Deskband

    procedure PostDeskBand
    (const MsgId : DWORD; const lParam : DWORD);

    // Post a String to Deskband

    procedure PostDeskBandStr(
      const StrId : DWORD;      // String Identity
      const Str   : string);    // String

    // Play a Playlist

    procedure PlayPlayList(const Index : integer);

    procedure Init;
    procedure Close;

  public
    constructor Create;
    destructor  Destroy; override;

    class procedure StartUp;
    class procedure ShutDown;

    // Set Application Window State

    procedure SetAppWindowState(const AWS : integer);

    // Send all Playlists to Deskband

    procedure SendPlayLists;

end;

//------------------------------------------------------------------------------
// Singleton: PlayerDeskBand
//------------------------------------------------------------------------------
var TheDeskBand : TPlayerDeskBand = nil;

implementation

uses
  TPmaLogUnit,          // Log
  TMediaUtilsUnit,      // Media
  TMediaPlayerUnit,     // Media Player
  TBassPlayerUnit,      // Bass Player
  TMediaFactoryUnit,    // Media Factory
  TPmaClassesUnit;

const
  DeskBandClass = 'TThePlayerVisibleBand';
  DeskBandTitle = 'ThePlayerVisibleBand';

//------------------------------------------------------------------------------
//  Enum Windows Process
//------------------------------------------------------------------------------
var FoundWindow : HWND = 0; // Global used by EnumWindowsProcess
//------------------------------------------------------------------------------
//  Enum Windows Process
//------------------------------------------------------------------------------
function EnumWindowsProc(Wnd : HWND; lParam: DWORD):BOOL; stdcall;
var
  W : HWND;
  //Title, ClassName: array[0..255] of char;
begin
  result := true;

  // does this Window has the DeskBand

  W := FindWindowEx(Wnd, 0, DeskBandClass, DeskBandTitle);
  if (W <> 0) then
    begin
      FoundWindow := W;
      result      := false;

      //GetWindowText(FoundWindow, Title, 255);
      //GetClassName (FoundWindow, ClassName, 255);

      //MyLog('Found DeskBand: ' + Title + ' / ' + ClassName);
    end
  else
    begin
      // Enumerate all Child Windows

      EnumChildWindows(Wnd, @EnumWindowsProc, lParam);
    end;
end;
//------------------------------------------------------------------------------
//  Log
//------------------------------------------------------------------------------
procedure Log(const Line : string);
begin
  if Assigned(PmaLog) then
    PmaLOg.Log(Line);
end;
//------------------------------------------------------------------------------
//  Create
//------------------------------------------------------------------------------
constructor TPlayerDeskBand.Create;
begin
  inherited;

  objDeskBandHwnd := 0;
  objPlayLists    := TStringList.Create;

end;
//------------------------------------------------------------------------------
//  Startup Deskband
//------------------------------------------------------------------------------
procedure TPlayerDeskBand.Init;
var
  Str : string;
begin
  objDeskBandHwnd := 0;
  FoundWindow     := 0;

  // Set Subscribtion on Messages I own

  if Assigned(MsgFactory) then
    begin
      objSubscriber := MsgFactory.Subscribe(self.ClassName, MsgProcess);

      // Messages from DeskBand

      objSubscriber.AddMessage(MSG_DB_DESKBAND_CMD);

      // Message about Bass Player State

      objSubscriber.AddMessage(MSG_BASS_STATE);

      // Message from Bass Player that a New Track has Stated

      objSubscriber.AddMessage(MSG_BASS_TRACK_START)
    end;

  // Find the Deskband Windows. Normally the Application is Started and
  // stopped while the Deskband will exist all day long. So when App is
  // started we try to find the Deskband using recursive Enum Windows

  // The Deskband will in its turn look for Player if it dosnt have the
  // App window already, or its became invalid when the Player has Closed.
  // If Player isnt found it will use the last App Exefile sent to it and
  // start the Player again

  // Enumerate To Find the DeskBand WIndow

  EnumWindows(@EnumWindowsProc, 0);
  objDeskBandHwnd := FoundWindow;
  if IsWindow(objDeskBandHwnd) then
    begin
      // We Got the DeskBand, Log it and Send Application Name to it

      //GetWindowText(objDeskBandHwnd, Title, 255);
      //GetClassName (objDeskBandHwnd, ClassName, 255);

     // MainLog('Got DeskBand ' + Title + ' / ' + ClassName);

      Str := Application.ExeName;
      PostDeskBandStr(1, Str);

      Log('  Found Deskband, Sent App Exefile to it: ' + Str);

      PostDeskBand(MSG_DB_APPWIN_STATE, 2);
    end
  else
    begin
      // Didnt Get it

      objDeskBandHwnd := 0;
      Log('  Count Not Find DeskBand');
    end;
end;
//------------------------------------------------------------------------------
//  ShutDown DeskBand
//------------------------------------------------------------------------------
procedure TPlayerDeskBand.Close;
begin
  // Tell Deskband we are closing

  PostDeskBand(MSG_DB_APPWIN_STATE, 3); 

  // Unsubscribe all Messages

  if Assigned(objSubscriber) then
    MsgFactory.DeSubscribe(objSubscriber);
    
end;
//------------------------------------------------------------------------------
//  Create
//------------------------------------------------------------------------------
destructor TPlayerDeskBand.Destroy;
begin

  objPlayLists.Free;

  inherited;
end;
//------------------------------------------------------------------------------
// Subscriber Message Process
//------------------------------------------------------------------------------
procedure TPlayerDeskBand.MsgProcess(var Msg : TMsg);
begin
  case Msg.message of

    // All These Messages Comes from the Deskband. We translate them
    // and forwards them to appropiate function

    MSG_DB_DESKBAND_CMD :
      begin
        SetDeskBandWnd(Msg.wParam);
        // Log('DeskBand Cmd: ' + IntToStr(Msg.lParam));
        
        case Msg.lParam of
          1 : PostMsg(MSG_MP_PLAY_PREV,0,0);

          2 : PostMsg(MSG_BASS_TRACK_PLAYPAUSE, 0, 0);

          3 : PostMsg(MSG_MP_PLAY_NEXT,0,0);

          // Toggle between Minimized and Normal Windows State

          4 :
            begin
              if objMinimized then
                begin
                  Application.Restore;
                  //MainLog('Restore');
                end
              else
                begin
                  Application.Minimize;
                  //MainLog('Minimize');
                end;
            end;

          5 : PostMessage(Application.Handle, WM_CLOSE, 0, 0);

        else

          if Msg.lParam > 7 then
            self.PlayPlayList(Msg.lParam);
        end;
      end;

    MSG_BASS_TRACK_START : if Assigned(TheMediaPlayer) then
      begin
        // The Player Has Started to Play a New Track, send Track Info
        // to Deskband

        PostDeskBandStr(2, TheMediaPlayer.pTrackInfo);
      end;

    MSG_BASS_STATE :
      begin
        // The Player has changed State, send State to Deskband
        
        PostDeskBand(MSG_DB_PLAYER_STATE, Msg.lParam);
      end;
  end;
end;
//------------------------------------------------------------------------------
//  Set Application Window State
//------------------------------------------------------------------------------
procedure TPlayerDeskBand.SetAppWindowState(const AWS : integer);
begin
  PostDeskBand(MSG_DB_APPWIN_STATE, AWS);

  case AWS of
    awsMinimized : objMinimized := true;
    awsRestored  : objMinimized := false;
    awsClosing   : ;
    awsActive    : ;
    awsInactive  : ;
  end;
end;
//------------------------------------------------------------------------------
//
//                                  DESKBAND METHODS
//
//------------------------------------------------------------------------------
//  Set DeskBand Window from a Received Message from DeskBand
//------------------------------------------------------------------------------
procedure TPlayerDeskBand.SetDeskBandWnd(const DeskBand : HWND);
var
  Str : string;
begin
  if (objDeskBandHwnd <> DeskBand) then
    begin
      // Got DeskBand from DeskBand Message

      objDeskBandHwnd := DeskBand;

      // Post the Application Exe File to Deskband

      Str := Application.ExeName;
      // Log('Got Deskband HWND, sends App File ' + Str);
      PostDeskBandStr(1, Str);
    end;
end;
//------------------------------------------------------------------------------
//  Post a Message To DeskBand
//------------------------------------------------------------------------------
procedure TPlayerDeskBand.PostDeskBand
    (const MsgId : DWORD; const lParam : DWORD);
begin
  // Post Message to Deskband if the DeskBand is Available

  if IsWindow(objDeskBandHwnd) then
    begin
        PostMessage(objDeskBandHwnd, MsgId, Application.Handle, lParam);
    end;
end;
//------------------------------------------------------------------------------
//  Post a String To DeskBand
//------------------------------------------------------------------------------
procedure TPlayerDeskBand.PostDeskBandStr(
      const StrId : DWORD;
      const Str   : string);
var
  Ind : integer;
begin
  // Log('Post Deskband Str ' + Str + ' (' + IntToStr(StrId) + ')');
  
  if IsWindow(objDeskBandHwnd) and (length(Str) > 0) then
    begin
      // Post All Charactes

      for Ind := 1 to length(str) do
        PostMessage(objDeskBandHwnd,
          MSG_DB_APP_STRING, StrId, DWORD(Ord(Str[Ind])));

      // Post Char = 0 as last Character

      PostMessage(objDeskBandHwnd, MSG_DB_APP_STRING, StrId, 0);
    end;
end;
//------------------------------------------------------------------------------
// Play a Playlist from its Index
//------------------------------------------------------------------------------
procedure TPlayerDeskBand.PlayPlayList(const Index : integer);
var
  Ind      : integer;
  PlayList : TMediaBase;
begin
  // Log('Playlist Ind ' + IntToStr(Index));

  Ind := Index - 10;
  if (Ind >= 0) and (Ind < objPlayLists.Count) then
    begin
      // Log('Playlist Ind ' + IntToStr(Ind));
      
      PlayList := TheMedia.FindPlayListByPath(objPlayLists[Ind]);
      if Assigned(PlayList) then
        PostMsg(MSG_MP_PLAY_CONTAINER, DWORD(PlayList), DWORD(nil));

    end;
end;
//------------------------------------------------------------------------------
// Send All Playlists to DeskBand
//------------------------------------------------------------------------------
procedure TPlayerDeskBand.SendPlayLists;
var
  Ind       : integer;
begin
  // This should be done when:
  //  - Media is Loaded
  //  - PlayList Query is Made
  
  if IsWindow(objDeskBandHwnd) then
    begin
      // Clear Playlists in DeskBand

      PostMessage(objDeskBandHwnd, MSG_DB_APP_STRING, 3, 0);

      // Clear My own version of the Playlist

      objPlayLists.Clear;

      // Get All Media Playlists from Media Factory

      if Assigned(TheMedia) and Assigned (TheMedia.pMDB) then
        TheMedia.pMDB.GetAllPlayLists(objPlayLists);

      // Send all Playlists found in Media Db

      if (objPlayLists.Count > 0) then
        for Ind := 0 to objPlayLists.Count - 1 do
          begin
            if SysUtils.FileExists(objPlayLists[Ind]) then
            begin
              PostDeskBandStr(4, objPlayLists[Ind]);
            end;
          end;
    end;
end;
//------------------------------------------------------------------------------
//  Startup
//------------------------------------------------------------------------------
class procedure TPlayerDeskBand.Startup;
begin
  if (not Assigned(TheDeskBand)) then
    begin
      Log('  TPlayerDeskBand.Startup');
      TheDeskBand := TPlayerDeskBand.Create;
      TheDeskBand.Init;
    end;
end;
//------------------------------------------------------------------------------
//  ShutDown
//------------------------------------------------------------------------------
class procedure TPlayerDeskBand.ShutDown;
begin
  if Assigned(TheDeskBand) then
    begin
      Log('  TPlayerDeskBand.ShutDown');
      TheDeskBand.Close;
      TheDeskBand.Free;
      TheDeskBand := nil;
    end;
end;
//------------------------------------------------------------------------------
//                              ININTIALIZATION
//------------------------------------------------------------------------------
initialization
  TPmaClassFactory.RegClass(TPlayerDeskBand);
end.
