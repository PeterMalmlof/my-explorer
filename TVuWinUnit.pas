unit TVuWinUnit;

interface
uses
  Windows, ExtCtrls, Graphics, Controls, Menus, Forms,

  TGenRectWinUnit,    // Base RectWin Class
  TBassPlayerUnit,    // Bass Player Object
  TWoferWinUnit,
  TGenAppPropUnit,    // Application Properties
  TVuBaseUnit;

const
  VuTypeBar       = 1;
  VuTypeAnalyzer  = 2;
  VuType3d        = 3;
  VuTypeFlerp     = 4;
  VuTypeAsteroids = 5;

//------------------------------------------------------------------------------
//  Object
//------------------------------------------------------------------------------
type TVuWin = class(TGenRectWin)
  protected
    objVuObj  : TVuBase;
    objVuType : integer;

    objBitmap : TBitmap;
    objBitmap2 : TBitmap;

    // Pointers to Wofer Objects

    objWoferLeft  : TWoferWin;
    objWoferRight : TWoferWin;

    objTimer : TTimer;

    objWin      : TForm;
    objEditMode : boolean;
    objRgn : HRGN;
    objWinRgn : HRGN;

    objVuLevels : TBassLevelBuf;

    objVuTypeProp : TGenAppPropInt;

    function  GetUid: string; override;

    procedure SetRect    (const Value : TRect);   override;
    procedure SetEnabled (const Value : boolean); override;
    procedure SetState   (const Value : integer); override;

    procedure SetType    (const Value : integer);
    procedure SetDataX   (const Value : integer);
    procedure SetDataY   (const Value : integer);

    procedure OnMyTimer (Sender: TObject);

    class function GetDefaultRect: TRect; override;

    class function IsSizeable: boolean; override;
  public
    constructor Create(
      const C          : TCanvas;
      const Win        : TForm;
      const WoferLeft  : TWoferWin;
      const WoferRight : TWoferWin); reintroduce;

    destructor  Destroy; override;

    procedure Reset;

    procedure Save;      override;
    procedure RefreshUi; override;
    procedure Paint;     override;

    procedure AddMenues (const MainMenu : TPopupMenu);
    procedure OnVuMode (Sender : TObject);
    procedure OnVuType (Sender : TObject);

    property pType     : integer read objVuType   write SetType;
    property pEditMode : boolean read objEditMode write objEditMode;

    property pDataX : integer write SetDataX;
    property pDataY : integer write SetDataY;

end;

implementation

uses
  SysUtils,
  Math,
  Types,

  TGenMenuUnit,       // TGenMenu Class
  TGenGraphicsUnit,
  
  TMediaPlayerResUnit,// Resource Strings
  TVuAnalyzerUnit,
  TVuBarUnit,
  TVu3dUnit,
  TVuFlerpUnit,
  TVuAteroidsUnit,

  TGenLogUnit,        // Log Object
  TGenClassesUnit;    // Class Management

const
  propVuType = 'VuType';


//------------------------------------------------------------------------------
//  Constructor
//------------------------------------------------------------------------------
constructor TVuWin.Create(
      const C          : TCanvas;
      const Win        : TForm;
      const WoferLeft  : TWoferWin;
      const WoferRight : TWoferWin);
begin

  objBitmap := TBitmap.Create;

  inherited Create(C);

  objBitmap2 := TBitmap.Create;

  objRgn    := 0;
  objWinRgn := 0;

  objWin := Win;
  objVuObj := nil;
  objEditMode := false;

  objVisible := true;
  objEnabled := false;

  objTimer := TTimer.Create(nil);
  objTimer.OnTimer  := OnMyTimer;
  objTimer.Interval := 20;
  objTimer.Enabled  := objEnabled;

  // Create the Volume Meter Object

  objVuTypeProp := TGenAppPropInt.Create(
                    GetUid, propVuType, 0) as TGenAppPropInt;
  objVuTypeProp.ForceRead;
  SetType(objVuTypeProp.pInt);

  objWoferLeft  := WoferLeft;
  objWoferRight := WoferRight;

end;
//------------------------------------------------------------------------------
//  Destructor
//------------------------------------------------------------------------------
destructor TVuWin.Destroy;
begin
  objBitmap.Free;
  objBitmap2.Free;

  if (objRgn    <> 0) then DeleteObject(objRgn);
  if (objWinRgn <> 0) then DeleteObject(objWinRgn);

  if Assigned(objVuObj) then objVuObj.Free;

  objTimer.Free;

  objVuTypeProp.Free;

  inherited;
end;
//------------------------------------------------------------------------------
// Get Default Window Position, must be overridden by all Windows
//------------------------------------------------------------------------------
function TVuWin.GetUid:string;
begin
  result := 'VuWin';
end;
//------------------------------------------------------------------------------
// Get Default Window Position, must be overridden by all Windows
//------------------------------------------------------------------------------
class function TVuWin.GetDefaultRect: TRect;
begin
  result := Rect(
    (Screen.Width div 2) + 10,
    120,
    Screen.Width - (Screen.Width div 8),
    (Screen.Height div 2) - 10);
end;
//------------------------------------------------------------------------------
// Return true if Window can be resized
//------------------------------------------------------------------------------
class function TVuWin.IsSizeable: boolean;
begin
  result := true;
end;
//------------------------------------------------------------------------------
//  Set State (Override for special management)
//------------------------------------------------------------------------------
procedure TVuWin.SetState(const Value : integer);
begin
  if (Value = StatePlayAudio) then
    begin
      objEnabled := true;
      objVisible := true;
      objTimer.Enabled := true;
    end
  else
    begin
      objEnabled := false;
      objVisible := false;
      objTimer.Enabled := false;
    end;
end;
//------------------------------------------------------------------------------
//  Refresh User Inter
//------------------------------------------------------------------------------
procedure TVuWin.Save;
begin
  inherited;

  objVuTypeProp.ForceWrite;
end;
//------------------------------------------------------------------------------
//  Set Rect
//------------------------------------------------------------------------------
procedure TVuWin.OnMyTimer (Sender: TObject);
begin
  if objEnabled then
    begin

      self.Paint;
    end;
end;
//------------------------------------------------------------------------------
//  Set Rect
//------------------------------------------------------------------------------
procedure TVuWin.SetEnabled (const Value : boolean);
begin
  objEnabled := Value;
  objVisible := Value;
  objTimer.Enabled := objEnabled;
end;
//------------------------------------------------------------------------------
//  Set Rect
//------------------------------------------------------------------------------
procedure TVuWin.SetType (const Value : integer);
begin
  // Remember New Type

  objVuTypeProp.pInt := Value;
  objVuType := Value;

  // Free any old Vu Meter

  if Assigned(objVuObj) then objVuObj.Free;

  // Create the New Vu Meter

  case Value of
    VuTypeAnalyzer  :
      begin
        objVuObj := TVuAnalyzer.Create(
                objWin, objBitmap.Canvas, objBackColor);
      end;

    VuType3d :
      begin
        objVuObj := TVu3d.Create(objWin,objBitmap.Canvas, objBackColor);
        objVuObj.pType := 2;
        TVu3d(objVuObj).pRotate    := false;
        TVu3d(objVuObj).pFilled    := true;
        TVu3d(objVuObj).pLines     := true;
        TVu3d(objVuObj).pDistance  := 11;
      end;
      
    VuTypeFlerp :
      begin
        objVuObj := TVuFlerp.Create(objWin,objBitmap.Canvas, objBackColor);
      end;

    VuTypeAsteroids :
      begin
        objVuObj := TVuAteroids.Create(objWin,objBitmap.Canvas, objBackColor);
      end;
  else
    objVuObj := TVuBar.Create(objWin,objBitmap.Canvas, objBackColor);
  end;
end;
//------------------------------------------------------------------------------
//  Set Rect
//------------------------------------------------------------------------------
procedure TVuWin.SetDataX (const Value : integer);
begin
  if Assigned(objVuObj) then
    begin
      if Value > 0 then
        begin
          if (objVuObj is TVu3d) then
            TVu3d(objVuObj).pAngle :=
              TVu3d(objVuObj).pAngle + 0.1
          else if (objVuObj is TVuFlerp) then
            TVuFlerp(objVuObj).SetValue(-0.2,0,0)
          else if (objVuObj is TVuBar) then
            TVuBar(objVuObj).SetValue(1,0,0);
        end
      else
        begin
          if (objVuObj is TVu3d) then
            TVu3d(objVuObj).pAngle :=
              TVu3d(objVuObj).pAngle - 0.1
          else if (objVuObj is TVuFlerp) then
            TVuFlerp(objVuObj).SetValue(+0.2,0,0)
          else if (objVuObj is TVuBar) then
            TVuBar(objVuObj).SetValue(-1,0,0);
        end;
    end;
end;
//------------------------------------------------------------------------------
//  Set Rect
//------------------------------------------------------------------------------
procedure TVuWin.SetDataY (const Value : integer);
begin
  if Assigned(objVuObj) then
    begin
      if Value > 0 then
        begin
          if (objVuObj is TVu3d) then
            TVu3d(objVuObj).pElevation :=
              TVu3d(objVuObj).pElevation + 0.1
          else if (objVuObj is TVuFlerp) then
            TVuFlerp(objVuObj).SetValue(0,0.2,0)
          else if (objVuObj is TVuBar) then
            TVuBar(objVuObj).SetValue(0,1,0);
        end
      else
        begin
          if (objVuObj is TVu3d) then
            TVu3d(objVuObj).pElevation :=
              TVu3d(objVuObj).pElevation - 0.1
          else if (objVuObj is TVuFlerp) then
            TVuFlerp(objVuObj).SetValue(0,-0.2,0)
          else if (objVuObj is TVuBar) then
            TVuBar(objVuObj).SetValue(0,-1,0);
        end;
    end;
end;
//------------------------------------------------------------------------------
//  Paint
//------------------------------------------------------------------------------
procedure TVuWin.Reset;
begin
  if Assigned(objVuObj) then
    objVuObj.Reset;
end;
//------------------------------------------------------------------------------
//  Set Rect
//------------------------------------------------------------------------------
procedure TVuWin.SetRect(const Value : TRect);
begin
  objRect := Value;

  objBitmap.Width  := objRect.Right - objRect.Left;
  objBitmap.Height := objRect.Bottom - objRect.Top;

  objMainRect.pRect := objRect;

  RefreshUi;
end;
//------------------------------------------------------------------------------
//  Refresh Ui
//------------------------------------------------------------------------------
procedure TVuWin.RefreshUi;
var
  tmpRgn : HRGN;
begin
  // Dont know yet


  // Get Overlap Region

  if TheWinList.GetOverlapRgn(self, tmpRgn) then
    begin
      if (objWinRgn <> 0) then DeleteObject(objWinRgn);

      // Create this Windows full Rect as a Region

      objWinRgn := Windows.CreateRectRgnIndirect(objRect);

      // Combine the new Region with this

      windows.CombineRgn(objWinRgn, tmpRgn, objWinRgn, RGN_XOR);

      // Remove Overlap Region

      DeleteObject(tmpRgn);
    end
  else
    begin
      // Remove all Regions

      if (objWinRgn <> 0) then DeleteObject(objWinRgn);
      objWinRgn := 0;
    end;
end;
//------------------------------------------------------------------------------
//  Paint
//------------------------------------------------------------------------------
procedure TVuWin.Paint;
var
  Ind           : integer;
  WoferMidLevel : double;
begin
  inherited;

  //objWin.TransparentColor := true;
  //objWin.TransparentColorValue := objBackColor;

  if objVisible and Assigned(objVuObj) and
     (ThePlayer.pState = PlayerStatusPlaying) then
    begin
      if objEditMode then
        objVuObj.pBkgColor := objHighColor
      else
        objVuObj.pBkgColor := objBackColor;

      // Get Meter Data from Player and Paint it

      if ThePlayer.GetSample(objVuLevels) then
        begin
          // objVuObj.Paint(
          //   Rect(objRect.Left + 1, objRect.Top + 1,
          //        objRect.Right - 1, objRect.Bottom - 1),

          // Draw the Volume Meter on objBitmap

          objVuObj.Paint( Rect(0,0, objRect.Right - objRect.Left - 2,
                                    objRect.Bottom - objRect.Top - 2),
          objVuLevels, 80);

          // Get Background Bitmap
          (*
          if TheWinList.GetBackground(self, BitMap) and (BitMap <> nil) then
            begin

              // Set Size to Window Size

              objBitmap2.Width  := objBitMap.Width;
              objBitmap2.Height := objBitMap.Height;

              // Clear this Bitmap to Background Color

              objBitmap2.Canvas.Brush.Style := bsSolid;
              objBitmap2.Canvas.Brush.Color := objBackColor;
              objBitmap2.Canvas.Pen.Style := psClear;
              objBitmap2.Transparent := false;

              //objBitmap2.Canvas.Rectangle(
              //  Rect(0,0, objBitmap2.Width, objBitmap2.Height));

              // Copy the Backgrond to this

              objBitmap2.Canvas.CopyRect(
                Rect(0,0,BitMap.Width,BitMap.Height),
                Bitmap.Canvas,
                Rect(0,0,BitMap.Width,BitMap.Height));

              // Copy Vu Bitmap (objBitmap) to the objBitmap2

              objBitmap.Transparent      := true;
              objBitmap.TransparentMode  := tmFixed;
              objBitmap.TransparentColor := objBackColor;

              // Draw the objBitmap to the objBitmap2

              objBitmap2.Canvas.Draw(0,0, objBitmap);

              // Copy this to Form

              objCanvas.CopyRect(
                Rect(objRect.Left + 1, objRect.Top + 1,
                     objRect.Right - 1, objRect.Bottom - 1),
              objBitmap2.Canvas,
                Rect(1,1, objRect.Right - objRect.Left - 2,
                          objRect.Bottom - objRect.Top - 2));

              // Draw a Red Dot in upper right corder

              objCanvas.Pen.Style := psClear;
              objCanvas.Brush.Color := RGB(255,0,0);
              DrawCircle(objCanvas, objRect.Right - 10,
              objRect.Top + 10, 5);

            end
          else
            begin  *)

          if (objWinRgn <> 0) then
            SelectClipRgn(objCanvas.Handle, objWinRgn);

          objCanvas.CopyRect(
            Rect(objRect.Left + 1, objRect.Top + 1,
                 objRect.Right - 1, objRect.Bottom - 1),
            objBitmap.Canvas,
            Rect(0,0, objRect.Right - objRect.Left - 2,
                                    objRect.Bottom - objRect.Top - 2));

          if (objWinRgn <> 0) then
            begin
              SelectClipRgn(objCanvas.Handle, 0);

              objCanvas.Pen.Style := psClear;
              objCanvas.Brush.Color := RGB(255,0,0);
              DrawCircle(objCanvas, objRect.Right - 10,
              objRect.Top + 10, 5);
            end;
            
          //  end;


          // Forward the Samples to both Wofers

          WoferMidLevel := 0;
          for Ind := 0 to 4 do
            WoferMidLevel := Max(WoferMidLevel,objVuLevels[Ind]);

          // Set Wofer Levels and Pan Level

          objWoferLeft.pLevel  := WoferMidLevel;
          objWoferRight.pLevel := WoferMidLevel;
        end;
    end;

  //objWin.TransparentColor := false;
 // TheWinList.GetBackground(self, BitMap);
end;
//------------------------------------------------------------------------------
//  Add Menues
//------------------------------------------------------------------------------
procedure TVuWin.AddMenues (const MainMenu : TPopupMenu);
var
  pMenu : TGenMenu;
begin
  pMenu := TGenMenu.Create(MainMenu);
  pMenu.Caption := resVuEditMode;
  pMenu.Checked := objEditMode;
  pMenu.OnClick := OnVuMode;
  MainMenu.Items.Add(pMenu);

  pMenu := TGenMenu.Create(MainMenu);
  pMenu.Caption   := resVuUseBar;
  pMenu.Checked   := objVuType = VuTypeBar;
  pMenu.RadioItem := true;
  pMenu.OnClick   := OnVuType;
  pMenu.Tag       := VuTypeBar;
  MainMenu.Items.Add(pMenu);

  pMenu := TGenMenu.Create(MainMenu);
  pMenu.Caption   := resVuUseAnalyzer;
  pMenu.Checked   := objVuType = VuTypeAnalyzer;
  pMenu.RadioItem := true;
  pMenu.OnClick   := OnVuType;
  pMenu.Tag       := VuTypeAnalyzer;
  MainMenu.Items.Add(pMenu);

  pMenu := TGenMenu.Create(MainMenu);
  pMenu.Caption   := resVuUse3d;
  pMenu.Checked   := objVuType = VuType3d;
  pMenu.RadioItem := true;
  pMenu.OnClick   := OnVuType;
  pMenu.Tag       := VuType3d;
  MainMenu.Items.Add(pMenu);

  pMenu := TGenMenu.Create(MainMenu);
  pMenu.Caption   := resVuUseFlerp;
  pMenu.Checked   := objVuType = VuTypeFlerp;
  pMenu.RadioItem := true;
  pMenu.OnClick   := OnVuType;
  pMenu.Tag       := VuTypeFlerp;
  MainMenu.Items.Add(pMenu);

  pMenu := TGenMenu.Create(MainMenu);
  pMenu.Caption   := resVuUseAteroids;
  pMenu.Checked   := objVuType = VuTypeAsteroids;
  pMenu.RadioItem := true;
  pMenu.OnClick   := OnVuType;
  pMenu.Tag       := VuTypeAsteroids;
  MainMenu.Items.Add(pMenu);
end;
//------------------------------------------------------------------------------
//  Menu Handler
//------------------------------------------------------------------------------
procedure TVuWin.OnVuMode (Sender : TObject);
begin
  if (Sender is TMenuItem) then
    self.pEditMode := not self.pEditMode;
end;
//------------------------------------------------------------------------------
//  Menu Handler
//------------------------------------------------------------------------------
procedure TVuWin.OnVuType (Sender : TObject);
begin
  if (Sender is TMenuItem) then
    self.pType := TMenuItem(Sender).Tag;
end;
//------------------------------------------------------------------------------
//                                 INITALIZE
//------------------------------------------------------------------------------
initialization
  FindClass(TVuWin);
end.

 