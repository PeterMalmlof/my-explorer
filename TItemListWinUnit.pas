unit TItemListWinUnit;

interface
uses
  Windows, ExtCtrls, Graphics, Types, Contnrs, Messages, Controls,

  TGenRectWinUnit,    // Base RectWin Class
  TGenAppPropUnit,    // Application Properties
  TTreeItemBaseUnit;

//------------------------------------------------------------------------------
//  MESSAGE IDS
//------------------------------------------------------------------------------
const
  SELECT_ITEM = WM_USER + 221; // A new Item was Selected

//------------------------------------------------------------------------------
//  Object
//------------------------------------------------------------------------------
type TItemListWin = class(TGenRectWin)
  protected
    objList     : TObjectList;       // List of Items
    objBaseItem : TTreeItemBase;     // Base Item

    objBaseIndex : integer;          // First Item shown
    objCurIndex  : integer;          // Current Item Index in objList
    objShowMax   : integer;          // Max number of rows

    objImages : TImageList;          // Pointer to Item Images

    objBaseProp : TGenAppPropString; // Base Folder App Property

    function  GetUid: string; override;

    procedure SetRect    (const Value : TRect);   override;
    procedure SetVisible (const Value : boolean); override;
    procedure SetState   (const Value : integer); override;

    procedure CalcRows;

    function  GetCurItem : TTreeItemBase;

    procedure SetBaseItem   (const pItem : TTreeItemBase);
    procedure SetBaseFolder (const sFolder : string);

    function IsValid(const pItem : TTreeItemBase): boolean;

    class function GetDefaultRect: TRect;   override;
    class function IsSizeable:     boolean; override;
  public
    constructor Create(
      const C : TCanvas;
      const I : TImageList); reintroduce;

    destructor  Destroy; override;

    procedure SelectParent;     // Select Parent of CUrrent Item
    procedure SelectChild;      // Select Children of Current Item
    procedure SelectNext;       // Select Next sibling of Curent Item
    procedure SelectPrev;       // Select Previous sibling of Curent Item
    procedure SelectRandom;     // Select a Random sibling of Curent Item

    procedure SelectNextPage;   // Select Next sibling of Curent Item
    procedure SelectPrevPage;   // Select Previous sibling of Curent Item

    procedure Save;      override;
    procedure RefreshUi; override;
    procedure Paint;     override;

    property  pCurItem : TTreeItemBase read GetCurItem;
end;

implementation

uses
  SysUtils,
  StrUtils,
  Math,
  Forms,

  TGenGraphicsUnit,   // Grapfics Functions
  TGenStrUnit,        // String Functions
  MyFileTimeUnit,     // TFileTime Function
  TGenTextFileUnit,   // Textfile Class

  TMediaPlayerResUnit,// Resource Strings
  TDocFolderUnit,     // Document Folder of some kind
  TItemListUnit,      // TheItems (List of all Items)
  TDocTrackUnit,      // Audio Track
  TDocMediaUnit,      // Video Track
  
  TGenLogUnit,        // Log Object
  TGenClassesUnit;    // Class Management

const
  propBaseFolder = 'BaseFolder';

  TXTSPC  =  4; // Space between Text rows

//------------------------------------------------------------------------------
//  Constructor
//------------------------------------------------------------------------------
constructor TItemListWin.Create(
      const C : TCanvas;
      const I : TImageList);
begin
  inherited Create(C);

  objBaseIndex :=  0;
  objCurIndex  :=  0;
  objShowMax   := 10;

  // Create Children Item List (It don't own the Items)

  objList := TObjectList.Create(false);

  // Start the Base Item List and Set My Documents as Base Item

  TItemList.StartUp(nil);
  SetBaseItem (TheItems.pMyDocItem);

  // If there was a Last Folder Set it as Base Folder instead

  objBaseProp := TGenAppPropString.Create(
                  self.GetUid, propBaseFolder, '');
  objBaseProp.ForceRead;

  if (length(objBaseProp.pString) > 0) then
    SetBaseFolder(objBaseProp.pString);

  objEnabled := true;
  objVisible := true;

  objImages := I;

  CalcRows;
end;
//------------------------------------------------------------------------------
//  Destructor
//------------------------------------------------------------------------------
destructor TItemListWin.Destroy;
begin
  objBaseProp.Free;

  // Free the Children Item List (It will not free the Items)

  objList.Free;

  inherited;
end;
//------------------------------------------------------------------------------
// Get Default Window Position, must be overridden by all Windows
//------------------------------------------------------------------------------
function TItemListWin.GetUid:string;
begin
  result := 'ItemListWin';
end;
//------------------------------------------------------------------------------
// Get Default Window Position, must be overridden by all Windows
//------------------------------------------------------------------------------
class function TItemListWin.GetDefaultRect: TRect;
begin
  result := Rect(
    Screen.Width  div 12,
    Screen.Height div 9,
    Screen.Width  div 2,
    Screen.Height - (Screen.Height div 5));
end;
//------------------------------------------------------------------------------
// Return true if Window can be resized
//------------------------------------------------------------------------------
class function TItemListWin.IsSizeable: boolean;
begin
  result := true;
end;
//------------------------------------------------------------------------------
//  Set State (Override for special management)
//------------------------------------------------------------------------------
procedure TItemListWin.SetState(const Value : integer);
begin
  if (Value = StateView) or (Value = StatePlayAudio) then
    begin
      objEnabled := true;
      objVisible := true;
    end
  else
    begin
      objEnabled := false;
      objVisible := false;
    end;
end;
//------------------------------------------------------------------------------
//  Refresh User Inter
//------------------------------------------------------------------------------
procedure TItemListWin.Save;
begin
  inherited;

  // Save Current Base Folder Name

  objBaseProp.pString := objBaseItem.pPath;
  objBaseProp.ForceWrite;
end;
//------------------------------------------------------------------------------
//  Calculate number of Rows possible
//------------------------------------------------------------------------------
procedure TItemListWin.CalcRows;
var
  H : integer;
begin
  H := objCanvas.TextHeight('X') + TXTSPC;
  if (H > 0) then
    objShowMax := trunc((objRect.Bottom - objRect.Top) / H) - 1
  else
    objShowMax := 10;
end;
//------------------------------------------------------------------------------
//  Refresh User Inter
//------------------------------------------------------------------------------
procedure TItemListWin.SetRect (const Value : TRect);
begin
  inherited;

  CalcRows;
end;
//------------------------------------------------------------------------------
//  Refresh User Inter
//------------------------------------------------------------------------------
procedure TItemListWin.RefreshUi;
begin
  CalcRows;
end;
//------------------------------------------------------------------------------
//  Set Rect
//------------------------------------------------------------------------------
procedure TItemListWin.SetVisible (const Value : boolean);
begin
  objVisible := Value;
  if objVisible then Paint
end;
//------------------------------------------------------------------------------
//  Set Base Item
//------------------------------------------------------------------------------
procedure TItemListWin.SetBaseItem(const pItem : TTreeItemBase);
var
  Ind  : integer;
  Iter : integer;
  pCur : TTreeItemBase;
  pSel : TTreeItemBase;
begin
  if (pItem = nil) then EXIT;

  TheLog.Log('SetBaseItem ' + pItem.pPath);
  
  // Remember Last Selected Item

  if  (objBaseItem <> nil) and
      (objCurIndex >= 0) and
      (objCurIndex < objList.Count) then
    objBaseItem.pSelected := objList[objCurIndex] as TTreeItemBase;

  // Set New Base Item, and Refresh it

  objBaseItem := pItem;
  objBaseItem.RefreshChildren;

  // Clear old Item List

  objList.Clear;
  objBaseIndex := 0;
  objCurIndex  := 0;

  // Get old Selection if any

  pSel := nil;
  pCur := objBaseItem.pSelected;
  if (pCur <> nil) and self.IsValid(pCur) then
    begin
      pSel := pCur;
    end;

  // Add All Valid Items to Items List

  Ind  := 0;
  Iter := 0;
  while objBaseItem.GetNextChild(Iter, pCur) do
    if self.IsValid(pCur) then
      begin
        objList.Add(pCur);

        if (pCur = pSel) then
          objCurIndex := Ind;

        Inc(Ind);
      end;

  // If Current Index is higher than 0, then adjust Base Index

  if (objCurIndex > 0) then
    begin
      if (objCurIndex >= objShowMax) then
        objBaseIndex := objCurIndex - objShowMax + 1;
    end;

  // Tell That the Selection has Changed

  PostMessage(Application.Handle, SELECT_ITEM, 0,0);
end;
//------------------------------------------------------------------------------
//  Set Folder as Base Item
//------------------------------------------------------------------------------
procedure TItemListWin.SetBaseFolder (const sFolder : string);
var
  Iter   : integer;
  pItem  : TTreeItemBase;
  pChild : TTreeItemBase;
  sTmp   : string;
  bGot : boolean;
begin
  // First walk all directories of sDir and Match to My Docuemnts

  Iter := 0;
  while TheItems.pMyDocItem.GetNextChild (Iter, pItem) do
    begin

      if (length(pItem.pPath) > 0) and
         AnsiStartsText(pItem.pPath, sFolder) then
        begin
          //TheLog.Log('Found: ' + pItem.pPath);

          pItem.RefreshChildren;
          sTmp := AnsiRightStr(sFolder, length(sFolder) -
                  length(InDelim(pItem.pPath)));

          while pItem <> nil do
            begin

              bGot := false;
              Iter := 0;
              while pItem.GetNextChild(Iter, pChild) do
                begin

                  if (length(pChild.pName) > 0) and
                      AnsiStartsText(pChild.pName, sTmp) then
                    begin

                      pChild.RefreshChildren;
                      pItem := pChild;
                      bGot  := true;

                      sTmp := AnsiRightStr(sTmp, length(sTmp) -
                          length(InDelim(pItem.pName)));

                      if (length(sTmp) = 0) then
                        begin
                          SetBaseItem(pItem);
                          EXIT;
                        end;

                      BREAK;
                    end;
                end;
              if not bGot then BREAK;
            end;
          BREAK;
        end;
    end;
end;
//------------------------------------------------------------------------------
//  Is This Item a Valid Item to show in media player
//------------------------------------------------------------------------------
function TItemListWin.IsValid(const pItem : TTreeItemBase): boolean;
begin
  // Valid Items is:
  //    TDocFolder:   Any Type of Folder can contain a Media File
  //    TDocMedia:    Any Type if Media File

  result := (pItem <> nil) and
            ( (pItem IS TDocFolder) or (pItem IS TDocMedia) );
end;
//------------------------------------------------------------------------------
//  Get Current Item
//------------------------------------------------------------------------------
function TItemListWin.GetCurItem : TTreeItemBase;
begin
  result := nil;

  if (objList.Count > 0) and
     (objCurIndex >= 0) and (objCurIndex < objList.Count) then
    result := objList[objCurIndex] as TTreeItemBase;
end;
//------------------------------------------------------------------------------
//  Select Parent Item
//------------------------------------------------------------------------------
procedure TItemListWin.SelectParent;
begin
  // Does the Current Base Item have a Parent

  if (objBaseItem <> nil) and
     (objBaseItem.pParent <> nil) and
     (length(objBaseItem.pParent.pPath) > 0) then
    SetBaseItem(objBaseItem.pParent);
end;
//------------------------------------------------------------------------------
//  Select Child Item
//------------------------------------------------------------------------------
procedure TItemListWin.SelectChild;
begin
  // Does Current Base Item has any Children to select, and is the
  // Current Index within limits

  if (objList.Count > 0) and
     (objCurIndex >= 0) and (objCurIndex < objList.Count) and

  // And is this Child a Folder in its turn

      (objList[objCurIndex] Is TDocFolder) then

    SetBaseItem(objList[objCurIndex] as TTreeItemBase);
end;
//------------------------------------------------------------------------------
//  Select Next Item
//------------------------------------------------------------------------------
procedure TItemListWin.SelectNext;
begin
  if (objList.Count > 0) then
    begin
      // Are there still ITems left

      if (objCurIndex < (objList.Count - 1)) then
        begin
          Inc(objCurIndex);

          // Do we need to increment the Base Index

          if (objCurIndex >= (objBaseIndex + objShowMax)) then
            objBaseIndex := objCurIndex - objShowMax + 1;
        end
      else
        begin
          // At End, Select first

          objCurIndex  := 0;
          objBaseIndex := 0;
        end;

      PostMessage(Application.Handle, SELECT_ITEM, 0,0);
    end;
end;
//------------------------------------------------------------------------------
//  Select Next Item
//------------------------------------------------------------------------------
procedure TItemListWin.SelectNextPage;
begin
  if (objList.Count > objShowMax) then
    begin
      // Are there still Items left

      if (objCurIndex < (objList.Count - 1 - objShowMax)) then
        begin
          Inc(objCurIndex, objShowMax);

          // Do we need to increment the Base Index

          if (objCurIndex >= (objBaseIndex + objShowMax)) then
            objBaseIndex := objCurIndex - objShowMax + 1;
        end
      else
        begin
          // At End, Select first

          objCurIndex  := 0;
          objBaseIndex := 0;
        end;

      PostMessage(Application.Handle, SELECT_ITEM, 0,0);
    end
  else
    SelectNext;
end;
//------------------------------------------------------------------------------
//  Select Previous Item
//------------------------------------------------------------------------------
procedure TItemListWin.SelectPrev;
begin
  if (objList.Count > 0) then
    begin
      // Are we above zero index

      if (objCurIndex > 0) then
        begin
          Dec(objCurIndex);

          // Do we need to decrement Base Index

          if (objCurIndex < objBaseIndex) then
            objBaseIndex := objCurIndex;
        end
      else
        begin
          // At End, Select Last

          objCurIndex := objList.Count - 1;

          if (objCurIndex >= (objBaseIndex + objShowMax)) then
            objBaseIndex := objCurIndex - objShowMax + 1;
        end;

      PostMessage(Application.Handle, SELECT_ITEM, 0,0);
    end;
end;
//------------------------------------------------------------------------------
//  Select Previous Item
//------------------------------------------------------------------------------
procedure TItemListWin.SelectPrevPage;
begin
  if (objList.Count > objShowMax) then
    begin
      // Are we above zero index

      if (objCurIndex > objShowMax) then
        begin
          Dec(objCurIndex, objShowMax);

          // Do we need to decrement Base Index

          if (objCurIndex < objBaseIndex) then
            objBaseIndex := objCurIndex;
        end
      else
        begin
          // At End, Select Last

          objCurIndex := objList.Count - 1;

          if (objCurIndex >= (objBaseIndex + objShowMax)) then
            objBaseIndex := objCurIndex - objShowMax + 1;
        end;

      PostMessage(Application.Handle, SELECT_ITEM, 0,0);
    end
  else
    SelectPrev;
end;
//------------------------------------------------------------------------------
//  Select a Random Valid Item
//------------------------------------------------------------------------------
procedure TItemListWin.SelectRandom;
var
  Ind   : integer;
  pItem : TTreeItemBase;
  bThis : boolean;
  Num   : integer;
begin
  if (objList.Count > 0) then
    begin
      bThis := false;
      pItem := nil;
      Ind   := -1;
      Num   := 0;

      // TheLog.Log('');
  
      // Try 100 times to get next Track Randomly

      while (not bThis) and (Num < 100) do
        begin
          // Note Random will return >= 0 ... < Count
      
          Ind := Random(objList.Count);
          // TheLog.Log('Want ' + IntToStr(Ind) +
          //            ' of 0..' + IntToStr(objList.Count-1));

          pItem := objList[Ind] as TTreeItemBase;

          if (pItem IS TDocTrack) and (not TDocTrack(pItem).pPlayed) then
            begin
              // TheLog.Log('Got it ' + IntToStr(Ind));
              bThis := true;
              BREAK;
            end;

          Inc(Num);
        end;

      // If next Track wasnt found, Just Get Next Not Played

      if (not bThis) then
        begin
          for Ind := 0 to objList.Count - 1 do
            begin
              pItem := objList[Ind] as TTreeItemBase;
              if (pItem IS TDocTrack) and (not TDocTrack(pItem).pPlayed) then
                begin
                  // TheLog.Log('Got next not played ' + IntToStr(Ind));
                  bThis := true;
                  BREAK;
                end;
            end;
        end;

      // Still Nothing, Unmark all Items, and just Select Next

      if (not bThis) then
        begin
          for Ind := 0 to objList.Count - 1 do
            begin
              pItem := objList[Ind] as TTreeItemBase;
              if (pItem IS TDocTrack) then
                TDocTrack(pItem).pPlayed := false;
            end;

          // TheLog.Log('Start again ' + IntToStr(Ind));
          self.SelectNext;
          EXIT;
        end;

      // If Track was found, Select It

      if bThis and (pItem <> nil) and (Ind >= 0) and (Ind < objList.Count) then
        begin
          objCurIndex := Ind;

          // Adjust Selection also
      
          if (objCurIndex >= (objBaseIndex + objShowMax)) then
            objBaseIndex := objCurIndex - objShowMax + 1;

          if (objCurIndex < objBaseIndex) then
            objBaseIndex := objCurIndex;

          PostMessage(Application.Handle, SELECT_ITEM, 0,0);
        end;
    end;
end;
//------------------------------------------------------------------------------
//  Paint
//------------------------------------------------------------------------------
procedure TItemListWin.Paint;
const
  LEFTBRD =  4; // Left Border to Base Text
  TOPBRD  =  2; // Top Border to Base Text (row 1)
  ICONBRD = 10; // Left Border to first Icon
  TXTBRD  = 10; // Border beteww Icon and Text
var
  Pos       : TPoint;
  Ind       : integer;
  pItem     : TTreeItemBase;
  TxtHeight : integer;
begin
  inherited;

  if objVisible then
    begin
      Pos.X := objRect.Left + LEFTBRD;
      Pos.Y := objRect.Top  + TOPBRD;

      // Get the Text Height
      
      TxtHeight := objCanvas.TextHeight('X');

      // Draw Base Folder Path

      objCanvas.Brush.Color := objBackColor;
      objCanvas.Brush.Style := bsSolid;

      if (objBaseItem <> nil) then
        DrawTextEll(objCanvas,
          Rect(Pos.X, Pos.Y, objRect.Right, Pos.Y + TxtHeight),
          Pos, false, objBaseItem.pPath)
      else
        DrawTextEll(objCanvas,
          Rect(Pos.X, Pos.Y, objRect.Right, Pos.Y + TxtHeight),
          Pos, false, 'No Base Item');

      Inc(Pos.Y, TxtHeight + TXTSPC);

      // Walk all Children of the Current Item

      if (objList.Count > 0) then
        begin
          for Ind := objBaseIndex to Min(objList.Count - 1,
                                     objBaseIndex + objShowMax - 1) do
            begin
              pItem := objList[Ind] as TTreeItemBase;

              // Draw Image depending on Type

              objImages.Draw(objCanvas,
                Pos.X + ICONBRD,
                Pos.Y + (TxtHeight - objImages.Width) div 2, pItem.pImageId, true);

              // If this item is selected, set High Brush Color

              if (Ind = objCurIndex) then
                begin
                  objCanvas.Brush.Color := objHighColor;
                  objCanvas.Pen.Color   := objForeColor;
                end
              else
                begin
                  objCanvas.Brush.Color := objBackColor;
                end;

              // Draw Text using a userfriendly Item Name

              DrawTextEll(objCanvas,
                Rect(Pos.X, Pos.Y, objRect.Right, Pos.Y + TxtHeight),
                Point(Pos.X + objImages.Width + ICONBRD + TXTBRD, Pos.Y),
                (Ind = objCurIndex),
                pItem.pNiceName);

              // Increment Y

              Inc(Pos.Y, TxtHeight + TXTSPC);
            end;
        end
      else
        begin
          // No Content in Folder

          objCanvas.TextOut(Pos.X, Pos.Y, resNoContent);
        end;
    end;
end;
//------------------------------------------------------------------------------
//                                 INITALIZE
//------------------------------------------------------------------------------
initialization
  FindClass(TItemListWin);
end.


 