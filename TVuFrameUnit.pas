unit TVuFrameUnit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, 
  Dialogs, StdCtrls, ExtCtrls, Menus,    Math,

  TGenAppPropUnit,    // App Properties       (Component)
  TWmMsgFactoryUnit,  // Message Factory      (Component)

  TVuMeterUnit,       // Vu PaintBox          (Component)
  TVuBaseUnit,        // Vu Base Unit
  TBassPlayerUnit,    // Bass Player          (Component)
  TGenButtonUnit;     // Button               (Component)
  
//------------------------------------------------------------------------------
// Vu FRame
//------------------------------------------------------------------------------
type
  TVuFrame = class(TFrame)
    VuMeter    : TVuMeter;
    WinLabel   : TLabel;
    WinVisible : TGenButton;
    procedure FrameResize(Sender: TObject);
    procedure WinVisibleClick(Sender: TObject);
    function  VuMeterCreateMenuItem(const Menu: TPopupMenu): TMenuItem;
    function VuMeterCreatePopupMenu: TPopupMenu;
    procedure Log(Line : string);
  private
    objState      : integer;           // Frame State
    objSubscriber : TWmMsgSubscriber;  // Subscriber

    objHeight   : integer;           // Frame Height

    objVisible  : TGenAppPropBool;   // Frame Visible

  protected

    function  GetHeight    : integer;
    function  GetMinHeight : integer;

    // Message Process called by Message Pump

    procedure ProcessMsg(var Msg : TMsg);

    procedure RefreshSamples(var Sample : array of single);

    // Log to LogFile


    // Frame Message Process

    procedure WndProc(var Message: TMessage); override;
  public
    constructor Create(AOwner: TComponent); override;

    procedure   StartUp;
    procedure   ShutDown;

    property    pHeight    : integer read GetHeight;
    property    pMinHeight : integer read GetMinHeight;

    // Reintroduced Properties

    property TabOrder;
    property TabStop;
  end;

implementation

{$R *.dfm}

uses

  TGenGraphicsUnit,
  TGenPopupMenuUnit,
  TPmaFormUtils,     // Form Utils
  TPmaLogUnit,

  TMainFormUnit,

  TPmaClassesUnit;

const
  BRD       = 4;    // Border Size
  BTNSIZE   = 12;   // Button Size
  DEFHEIGHT = 140;  // Default Window Height

  prefWindow  = 'VuWindow';
  prefVisible = 'On';

  StateOpening = 0; // Frame is opening up
  StateRunning = 2; // Frame is Running
  StateClosing = 3; // Frame is Closing

resourcestring
  resHintFrame     = 'Volume Meter Window';
  resHintFrameDis  = 'Volume Meter Window Hidden';
  resHintVisible   = 'Click to Hide Volume Meter Window';
  resHintInVisible = 'Click to Show Volume Meter Window';

//------------------------------------------------------------------------------
//  Frame Message Pump
//------------------------------------------------------------------------------
procedure TVuFrame.WndProc(var Message: TMessage);
begin
  if (Message.Msg = MSG_STARTUP) then
    begin
      if BOOLEAN(Message.LParam) then
        self.StartUp
      else
        self.ShutDown;
    end
  else
    inherited;
end;
//------------------------------------------------------------------------------
//  Create
//------------------------------------------------------------------------------
constructor TVuFrame.Create(AOwner: TComponent);
begin
  inherited;
  objState := StateOpening;

  objHeight := DEFHEIGHT;

  // Make sure some settings is ok

  self.AutoSize              := false;
  self.AutoScroll            := false;
  self.VertScrollBar.Visible := false;
  self.HorzScrollBar.Visible := false;

  // Read Window Properties

  objVisible := nil;

  // Setup VuMeter Main Menu

  TGenMenuItem.PopupMenuSetup(VuMeter.pPopupMenu);

  objSubscriber := nil;
end;
//------------------------------------------------------------------------------
//  StartUp
//------------------------------------------------------------------------------
procedure TVuFrame.StartUp;
begin
  Log('  ' + self.ClassName + '.StartUp (' + self.Name + ')');

  // Read Window Properties

  objVisible := App.CreatePropBool(prefWindow, prefVisible, true);
  WinVisible.Checked := objVisible.pBool;

  if Assigned(MsgFactory) then
    begin
      // Subscribe on Start Track

      objSubscriber := MsgFactory.Subscribe(self.ClassName, ProcessMsg);
      objSubscriber.AddMessage(MSG_BASS_TRACK_START);
    end;

  // Startup Vu Meter

  VuMeter.StartUp;

  // Set On Sample Callback in Bass Player

  ThePlayer.OnSample := self.RefreshSamples;

  // Set Hint

  if objVisible.pBool then
    begin
      self.Hint       := resHintFrame;
      WinVisible.Hint := resHintVisible;
    end
  else
    begin
      self.Hint       := resHintFrameDis;
      WinVisible.Hint := resHintInVisible;
    end;

  objState := StateRunning;
end;
//------------------------------------------------------------------------------
//  Process a Message
//------------------------------------------------------------------------------
procedure TVuFrame.ProcessMsg(var Msg : TMsg);
begin
  case Msg.message of

    // We need to know when a Track has Started Player to reset VU meter

    MSG_BASS_TRACK_START :
      begin
        VuMeter.Reset;
        VuMeter.Color := self.Color;
      end;
  end;
end;
//------------------------------------------------------------------------------
//  ShutDown
//------------------------------------------------------------------------------
procedure TVuFrame.ShutDown;
begin
  Log('  ' + self.ClassName + '.ShutDown (' + self.Name + ')');

  objState := StateClosing;

  // Desubscribe our Messages

  if Assigned(MsgFactory) then
    MsgFactory.DeSubscribe(objSubscriber);
  objSubscriber := nil;
  
  // ShutDown Vu Meter

  VuMeter.ShutDown;

end;
//------------------------------------------------------------------------------
//  Log
//------------------------------------------------------------------------------
procedure TVuFrame.Log(Line : string);
begin
  if Assigned(PmaLog) then
    PmaLog.Log(Line);
end;
//------------------------------------------------------------------------------
//  Create a Popup Menu
//------------------------------------------------------------------------------
function TVuFrame.VuMeterCreatePopupMenu : TPopupMenu;
begin
  Log('Create a TGenPopupMenu');
  result := TGenPopupMenu.Create(self);

  TGenPopupMenu(result).BackColor := App.pBackColor;
  TGenPopupMenu(result).ForeColor := App.pForeColor;
  TGenPopupMenu(result).HighColor := App.pHighColor;
  TGenPopupMenu(result).Font      := App.pFont;
end;
//------------------------------------------------------------------------------
//  Create Menu Item function that returns TGenMenu items
//------------------------------------------------------------------------------
function TVuFrame.VuMeterCreateMenuItem(const Menu: TPopupMenu): TMenuItem;
begin
  result := TGenMenuItem.Create(Menu);
end;
//------------------------------------------------------------------------------
//  Update all Samples
//------------------------------------------------------------------------------
procedure TVuFrame.RefreshSamples(var Sample : array of single);
begin
  if (objState = StateRunning) and objVisible.pBool and
      Assigned(VuMeter) then
    begin
      VuMeter.UpdateSample(Sample);
    end;
end;
//------------------------------------------------------------------------------
//  Resize
//------------------------------------------------------------------------------
procedure TVuFrame.FrameResize(Sender: TObject);
var
  LabelHgt : integer;
  Pos : integer;
begin
  // Get the Height of Current Label in pixels (must be 2 + CheckBox Height)

  LabelHgt := Max(WinLabel.Height, BTNSIZE + 2);

  // Set Label Text

  WinLabel.Left := BTNSIZE + BRD*2;
  WinLabel.Top  := (LabelHgt - WinLabel.Height) div 2;

  // Set Checkbox position

  Pos := (LabelHgt - BTNSIZE + 1)  div 2;
  SetCtrlSize(WinVisible, Pos, Pos, BTNSIZE, BTNSIZE);

  if Assigned(objVisible) and objVisible.pBool then
    begin
      if (objHeight <> self.Height) then self.Height := objHeight;

      // Vu Things

      if (not VuMeter.Visible) then VuMeter.Visible := true;

      SetCtrlRect(VuMeter, Rect(
        0, LabelHgt + 1,
        self.ClientWidth, self.ClientHeight) );
    end
  else
    begin
      if (objHeight <> self.pMinHeight) then self.Height := self.pMinHeight;

      // Vu Things

      if VuMeter.Visible then VuMeter.Visible := false;
    end;
end;
//------------------------------------------------------------------------------
//  Get real Height
//------------------------------------------------------------------------------
function  TVuFrame.GetHeight: integer;
begin
  if Assigned(objVisible) and objVisible.pBool then
    result := objHeight
  else
    result := self.pMinHeight;
end;
//------------------------------------------------------------------------------
//  Return Minimum Height
//------------------------------------------------------------------------------
function TVuFrame.GetMinHeight: integer;
begin
  result := BRD + WinLabel.Height + 2;
end;
//------------------------------------------------------------------------------
//  User Clicked on Visible Button
//------------------------------------------------------------------------------
procedure TVuFrame.WinVisibleClick(Sender: TObject);
begin
  if (objState = StateRunning) then
    begin
      objVisible.Toggle;

      WinVisible.Checked := objVisible.pBool;
      PostMessage(Application.Handle, MSG_APP_RESIZE_LEFTPANEL, 0,0);

      // Set Hint

      if objVisible.pBool then
        begin
          self.Hint       := resHintFrame;
          WinVisible.Hint := resHintVisible;
        end
      else
        begin
          self.Hint       := resHintFrameDis;
          WinVisible.Hint := resHintInVisible;
        end;
    end;
end;
//------------------------------------------------------------------------------
//                              ININTIALIZATION
//------------------------------------------------------------------------------
initialization
  TPmaClassFactory.RegClass(TVuFrame);
end.
