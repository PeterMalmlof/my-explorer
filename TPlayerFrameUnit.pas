unit TPlayerFrameUnit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, 
  Dialogs, ExtCtrls, Commctrl,

  TWmMsgFactoryUnit,  // Message Factory              (Component)
  TGenButtonUnit,     // Button                       (Component)
  TGenVuMeterUnit,    // Volume Meter                 (Component)
  TBassPlayerUnit,    // Bass Player                  (Component)
  TMasterMixerUnit,   // Master Mixer                 (Component)
  TGenSliderUnit;     // Volume Slider and Track Bar  (Component)


//------------------------------------------------------------------------------
// Player Frame
//------------------------------------------------------------------------------
type
  TPlayerFrame = class(TFrame)
    ButPrev      : TGenButton;
    ButStop      : TGenButton;
    ButPlay      : TGenButton;
    ButNext      : TGenButton;
    ButMute      : TGenButton;
    VuLeft       : TGenVuMeter;
    VuRight      : TGenVuMeter;
    VolumeSlider : TGenSlider;
    TrackBar     : TGenSlider;
    ButShuffle   : TGenButton;
    ButTracking  : TGenButton;

    procedure FrameResize          (Sender: TObject);
    procedure TrackBarDblClick     (Sender: TObject);
    procedure VolumeSliderDblClick (Sender: TObject);
    procedure Log(const Line : string);
  private
    objState      : integer;           // Frame State
    objSubscriber : TWmMsgSubscriber;  // Subscriber

  protected

    // Message Process called by Message Pump

    procedure MsgProc(var Msg : TMsg);

    // Update Volume Meter Levels (Call Back for Bass Player)

    procedure RefreshLevels(out LL, LR : single);

    // Frame Message Process

    procedure WndProc(var Message: TMessage); override;

    procedure StartUp;
    procedure ShutDown;
  public
    constructor Create(AOwner: TComponent);override;

    // Get Current Hight of Frame

    class function GetHeight   : integer;

    // Get Minimum Hight (when minimized) of Frame

    class function GetMinWidth : integer;

    // Reintroduced Properties

    property TabOrder;
    property TabStop;
  end;
//------------------------------------------------------------------------------
//                                 IMPLEMENTATION
//------------------------------------------------------------------------------
implementation

{$R *.dfm}

uses
  TPmaLogUnit,
  TGenAppPropUnit,    // App Properties
  TGenGraphicsUnit,   // Graphic Functions
  TMediaUtilsUnit,    // Media Utilities
  TPmaFormUtils,      // Form Utils
  TMainFormUnit,      // Parent Form (MSG_ID)
  TPmaClassesUnit;    // Generic Classes

const
  BRD     =   4;  // Border between Buttons
  FBRD    =   2;  // Border to Frame
  BTNSIZE =  24;  // Height and Width of all Buttons
  VUSIZE  = 140;  // Volume Meter  Width
  VOLSIZE = 120;  // Volume Slider Width

  StateOpening = 0; // Frame is opening up
  StateRunning = 1; // Frame is Running
  StateClosing = 2; // Frame is Closing

resourcestring
  resHintFrame    = 'Player Control';
  resHintPrev     = 'Play Previous Track';
  resHintStop     = 'Stop Current Track';
  resHintStopDis  = 'Stop Current Track (Disabled, No Track Playing)';
  resHintPlay     = 'Playing: Pause Current Track';
  resHintPause    = 'Paused: Continue Playing Current Track';
  resHintIdle     = 'Idle: Play Selected Track';
  resHintNext     = 'Play Next Track';
  resHintMute     = 'Toggle Master Volume On/Off';
  resHintLeft     = 'Left Channel Level ';
  resHintRight    = 'Right Channel Level ';
  resHintVolume   = 'Player Volume';
  resHintTrackPos = 'Position in Track. Move to Adjust';
  resHintTracking = 'Toggle Tracking On/Off';
  resHintShuffle  = 'Toggle Shuffle On/Off';

//------------------------------------------------------------------------------
//  Frame Message Pump (Used only for StartUp and ShutDown)
//------------------------------------------------------------------------------
procedure TPlayerFrame.WndProc(var Message: TMessage);
begin
  if (Message.Msg = MSG_STARTUP) then
    begin
      if BOOLEAN(Message.LParam) then
        self.StartUp
      else
        self.ShutDown;
    end
  else
    inherited;
end;
//------------------------------------------------------------------------------
//  Create
//------------------------------------------------------------------------------
constructor TPlayerFrame.Create(AOwner: TComponent);
begin
  inherited;

  objState := StateOpening;

  // Make Sure some settings are correct

  self.AutoScroll            := false;
  self.AutoSize              := false;
  self.VertScrollBar.Visible := false;
  self.HorzScrollBar.Visible := false;
  self.ParentColor           := true;
  self.ParentShowHint        := true;
  self.ParentFont            := true;
end;
//------------------------------------------------------------------------------
//  Get Height of Player FRame
//------------------------------------------------------------------------------
procedure TPlayerFrame.StartUp;
begin
  Log('  ' + self.ClassName + '.StartUp (' + self.Name + ')');

  // Initiate Volume Meter Component

  VuLeft.pInfo  := resHintLeft;
  VuRight.pInfo := resHintRight;

  VuLeft.BackColor  := self.Color;
  VuRight.BackColor := self.Color;

  VuLeft.Position  := 0;
  VuRight.Position := 0;

  VuLeft.MaxValue  := 1;
  VuRight.MaxValue := 1;

  VuLeft.Reset;
  VuRight.Reset;

  // Get Left & Right Levels from Bass Player using a Callback

  ThePlayer.OnLevel := self.RefreshLevels;

  // Initiate Volume Slider

  VolumeSlider.CmdId := MSG_BASS_VOLUME;

  // Initiate Track Position Slider

  TrackBar.CmdId := MSG_BASS_TRACK_POS;

  // Initiate all Buttons

  ButPrev.CmdId     := MSG_MP_PLAY_PREV;
  ButStop.CmdId     := MSG_BASS_TRACK_STOP;
  ButPlay.CmdId     := MSG_BASS_TRACK_PLAYPAUSE;
  ButNext.CmdId     := MSG_MP_PLAY_NEXT;
  ButMute.CmdId     := MSG_MIX_MASTER_MUTE;
  ButShuffle.CmdId  := MSG_MP_SHUFFLE;
  ButTracking.CmdId := MSG_APP_TRACKING;

  // Setup the Message Pump Processing

  if Assigned(MsgFactory) then
    begin
      objSubscriber := MsgFactory.Subscribe(self.ClassName, MsgProc);

      // Subscribe on Bass Player State Changes

      objSubscriber.AddMessage(MSG_BASS_STATE);
    end;

  // Set Hints on Frame and all Controls

  self.Hint := resHintFrame;

  ButPrev.Hint := resHintPrev;
  ButStop.Hint := resHintStop;
  ButPlay.Hint := resHintPlay;
  ButNext.Hint := resHintNext;
  ButMute.Hint := resHintMute;

  VuLeft.Hint   := resHintLeft;
  VuRight.Hint  := resHintRight;

  VolumeSlider.Hint := resHintVolume;
  TrackBar.Hint     := resHintTrackPos;
  ButTracking.Hint  := resHintTracking;
  ButShuffle.Hint   := resHintShuffle;

  // The TrackBar needs some extra Properties

  TrackBar.DelayTime   := 400; // Nr of mS before new Position is used
  TrackBar.MinPosition := 0;   // Track Start at 0 Sec

  // Now Frame is Running

  objState := StateRunning;

  // Designer Notes:
  //  ParentColor should be set to true on all Child Controls
  //  ShowParentHint should be set to true on all Child Controls
  //  Set TabOrder and Set TabStop = true on all Buttons
  //  Transparent should be set on all TGenButton
  //  Set OverType of otAll on all TGenButton
  //  No Extra Images on Buttons, except Play, Mute, Shuffle and Tracking

end;
//------------------------------------------------------------------------------
//  ShutDown
//------------------------------------------------------------------------------
procedure TPlayerFrame.ShutDown;
begin
  Log('  ' + self.ClassName + '.ShutDown (' + self.Name + ')');

  objState := StateClosing;

  // Unsubscribe all Messages in Message Pump

  if Assigned(objSubscriber) then
    MsgFactory.DeSubscribe(objSubscriber);

  // ShutDown all Components

  StartupAllComps(self, false);
end;
//------------------------------------------------------------------------------
//  Log
//------------------------------------------------------------------------------
procedure TPlayerFrame.Log(const Line : string);
begin
  if Assigned(PmaLog) then
    PmaLog.Log(Line);
end;
//------------------------------------------------------------------------------
//  Message Pump Processing
//------------------------------------------------------------------------------
procedure TPlayerFrame.MsgProc(var Msg : TMsg);
begin
 if (objState = StateRunning) then
    begin
      // Manage Player State Changes

      case Msg.message of

        // Enable/Disable Buttons and Sliders when Players State has changed

        MSG_BASS_STATE : if (Msg.WParam = WP_CMD_CHANGED) then
          begin
            case Msg.LParam of
              Ord(bsIdle) :
                begin
                  ButStop.Enabled := false;
                  ButPlay.Checked := false;
                  ButPlay.Hint    := resHintIdle;
                  ButStop.Hint    := resHintStopDis;
                  TrackBar.Used   := false;
                end;

              Ord(bsPlaying) :
                begin
                  ButStop.Enabled := true;
                  ButPlay.Checked := false;
                  ButPlay.Hint    := resHintPlay;
                  ButStop.Hint    := resHintStop;
                  TrackBar.Used   := true;
                end;

              Ord(bsPaused) :
                begin
                  ButStop.Enabled := true;
                  ButPlay.Checked := true;
                  ButPlay.Hint    := resHintPause;
                  ButStop.Hint    := resHintStop;
                  TrackBar.Used   := true;
                end;
            end;{case}
        end;
      end;{case msg}
  end;{running}
end;
//------------------------------------------------------------------------------
//  New Levels on Volume Meter
//------------------------------------------------------------------------------
procedure TPlayerFrame.RefreshLevels(out LL, LR : single);
begin
  if (objState = StateRunning) then
    begin
      VULeft.Position  := LL;
      VURight.Position := LR;
    end;
end;
//------------------------------------------------------------------------------
//  User has DoubleClicked TrackBar (Need to override default behaviour)
//------------------------------------------------------------------------------
procedure TPlayerFrame.TrackBarDblClick(Sender: TObject);
begin
  TrackBar.Enabled := true;
end;
//------------------------------------------------------------------------------
//  User has Double Clicked the Volume (Need to override default behaviour)
//------------------------------------------------------------------------------
procedure TPlayerFrame.VolumeSliderDblClick(Sender: TObject);
begin
  VolumeSlider.Used := true;
end;
//------------------------------------------------------------------------------
//  Resize Frame
//------------------------------------------------------------------------------
procedure TPlayerFrame.FrameResize(Sender: TObject);
var
  P : integer;
begin
  // Buttons from Left

  P := BRD;

  SetCtrlRect(ButPrev, Rect(P, FBRD div 2, P + BTNSIZE, BRD + BTNSIZE));
  Inc(P, BRD + BTNSIZE);

  SetCtrlRect(ButStop, Rect(P, FBRD, P + BTNSIZE, BRD + BTNSIZE));
  Inc(P, BRD + BTNSIZE);

  SetCtrlRect(ButPlay, Rect(P, FBRD, P + BTNSIZE, BRD + BTNSIZE));
  Inc(P, BRD + BTNSIZE);

  SetCtrlRect(ButNext, Rect(P, FBRD, P + BTNSIZE, BRD + BTNSIZE));
  Inc(P, BRD + BTNSIZE);

  SetCtrlRect(ButMute, Rect(P, FBRD, P + BTNSIZE, BRD + BTNSIZE));
  Inc(P, BRD + BTNSIZE);

  SetCtrlRect(VuLeft,  Rect(P, FBRD     , P + VUSIZE, FBRD + 12));
  SetCtrlRect(VuRight, Rect(P, FBRD + 12, P + VUSIZE, FBRD + BTNSIZE));
  Inc(P, BRD + VUSIZE);
  
  SetCtrlRect(VolumeSlider, Rect(P, FBRD, P + VOLSIZE, BRD + BTNSIZE));
  Inc(P, BRD + VOLSIZE);

  SetCtrlRect(TrackBar,
    Rect(P, FBRD, self.ClientWidth - 3 * BRD - 2 * BTNSIZE, BRD + BTNSIZE));

  // Buttons from Right

  P := self.ClientWidth - BRD - BTNSIZE;
  SetCtrlRect(ButShuffle, Rect(P, FBRD, P + BTNSIZE, BRD + BTNSIZE));

  Dec(P, BRD + BTNSIZE);
  SetCtrlRect(ButTracking, Rect(P, FBRD, P + BTNSIZE, BRD + BTNSIZE));

end;
//------------------------------------------------------------------------------
//  Get Height of Player Frame
//------------------------------------------------------------------------------
class function TPlayerFrame.GetHeight: integer;
begin
  result := FBRD + BTNSIZE;
end;
//------------------------------------------------------------------------------
//  Get Height of Player Frame
//------------------------------------------------------------------------------
class function TPlayerFrame.GetMinWidth: integer;
begin
  result := BRD * 2 + BTNSIZE * 12 + VOLSIZE + VUSIZE;
end;
//------------------------------------------------------------------------------
//                                    INITIALIZE
//------------------------------------------------------------------------------
initialization
  TPmaClassFactory.RegClass(TPlayerFrame);
end.
