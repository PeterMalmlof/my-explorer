program MyExplorer;

uses
  Forms,
  Windows,
  TMainFormUnit in 'TMainFormUnit.pas' {PlayerMainForm},
  TPlayerFrameUnit in 'TPlayerFrameUnit.pas' {PlayerFrame: TFrame},
  TLogFrameUnit in 'TLogFrameUnit.pas' {LogFrame: TFrame},
  TMixerFrameUnit in 'TMixerFrameUnit.pas' {MixerFrame: TFrame},
  TVuFrameUnit in 'TVuFrameUnit.pas' {VuFrame: TFrame},
  TWmMsgFactoryUnit in '..\PmaComp\TWmMsgFactoryUnit.pas',
  TMediaVideoLinkUnit in '..\PmaComp\TMediaVideoLinkUnit.pas',
  TMasterMixerUnit in '..\PmaComp\TMasterMixerUnit.pas',
  TMediaAlbumUnit in '..\PmaComp\TMediaAlbumUnit.pas',
  TMediaArtistLinkUnit in '..\PmaComp\TMediaArtistLinkUnit.pas',
  TMediaArtistUnit in '..\PmaComp\TMediaArtistUnit.pas',
  TMediaBaseUnit in '..\PmaComp\TMediaBaseUnit.pas',
  TMediaFactoryUnit in '..\PmaComp\TMediaFactoryUnit.pas',
  TMediaPlayListLinkUnit in '..\PmaComp\TMediaPlayListLinkUnit.pas',
  TMediaPlayListUnit in '..\PmaComp\TMediaPlayListUnit.pas',
  TMediaPropUnit in '..\PmaComp\TMediaPropUnit.pas',
  TMediaTestThreadUnit in '..\PmaComp\TMediaTestThreadUnit.pas',
  TMediaTrackUnit in '..\PmaComp\TMediaTrackUnit.pas',
  TMediaUtilsUnit in '..\PmaComp\TMediaUtilsUnit.pas',
  DetailFrameUnit in 'DetailFrameUnit.pas' {DetailFrame: TFrame},
  TMediaMovieUnit in '..\PmaComp\TMediaMovieUnit.pas',
  TMediaContainerUnit in '..\PmaComp\TMediaContainerUnit.pas',
  TMediaLinkUnit in '..\pmacomp\TMediaLinkUnit.pas',
  TMediaItemUnit in '..\PmaComp\TMediaItemUnit.pas',
  TMediaDescUnit in '..\PmaComp\TMediaDescUnit.pas',
  TMediaInfoUnit in '..\PmaComp\TMediaInfoUnit.pas',
  TMediaInfoFrameUnit in 'TMediaInfoFrameUnit.pas' {MediaInfoFrame: TFrame},
  TMediaFileUnit in '..\PmaComp\TMediaFileUnit.pas',
  TMediaStorageUnit in '..\PmaComp\TMediaStorageUnit.pas',
  TBaseObjectUnit in '..\PmaComp\TBaseObjectUnit.pas',
  TMediaDataBaseUnit in '..\PmaComp\TMediaDataBaseUnit.pas',
  TMediaPlayerUnit in '..\PmaComp\TMediaPlayerUnit.pas',
  TMediaTaggerUnit in '..\PmaComp\TMediaTaggerUnit.pas',
  TMediaTagsThreadUnit in '..\PmaComp\TMediaTagsThreadUnit.pas',
  TMediaSoundAsThreadUnit in '..\PmaComp\TMediaSoundAsThreadUnit.pas',
  TMediaMonThreadUnit in '..\PmaComp\TMediaMonThreadUnit.pas',
  TGenToolTipUnit in '..\TGenUnits\TGenToolTipUnit.pas',
  TVuMeterUnit in '..\PmaComp\TVuMeterUnit.pas',
  TBassPlayerUnit in '..\PmaComp\TBassPlayerUnit.pas',
  TBkgThreadQueueUnit in '..\PmaComp\TBkgThreadQueueUnit.pas',
  TGenButtonUnit in '..\PmaComp\TGenButtonUnit.pas',
  TGenFileSystemUnit in '..\PmaComp\TGenFileSystemUnit.pas',
  TGenLightUnit in '..\PmaComp\TGenLightUnit.pas',
  TGenListViewUnit in '..\PmaComp\TGenListViewUnit.pas',
  TGenMemoUnit in '..\PmaComp\TGenMemoUnit.pas',
  TGenPopupMenuUnit in '..\PmaComp\TGenPopupMenuUnit.pas',
  TGenProgressBarUnit in '..\PmaComp\TGenProgressBarUnit.pas',
  TGenRemoteUnit in '..\PmaComp\TGenRemoteUnit.pas',
  TGenScrollBarUnit in '..\PmaComp\TGenScrollBarUnit.pas',
  TGenSliderUnit in '..\PmaComp\TGenSliderUnit.pas',
  TGenSplitterUnit in '..\PmaComp\TGenSplitterUnit.pas',
  TGenVuMeterUnit in '..\PmaComp\TGenVuMeterUnit.pas',
  TMediaListViewUnit in '..\PmaComp\TMediaListViewUnit.pas',
  TMediaTreeViewUnit in '..\PmaComp\TMediaTreeViewUnit.pas',
  TPmaLogUnit in '..\PmaComp\TPmaLogUnit.pas',
  TVolumeMeterUnit in '..\PmaComp\TVolumeMeterUnit.pas',
  TVuAnalyzerUnit in '..\PmaComp\TVuAnalyzerUnit.pas',
  TVuAteroidsUnit in '..\PmaComp\TVuAteroidsUnit.pas',
  TVuBarUnit in '..\PmaComp\TVuBarUnit.pas',
  TVuBaseUnit in '..\PmaComp\TVuBaseUnit.pas',
  TVuBassSolUnit in '..\PmaComp\TVuBassSolUnit.pas',
  TVuFlerpUnit in '..\PmaComp\TVuFlerpUnit.pas',
  TVuGraphicsUnit in '..\PmaComp\TVuGraphicsUnit.pas',
  TGenHintManagerUnit in '..\PmaComp\TGenHintManagerUnit.pas',
  TPlayerDeskBandUnit in 'TPlayerDeskBandUnit.pas',
  TPlayerKeyBoardUnit in 'TPlayerKeyBoardUnit.pas',
  TStateFrameUnit in 'TStateFrameUnit.pas' {StateFrame: TFrame},
  TPmaClassesUnit in '..\PmaComp\TPmaClassesUnit.pas',
  TGenColorPickUnit in '..\PmaComp\TGenColorPickUnit.pas' {GenColorPick},
  TGenFileUtils in '..\PmaComp\TGenFileUtils.pas',
  TGenGraphicsUnit in '..\PmaComp\TGenGraphicsUnit.pas',
  TGenHSVUnit in '..\PmaComp\TGenHsvUnit.pas',
  TGenPickFolderUnit in '..\PmaComp\TGenPickFolderUnit.pas',
  TGenPickFontUnit in '..\PmaComp\TGenPickFontUnit.pas' {GenPickFont},
  TGenStrUnit in '..\PmaComp\TGenStrUnit.pas',
  TGenTreeViewUnit in '..\PmaComp\TGenTreeViewUnit.pas',
  TPmaDateTimeUnit in '..\PmaComp\TPmaDateTimeUnit.pas',
  TPmaFormUtils in '..\PmaComp\TPmaFormUtils.pas',
  TPmaListViewUnit in '..\PmaComp\TPmaListViewUnit.pas',
  TPmaTimerUnit in '..\PmaComp\TPmaTimerUnit.pas',
  TPmaExceptionUnit in '..\PmaComp\TPmaExceptionUnit.pas',
  TMyExceptionDlgUnit in 'TMyExceptionDlgUnit.pas' {MyExceptionDlg},
  TPmaDebugBaseUnit in '..\PmaComp\TPmaDebugBaseUnit.pas';

{$R *.res}

var
  Sem  : THandle;
begin

  Sem := Windows.CreateSemaphore(nil, 0, 1, 'TPlayerApplication');
  if ((Sem <> 0) and (GetLastError = ERROR_ALREADY_EXISTS)) then
    begin
      Windows.MessageBeep(MB_ICONHAND);
      Halt;
    end;

  Application.Initialize;
  Application.Title := 'Player';
  Application.CreateForm(TPlayerMainForm, PlayerMainForm);
  Application.Run;
end.
