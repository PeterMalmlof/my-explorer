unit TPlayerUiUnit;

interface

uses
  Windows, Classes, Contnrs, ComCtrls, ExtCtrls, Messages,

  TGenLogUnit,        // Logging
  TWmMsgFactoryUnit,  // Message Factory
  TMediaBaseUnit,
  TMediaPlaylistUnit, // PlayList
  TMediaArtistUnit,    // Track Item
  TMediaAlbumUnit,    // Track Item
  TMediaTrackUnit,    // Track Item
  TGenAppPropUnit,    // Application Properties
  TMediaFactoryUnit;  // Item Base Class

//------------------------------------------------------------------------------
//  Player Ui is a FrontEnd for the MediaFactory
//------------------------------------------------------------------------------
type TPlayerUi = Class(TObject)
  protected
    objState : integer;

    objList : TObjectList; // List of all Tracks in Playlist
    objPrev : TObjectList; // List of all Played Tracks Played

    // Current Track in Playlist

    objShuffle     : boolean;
    objPlayerState : integer;
    objMsgPump     : TWmMsgFactory;
    objMe          : TWmMsgSubscriber; // Subscriber

    objContainer : TMediaBase;
    objCurTrack  : TMediaTrack;

    procedure MsgPumpProc(var Msg : TMsg);

    // Add Children Items (if Track) recursivly

    // procedure AddChildren(const pItem : TTreeItemBase);

  public
    constructor Create(const MsgPump : TWmMsgFactory);
    destructor  Destroy; override;

    // Startup and Shutdown ThePlaylist

    class procedure StartUp(const MsgPump : TWmMsgFactory);
    class procedure Shutdown;

    // Play this Track


    // Return Currently selected Track

    property pSelected : TMediaTrack read objCurTrack;

end;

var ThePlayList : TPlayerUi = nil;

implementation

uses
  Forms,
  SysUtils,
  
  TMyMessageUnit,   // Specif Messages
  TGenClassesUnit;  // Class Management

const
  StateOpening = 0;
  StateRunning = 1;
  StateClosing = 2;

//------------------------------------------------------------------------------
// Create
//------------------------------------------------------------------------------
constructor TPlayerUi.Create(const MsgPump : TWmMsgFactory);
begin
  inherited Create;

  objMsgPump := MsgPump;

  objState := StateOpening;

  objCurTrack := nil;

  objList := TObjectList.Create(false);
  objPrev := TObjectList.Create(false);

  objShuffle := false;

  objState := StateRunning;

  // Setup the Message Pump Proc

  objMe := objMsgPump.Subscribe(self.ClassName, MsgPumpProc);
  objMe.AddMessage(MSG_PLAYER_SHUFFLE);
  objMe.AddMessage(MSG_PLAYER_STATE_CHANGE);
  objMe.AddMessage(MSG_PLAYER_TRACK_ENDED);
end;
//------------------------------------------------------------------------------
// Destroy
//------------------------------------------------------------------------------
destructor TPlayerUi.Destroy;
begin
  objState := StateClosing;

  objMsgPump.DeSubscribe(objMe);

  objList.Free;
  objPrev.Free;

  inherited;
end;
//------------------------------------------------------------------------------
// Handle Messages
//------------------------------------------------------------------------------
procedure TPlayerUi.MsgPumpProc(var Msg : TMsg);
begin
  if (objState = StateRunning) then
  case Msg.message of

    MSG_PLAYER_SHUFFLE : if (Msg.WParam = WP_CMD_CHANGED) then
      objShuffle := BOOLEAN(MSg.LParam);

    MSG_PLAYER_STATE_CHANGE : if (Msg.WParam = WP_CMD_CHANGED) then
      objPlayerState := INTEGER(MSg.LParam);


  end;
end;
//------------------------------------------------------------------------------
//
//                              STARTUP & SHUTDOWN
//
//------------------------------------------------------------------------------
// Startup The Player User Interface
//------------------------------------------------------------------------------
class procedure TPlayerUi.Startup(const MsgPump : TWmMsgFactory);
begin
  if (ThePlayList = nil) then
    begin
      ThePlayList := TPlayerUi.Create(MsgPump);
    end;
end;
//------------------------------------------------------------------------------
// ShutDown The Player User Interface
//------------------------------------------------------------------------------
class procedure TPlayerUi.ShutDown;
begin
  if (ThePlayList <> nil) then
    begin
      ThePlayList.Free;
      ThePlayList := nil;
    end;
end;
//------------------------------------------------------------------------------
//                                    INITIALIZE
//------------------------------------------------------------------------------
initialization
  RegClass(TPlayerUi);
end.
