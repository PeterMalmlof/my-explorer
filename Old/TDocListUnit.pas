//ds----------------------------------------------------------------------------
//  Document List is a Folder and contains other Documents
//
//
//
//
//de----------------------------------------------------------------------------
unit TDocListUnit;

interface

uses
  Windows, Classes, Contnrs, ComCtrls, Menus,

  TGenLogUnit,        // Log�Object
  TDocFolderUnit,    // Base Class
  TTreeItemBaseUnit; // Item Base Class

//------------------------------------------------------------------------------
//  Tree Item Base Object
//------------------------------------------------------------------------------
type TDocList = Class(TDocFolder)
  protected

    procedure AddNewChild (Sender: TObject); override;

    function GetPath   : string; override;

  public
    constructor Create(
          const pParent : TTreeItemBase); reintroduce;

    destructor  Destroy; override;

    class function ChildrenClass: TItemClass; override;
    class function GetTypeStr: string;  override;
    class function GetImageId: integer; override;

    procedure Save; override;
    procedure Load; override;

    procedure AddTreeMenuItems(const PopupMenu : TPopupMenu); override;

    // Walk all Documents in Document list and rebuild their Media Content

    procedure MediaDbRebuild
      (var Esc : boolean; var Count, Loop : integer); override;
end;

//------------------------------------------------------------------------------
//                                  IMPLEMENTATION
//------------------------------------------------------------------------------
implementation

uses
  SysUtils,
  StrUtils,
  Forms,

  TGenStrUnit,
  TGenTextFileUnit,   // Textfile Class
  MyFileTimeUnit,     // TFileTime Functions
  TGenMenuUnit,       // TGenMenu Class
  TGenTimerUnit,

  TMyMessageUnit,     // App Messages
  TMediaPlayerResUnit,// Resource Strings
  TDocGrpUnit,        // Document Group Class
  TGenClassesUnit;    // Class Management

const
  PropFileName = 'My Ducuments.txt';

//------------------------------------------------------------------------------
// Create
//------------------------------------------------------------------------------
constructor TDocList.Create(
      const pParent : TTreeItemBase);
begin
  inherited Create(pParent, resDocListName,
          0, 0, MyFileTimeZero, MyFileTimeZero, false, false);
          
  // Load Document Groups

  self.Load;
end;
//------------------------------------------------------------------------------
// Destroy
//------------------------------------------------------------------------------
destructor TDocList.Destroy;
begin
  if self.pDirty then self.Save;

  inherited;
end;
//------------------------------------------------------------------------------
// Return What Class Items Childrens are
//------------------------------------------------------------------------------
class function TDocList.ChildrenClass: TItemClass;
begin
  result := TDocGrp;
end;
//------------------------------------------------------------------------------
// Get Path
//------------------------------------------------------------------------------
function TDocList.GetPath:string;
begin
  result := self.pName;
end;
//------------------------------------------------------------------------------
// Get Type String
//------------------------------------------------------------------------------
class function TDocList.GetTypeStr: string;
begin
  result := resDocListType;
end;
//------------------------------------------------------------------------------
// Return What Class Items Childrens are
//------------------------------------------------------------------------------
class function TDocList.GetImageId:integer;
begin
  result := ImageIdFolder;
end;
//------------------------------------------------------------------------------
// Save This Item
//------------------------------------------------------------------------------
procedure TDocList.Save;
var
  TF : TGenTextFile;
begin
  EXIT;
  // Open a File to save things in

  TF := TGenTextFile.CreateForWrite(ApplicationFile(PropFileName));
  objList.SaveToFile(TF);
  TF.Free;
end;
//------------------------------------------------------------------------------
// Load This Item
//------------------------------------------------------------------------------
procedure TDocList.Load;
var
  pFile         : TGenTextFile;
  sProp, sValue : string;
begin
  // Open a File to read all Application Groups

  pFile := TGenTextFile.CreateForRead(ApplicationFile(PropFileName));

  // Walk all Lines and Create Objects

  while pFile.ReadProp(sProp, sValue) do
    begin
      // Is this the Start of a new Document Group

      if AnsiSameText(PropDocGrp, sProp) then
        begin
          // Create a New Document Group

          objList.AddItem(TDocGrp.CreateFromFile(self, pFile));
        end;
    end;
      
  pFile.Free;

  // TheLOg.Log('My Documents Created ' + IntToStr(self.GetChildCount));
end;
//------------------------------------------------------------------------------
// Add Menus accoding to Current Selected Item in TreeView
//------------------------------------------------------------------------------
procedure TDocList.AddTreeMenuItems(const PopupMenu : TPopupMenu);
var
  pMenu : TGenMenu;
begin
  inherited AddTreeMenuItems(PopupMenu);

  // Add New Child

  pMenu := TGenMenu.Create(PopupMenu);
  pMenu.Caption := resAdd + SPC + resDocGrpType;
  pMenu.OnClick := AddNewChild;
  PopupMenu.Items.Add(pMenu);

end;
//------------------------------------------------------------------------------
// Menu: Add a Child Item
//------------------------------------------------------------------------------
procedure TDocList.AddNewChild(Sender: TObject);
begin
  objList.AddItem(TDocGrp.Create(
    self, resNew + SPC + resDocGrpType,
    0, 0, MyFileTimeZero, MyFileTimeZero, false, false));
  self.pDirty := true;

  //PostMessage(Application.Handle, MSG_TREEVIEW_REFRESH, 0, 0);
  //PostMessage(Application.Handle, MSG_LISTVIEW_REFRESH, 0, 0);
end;
//------------------------------------------------------------------------------
// Update all Media Tracks on All Document Groups
//------------------------------------------------------------------------------
procedure TDocList.MediaDbRebuild
  (var Esc : boolean; var Count,Loop : integer);
var
  Iter  : integer;
  pItem : TTreeItemBase;
begin
  TheLog.Log('Rebuild MediaDb on: ' + self.pPath);

  Iter := 0;
  while self.GetNextChild(Iter, pItem) do
    if (pItem is TDocGrp) then
      begin
        TheLog.Log('Rebuild MediaDb on: ' + pItem.pPath);
        pItem.MediaDbRebuild(Esc, Count, Loop);
        if Esc then BREAK;
      end;
end;
//------------------------------------------------------------------------------
//                                    INITIALIZE
//------------------------------------------------------------------------------
initialization
  RegClass(TDocList);
end.

