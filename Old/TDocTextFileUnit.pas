unit TDocTextFileUnit;

interface

uses
  Windows, Classes, Contnrs, ComCtrls,

  TGenLogUnit,        // Log Object
  TGenTextFileUnit,   // TextFile Class  
  TDocFileUnit,       // Item Base Class
  TTreeItemBaseUnit;  // Item Base Class

//------------------------------------------------------------------------------
//  Docuemnt Item
//------------------------------------------------------------------------------
type TDocTextFile = Class(TDocFile)
  protected

  public
    class function  GetTypeStr: string; override;

    procedure Execute(const Col : integer); override;
end;

//------------------------------------------------------------------------------
//                                  IMPLEMENTATION
//------------------------------------------------------------------------------
implementation

uses
  SysUtils,
  StrUtils,
  Forms,

  TMyMessageUnit,     // App Messages
  TMediaPlayerResUnit,// Resource Strings
  
  TGenClassesUnit;    // Class Management

//------------------------------------------------------------------------------
// Get Type String with ReadOnly and Hidden Flgs attached
//------------------------------------------------------------------------------
class function TDocTextFile.GetTypeStr: string;
begin
  result := resDocTextFileType;
end;
//------------------------------------------------------------------------------
//  User Double Clicked
//------------------------------------------------------------------------------
procedure TDocTextFile.Execute(const Col : integer);
begin
  PostMessage(Application.Handle, MSG_LISTVIEW_OPEN_TEXTFILE, 0,0);
end;
//------------------------------------------------------------------------------
//                                    INITIALIZE
//------------------------------------------------------------------------------
initialization
  RegClass(TDocTextFile);
end.

