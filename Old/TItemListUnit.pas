//ds----------------------------------------------------------------------------
//  TItemList is the container of all Programs, Extensions, Verbs, and
//  My Documents.
//
//  Program   List
//  Extension List
//  Verb      List
//  Document  List
//de----------------------------------------------------------------------------
unit TItemListUnit;

interface

uses
  Windows, Classes, Contnrs, ComCtrls, ExtCtrls,

  TGenLogUnit,       // Log Object
  TDocListUnit,      // My Documents
  TWmMsgFactoryUnit, // Message Pump
  TMediaFactoryUnit, // Media Factory
  TTreeItemBaseUnit;

//------------------------------------------------------------------------------
//  Tree Item List Object
//------------------------------------------------------------------------------
type TItemList = Class(TItemListBase)
  private
    objMyProgList  : TTreeItemBase;
    objMyExtList   : TTreeItemBase;
    objMyVerbList  : TTreeItemBase;
    objMyDocuments : TDocList;

    objMe      : TWmMsgSubscriber; // Subscriber
    objMsgPump : TWmMsgFactory;    // Message Pump
    objEscaped : boolean;
    objCounter : integer;
    objTimer   : TTimer;

    procedure OnMyTimer(Sender: TObject);
    procedure MsgPumpProc(var Msg : TMsg);
  public
    constructor Create;
    destructor  Destroy; override;

    function FindProgFromLoadedUID(const Id : Cardinal): TTreeItemBase;
    function FindVerbFromLoadedUID(const Id : Cardinal): TTreeItemBase;
    function FindExtFromLoadedUID (const Id : Cardinal): TTreeItemBase;

    function FindProgFromUID(const Id : Cardinal): TTreeItemBase;
    function FindVerbFromUID(const Id : Cardinal): TTreeItemBase;
    function FindExtFromUID (const Id : Cardinal): TTreeItemBase;

    function IsProg(const Item : TTreeItemBase): boolean;
    function IsVerb(const Item : TTreeItemBase): boolean;
    function IsExt (const Item : TTreeItemBase): boolean;

    function GetDefProg(
      const sExt  : string;
      const sVerb : string):TTreeItemBase;

    function GetNextVerb(
      var Iter  : integer;
      out pItem : TTreeItemBase)
                : boolean;

    function GetNextExt(
      var Iter  : integer;
      out pItem : TTreeItemBase)
                : boolean;

    procedure AddListViewExtCoup(
      const pExt     : TTreeItemBase;
      const ListView : TListView);

    procedure AddListViewVerbCoup(
      const pVerb    : TTreeItemBase;
      const ListView : TListView);

    // Rebuild Media Database from scratch

    procedure MediaDbRebuild;

    class procedure StartUp(const MsgPump : TWmMsgFactory);
    class procedure ShutDown;

    property pMyDocItem : TDocList read objMyDocuments;
end;

//------------------------------------------------------------------------------
//  Singleton
//------------------------------------------------------------------------------
var TheItems : TItemList = nil;

//------------------------------------------------------------------------------
//                                  IMPLEMENTATION
//------------------------------------------------------------------------------
implementation

uses
  SysUtils,
  StrUtils,
  Forms,

  TGenStrUnit,
  TGenTimerUnit,
  TProgListUnit,   // Program List Class
  TVerbListUnit,   // Verb List Class
  TExtListUnit,    // Extension List Class
  TCoupItemUnit,
  TMyMessageUnit,     // App Messages

  TGenClassesUnit; // Class Management

//------------------------------------------------------------------------------
// Create
//------------------------------------------------------------------------------
constructor TItemList.Create;
begin
  inherited Create;

  // WE MUST SET THIS AT START SINCE IT WILL CALL THIS LATER WHILE CREATING
  
  TheItems := self;

  // Create Verb List, TreeNode, and Add it

  objMyVerbList := TVerbList.Create;
  objList.Add(objMyVerbList);

  // Create Extension List, TreeNode, and Add it

  objMyExtList := TExtList.Create;
  objList.Add(objMyExtList);

  // Create Program List, TreeNode, and Add it

  objMyProgList := TProgList.Create(nil);
  objList.Add(objMyProgList);

  // Create My Document List, and add it to List

  objMyDocuments := TDocList.Create(nil);
  objList.Add(objMyDocuments);

  // Refresh All Nodes

  //PostMessage(Application.Handle, MSG_TREEVIEW_REFRESH, 0, 0);
end;
//------------------------------------------------------------------------------
// Destroy
//------------------------------------------------------------------------------
destructor TItemList.Destroy;
begin
  if objMyProgList.pDirty or
     objMyVerbList.pDirty or
     objMyExtList.pDirty then
    begin
      objMyProgList.Save;
      objMyVerbList.Save;
      objMyExtList.Save;
    end;

  // Note: We dont need to destroy the objXxxx placeholdres
  //       These will be destroyed by the base class

  inherited;
end;
//------------------------------------------------------------------------------
// Destroy
//------------------------------------------------------------------------------
function TItemList.FindProgFromLoadedUID(const Id : Cardinal): TTreeItemBase;
begin
  if (objMyProgList <> nil) then
    result := objMyProgList.GetChildFromUidLoaded(Id)
  else
    result := nil;
end;
//------------------------------------------------------------------------------
// Destroy
//------------------------------------------------------------------------------
function TItemList.FindProgFromUID(const Id : Cardinal): TTreeItemBase;
begin
  if (objMyProgList <> nil) then
    result := objMyProgList.GetChildFromUid(Id)
  else
    result := nil;
end;
//------------------------------------------------------------------------------
// Destroy
//------------------------------------------------------------------------------
function TItemList.FindVerbFromLoadedUID(const Id : Cardinal): TTreeItemBase;
begin
  if (objMyVerbList <> nil) then
    result := objMyVerbList.GetChildFromUidLoaded(Id)
  else
    result := nil;
end;
//------------------------------------------------------------------------------
// Destroy
//------------------------------------------------------------------------------
function TItemList.FindVerbFromUID(const Id : Cardinal): TTreeItemBase;
begin
  if (objMyVerbList <> nil) then
    result := objMyVerbList.GetChildFromUid(Id)
  else
    result := nil;
end;
//------------------------------------------------------------------------------
// Destroy
//------------------------------------------------------------------------------
function TItemList.FindExtFromLoadedUID (const Id : Cardinal): TTreeItemBase;
begin
  if (objMyExtList <> nil) then
    result := objMyExtList.GetChildFromUidLoaded(Id)
  else
    result := nil;
end;
//------------------------------------------------------------------------------
// Destroy
//------------------------------------------------------------------------------
function TItemList.FindExtFromUID (const Id : Cardinal): TTreeItemBase;
begin
  if (objMyExtList <> nil) then
    result := objMyExtList.GetChildFromUid(Id)
  else
    result := nil;
end;
//------------------------------------------------------------------------------
// Destroy
//------------------------------------------------------------------------------
function TItemList.IsProg(const Item : TTreeItemBase): boolean;
begin
  if (objMyProgList <> nil) then
    result := objMyProgList.IsItem(Item)
  else
    result := false;
end;
//------------------------------------------------------------------------------
// Destroy
//------------------------------------------------------------------------------
function TItemList.IsVerb(const Item : TTreeItemBase): boolean;
begin
  if (objMyProgList <> nil) then
    result := objMyVerbList.IsItem(Item)
  else
    result := false;
end;
//------------------------------------------------------------------------------
// Destroy
//------------------------------------------------------------------------------
function TItemList.IsExt (const Item : TTreeItemBase): boolean;
begin
  if (objMyProgList <> nil) then
    result := objMyProgList.IsItem(Item)
  else
    result := false;
end;
//------------------------------------------------------------------------------
// Get Next Verb
//------------------------------------------------------------------------------
function TItemList.GetNextVerb(
      var Iter  : integer;
      out pItem : TTreeItemBase)
                : boolean;
begin
  if (objMyVerbList <> nil) then
    result := objMyVerbList.GetNextChild(Iter, pItem)
  else
    result := false;
end;
//------------------------------------------------------------------------------
// Get Next Ext
//------------------------------------------------------------------------------
function TItemList.GetNextExt(
      var Iter  : integer;
      out pItem : TTreeItemBase)
                : boolean;
begin
  if (objMyExtList <> nil) then
    result := objMyExtList.GetNextChild(Iter, pItem)
  else
    result := false;
end;
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
procedure TItemList.AddListViewExtCoup(
      const pExt : TTreeItemBase;
      const ListView : TListView);
var
  ProgGrpIter : integer;
  ProgGrpItem : TTreeItemBase;

  ProgItemIter : integer;
  ProgItemItem : TTreeItemBase;

  ProgCoupIter : integer;
  ProgCoupItem : TTreeItemBase;
begin

  ProgGrpIter := 0;
  while objMyProgList.GetNextChild(ProgGrpIter, ProgGrpItem) do
    begin
      ProgItemIter := 0;
      while ProgGrpItem.GetNextChild(ProgItemIter, ProgItemItem) do
        begin
          ProgCoupIter := 0;
           while ProgItemItem.GetNextChild(ProgCoupIter, ProgCoupItem) do
            if (TCoupItem(ProgCoupItem).pExtItem = pExt) then
              begin
                // ProgCoupItem.AddItemToListView(ListView);
              end;
        end;
    end;
end;
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
procedure TItemList.AddListViewVerbCoup(
      const pVerb    : TTreeItemBase;
      const ListView : TListView);
var
  ProgGrpIter : integer;
  ProgGrpItem : TTreeItemBase;

  ProgItemIter : integer;
  ProgItemItem : TTreeItemBase;

  ProgCoupIter : integer;
  ProgCoupItem : TTreeItemBase;
begin

  ProgGrpIter := 0;
  while objMyProgList.GetNextChild(ProgGrpIter, ProgGrpItem) do
    begin
      ProgItemIter := 0;
      while ProgGrpItem.GetNextChild(ProgItemIter, ProgItemItem) do
        begin
          ProgCoupIter := 0;
           while ProgItemItem.GetNextChild(ProgCoupIter, ProgCoupItem) do
            if (TCoupItem(ProgCoupItem).pVerbItem = pVerb) then
              begin
                // ProgCoupItem.AddItemToListView(ListView);
              end;
        end;
    end;
end;
//------------------------------------------------------------------------------
//  Det Default Program from an Extension Name
//------------------------------------------------------------------------------
function TItemList.GetDefProg(
      const sExt  : string;
      const sVerb : string):TTreeItemBase;
var
  ProgGrpIter : integer;
  ProgGrpItem : TTreeItemBase;

  ProgItemIter : integer;
  ProgItemItem : TTreeItemBase;

  ProgCoupIter : integer;
  ProgCoupItem : TTreeItemBase;
begin
  result := nil;

  ProgGrpIter := 0;
  while objMyProgList.GetNextChild(ProgGrpIter, ProgGrpItem) do
    begin
      ProgItemIter := 0;
      while ProgGrpItem.GetNextChild(ProgItemIter, ProgItemItem) do
        begin
          ProgCoupIter := 0;
           while ProgItemItem.GetNextChild(ProgCoupIter, ProgCoupItem) do
            if (TCoupItem(ProgCoupItem).pVerbItem <> nil) and
               (AnsiSameText(TCoupItem(ProgCoupItem).pVerbItem.pName, sVerb)) and
               (TCoupItem(ProgCoupItem).pExtItem <> nil) and
               (AnsiSameText(TCoupItem(ProgCoupItem).pExtItem.pName, sExt)) and
               (TCoupItem(ProgCoupItem).pDefault) then
              begin
                result := ProgItemItem;
                BREAK;
              end;
        end;
    end;

end;
//------------------------------------------------------------------------------
// Update all Tracks on All Document Groups
//------------------------------------------------------------------------------
procedure TItemList.MediaDbRebuild;
var
  Timer : TGenTimer;
  Loop  : integer;
begin
  TheLog.Log('');
  TheLog.Log('Rebuilding Media Db');

  Timer := TGenTimer.Create;

  // Setup the Escape Message subscription

  objMe := objMsgPump.Subscribe(self.ClassName, MsgPumpProc);
  objMe.AddMessage(MSG_ESCAPE);

  // Tell Application that Progress Bar should be turned on

  PostMessage(Application.Handle, MSG_STATE_COUNTER_ON, 0, 0);
  Application.ProcessMessages;

  PostMessage(Application.Handle,
        MSG_STATE_COUNTER_MAX, DWORD(29000),0);

      //  TheMedia.pTrackCount), 0);
  Application.ProcessMessages;

  // Setup the Counter Timer

  objTimer := TTimer.Create(nil);
  objTimer.Interval := 200;
  objTimer.OnTimer  := OnMyTimer;
  objTimer.Enabled  := true;

  // Perform the Rebuild

  objEscaped := false;
  objCounter := 0;
  Loop := 0;
  objMyDocuments.MediaDbRebuild(objEscaped, objCounter, Loop);

  // Firts Free the Timer

  objTimer.Free;

  // Unsubscribe Escape Message

  objMsgPump.DeSubscribe(objMe);
  objMe := nil;

  // Tell App to close the Progress Bar

  PostMessage(Application.Handle, MSG_STATE_COUNTER_OFF, 0, 0);
  Application.ProcessMessages;

  // Tell the Success, Counter, Escaped etc

  if objEscaped then
    TheLog.Log('Rebuilt Media Db Escaped')
  else
    TheLog.Log('Rebuilt Media Db ' + IntToStr(objCounter) + ' (Media) in ' +
                  ToStr(Timer.Measure/1000) + '(Sec)');
  Timer.Free;
end;
//------------------------------------------------------------------------------
//  Receive Messages Subscribed on
//------------------------------------------------------------------------------
procedure TItemList.MsgPumpProc(var Msg : TMsg);
begin
  if (Msg.message = MSG_ESCAPE) then objEscaped := true;
end;
//------------------------------------------------------------------------------
//  Post a Message about current Counter
//------------------------------------------------------------------------------
procedure TItemList.OnMyTimer(Sender : TObject);
begin
  PostMessage(Application.Handle, MSG_STATE_COUNTER_CUR, DWORD(objCounter), 0);
end;
//------------------------------------------------------------------------------
//
//                               STARTUP & SHUTDOWN
//
//------------------------------------------------------------------------------
//  Startup
//------------------------------------------------------------------------------
class procedure TItemList.StartUp(const MsgPump : TWmMsgFactory);
begin
  if (TheItems = nil) then
    begin
      TItemList.Create;
      TheItems.objMsgPump := MsgPump;
    end;
end;
//------------------------------------------------------------------------------
//  ShutDown
//------------------------------------------------------------------------------
class procedure TItemList.ShutDown;
var
  nCount : integer;
begin
  if (TheItems <> nil) then
    begin
      nCount := TTreeItemBase.GetDebugCount;
      
      TheItems.Free;
      TheItems := nil;

      TheLog.Log('Item Count ' + IntToStr(TTreeItemBase.GetDebugCount) +
                 ' (' + INtToStr(nCount) + ')');
    end;
end;
//------------------------------------------------------------------------------
//                                    INITIALIZE
//------------------------------------------------------------------------------
initialization

  RegClass(TItemList);
end.
