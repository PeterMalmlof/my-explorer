unit TDocVideoUnit;

interface

uses
  Windows, Classes, Contnrs, ComCtrls,

  TGenLogUnit,        // Log Object
  TDocMediaUnit,      // Item Base Class
  TTreeItemBaseUnit; // Item Base Class

//------------------------------------------------------------------------------
//  Docuemnt Item
//------------------------------------------------------------------------------
type TDocVideo = Class(TDocMedia)
  protected

  public

    class function GetTypeStr: string;  override;
    class function GetImageId: integer; override;
    
    procedure Execute(const Col : integer); override;

end;

//------------------------------------------------------------------------------
//                                  IMPLEMENTATION
//------------------------------------------------------------------------------
implementation

uses
  SysUtils,
  StrUtils,

  TGenStrUnit,        // String Functions
  TGenShellUnit,      // Shell Functions

  TMediaPlayerResUnit,// Resource Strings
  TItemListUnit,
  TProgItemUnit,

  TGenClassesUnit;    // Class Management

//------------------------------------------------------------------------------
// Get Type String with ReadOnly and Hidden Flgs attached
//------------------------------------------------------------------------------
class function TDocVideo.GetTypeStr: string;
begin
  result := resDocVideoType;
end;
//------------------------------------------------------------------------------
// Return What Class Items Childrens are
//------------------------------------------------------------------------------
class function TDocVideo.GetImageId:integer;
begin
  result := ImageIdVideo;
end;
//------------------------------------------------------------------------------
// Play this Video
//------------------------------------------------------------------------------
procedure TDocVideo.Execute(const Col : integer);
const
  cPlay = 'play';
var
  sExt  : string;
  sPar  : string;
  sErr  : string;
  pProg : TTreeItemBase;
begin
  // Get Extension from FileName

  sExt := SysUtils.ExtractFileExt(self.pName);
  if length(sExt) > 0 then
    sExt := AnsiRightStr(sExt, length(sExt) - 1);

  pProg := TheItems.GetDefProg(sExt,cPlay);
  if (pProg <> nil) and (pProg is TProgItem) then
    begin
      sPar := '"' + self.GetPath + '" ' + TProgItem(pProg).pDesc;

      TheLog.Log('Open: ' + self.pName +
                 ' Ext: ' + sExt +
                 ' With ' + pProg.pName + ' ' + sPar);

      if not DoShellCmd(TProgItem(pProg).pFile, sPar, false, sErr) then
        begin
          TheLog.Log('ERROR: ' + sErr);
        end;
    end;
end;
//------------------------------------------------------------------------------
//                                    INITIALIZE
//------------------------------------------------------------------------------
initialization
  RegClass(TDocVideo);
end.

