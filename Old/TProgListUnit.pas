unit TProgListUnit;

interface

uses
  Windows, Classes, Contnrs, ComCtrls, Menus,

  TGenLogUnit,        // Log Object
  TProgBaseUnit,      // Base Class
  TTreeItemBaseUnit; // Base Class

//------------------------------------------------------------------------------
//  Tree Item Base Object
//------------------------------------------------------------------------------
type TProgList = Class(TProgBase)
  protected

    procedure AddNewChild (Sender: TObject); override;

  public
    constructor Create(
            const pParent : TTreeItemBase);  reintroduce;

    destructor  Destroy; override;

    class function  ChildrenClass: TItemClass; override;
    class function  GetTypeStr: string; override;

    procedure Save; override;
    procedure Load; override;

    procedure AddTreeMenuItems(const PopupMenu : TPopupMenu); override;
end;

//------------------------------------------------------------------------------
//                                  IMPLEMENTATION
//------------------------------------------------------------------------------
implementation

uses
  SysUtils,
  StrUtils,
  Forms,

  TGenTextFileUnit,   // Textfile Class
  TGenMenuUnit,       // TGenMenu Class

  TMyMessageUnit,     // App Messages
  TMediaPlayerResUnit,// Resource Strings
  TProgGrpUnit,       // Program Group Class
  TGenClassesUnit;    // Class Management

const
  PropFileName = 'My Programs.txt';

//------------------------------------------------------------------------------
// Create
//------------------------------------------------------------------------------
constructor TProgList.Create(
      const pParent : TTreeItemBase);
begin
  inherited Create(pParent, resProgListName);

  objDesc := resProgListDesc;

  // Load Program Groups

  self.Load;
end;
//------------------------------------------------------------------------------
// Destroy
//------------------------------------------------------------------------------
destructor TProgList.Destroy;
begin

  inherited;
end;
//------------------------------------------------------------------------------
// Return What Class Items Childrens are
//------------------------------------------------------------------------------
class function TProgList.ChildrenClass: TItemClass;
begin
  result := TProgGrp;
end;
//------------------------------------------------------------------------------
// Get Type String with ReadOnly and Hidden Flgs attached
//------------------------------------------------------------------------------
class function TProgList.GetTypeStr: string;
begin
  result := resProgListType;
end;
//------------------------------------------------------------------------------
// Save This Item
//------------------------------------------------------------------------------
procedure TProgList.Save;
var
  TF : TGenTextFile;
begin
  EXIT;

  TheLog.Log('TProgList Saved');

  // Open a File to save things in

  TF := TGenTextFile.CreateForWrite(ApplicationFile(PropFileName));

  // Save All Items

  objList.SaveToFile(TF);

  TF.Free;
end;
//------------------------------------------------------------------------------
// Load This Item
//------------------------------------------------------------------------------
procedure TProgList.Load;
var
  pFile         : TGenTextFile;
  sProp, sValue : string;
begin
  // Open a File to read all Application Groups

  pFile := TGenTextFile.CreateForRead(ApplicationFile(PropFileName));

  // Walk all Lines and Create Objects

  while pFile.ReadProp(sProp, sValue) do
    begin
      // Is this the Start of a new Program Group

      if AnsiSameText(PropPrgGrp, sProp) then
        begin
          // Create a New Program Group

          objList.AddItem(TProgGrp.CreateFromFile(self, pFile));
        end;
    end;
      
  pFile.Free;
end;
//------------------------------------------------------------------------------
// Add Menus accoding to Current Selected Item in TreeView
//------------------------------------------------------------------------------
procedure TProgList.AddTreeMenuItems(const PopupMenu : TPopupMenu);
var
  pMenu : TGenMenu;
begin
  inherited AddTreeMenuItems(PopupMenu);

  // Add New Child

  pMenu := TGenMenu.Create(PopupMenu);
  pMenu.Caption := resAdd + SPC + TProgGrp.GetTypeStr;
  pMenu.OnClick := AddNewChild;
  PopupMenu.Items.Add(pMenu);
end;
//------------------------------------------------------------------------------
// Menu: Add a Child Item
//------------------------------------------------------------------------------
procedure TProgList.AddNewChild(Sender: TObject);
begin
  objList.AddItem(TProgGrp.Create(self, resNew + SPC + TProgGrp.GetTypeStr));
  self.pDirty := true;

  //PostMessage(Application.Handle, MSG_TREEVIEW_REFRESH, 0, 0);
  //PostMessage(Application.Handle, MSG_LISTVIEW_REFRESH, 0, 0);
end;
//------------------------------------------------------------------------------
//                                    INITIALIZE
//------------------------------------------------------------------------------
initialization

  RegClass(TProgList);
end.

