unit TMemoFrameUnit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, 
  Dialogs, Math, Contnrs, Inifiles,

  TGenScrollBarUnit, TPmaListViewUnit;

(*
//------------------------------------------------------------------------------
// TPmaColumn
//------------------------------------------------------------------------------
type TPmaColumn = class(TObject)
  private
    FCaption  : string;   // List of Columns
    FStart    : integer;  // Start Position (X) of this Column
    FWidth    : integer;  // Width of this Column
    FImageId  : integer;  // Image Id
    FSortDown : boolean;  // Sort Order Down
    FDirty    : boolean;  // Need Repainting

  protected

    procedure SetCaption  (const Value : string);
    procedure SetStart    (const Value : integer);
    procedure SetWidth    (const Value : integer);
    procedure SetImageId  (const Value : integer);
    procedure SetSortDown (const Value : boolean);
  public
    constructor Create(const Caption : string);
    destructor  Destroy; override;

    procedure Paint(
      const Canvas : TCanvas;
      const Over   : boolean;
      //const PosX   : integer;
      const Height : integer);

    property Caption  : string  read FCaption  write SetCaption;
    property Start    : integer read FStart    write SetStart;
    property Width    : integer read FWidth    write SetWidth;
    property ImageId  : integer read FImageId  write SetImageId;
    property SortDown : boolean read FSortDown write SetSortDown;
end;

//------------------------------------------------------------------------------
// TPmaColumnList Event Types
//------------------------------------------------------------------------------

  // Event That User Want to Sort a Column

  TMAPmaColumnListSortEvent = procedure(
    Sender : TObject;   // TPmaColumnList firent this Event
    Column : integer;   // Column To Sort
    Down   : boolean)   // Sort Down
             of object;

//------------------------------------------------------------------------------
// TPmaColumnList
//------------------------------------------------------------------------------
type TPmaColumnList = class(TCustomControl)
  private
    FResize  : boolean;     // Need to Resize
    
    FColumns : TObjectList; // List of Columns
    FAdjust  : integer;

    // Mouse Managing when resizing Column Widths

    FSplit      : integer; // Column where Split is detected
    FOver       : integer; // Column Where Mouse Is
    FMouseDown  : boolean; // Mouse Left Button Pressed
    FMousePos   : integer; // Last Mouse Position when MouseDown
    FMouseMoved : boolean; // TRue if Mouse was Moved

    FColumnSortEvent : TMAPmaColumnListSortEvent;
  protected

    procedure SetAdjust (const Value : integer);
    function  GetCount : integer;

    procedure DoColumnSort(const Column : integer; const Down : boolean);
  public
    constructor Create(AOwner : TCOmponent); override;
    destructor  Destroy; override;

    procedure Clear;

    // Standard Overrides

    procedure Resize; override;
    procedure Paint; override;

    procedure MouseDown (Button: TMouseButton;
                         Shift: TShiftState; X, Y: Integer); override;

    procedure MouseMove(Shift: TShiftState; X, Y: Integer);override;

    procedure MouseUp   (Button: TMouseButton;
                         Shift: TShiftState; X, Y: Integer); override;

    procedure MsgMouseLeave (var Message: TMessage); message CM_MouseLeave;

    // Add a New Column to ListView

    function  AddColumn(const Caption : string): TPmaColumn;

    // Get Width of a Specific Column

    function  ColumnWidth(const Col : integer): integer;

    // Get a Specific Columns Rect (Left & Right are useful)

    function  ColumnRect(const Col : integer): TRect;

    // Get a Specific Columns from a X position

    function  GetColumnFromX(const X : integer): integer;

    // Load and Save Column Widths from INi File

    procedure LoadColumns(const Prf : string);
    procedure SaveColumns(const Prf : string);

    // Set Which Column to take up the Slack

    property  Adjust : integer read FAdjust write SetAdjust;
    property  Count  : integer read GetCount;

  published

    property OnSortColumn : TMAPmaColumnListSortEvent
                             read  FColumnSortEvent
                             write FColumnSortEvent;

    // Reintroduced Properties

    property Align;
    //property Alignment;
    property Anchors;
    property Color;
    property Constraints;
    property Cursor;
    property DragCursor;
    property DragKind;
    property DragMode;
    property Enabled;
    property Font;
    property Height;
    property HelpContext;
    property HelpKeyword;
    property HelpType;
    property Hint;
    property Left;
    property ParentColor;
    property ParentFont;
    property ParentShowHint;
    property ShowHint;
    property TabOrder;
    property TabStop;
    property Top;
    property Visible;
    property Width;

    // Usable Events from Baseclass

    property OnClick;
    property OnContextPopup;
    property OnDragDrop;
    property OnDblClick;
    property OnEndDock;
    property OnEnter;
    property OnExit;
    property OnKeyDown;
    property OnKeyPress;
    property OnKeyUp;
    property OnStartDock;
    property OnStartDrag;
end;

//------------------------------------------------------------------------------
// TPmaListItem
//------------------------------------------------------------------------------
type TPmaListItem = class(TObject)
  private
    FTitles  : TStringList; // All Column Strings
    FImageId : integer;     // Image Id of this Item
    FDirty   : boolean;     // Has Changed, need repainting
    FObject  : TObject;     // Data Object

  protected
    procedure SetImageId  (const Value : integer);
    procedure SetCaption  (const Value : string);
    function  GetCaption: string;
    function  GetColumns: integer;
  public
    constructor Create(const Caption : string);
    destructor  Destroy; override;

    // Add a New Column to the Item

    procedure AddColumn(const Text : string);

    // Paint This Item at specified Position

    procedure Paint(
      const Canvas   : TCanvas;        // Canvas to Draw on
      const Selected : boolean;        // Selected
      const PosY     : integer;        // Y Position of Item
      const Width    : integer;        // Width of Item Row
      const Columns  : TPmaColumnList; // Column Widths
      const Height   : integer);       // Height of Item

    // Compare this Item with another (used for Sorting)

    function Compare(
      const ListItem : TPmaListItem;
      const Column   : integer): integer;

    // Get/Set Column Text

    function  GetItemText(const Col : integer): string;
    procedure SetItemText(const Value : string;const Col : integer);

    // Properties

    property ImageId : integer  read FImageId    write SetImageId;
    property Caption : string   read GetCaption  write SetCaption;
    property Data    : TObject  read FObject     write FObject;
    property Columns : integer  read GetColumns;
end;

//------------------------------------------------------------------------------
// TPmaListView
//------------------------------------------------------------------------------
Type TPmaListView = class(TCustomControl)
  private
    FColumns      : TPmaColumnList;  // The Columns of the ListViw

    FLines     : TObjectList; // All Lines
    FLineFirst : integer;     // The First Line Drawn
    FLineHgt   : integer;     // Current Line Height
    FLineVis   : integer;     // Current Number of Lines Visible
    FLinesRect : TRect;       // Lines Rect

    // Mouse Over Management

    FOverDrawn  : boolean;
    FOverIndex  : integer;
    FOverColumn : integer;
    FOverRect   : TRect;

    // Select Manamement

    FSelectIndex  : integer;       // Selected Index in FLines
    FSelectColumn : integer;       // Selected Column
    FSelectItem   : TPmaListItem;  // Selected Item

    // Scroll Bar Object

    FScrollBar : TGenScrollBar; // Vertical Scrollbar

    // User Interface properties

    FBorder : boolean;

  protected

    procedure SetBorder (const Value : boolean);
    procedure SetColumnAdjust (const Value : integer);
    function  GetColumnAdjust : integer;

    procedure CalcLineHgtAndVisible;

    procedure OnScrollBarValue(Sender : TObject; Value : integer);

    procedure DoEnter; override;
    procedure DoExit;  override;

    procedure PaintLinesRect;

    function  GetItemAt(
      const pos    : TPoint;        // Positin of Mouse
      out   Index  : integer;       // Index returned
      out   Column : integer;       // Column Returned
      out   Item   : TPmaListItem)  // Item returned
                   : boolean;       // An Item was Found

    function GetItemRect(const Line, Column : integer): TRect;

  public
    constructor Create(AOwner : TCOmponent); override;
    destructor  Destroy; override;

    // Standard Overrides

    procedure Resize; override;
    procedure Paint;  override;

    procedure MouseDown
      (Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;

    procedure MouseMove(Shift: TShiftState; X, Y: Integer);override;

    function DoMouseWheel (Shift: TShiftState;
          WheelDelta: Integer; MousePos: TPoint): Boolean; override;

    procedure MsgMouseLeave (var Message: TMessage); message CM_MouseLeave;

    function AddItem(const Caption : string):TPmaListItem;

    procedure Clear;

    procedure Sort(Sender : TObject; Column : integer; Down : boolean);

    procedure LoadSettings;

    procedure SaveSettings;

    // A Pointer to the Colums (read only)

    property Columns : TPmaColumnList read FColumns;
  published

    property Border       : boolean  read FBorder       write SetBorder;
    property ColumnAdjust : integer  read GetColumnAdjust write SetColumnAdjust;

    // Reintroduced Properties

    property Align;
    //property Alignment;
    property Anchors;
    property Color;
    property Constraints;
    property Cursor;
    property DragCursor;
    property DragKind;
    property DragMode;
    property Enabled;
    property Font;
    property Height;
    property HelpContext;
    property HelpKeyword;
    property HelpType;
    property Hint;
    property Left;
    property ParentColor;
    property ParentFont;
    property ParentShowHint;
    property ShowHint;
    property TabOrder;
    property TabStop;
    property Top;
    property Visible;
    property Width;

    // Usable Events from Baseclass

    property OnClick;
    property OnContextPopup;
    property OnDragDrop;
    property OnDblClick;
    property OnEndDock;
    property OnEnter;
    property OnExit;
    property OnKeyDown;
    property OnKeyPress;
    property OnKeyUp;
    property OnStartDock;
    property OnStartDrag;

end; *)
//------------------------------------------------------------------------------
// TMemoFrame
//------------------------------------------------------------------------------
type
  TMemoFrame = class(TFrame)
    LineView: TPmaListView;


    procedure FrameResize(Sender: TObject);
  private
    //FLineView : TPmaListView;

  public
    constructor Create(AOwner : TComponent); override;
    destructor Destroy; override;

    //property LineView : TPmaListView read FLineView;
  end;

implementation

uses
  TGenLogUnit;

{$R *.dfm}

//var SortColumn : integer = 0;
//    SortDown   : boolean = true;

//------------------------------------------------------------------------------
//
//                                  TPmaColumn
//
//------------------------------------------------------------------------------
// Create
//------------------------------------------------------------------------------
(*constructor TPmaColumn.Create(const Caption : string);
begin
  inherited Create;

  // Set Caption of Column Header

  FCaption  := Caption;

  // Set Default Values

  FStart    :=  1;
  FWidth    := 40;
  FImageId  := -1;
  FSortDown := true;
  FDirty    := true;
end;
//------------------------------------------------------------------------------
// Destroy
//------------------------------------------------------------------------------
destructor TPmaColumn.Destroy;
begin

  inherited;
end;
//------------------------------------------------------------------------------
// Set Width
//------------------------------------------------------------------------------
procedure TPmaColumn.SetCaption(const Value : string);
begin
  if (not AnsiSameStr(FCaption, Value)) then
    begin
      FDirty   := true;
      FCaption := Value;
    end;
end;
//------------------------------------------------------------------------------
// Set Width
//------------------------------------------------------------------------------
procedure TPmaColumn.SetStart(const Value : integer);
begin
  if (FStart <> Value) then
    begin
      FDirty := true;
      FStart := Value;
    end;
end;
//------------------------------------------------------------------------------
// Set Width
//------------------------------------------------------------------------------
procedure TPmaColumn.SetWidth(const Value : integer);
begin
  if (FWidth <> Value) and (Value > 20) then
    begin
      FDirty := true;
      FWidth := Value;
    end;
end;
//------------------------------------------------------------------------------
// Set Image Id
//------------------------------------------------------------------------------
procedure TPmaColumn.SetImageId(const Value : integer);
begin
  if (FImageId <> Value) then
    begin
      FDirty   := true;
      FImageId := Value;
    end;
end;
//------------------------------------------------------------------------------
// Set Sort Order
//------------------------------------------------------------------------------
procedure TPmaColumn.SetSortDown(const Value : boolean);
begin
  if (FSortDown <> Value) then
    begin
      FDirty    := true;
      FSortDown := Value;
    end;
end;
//------------------------------------------------------------------------------
// Paint this Column
//------------------------------------------------------------------------------
procedure TPmaColumn.Paint(
      const Canvas : TCanvas;
      const Over   : boolean;
      //const PosX   : integer;
      const Height : integer);
var
  TxtR : TRect;
  pArr : array [0..2] of TPoint;
  Size : integer;
begin
  //----------------------------------------------------------------------------
  // Draw the Column Header as a Rounded Rect with Pen

  Canvas.Pen.Style   := psSolid;

  // If Mouse is Over this Column, Highlight it

  if Over then
    Canvas.Brush.Color := clCream
  else
    Canvas.Brush.Color := RGB(210,210,210);

  // Draw the Column Rounded Rect (Margin 2 all around the real Rect)

  Canvas.RoundRect(FStart + 2, + 2, FStart + FWidth - 2, Height - 2, 5, 5);

  //----------------------------------------------------------------------------
  // Calc Size of Arrow and make it odd so the Arrow will look nice

  Size := Height div 4; if (not Odd(Size)) then Inc(Size);

  //----------------------------------------------------------------------------
  // Draw The Caption transparent (Left after Arrow, Top +4

  SetBkMode(Canvas.Handle, TRANSPARENT);

  TxtR := Rect(FStart + Size * 3 + 4, 4,
               FStart + FWidth - 6, Height - 4);

  DrawTextEx(Canvas.Handle, PChar(FCaption), -1, TxtR,
      //DT_END_ELLIPSIS	, nil);
      DT_VCENTER or DT_END_ELLIPSIS	, nil);

  //----------------------------------------------------------------------------
  // Draw the Sort Order Symbol as a Polygon without Pen

  Canvas.Brush.Color := RGB(110,110,110);
  Canvas.Pen.Style   := psClear;

  if FSortDown then
    begin
      pArr[0] := Point(FStart +     Size,          Size + 1);
      pArr[1] := Point(FStart + 3 * Size,          Size + 1);
      pArr[2] := Point(FStart + 2 * Size, Height - Size - 1);
    end
  else
    begin
      pArr[0] := Point(FStart +     Size, Height - Size - 1);
      pArr[1] := Point(FStart + 3 * Size, Height - Size - 1);
      pArr[2] := Point(FStart + 2 * Size,          Size + 1);
    end;

  Canvas.Polygon(pArr);
end;
//------------------------------------------------------------------------------
//
//                                  TPmaColumnList
//
//------------------------------------------------------------------------------
// Create
//------------------------------------------------------------------------------
constructor TPmaColumnList.Create(AOwner : TComponent);
begin
  inherited;

  // Create The Columns Object List (It will own the Objects)

  FColumns   := TObjectList.Create(true);

  // Set Default property Values

  FAdjust    := -1;
  FSplit     := -1;
  FOver      := -1;

  FMouseDown  := false;
  FMousePos   := 0;
  FMouseMoved := false;

  FResize := true;
end;
//------------------------------------------------------------------------------
// Destroy
//------------------------------------------------------------------------------
destructor TPmaColumnList.Destroy;
begin

  // Free The Columns

  FreeAndNil(FColumns);

  inherited;
end;
//------------------------------------------------------------------------------
// Set Adjusted Column
//------------------------------------------------------------------------------
procedure TPmaColumnList.SetAdjust(const Value : integer);
begin
  if (FAdjust <> Value) then
    begin
      FAdjust := Value;
      FResize := true;
      self.Invalidate;
    end;
end;
//------------------------------------------------------------------------------
// Get Number of Columns
//------------------------------------------------------------------------------
function TPmaColumnList.GetCount : integer;
begin
  result := FColumns.Count;
end;
//------------------------------------------------------------------------------
// Add a New Column
//------------------------------------------------------------------------------
function TPmaColumnList.AddColumn(const Caption : string): TPmaColumn;
begin
  result := TPmaColumn.Create(Caption);
  FColumns.Add(result);

  FResize := true;
  self.Invalidate;
end;
//------------------------------------------------------------------------------
// Clear All Columns
//------------------------------------------------------------------------------
procedure TPmaColumnList.Clear;
begin
  FColumns.Clear;

  FResize    := true;
  FSplit     := -1;
  FOver      := -1;
  FMouseDown := false;

  self.Invalidate;
end;
//------------------------------------------------------------------------------
// Perform a Column Sort
//------------------------------------------------------------------------------
procedure TPmaColumnList.DoColumnSort
            (const Column : integer; const Down : boolean);
begin
  if Assigned(FColumnSortEvent) then
    FColumnSortEvent(self, Column, Down);
end;
//------------------------------------------------------------------------------
//  Return Width of a Specific Column
//------------------------------------------------------------------------------
function TPmaColumnList.ColumnWidth(const Col : integer): integer;
begin
  if (Col >= 0) and (Col < FColumns.Count) then
    result := TPmaColumn(FColumns[Col]).Width
  else
    result := 20;
end;
//------------------------------------------------------------------------------
//  Return Rect of a Specific Column
//------------------------------------------------------------------------------
function TPmaColumnList.ColumnRect(const Col : integer): TRect;
begin
  result.Top    := self.Top;
  result.Bottom := self.Top + self.Height;

  if (Col >= 0) and (Col < FColumns.Count) then
    begin
      result.Left  := TPmaColumn(FColumns[Col]).Start;
      result.Right := result.Left + TPmaColumn(FColumns[Col]).Width;
    end
  else
    begin
      result.Left  := 0;
      result.Right := 20;
    end;
end;
//------------------------------------------------------------------------------
//  Get a Specific Columns from a X position
//------------------------------------------------------------------------------
function TPmaColumnList.GetColumnFromX(const X : integer): integer;
var
  Ind : integer;
begin

  result := -1;
  if (FColumns.Count > 0) then
    for Ind := 0 to FColumns.Count - 1 do
      begin
        if (X > TPmaColumn(FColumns[Ind]).Start) and
           (X < (TPmaColumn(FColumns[Ind]).Start +
                 TPmaColumn(FColumns[Ind]).Width)) then
          begin
            result := Ind;
            BREAK;
          end;
      end;
end;
//------------------------------------------------------------------------------
//  Load Ini Settings
//------------------------------------------------------------------------------
procedure TPmaColumnList.LoadColumns(const Prf : string);
var
  sTmp : string;
  Ini  : TIniFile;
  Ind  : integer;
begin
  if (FColumns.Count > 0) then
    begin
      sTmp := ChangeFileExt(Application.ExeName,'.ini');

      if FileExists(sTmp) and (length(Prf) > 0) then
        begin
          Ini := TIniFile.Create(sTmp);

          // Load all Column Widths

          for Ind := 0 to FColumns.Count - 1 do
            TPmaColumn(FColumns[Ind]).FWidth :=
              Ini.ReadInteger(Prf, TPmaColumn(FColumns[Ind]).Caption, 100);

          Ini.Free();

          FResize := true;
        end;
    end;
end;
//------------------------------------------------------------------------------
//  Save Ini Settings
//------------------------------------------------------------------------------
procedure TPmaColumnList.SaveColumns(const Prf : string);
var
  sTmp : string;
  Ini : TIniFile;
  Ind : integer;
begin
  if (FColumns.Count > 0) then
    begin
      sTmp := ChangeFileExt(Application.ExeName,'.ini');

      if (length(Prf) > 0) and (length(sTmp) > 0) then
        begin
          Ini := TIniFile.Create(sTmp);

          // Save all Column Widths

          for Ind := 0 to self.FColumns.Count - 1 do
            Ini.WriteInteger(Prf, TPmaColumn(FColumns[Ind]).Caption,
                       TPmaColumn(FColumns[Ind]).Width);
          Ini.Free();
        end;
    end;
end;
//------------------------------------------------------------------------------
//  Mouse Down Event
//------------------------------------------------------------------------------
procedure TPmaColumnList.MouseDown
          (Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  if (Button = mbLeft) then
    begin
      FMouseDown  := true;
      FMousePos   := X;
      FMouseMoved := false;
    end;
end;
//------------------------------------------------------------------------------
// MouseMove
//------------------------------------------------------------------------------
procedure TPmaColumnList.MouseMove(Shift: TShiftState; X, Y: Integer);
var
  Delta : integer;
  Ind, Nxt, Pos, Over, Split : integer;
begin
  // First Handle when Resizing using Splitter

  if FMouseDown and (Fsplit >= 0) and (FSPlit < (FColumns.Count-1)) then
    begin
      FMouseMoved := true;

      // FSplit tells if which Column to resize

      Delta := (X - FMousePos);
      if (Delta <> 0) then
        begin
          // Increment the Split Column with dWdt and decrease the next

          if (TPmaColumn(FColumns[FSplit  ]).Width > (20 - Delta)) then
          if (TPmaColumn(FColumns[FSplit+1]).Width > (20 + Delta)) then
            begin
              TPmaColumn(FColumns[FSplit]).Width :=
                TPmaColumn(FColumns[FSplit]).Width + Delta;

              TPmaColumn(FColumns[FSplit+1]).Width :=
                TPmaColumn(FColumns[FSplit+1]).Width - Delta;

              FResize := true;
              self.Invalidate;
              self.Parent.Invalidate;
            end;
        end;

      FMousePos := X;
    end

  // Now Test for Moving Columns (not firs or last)

  else if FMouseDown and (Fsplit < 0) and
          (FOver > 0) and (FOver < (FColumns.Count-1)) then
    begin
      FMouseMoved := true;

      // Move Column using Delta X

      Delta := (X - FMousePos);
      if (Delta <> 0) then
        begin
          // Decrement the Width of the Column Before

          if (TPmaColumn(FColumns[FOver-1]).Width > (20 - Delta)) then
          if (TPmaColumn(FColumns[FOver+1]).Width > (20 + Delta)) then
            begin
              TPmaColumn(FColumns[FOver-1]).Width :=
                  TPmaColumn(FColumns[FOver-1]).Width + Delta;

              // Increment the Width of the Column After

              TPmaColumn(FColumns[FOver+1]).Width :=
                  TPmaColumn(FColumns[FOver+1]).Width - Delta;

              FResize := true;
              self.Invalidate;
              self.Parent.Invalidate;
            end;
        end;

      FMousePos := X;
    end

  // Not Moving anything, just Highligt
  
  else
    begin
      // Its inside on X, Walk all Columns to test Y to get FOver

      Nxt   := 0;
      Pos   := 0;
      Over  := -1;
      Split := -1;
      for Ind := 0 to FColumns.Count - 1 do
        begin
          // First Test if on Splitter

          Nxt := Nxt + TPmaColumn(FColumns[Ind]).Width;
          if (Ind >= 0) and (Ind < (FColumns.Count - 1)) and
             (X > (Nxt - 6)) and (X < (Nxt + 6)) then
            begin
              Split := Ind;
              BREAK;
            end
          else if (X > Pos) and
                  (X < (Pos + TPmaColumn(FColumns[Ind]).Width)) then
            begin
              Over := Ind;
              BREAK;
            end;
          Pos := Nxt;
        end;

      if (FSplit <> Split) then
        begin
          FSplit := Split;
          self.Invalidate;
        end;

      if (FOver <> Over) then
        begin
          FOver := Over;
          self.Invalidate;
        end;

      if (FSplit >= 0) then
        self.Cursor := crSizeWE
      else if (FOver >= 0) then
        self.Cursor := crHandPoint
      else
        self.Cursor := crDefault;
    end;

  inherited;
end;
//------------------------------------------------------------------------------
//  Mouse Up Event
//------------------------------------------------------------------------------
procedure TPmaColumnList.MouseUp
          (Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  // If Mouse was pressed Down, but not Moved, and Mouse was over
  // any of the Columns, then its a Sorting Click

  if FMouseDown and (not FMouseMoved) and
    (FOver >= 0) and (FOver < FColumns.Count) then
    begin
      // Toggle the Sorting Order

      TPmaColumn(FColumns[FOver]).SortDown :=
        not TPmaColumn(FColumns[FOver]).SortDown;

      // Perform the Sorting

      DoColumnSort(FOver, TPmaColumn(FColumns[FOver]).SortDown);

      // Repaint Sorting Button and Parent

      self.Invalidate;
      self.Parent.Invalidate;
    end;

  FMouseDown := false;
end;
//------------------------------------------------------------------------------
//  Message Leave
//------------------------------------------------------------------------------
procedure TPmaColumnList.MsgMouseLeave (var Message: TMessage);
begin
  if (FOver >= 0) then
    begin
      FOver := -1;
      self.Invalidate;
    end;
end;
//------------------------------------------------------------------------------
// Paint All Columns
//------------------------------------------------------------------------------
procedure TPmaColumnList.Paint;
var
  Ind : integer;
  Pos : integer;
begin

  if (FColumns.Count > 0) then
    begin
      if FResize then Resize;

      // Set the Pen Color

      self.Canvas.Pen.Color := RGB(70,70,70);

      // Set the Header inside Color

      self.Canvas.Brush.Color := RGB(210,210,210);

      // Set Canvas Font to the same as Control

      self.Canvas.Font := self.Font;

      Pos := 1;
      for Ind := 0 to FColumns.Count - 1 do
        begin
          // Paint this Column Header

          TPmaColumn(FColumns[Ind]).Paint(self.Canvas,
                (FOver = Ind), Height);

          // Increment the Position for next Column

          Inc(Pos, TPmaColumn(FColumns[Ind]).Width);
        end;
    end;
end;
//------------------------------------------------------------------------------
// Resize
//------------------------------------------------------------------------------
procedure TPmaColumnList.Resize;
var
  Wdt, Ind, Start : integer;
begin
  if (FAdjust >= 0) and (FAdjust < FColumns.Count) then
    begin
      // Sum all Column Widths except the Adjustable one

      Wdt := 0;
      for Ind := 0 to FColumns.Count - 1 do
        if (Ind <> FAdjust) then
          Inc(Wdt, TPmaColumn(FColumns[Ind]).Width);

      // Adjust the Adjustable Column the the rest of Width

      TPmaColumn(FColumns[FAdjust]).Width := self.ClientWidth - Wdt;

      // Set Start Position of all Colums

      Start := 0;
      for Ind := 0 to FColumns.Count - 1 do
        begin
          TPmaColumn(FColumns[Ind]).Start := Start;
          Inc(Start, TPmaColumn(FColumns[Ind]).Width);
        end;

      self.Invalidate;
    end;

  FResize := false;
end;
//------------------------------------------------------------------------------
//
//                                 LIST ITEM
//
//------------------------------------------------------------------------------
// Create
//------------------------------------------------------------------------------
constructor TPmaListItem.Create(const Caption : string);
begin
  inherited Create;

  // Create String List and add Caption as the first string

  FTitles := TStringList.Create;
  FTitles.Add(Caption);

  // Set Default Property Values

  FImageId := -1;
  FDirty   := true;
  FObject  := nil;
end;
//------------------------------------------------------------------------------
// Destroy
//------------------------------------------------------------------------------
destructor TPmaListItem.Destroy;
begin
  FTitles.Free;

  inherited;
end; 
//------------------------------------------------------------------------------
// Add Column Text
//------------------------------------------------------------------------------
procedure TPmaListItem.AddColumn(const Text : string);
begin
  FTitles.Add(Text);
  FDirty := true;
end;
//------------------------------------------------------------------------------
// Get Number of Columns
//------------------------------------------------------------------------------
function TPmaListItem.GetColumns: integer;
begin
  result := FTitles.Count;
end;
//------------------------------------------------------------------------------
// Get Caption (The First String)
//------------------------------------------------------------------------------
function TPmaListItem.GetCaption : string;
begin
  if (FTitles.Count > 0) then
    result := FTitles[0]
  else
    result := '';
end;
//------------------------------------------------------------------------------
// Set Caption
//------------------------------------------------------------------------------
procedure TPmaListItem.SetCaption(const Value : string);
begin
  if (FTitles.Count > 0) and
     (not AnsiSameStr(FTitles[0], Value)) then
    begin
      FDirty     := true;
      FTitles[0] := Value;
    end;
end;
//------------------------------------------------------------------------------
// Get Text from an Column
//------------------------------------------------------------------------------
function TPmaListItem.GetItemText(const Col : integer) : string;
begin
  if (Col >= 0) and (Col < FTitles.Count) then
    result := FTitles[Col]
  else
    result := '';
end;
//------------------------------------------------------------------------------
// Set Text on an Existing Column
//------------------------------------------------------------------------------
procedure TPmaListItem.SetItemText(const Value : string;const Col : integer);
begin
  if (Col >= 0) and (Col < FTitles.Count) and
     (not AnsiSameStr(FTitles[Col], Value)) then
    begin
      FDirty       := true;
      FTitles[Col] := Value;
    end;
end;
//------------------------------------------------------------------------------
// Set Image Id
//------------------------------------------------------------------------------
procedure TPmaListItem.SetImageId(const Value : integer);
begin
  if (FImageId <> Value) then
    begin
      FDirty   := true;
      FImageId := Value;
    end;
end;
//------------------------------------------------------------------------------
// Compare Two Items
//------------------------------------------------------------------------------
function TPmaListItem.Compare(
      const ListItem : TPmaListItem;
      const Column   : integer): integer;
begin
  if (Column >= 0) and (Column < FTitles.Count) and
     (Column < ListItem.FTitles.Count) then
    result := CompareStr(self.FTitles[Column], ListItem.FTitles[Column])
  else
    begin
      result := 0;

      if (Column < FTitles.Count) then
        result := +1
      else if (Column < ListItem.FTitles.Count) then
        result := -1;
    end;

  if (not SortDown) then result := - result;
end;
//------------------------------------------------------------------------------
// Paint this Column
//------------------------------------------------------------------------------
procedure TPmaListItem.Paint(
      const Canvas   : TCanvas;        // Canvas to Draw on
      const Selected : boolean;        // Selected
      const PosY     : integer;        // Y Position of Item
      const Width    : integer;        // Width of Item Row
      const Columns  : TPmaColumnList; // Column Widths
      const Height   : integer);       // Height of Item
var
  TxtRect : TRect;
  Ind     : integer;
begin
  // If Item is Selected, then draw the Rectangle under the Text

  if Selected then
    Canvas.Rectangle(4,PosY, Width - 1, PosY + Height);

  //----------------------------------------------------------------------------
  // Draw all Column Texts

  if (FTitles.Count > 0) then
    begin
      // Draw The Text transparent

      SetBkMode(Canvas.Handle, TRANSPARENT);

      TxtRect.Top    := PosY;
      TxtRect.Bottom := PosY + Height;

      // Start some pixels in from Left

      TxtRect.Left := 4;
      for Ind := 0 to FTitles.Count - 1 do
        begin
          TxtRect.Right := TxtRect.Left + Columns.ColumnWidth(Ind) - 4;

          DrawtextEx(Canvas.Handle,
                PChar(FTitles[Ind]), -1, TxtRect,
                DT_VCENTER or DT_END_ELLIPSIS	, nil);

          TxtRect.Left := TxtRect.Right + 4;
        end;
    end;
end;
//------------------------------------------------------------------------------
//
//                                  LIST VIEW
//
//------------------------------------------------------------------------------
// Create
//------------------------------------------------------------------------------
constructor TPmaListView.Create(AOwner : TComponent);
begin
  inherited;

  // Create Columns

  FColumns := TPmaColumnList.Create(self);
  FColumns.Parent := self;
  FColumns.OnSortColumn := Sort;
  //FColumns.DoubleBuffered := true;

  // Create the List of Lines

  FLines     := TObjectList.Create(true);
  FLineFirst := 0;
  FLineVis   := 0;
  FLineHgt   := 0;

  // Create the Vertical Scrollbar

  FScrollBar := TGenScrollBar.Create(self);
  FScrollBar.Parent    := self;

  FScrollBar.Vertical := true;
  FScrollBar.ValueMax := 0;
  FScrollBar.ValueMin := 0;
  FScrollBar.ValueCur := 0;
  FScrollBar.ValueVis := FLineVis;
  //FScrollBar.DoubleBuffered := true;

  FScrollBar.OnValue := OnScrollBarValue;
  
  FLinesRect := Rect(0,0,self.Width - FScrollBar.Size - 2, self.Height);

  FOverDrawn  := false;
  FOverIndex  := -1;
  FOverColumn := -1;

  FSelectIndex  := -1;
  FSelectColumn := -1;
  FSelectItem   := nil;
end;
//------------------------------------------------------------------------------
// Destroy
//------------------------------------------------------------------------------
destructor TPmaListView.Destroy;
begin

  // Destroy all Lines

  FLines.Free;

  inherited;
end;
//------------------------------------------------------------------------------
// Add a New Item to the List
//------------------------------------------------------------------------------
function TPmaListView.AddItem(const Caption : string):TPmaListItem;
begin
  // Create List Item, Add it to Lines, return it, and Adjust Scrollbar

  result := TPmaListItem.Create(Caption);
  FLines.Add(result);
  FScrollBar.ValueMax := Flines.Count;
end;
//------------------------------------------------------------------------------
// Clear All Items in the List
//------------------------------------------------------------------------------
procedure TPmaListView.Clear;
begin
  // Clear All Items, reset properties

  FLines.Clear;
  FLineFirst := 0;

  FSelectIndex := -1;
  FSelectColumn := -1;
  FSelectItem   := nil;
  
  FScrollBar.ValueMax := 0;
  FScrollBar.ValueCur := 0;

  self.Invalidate;
end;
//------------------------------------------------------------------------------
// Add a New Item to the List
//------------------------------------------------------------------------------
function CompareItems(Item1, Item2: Pointer): Integer;
begin
  result := TPmaListItem(Item1).Compare(Item2, SortColumn);
end;
//------------------------------------------------------------------------------
// Sort on a Specic Column
//------------------------------------------------------------------------------
procedure TPmaListView.Sort(Sender : TObject; Column : integer; Down : boolean);
begin
  if (Column >= 0) and (Column < FColumns.Count) then
    begin
      SortColumn := Column;
      SortDown   := Down;
      FLines.Sort(CompareItems);
    end;
end;
//------------------------------------------------------------------------------
// Receive Scrolling Position from ScrollBar
//------------------------------------------------------------------------------
procedure TPmaListView.OnScrollBarValue(Sender : TObject; Value : integer);
var
  NewFirst : integer;
begin
  // Make sure the new Position is within limits

  NewFirst := Max(0, Min( FLines.Count - FLineVis, Value));

  // if Changed, Set new Scrollbar position and repaint

  if (FLineFirst <> NewFirst) then
    begin
      FLineFirst := NewFirst;
      FScrollBar.ValueCur := NewFirst;

      // Paint Only Myself, not the ScrollBar

      PaintLinesRect;
    end;
end;
//------------------------------------------------------------------------------
// Set Border
//------------------------------------------------------------------------------
procedure TPmaListView.SetBorder(const Value : boolean);
begin
  if (FBorder <> Value) then
    begin
      FBorder := Value;
      FScrollBar.pBorder := false;
      self.Invalidate;
    end;
end;
//------------------------------------------------------------------------------
// Set Adjusted Column
//------------------------------------------------------------------------------
function TPmaListView.GetColumnAdjust : integer;
begin
  result := FColumns.Adjust;
end;
//------------------------------------------------------------------------------
// Set Adjusted Column
//------------------------------------------------------------------------------
procedure TPmaListView.SetColumnAdjust(const Value : integer);
begin
  FColumns.Adjust := Value;
  self.Invalidate;
end;
//------------------------------------------------------------------------------
//  Control has got Focus (needed for repaint focus change)
//------------------------------------------------------------------------------
procedure TPmaListView.DoEnter;
begin
  PaintLinesRect;
  inherited;
end;
//------------------------------------------------------------------------------
//  Control Has Lost Focus (needed for repaint focus change)
//------------------------------------------------------------------------------
procedure TPmaListView.DoExit;
begin
  PaintLinesRect;
  inherited;
end;
//------------------------------------------------------------------------------
//  Find Item at Mouse Position
//------------------------------------------------------------------------------
function TPmaListView.GetItemAt(
      const pos    : TPoint;        // Positin of Mouse
      out   Index  : integer;       // Index returned
      out   Column : integer;       // Column Returned
      out   Item   : TPmaListItem)  // Item returned
                   : boolean;       // An Item was Found
var
  CurLine    : integer;
  CurCol     : integer;
begin
  result := false;
  Index  := -1;
  Column := -1;
  Item   := nil;

  // Get Line from Y Position, and Column from Columns X

  CurLine := (Pos.Y - FLinesRect.Top) div FLineHgt;
  CurCol  := FColumns.GetColumnFromX(Pos.X);

  // Is there actually a Line/Column under Mouse

  if (CurCol >= 0) and (CurLIne < FLineVis) and 
     ((CurLine + FLineFirst) < FLines.Count) then
    begin
      result := true;
      Index  := CurLine;
      Column := CurCol;
      Item   := FLines[FLineFirst + Index] as TPmaListItem;
    end;
end;
//------------------------------------------------------------------------------
//  Return Rectangle on Index and Column
//------------------------------------------------------------------------------
function TPmaListView.GetItemRect(const Line, Column : integer): TRect;
begin
  // Get Left and Right from FColumns

  result := FColumns.ColumnRect(Column);

  // Get Top and Bottom from Line

  result.Top    := FLinesRect.Top +  Line    * FLineHgt;
  result.Bottom := FLinesRect.Top + (Line+1) * FLineHgt + 1;
end;
//------------------------------------------------------------------------------
//  Mouse Down Event
//------------------------------------------------------------------------------
procedure TPmaListView.MouseDown
          (Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  LineIndex : integer;
  ColIndex  : integer;
  pItem     : TPmaListItem;
begin
  // Make Sure the ListBox is Focused

  if (not self.Focused) and self.Enabled then
    self.SetFocus;

  // If we are Over an Item set this Selected

  if self.GetItemAt(Point(X,Y), LineIndex, ColIndex, pItem) then
    begin
      // Set Item as Selected

      FSelectIndex  := LineIndex;
      FSelectColumn := ColIndex;
      FSelectItem   := pItem;

      PaintLinesRect;
    end
  else if (FSelectIndex >= 0) then
    begin
      // Deselect any Selected Item
      FSelectIndex  := -1;
      FSelectColumn := -1;
      FSelectItem   := nil;

      PaintLinesRect;
    end;

  inherited;
end;
//------------------------------------------------------------------------------
// MouseMove
//------------------------------------------------------------------------------
procedure TPmaListView.MouseMove(Shift: TShiftState; X, Y: Integer);
var
  LineIndex : integer;
  ColIndex  : integer;
  pItem     : TPmaListItem;
begin

  // FInd Which Line and Column the Mouse is under

  if self.GetItemAt(Point(X,Y), LineIndex, ColIndex, pItem) then
    begin
      // Draw the New Rectangle if it has Changed

      if (not FOverDrawn) or
         (FOverIndex <> LineIndex) or (FOverColumn <> ColIndex) then
        begin
          if FOverDrawn then
            DrawFocusRect(self.Canvas.Handle, FOverRect);

          FOverDrawn  := true;
          FOverIndex  := LineIndex;
          FOverColumn := ColIndex;

          FOverRect := self.GetItemRect(LineIndex, ColIndex);

          DrawFocusRect(self.Canvas.Handle, FOverRect);
        end;
    end
  else
    begin
      // Draw old Rectangle away

      if FOverDrawn then
        DrawFocusRect(self.Canvas.Handle, FOverRect);
      FOverDrawn := false;
    end;

  if PtInRect(FLinesRect, Point(X,Y)) then
    self.Cursor := crDefault // crIBeam
  else
    self.Cursor := crDefault;

  inherited;
end;
//------------------------------------------------------------------------------
//  Message Leave
//------------------------------------------------------------------------------
procedure TPmaListView.MsgMouseLeave (var Message: TMessage);
begin
  if fOverDrawn then
    begin
      fOverDrawn  := false;
      FOverIndex  := -1;
      FOverColumn := -1;
      DrawFocusRect(self.Canvas.Handle, FOverRect);
    end;
end;
//------------------------------------------------------------------------------
// Mouse Wheel Event
//------------------------------------------------------------------------------
function TPmaListView.DoMouseWheel (Shift: TShiftState;
          WheelDelta: Integer; MousePos: TPoint): Boolean;
var
  Pos : TPoint;
begin
  if (WheelDelta > 0) and (FLineFirst < (FLines.Count - FLineVis)) then
    begin
      Inc(FLineFirst);
      FScrollBar.ValueCur := FLineFirst;
      PaintLinesRect;

      Pos := self.ScreenToClient(Mouse.CursorPos);
      self.MouseMove([], Pos.X, Pos.Y);
    end
  else if (WheelDelta < 0) and (FLineFirst > 0) then
    begin
      Dec(FLineFirst);
      FScrollBar.ValueCur := fLineFirst;
      PaintLinesRect;

      Pos := self.ScreenToClient(Mouse.CursorPos);
      self.MouseMove([], Pos.X, Pos.Y);
    end;

  result := true;
end;
//------------------------------------------------------------------------------
// Paint Only Text Part
//------------------------------------------------------------------------------
procedure TPmaListView.PaintLinesRect;
var
  PosY     : integer;
  Ind      : integer;
  LineRect : TRect;
  CurColor : TColor;
begin
  // Reset any Over Drawn

  fOverDrawn := false;
  FOverIndex := -1;
  FOverColumn := -1;

  //--------------------------------------------------------------------------
  // Draw Text Rect Background using Background Color

  if FBorder then
    begin
      if self.Focused then
        self.Canvas.Pen.Style := psDot
      else
        self.Canvas.Pen.Style := psSolid;
    end
  else
    begin
      if self.Focused then
        self.Canvas.Pen.Style := psClear
      else
        self.Canvas.Pen.Style := psClear;
    end;

  self.Canvas.Pen.Color := RGB(70,70,70);

  self.Canvas.Brush.Style := bsSolid;

  if (self.ParentColor) then
    CurColor := self.Parent.Brush.Color
  else
    CurColor := self.Color;

  self.Canvas.Brush.Color := CurColor;
  self.Canvas.Rectangle(FLinesRect);

  //--------------------------------------------------------------------------
  // Draw the Lines from FLinesFirst to Visible Lines

  self.Canvas.Pen.Style := psClear;
  
  PosY := FLinesRect.Top + 1;

  if (FLines.Count > 0) and (FLineHgt > 0) then
  For Ind := FLineFirst to Min(FLines.Count -1, (FLineFirst + FLineVis - 1)) do
    begin
      LineRect := Rect(
        2, PosY, self.Width - FScrollBar.Size - 3, PosY + FLineHgt);

      if (FSelectItem = FLines[Ind]) then
        self.Canvas.Brush.Color := clCream
      else
        self.Canvas.Brush.Color := CurColor;
        
      TPmaListItem(FLines[Ind]).Paint(
        self.Canvas, (FSelectItem = FLines[Ind]),
        PosY, FLinesRect.Right, FColumns, FLineHgt);

      Inc(PosY, FLineHgt);
    end;
end;
//------------------------------------------------------------------------------
// Paint
//------------------------------------------------------------------------------
procedure TPmaListView.Paint;
begin
  inherited;
  
  //--------------------------------------------------------------------------
  // Draw The Backgrond behind both Text and Scrollbar

  self.Canvas.Pen.Style   := psClear;
  self.Canvas.Brush.Style := bsSolid;

  self.Canvas.Brush.Color := self.Parent.Brush.Color;

  self.Canvas.Rectangle(Rect(0,0, self.Width, self.Height));

  //--------------------------------------------------------------------------
  // Draw the ListItems

  PaintLinesRect;
end;
//------------------------------------------------------------------------------
//  Load Ini Settings
//------------------------------------------------------------------------------
procedure TPmaListView.LoadSettings;
begin
  FColumns.LoadColumns(self.Name);
  self.Invalidate;
end;
//------------------------------------------------------------------------------
//  Save Ini Settings
//------------------------------------------------------------------------------
procedure TPmaListView.SaveSettings;
begin
  FColumns.SaveColumns(self.Name);
end;
//------------------------------------------------------------------------------
// Resize
//------------------------------------------------------------------------------
procedure TPmaListView.Resize;
begin
  if Assigned(FScrollBar) then
    begin

      CalcLineHgtAndVisible;

      // Resize the Column Header

      FColumns.Font   := self.Font;
      FColumns.Left   := 0;
      FColumns.Width  := self.Width - FScrollBar.Size;
      FColumns.Top    := 0;
      FColumns.Height := FLineHgt + 8;
      FColumns.Resize;

      // Resize the ScrollBar

      FScrollBar.ValueVis := FLIneVis;

      FScrollBar.Left   := self.Width - FScrollBar.Size;
      FScrollBar.Width  := FScrollBar.Size;
      FScrollBar.Top    := FLineHgt + 6;
      FScrollBar.Height := self.Height - (FLineHgt + 6);
      FScrollBar.Font   := self.Font;
      FScrollBar.Resize;
    end;
end;
//------------------------------------------------------------------------------
// Recalculate Line Height and Visible Lines (When Font Change)
//------------------------------------------------------------------------------
procedure TPmaListView.CalcLineHgtAndVisible;
begin
  // Get Line Hight of current Font

  self.Canvas.Font := self.Font;
  FLineHgt := self.Canvas.TextHeight('Xg9');

  FLinesRect.Left   := 0;
  FLinesRect.Right  := self.Width - FScrollBar.Width - 2;
  FLinesRect.Top    := FLineHgt + 9;
  FLinesRect.Bottom := Self.Height;

  if (FLineHgt > 0) then
    FLineVis := trunc((FLinesRect.Bottom - FLinesRect.Top) / FLineHgt)
  else
    FLineVis := 0;
end; *)
//------------------------------------------------------------------------------
//
//                                    FRAME
//
//------------------------------------------------------------------------------
// Create
//------------------------------------------------------------------------------
constructor TMemoFrame.Create(AOwner : TComponent);
begin
  inherited;

  //FLineView := TPmaListView.Create(AOwner);
  //FLineView.Parent := self;
  //FLineView.Name   := 'LineView';

  LineView.Border := false;

  LineView.DoubleBuffered := true;

  // Add the Columns
  (*
  Column := FLineView.Columns.AddColumn('Artist');
  Column.Width := 50;

  Column := FLineView.Columns.AddColumn('Year');
  Column.Width := 80;

  Column := FLineView.Columns.AddColumn('Album');
  Column.Width := 50;

  Column := FLineView.Columns.AddColumn('Nr');
  Column.Width := 50;

  Column := FLineView.Columns.AddColumn('Track');
  Column.Width := 50;

  Column := FLineView.Columns.AddColumn('Rating');
  Column.Width := 50;

  Column := FLineView.Columns.AddColumn('BitRate');
  Column.Width := 50;

  Column := FLineView.Columns.AddColumn('Length');
  Column.Width := 50;
  *)
  //FLineView.ColumnAdjust := 0;

  //FLineView.LoadSettings;

  // Add some Lines
  (*
  pItem := FLIneView.AddItem('Line 01');
  pItem.AddColumn('Col 1.1');
  pItem.AddColumn('Col 2.1');
  pItem.AddColumn('Col 3.1');
  pItem.AddColumn('Col 4.1');
  pItem.AddColumn('Col 5.1');
  pItem.AddColumn('Col 6.1');
  pItem.AddColumn('Col 7.1');

  pItem := FLIneView.AddItem('Line 02');
  pItem.AddColumn('Col 1.2');
  pItem.AddColumn('Col 2.2');
  pItem.AddColumn('Col 3.2');
  pItem.AddColumn('Col 4.2');
  pItem.AddColumn('Col 5.2');
  pItem.AddColumn('Col 6.2');
  pItem.AddColumn('Col 7.2');

  pItem := FLIneView.AddItem('Line 03');
  pItem.AddColumn('Col 1.3');
  pItem.AddColumn('Col 2.3');
  pItem.AddColumn('Col 3.3');
  pItem.AddColumn('Col 4.3');
  pItem.AddColumn('Col 5.3');
  pItem.AddColumn('Col 6.3');
  pItem.AddColumn('Col 7.3');

  pItem := FLIneView.AddItem('Line 04');
  pItem.AddColumn('Col 1.4');
  pItem.AddColumn('Col 2.4');
  pItem.AddColumn('Col 3.4');
  pItem.AddColumn('Col 4.4');
  pItem.AddColumn('Col 5.4');
  pItem.AddColumn('Col 6.4');
  pItem.AddColumn('Col 7.4');

  pItem := FLIneView.AddItem('Line 05');
  pItem.AddColumn('Col 1.5');
  pItem.AddColumn('Col 2.5');
  pItem.AddColumn('Col 3.5');
  pItem.AddColumn('Col 4.5');
  pItem.AddColumn('Col 5.5');
  pItem.AddColumn('Col 6.5');
  pItem.AddColumn('Col 7.5');

  pItem := FLIneView.AddItem('Line 06');
  pItem.AddColumn('Col 1.6');
  pItem.AddColumn('Col 2.6');
  pItem.AddColumn('Col 3.6');
  pItem.AddColumn('Col 4.6');
  pItem.AddColumn('Col 5.6');
  pItem.AddColumn('Col 6.6');
  pItem.AddColumn('Col 7.6');

  pItem := FLIneView.AddItem('Line 07');
  pItem.AddColumn('Col 1.7');
  pItem.AddColumn('Col 2.7');
  pItem.AddColumn('Col 3.7');
  pItem.AddColumn('Col 4.7');
  pItem.AddColumn('Col 5.7');
  pItem.AddColumn('Col 6.7');
  pItem.AddColumn('Col 7.7');

  pItem := FLIneView.AddItem('Line 08');
  pItem.AddColumn('Col 1.8');
  pItem.AddColumn('Col 2.8');
  pItem.AddColumn('Col 3.8');
  pItem.AddColumn('Col 4.8');
  pItem.AddColumn('Col 5.8');
  pItem.AddColumn('Col 6.8');
  pItem.AddColumn('Col 7.8');

  pItem := FLIneView.AddItem('Line 09');
  pItem.AddColumn('Col 1.9');
  pItem.AddColumn('Col 2.9');
  pItem.AddColumn('Col 3.9');
  pItem.AddColumn('Col 4.9');
  pItem.AddColumn('Col 5.9');
  pItem.AddColumn('Col 6.9');
  pItem.AddColumn('Col 7.9');

  pItem := FLIneView.AddItem('Line 10');
  pItem.AddColumn('Col 1.10');
  pItem.AddColumn('Col 2.10');
  pItem.AddColumn('Col 3.10');
  pItem.AddColumn('Col 4.10');
  pItem.AddColumn('Col 5.10');
  pItem.AddColumn('Col 6.10');
  pItem.AddColumn('Col 7.10');

  pItem := FLIneView.AddItem('Line 11');
  pItem.AddColumn('Col 1.11');
  pItem.AddColumn('Col 2.11');
  pItem.AddColumn('Col 3.11');
  pItem.AddColumn('Col 4.11');
  pItem.AddColumn('Col 5.11');
  pItem.AddColumn('Col 6.11');
  pItem.AddColumn('Col 7.11');

  pItem := FLIneView.AddItem('Line 12');
  pItem.AddColumn('Col 1.12');
  pItem.AddColumn('Col 2.12');
  pItem.AddColumn('Col 3.12');
  pItem.AddColumn('Col 4.12');
  pItem.AddColumn('Col 5.12');
  pItem.AddColumn('Col 6.12');
  pItem.AddColumn('Col 7.12');

  pItem := FLIneView.AddItem('Line 13');
  pItem.AddColumn('Col 1.13');
  pItem.AddColumn('Col 2.13');
  pItem.AddColumn('Col 3.13');
  pItem.AddColumn('Col 4.13');
  pItem.AddColumn('Col 5.13');
  pItem.AddColumn('Col 6.13');
  pItem.AddColumn('Col 7.13');

  pItem := FLIneView.AddItem('Line 14');
  pItem.AddColumn('Col 1.14');
  pItem.AddColumn('Col 2.14');
  pItem.AddColumn('Col 3.14');
  pItem.AddColumn('Col 4.14');
  pItem.AddColumn('Col 5.14');
  pItem.AddColumn('Col 6.14');
  pItem.AddColumn('Col 7.14');

  pItem := FLIneView.AddItem('Line 15');
  pItem.AddColumn('Col 1.15');
  pItem.AddColumn('Col 2.15');
  pItem.AddColumn('Col 3.15');
  pItem.AddColumn('Col 4.15');
  pItem.AddColumn('Col 5.15');
  pItem.AddColumn('Col 6.15');
  pItem.AddColumn('Col 7.15');

  pItem := FLIneView.AddItem('Line 16');
  pItem.AddColumn('Col 1.16');
  pItem.AddColumn('Col 2.16');
  pItem.AddColumn('Col 3.16');
  pItem.AddColumn('Col 4.16');
  pItem.AddColumn('Col 5.16');
  pItem.AddColumn('Col 6.16');
  pItem.AddColumn('Col 7.16');

  pItem := FLIneView.AddItem('Line 17');
  pItem.AddColumn('Col 1.17');
  pItem.AddColumn('Col 2.17');
  pItem.AddColumn('Col 3.17');
  pItem.AddColumn('Col 4.17');
  pItem.AddColumn('Col 5.17');
  pItem.AddColumn('Col 6.17');
  pItem.AddColumn('Col 7.17');
  *)
end;
//------------------------------------------------------------------------------
// Destroy
//------------------------------------------------------------------------------
destructor TMemoFrame.Destroy;
begin
  //FLineView.SaveSettings;
  
  inherited;
end;
//------------------------------------------------------------------------------
// Resize
//------------------------------------------------------------------------------
procedure TMemoFrame.FrameResize(Sender: TObject);
begin
  LineView.Left   := 0;
  LineView.Width  := self.ClientWidth;
  LineView.Top    := 0;
  LineView.Height := self.Height;

  LIneView.Font := self.Font;
  LineView.Resize;
end;
//------------------------------------------------------------------------------
// Perform a
//------------------------------------------------------------------------------
end.
