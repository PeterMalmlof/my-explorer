unit TProgBaseUnit;

interface

uses
  Windows, Classes, Contnrs, ComCtrls, Menus,

  TGenLogUnit,        // Log Object
  TTreeItemBaseUnit; // Base Class

//------------------------------------------------------------------------------
//  Tree Item Base Object
//------------------------------------------------------------------------------
type TProgBase = Class(TTreeItemBase)
  protected
    objDesc      : string;         // Description

    procedure SetDesc(const Value : string);

  public
    class function  GetPropCount: integer; override;
    class function  GetPropEdit (const Col : integer): boolean; override;
    class function  GetPropName (const Col : integer): string; override;
          function  GetPropValue(const Col : integer): string; override;
          procedure SetPropValue(const Col : integer; const Txt : string); override;

    property pDesc    : string    read objDesc      write SetDesc;
end;

//------------------------------------------------------------------------------
//                                  IMPLEMENTATION
//------------------------------------------------------------------------------
implementation

uses
  SysUtils,
  StrUtils,
  Forms,

  TGenTextFileUnit,   // Textfile Class
  TMediaPlayerResUnit,

  TGenClassesUnit;    // Class Management

//------------------------------------------------------------------------------
// Set a New Name
//------------------------------------------------------------------------------
procedure TProgBase.SetDesc(const Value : string);
begin
  if (not AnsiSameStr(objDesc, Value)) then
    begin
      TheLog.Log('SetDesc Dirty ' + objDesc + ' to ' + Value);
      self.pDirty := true;
      objDesc     := Value;
    end;
end;
//------------------------------------------------------------------------------
// Return Number of Colums in Item Class
//------------------------------------------------------------------------------
class function TProgBase.GetPropCount: integer;
begin
  result := 2;
end;
//------------------------------------------------------------------------------
//  Return true if a specific Column is InlineEditable in ListView
//------------------------------------------------------------------------------
class function TProgBase.GetPropEdit(const Col : integer): boolean;
begin
  case Col of
    0 : result := true;
    1 : result := true;
  else
    result := false;
  end;
end;
//------------------------------------------------------------------------------
//  Get Column Text
//------------------------------------------------------------------------------
class function TProgBase.GetPropName(const Col : integer): string;
begin
  result := '';
  Case Col of
    0 : result := resProgItemType;
    1 : result := resDesc;
  end;
end;
//------------------------------------------------------------------------------
//  Get Column Text
//------------------------------------------------------------------------------
function TProgBase.GetPropValue(const Col : integer): string;
begin
  result := '';
  Case Col of
    0 : result := self.pName;
    1 : result := self.pDesc;
  end;
end;
//------------------------------------------------------------------------------
//  Update Coumn Text
//------------------------------------------------------------------------------
procedure TProgBase.SetPropValue(const Col : integer; const Txt : string);
begin
  Case Col of
    0 : self.pName := Txt;
    1 : self.pDesc := Txt;
  end;
end;
//------------------------------------------------------------------------------
//                                    INITIALIZE
//------------------------------------------------------------------------------
initialization

  RegClass(TProgBase);
end.

