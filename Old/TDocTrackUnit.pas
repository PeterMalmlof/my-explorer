unit TDocTrackUnit;

interface

uses
  Windows, Classes, Contnrs, ComCtrls, Menus,

  TGenLogUnit,        // Log Object
  TWmMsgFactoryUnit,  // Message Pump
  TMyMessageUnit,     // Application Messages
  TMediaTrackUnit,
  TDocMediaUnit,      // Item Base Class
  TTreeItemBaseUnit;  // Item Base Class

//------------------------------------------------------------------------------
//  Docuemnt Item
//------------------------------------------------------------------------------
type TDocTrack = Class(TDocMedia)
  protected
    objLoaded : boolean;      // True if Loaded
    objMedia  : TMediaTrack;  // Reference to Media Track

    procedure LoadTrackData;

    function  GetPath : string; override;

    function GetArtist     : string;
    function GetYear       : string;
    function GetYearInt    : integer;
    function GetAlbum      : string;
    function GetTrack      : string;
    function GetTrackNo    : string;
    function GetTrackNoInt : integer;
    function GetGenre      : string;
    function GetRating     : string;
    function GetLength     : double;
    function GetBitRate    : DWORD;

    procedure PlayTrack(Sender : TObject);
  public
    // Constructor

    constructor Create(
      const pParent   : TTreeItemBase;
      const Name      : string;
      const nType     : integer;
      const nSize     : int64;
      const tCre      : TFileTime;
      const tMod      : TFIleTime;
      const bReadonly : boolean;
      const bHidden   : boolean); override;

    constructor CreateFromMedia(
      const pParent : TTreeItemBase;
      const Name    : string;
      const pTrack  : TMediaTrack);

    class function GetTypeStr: string;  override;
    class function GetImageId: integer; override;

    // Perform the default Action of this Item

    procedure Execute(const Col : integer); override;

    // Add ListView Menu

    procedure AddListMenuItems(
      const PopupMenu : TPopupMenu;
      const Column    : integer); override;

    // Properties By Column

    class function  GetPropCount: integer; override;

    class function  GetPropJust  (const Col : integer): TItemJust; override;
    class function  GetPropName  (const Col : integer): string;    override;
          function  GetPropValue (const Col : integer): string;    override;

    // Compare this Item with another (used for sorting ListView)

    function  Compare(
      const pItem : TTreeItemBase;
      const Col   : integer): integer; override;

    // Properties of a Track

    property pArtist     : string   read GetArtist;
    property pYear       : string   read GetYear;
    property pYearInt    : integer  read GetYearInt;
    property pAlbum      : string   read GetAlbum;
    property pTrack      : string   read GetTrack;
    property pTrackNo    : string   read GetTrackNo;
    property pTrackNoInt : integer  read GetTrackNoInt;
    property pGenre      : string   read GetGenre;
    property pRating     : string   read GetRating;
    property pLength     : double   read GetLength;
    property pBitRate    : DWORD    read GetBitRate;

    property pMediaTRack : TMediaTrack read objMedia;
end;
//------------------------------------------------------------------------------
//                                  IMPLEMENTATION
//------------------------------------------------------------------------------
implementation

uses
  SysUtils,
  StrUtils,
  Math,
  
  TGenStrUnit,        // String Functions
  TGenMenuUnit,
  TGenTrackTagUnit,
  TGenTextFileUnit,
  MyFileTimeUnit,

  TMediaPlayerResUnit,// Resource Strings

  TMediaFactoryUnit,  // Media Factory

  TGenClassesUnit;    // Class Management

//------------------------------------------------------------------------------
// Create
//------------------------------------------------------------------------------
constructor TDocTrack.Create(
      const pParent   : TTreeItemBase;
      const Name      : string;
      const nType     : integer;
      const nSize     : int64;
      const tCre      : TFileTime;
      const tMod      : TFIleTime;
      const bReadonly : boolean;
      const bHidden   : boolean);
begin
  inherited;

  TheLog.Log('TDocTrack.Create "' + Name + '"');

  objMedia  := nil;
  objLoaded := false;

  LoadTrackData;
end;
//------------------------------------------------------------------------------
// Create
//------------------------------------------------------------------------------
constructor TDocTrack.CreateFromMedia(
      const pParent : TTreeItemBase;
      const Name    : string;
      const pTrack  : TMediaTrack);
begin
  inherited Create(pParent, Name, 0, 0, MyFileTimeZero, MyFileTimeZero, false, false);

  objMedia  := pTrack;
  objLoaded := true;
end;
//------------------------------------------------------------------------------
// Get Track Path
//------------------------------------------------------------------------------
function TDocTrack.GetPath: string;
begin
  if Assigned(objMedia) then
    result := objMedia.pPath
  else
    result := inherited GetPath;
end;
//------------------------------------------------------------------------------
// Get Type String with ReadOnly and Hidden Flgs attached
//------------------------------------------------------------------------------
class function TDocTrack.GetTypeStr: string;
begin
  result := resDocTrackType;
end;
//------------------------------------------------------------------------------
// Return What Class Items Childrens are
//------------------------------------------------------------------------------
class function TDocTrack.GetImageId:integer;
begin
  result := ImageIdAudio;
end;
//------------------------------------------------------------------------------
// Load Track Info
//------------------------------------------------------------------------------
procedure TDocTrack.LoadTrackData;
begin
  if (not objLoaded) then
    begin
      objLoaded := true;

      TheLog.Log('TDocTrack.LoadTrackData "' + self.pPath + '"');

      // Update the Media Library with this Track

      objMedia := TheMedia.FindTrackByPath(self.pPath);
    end;
end;
//------------------------------------------------------------------------------
// Get Property
//------------------------------------------------------------------------------
function TDocTrack.GetArtist  : string;
begin
  if Assigned(objMedia) then
    result := objMedia.pArtist
  else
    result := '';
end;
//------------------------------------------------------------------------------
function TDocTrack.GetYear   : string;
var
  Value : integer;
begin
  if Assigned(objMedia) then
    begin
      Value := objMedia.pYear;
      if (Value > 0) then
        result := IntToStr(Value)
      else
        result := '';
    end
  else
    result := '';
end;
//------------------------------------------------------------------------------
function TDocTrack.GetYearInt   : integer;
begin
  if Assigned(objMedia) then
    result := objMedia.pYear
  else
    result := 0;
end;
//------------------------------------------------------------------------------
function TDocTrack.GetAlbum   : string;
begin
  if Assigned(objMedia) then
    result := objMedia.pAlbum
  else
    result := '';
end;
//------------------------------------------------------------------------------
function TDocTrack.GetTrack   : string;
begin
  if Assigned(objMedia) then
    result := objMedia.pTrack
  else
    result := '';
end;
//------------------------------------------------------------------------------
function TDocTrack.GetTrackNo : string;
var
  Value : integer;
begin
  if Assigned(objMedia) then
    begin
      Value := objMedia.pNumber;
      if (Value > 0) then
        result := IntToStr(Value)
      else
        result := '';
    end
  else
    result := '';
end;
//------------------------------------------------------------------------------
function TDocTrack.GetTrackNoInt : integer;
begin
  if Assigned(objMedia) then
    result := objMedia.pNumber
  else
    result := 0;
end;
//------------------------------------------------------------------------------
function TDocTrack.GetGenre   : string;
begin
  if Assigned(objMedia) then
    result := objMedia.pGenre
  else
    result := '';
end;
//------------------------------------------------------------------------------
function TDocTrack.GetRating : string;
var
  Value : integer;
begin
  if Assigned(objMedia) then
    begin
      Value := objMedia.pRating;
      if (Value > 0) then
        result := IntToStr(Value)
      else
        result := '';
    end
  else
    result := '';
end;
//------------------------------------------------------------------------------
function TDocTrack.GetLength  : double;
begin
  if Assigned(objMedia) then
    result := objMedia.pLength
  else
    result := 0;
end;
//------------------------------------------------------------------------------
function TDocTrack.GetBitRate : DWORD;
begin
  if Assigned(objMedia) then
    result := objMedia.pBitrate
  else
    result := 0;
end;
//------------------------------------------------------------------------------
//
//                                COLUMN PROPERTY
//
//------------------------------------------------------------------------------
// Return Number of Colums in Item Class
//------------------------------------------------------------------------------
class function TDocTrack.GetPropCount: integer;
begin
  result := 9;
end;
//------------------------------------------------------------------------------
//  Get the Property (text) of a specified Column in ListView
//------------------------------------------------------------------------------
class function TDocTrack.GetPropName(const Col : integer): string;
begin
  case Col of
    0 : result := resDocTrackArtist;
    1 : result := resDocTrackYear;
    2 : result := resDocTrackAlbum;
    3 : result := resDocTrackNo;
    4 : result := resDocTrackTrack;
    5 : result := resDocTrackRating;
    6 : result := resDocTrackBitrate;
    7 : result := resDocTrackLength;
    8 : result := resDocTrackGenre;
  end;
end;
//------------------------------------------------------------------------------
//  Get the Property (text) of a specified Column in ListView
//------------------------------------------------------------------------------
function TDocTrack.GetPropValue(const Col : integer): string;
begin
  case Col of
    0 : result := self.pArtist;
    1 : result := self.pYear;
    2 : result := self.pAlbum;
    3 : result := self.pTrackNo;
    4 : result := self.pTrack;
    5 : result := self.pRating;
    6 : result := TGenStrUnit.SizeToStr(self.pBitRate,'') + 'Kbps';
    7 : result := TGenStrUnit.ToTime(round(self.pLength));
    8 : result := self.pGenre;
  else
    result := self.pNiceName;
  end;
end;
//------------------------------------------------------------------------------
// Get the Property (string) of a specified Property Index
//------------------------------------------------------------------------------
class function TDocTrack.GetPropJust(const Col : integer): TItemJust;
begin
  case Col of
    0 : result := ijLeft;
    1 : result := ijCenter;
    2 : result := ijLeft;
    3 : result := ijCenter;
    4 : result := ijLeft;
    5 : result := ijCenter;
    6 : result := ijRight;
    7 : result := ijRight;
    8 : result := ijLeft;
  else
    result := ijLeft;
  end;
end;
//------------------------------------------------------------------------------
// Compare this Item with another (used for sorting ListView)
//------------------------------------------------------------------------------
function TDocTrack.Compare(
      const pItem : TTreeItemBase;
      const Col   : integer): integer;
begin
  if (pItem is TDocTrack) then
    case Col of
      0 : result := AnsiCompareStr(self.pArtist, TDocTrack(pItem).pArtist);
      1 : result := CompareValue(self.pYearInt, TDocTrack(pItem).pYearInt);
      2 : result := AnsiCompareStr(self.pAlbum, TDocTrack(pItem).pAlbum);

      3 : result := CompareValue(self.pTrackNoInt, TDocTrack(pItem).pTrackNoInt);

      4 : result := AnsiCompareStr(self.pTrack, TDocTrack(pItem).pTrack);
      5 : result := AnsiCompareStr(self.pRating, TDocTrack(pItem).pRating);
      6 : result := CompareValue(self.pBitRate, TDocTrack(pItem).pBitRate);
      7 : result := CompareValue(self.pLength, TDocTrack(pItem).pLength);
      8 : result := AnsiCompareStr(self.pGenre, TDocTrack(pItem).pGenre);
    else
      result := inherited Compare(pItem, Col);
    end
  else
    result := inherited Compare(pItem, Col);
end;


//------------------------------------------------------------------------------
// Play This Track
//------------------------------------------------------------------------------
procedure TDocTrack.Execute(const Col : integer);
var
  p : PAnsiChar;
begin
  self.pPlayed := true;

  if (length(self.pPath) > 0) and (length(self.pPath) < 1024) then
    begin
      TheLog.Log('PlayThis ' + self.pPath);

      GetMem(p, 1024);
      StrLCopy(p,PAnsiChar(self.pPath), 1023);

      //PostMsg(MSG_PLAYER_PLAY_THIS, WP_CMD_SET, DWORD(p));

      // Note the Message must be delivered and memory freed
    end;
end;
//------------------------------------------------------------------------------
// Add Menus accoding to Current Selected Item in TreeView
//------------------------------------------------------------------------------
procedure TDocTrack.AddListMenuItems(
      const PopupMenu : TPopupMenu;
      const Column    : integer);
var
  pMenu : TGenMenu;
begin
  inherited AddListMenuItems(PopupMenu, Column);

  // Add Play This Track

  pMenu := TGenMenu.Create(PopupMenu);
  pMenu.Caption := 'Play Track';
  pMenu.OnClick := PlayTrack;
  PopupMenu.Items.Add(pMenu);
end;
//------------------------------------------------------------------------------
// Play This Track
//------------------------------------------------------------------------------
procedure TDocTrack.PlayTrack(Sender : TObject);
begin
  self.Execute(0);
end;
//------------------------------------------------------------------------------
//                                    INITIALIZE
//------------------------------------------------------------------------------
initialization
  RegClass(TDocTrack);
end.

