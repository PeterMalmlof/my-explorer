unit TVerbListUnit;

interface

uses
  Windows, Classes, Contnrs, ComCtrls, Menus,

  TGenLogUnit,        // Log Object
  TProgBaseUnit,      // Base Class
  TTreeItemBaseUnit; // Base Class

//------------------------------------------------------------------------------
//  Tree Item Base Object
//------------------------------------------------------------------------------
type TVerbList = Class(TProgBase)
  protected

    procedure AddNewChild (Sender: TObject); override;

  public
    constructor Create;  reintroduce;

    destructor  Destroy; override;

    class function  ChildrenClass: TItemClass; override;
    class function  GetTypeStr: string; override;

    procedure Save; override;
    procedure Load; override;

    class function GetPropCount: integer; override;
          function GetPropValue(const Col : integer): string; override;

    procedure AddTreeMenuItems(const PopupMenu : TPopupMenu); override;
end;

//------------------------------------------------------------------------------
//                                  IMPLEMENTATION
//------------------------------------------------------------------------------
implementation

uses
  SysUtils,
  StrUtils,
  Forms,

  TGenTextFileUnit,   // Textfile Class
  TGenMenuUnit,       // TGenMenu Class

  TMyMessageUnit,     // App Messages
  TMediaPlayerResUnit,// Resource Strings
  TVerbItemUnit,      // Program Group Class
  TGenClassesUnit;    // Class Management

const
  VerbListFileName = 'My Verbs.txt';

//------------------------------------------------------------------------------
// Create
//------------------------------------------------------------------------------
constructor TVerbList.Create;
begin
  inherited Create(nil, resVerbListName);

  objDesc := resVerbListDesc;

  // Load Verbs

  self.Load;
end;
//------------------------------------------------------------------------------
// Destroy
//------------------------------------------------------------------------------
destructor TVerbList.Destroy;
begin

  inherited;
end;
//------------------------------------------------------------------------------
// Return What Class Items Childrens are
//------------------------------------------------------------------------------
class function TVerbList.ChildrenClass: TItemClass;
begin
  result := TVerbItem;
end;
//------------------------------------------------------------------------------
// Get Type String with ReadOnly and Hidden Flgs attached
//------------------------------------------------------------------------------
class function TVerbList.GetTypeStr: string;
begin
  result := resVerbListType;
end;
//------------------------------------------------------------------------------
// Save This Item
//------------------------------------------------------------------------------
procedure TVerbList.Save;
var
  TF : TGenTextFile;
begin
  EXIT;
  TheLog.Log('TVerbList Saved');

  // Open a File to save things in

  TF := TGenTextFile.CreateForWrite(ApplicationFile(VerbListFileName));

  // Save All Items

  objList.SaveToFile(TF);

  TF.Free;
end;
//------------------------------------------------------------------------------
// Load This Item
//------------------------------------------------------------------------------
procedure TVerbList.Load;
var
  TF     : TGenTextFile;
  sProp  : string;
  sValue : string;
begin
  // Open a File to read all Application Groups

  TF := TGenTextFile.CreateForRead(ApplicationFile(VerbListFileName));

  // Walk all Lines and Create Objects

  while TF.ReadProp(sProp, sValue) do
    begin
      // Is this the Start of a new Program Group

      if AnsiSameText(PropVerbItem, sProp) then
        begin
          // Create a New Extension Item

          objList.AddItem(TVerbItem.CreateFromFile(self, TF));
        end;
    end;
      
  TF.Free;
end;
//------------------------------------------------------------------------------
// Return Number of Colums in Item Class
//------------------------------------------------------------------------------
class function TVerbList.GetPropCount: integer;
begin
  result := 2;
end;
//------------------------------------------------------------------------------
//  Get the Property (text) of a specified Column in ListView
//------------------------------------------------------------------------------
function TVerbList.GetPropValue(const Col : integer): string;
begin
  case Col of
    0 : result := self.pName;
    1 : result := self.pDesc;
  else
    result := self.pName;
  end;
end;
//------------------------------------------------------------------------------
// Add Menus accoding to Current Selected Item in TreeView
//------------------------------------------------------------------------------
procedure TVerbList.AddTreeMenuItems(const PopupMenu : TPopupMenu);
var
  pMenu : TGenMenu;
begin
  inherited AddTreeMenuItems(PopupMenu);

  // Add New Child

  pMenu := TGenMenu.Create(PopupMenu);
  pMenu.Caption := resAdd + SPC + TVerbItem.GetTypeStr;
  pMenu.OnClick := AddNewChild;
  PopupMenu.Items.Add(pMenu);
end;
//------------------------------------------------------------------------------
// Menu: Add a Child Item
//------------------------------------------------------------------------------
procedure TVerbList.AddNewChild(Sender: TObject);
begin
  objList.AddItem(TVerbItem.Create(self, resNew + SPC + TVerbItem.GetTypeStr));
  self.pDirty := true;

  //PostMessage(Application.Handle, MSG_TREEVIEW_REFRESH, 0, 0);
  //PostMessage(Application.Handle, MSG_LISTVIEW_REFRESH, 0, 0);
end;
//------------------------------------------------------------------------------
//                                    INITIALIZE
//------------------------------------------------------------------------------
initialization

  RegClass(TVerbList);
end.

 