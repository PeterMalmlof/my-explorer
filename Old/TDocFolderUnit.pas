unit TDocFolderUnit;

interface

uses
  Windows, Classes, Contnrs, ComCtrls,

  TGenLogUnit,        // Log Object
  TGenTextFileUnit,  // TextFile Class
  TDocItemUnit,      // Item Base Class
  TTreeItemBaseUnit; // Item Base Class

//------------------------------------------------------------------------------
//  Docuemnt Item
//------------------------------------------------------------------------------
type TDocFolder = Class(TDocItem)
  protected

  public

    class function ChildrenClass: TItemClass; override;
    class function GetTypeStr: string;  override;
    class function GetImageId: integer; override;

    procedure RefreshChildren;override;

    // Rebuild Media Content of this Folder

    procedure MediaDbRebuild
      (var Esc : boolean; var Count, Loop : integer); override;

    function Compare(
      const pItem : TTreeItemBase;
      const Col   : integer): integer; override;

    class function IsTreeNode: boolean; override;
end;

//------------------------------------------------------------------------------
//                                  IMPLEMENTATION
//------------------------------------------------------------------------------
implementation

uses
  SysUtils,
  StrUtils,
  Forms,

  TGenStrUnit,        // String Functions
  TGenShellUnit,      // Shell Functions
  MyFileTimeUnit,     // TFileTime Function
  TGenFindFilesUnit,  // FindFile Class

  TMediaPlayerResUnit,// Resource Strings
  TMediaFactoryUnit,  // Media Factory
  TMediaTrackunit,    // Track Media
  TMediaPlayListUnit, // PlayList Media
  
  TGenClassesUnit;    // Class Management

//------------------------------------------------------------------------------
// Should it have TreeNodes. Only Folders Has TreeNodes
//------------------------------------------------------------------------------
class function TDocFolder.IsTreeNode: boolean;
begin
  result := true;
end;
//------------------------------------------------------------------------------
// Return What Class Items Childrens are
//------------------------------------------------------------------------------
class function TDocFolder.ChildrenClass: TItemClass;
begin
  result := TDocItem;
end;
//------------------------------------------------------------------------------
// Get Type String with ReadOnly and Hidden Flgs attached
//------------------------------------------------------------------------------
class function TDocFolder.GetTypeStr: string;
begin
  result := resDocFolderType;
end;
//------------------------------------------------------------------------------
// Return What Class Items Childrens are
//------------------------------------------------------------------------------
class function TDocFolder.GetImageId:integer;
begin
  result := ImageIdFolder;
end;
//------------------------------------------------------------------------------
// Compare two Items (Used for sorting)
//------------------------------------------------------------------------------
function TDocFolder.Compare(
      const pItem : TTreeItemBase;
      const Col   : integer): integer;
begin
  if (pItem is TDocFolder) then
    result := AnsiCompareStr(self.GetPropValue(Col), pItem.GetPropValue(Col))
  else
    result := -1;  // Folders are always first
end;
//------------------------------------------------------------------------------
// Add Item Childrens to ListVoew
//------------------------------------------------------------------------------
procedure TDocFolder.RefreshChildren;
var
  fObj  : TGenFindFiles;
  pItem : TTreeItemBase;
begin
  TheLog.Log('Refresh Folder ' + self.pPath);

  // First Unmark all Current Children

  objList.pMark := false;

  //thelog.Log('RefreshChildren ' + GetPath);

  fObj := TGenFindFiles.Create(GetPath);
  while fObj.GetNext do
    begin
      //thelog.Log('Got ' + fObj.pName);

      // Find out if the Item already exist as Children of this Item

      pItem := objList.FindItemHereFromName(fObj.pName);
      if (pItem <> nil) and (not pItem.pMark) then
        begin
          pItem.pMark := true;
        end
      else
        begin
          // Create a new Item

          pITem := TDocItem.CreateItem(self,
            fObj.pName, fObj.pType, fObj.pSize, fObj.pCre, fObj.pMod,
            fObj.pReadonly, fObj.pHidden);

          pItem.pMark := true;

          objList.AddItem(pItem);
        end;
    end;

  fObj.Free;

  // Remove all Unmarked Items

  objList.RemoveUnmarked;

  objList.SortList;
end; 
//------------------------------------------------------------------------------
// Walk all Files of this Folder and rebuild Media Db for all Media Files
// Walk also all folders recusivly and rubuild them too
//------------------------------------------------------------------------------
procedure TDocFolder.MediaDbRebuild
  (var Esc : boolean; var Count, Loop : integer);

  procedure WaitALittle;
    begin
      Application.ProcessMessages;
      Inc(Loop);
      if (Loop > 5) then
        begin
          Loop := 0;
          Sleep(1);
        end;
    end;
var
  fObj  : TGenFindFiles;
  pItem : TTreeItemBase;
begin
  //TheLog.Log('Rebuild Folder ' + self.pPath);

  // a) Start the Find Files query

  fObj := TGenFindFiles.Create(self.pPath);
  while fObj.GetNext do
    begin
      if Esc then BREAK;

      // Test if this is a Folder

      if (fObj.pType = ftFolder) then
        begin
          // Create a new Item Folder

          pITem := TDocItem.CreateItem(self,
            fObj.pName, fObj.pType, fObj.pSize, fObj.pCre, fObj.pMod,
            fObj.pReadonly, fObj.pHidden);

          // Add the Folder as Children to this Folder

          objList.AddItem(pItem);

          // Refresh All Tracks in This New Folder (recusivly)

          pItem.MediaDbRebuild(Esc, Count, Loop);

          WaitALittle;
        end

      // This is a File, figure out if its a Track Media

      else if AnsiSameText(ExtractFileExt(fObj.pName), TrackExt) then
        begin
          Inc(Count);

          // Find the MediaTrack or Create it

          TheMedia.FindTrackByPath(fObj.pPath);

          // Note we dont create an Track Item when doing this

          WaitALittle;
        end

      // Is it a PlayList

      else if AnsiSameText(ExtractFileExt(fObj.pName), PlayListExt) then
        begin
          Inc(Count);

          // Find the Media Playlist or Create it

          TheMedia.FindPlayListByPath(fObj.pPath);

          // Note we dont create an Track Item when doing this

          WaitALittle;
        end;
    end;

  // Free The Find Files Query

  fObj.Free;
end;
//------------------------------------------------------------------------------
//                                    INITIALIZE
//------------------------------------------------------------------------------
initialization
  RegClass(TDocFolder);
end.

