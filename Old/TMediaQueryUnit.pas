unit TMediaQueryUnit;

interface

uses
  Windows,            // Lots...
  SysUtils,           // Lots...
  StrUtils,           // Strings

  TMediaUtilsUnit,    // Media Utilities
  TMediaBaseUnit;     // Base Class

//------------------------------------------------------------------------------
//  TMediaQuery
//------------------------------------------------------------------------------
type
  // Method Address callback used for returning Tracks found in Query Search
  
  TMAOnTrack = procedure(Track : TMediaBase) of object;

  TMediaQuery = class(TMediaBase)
  protected
    objArtist : string;   // Artist Search Criteria
    objYear   : string;   // Year   Search Criteria
    objAlbum  : string;   // Album  Search Criteria
    objNumber : string;   // Number Search Criteria
    objTrack  : string;   // Track  Search Criteria
    objRating : string;   // Rating Search Criteria
    objGenre  : string;   // Genre  Search Criteria

    objOnTrack : TMAOnTrack; // Method Address CallBack

    //--------------------------------------------------------------------------
    // Get & Set
    //--------------------------------------------------------------------------

    function  GetArtist: string;
    procedure SetArtist(const Value : string);

    function  GetYear: string;
    procedure SetYear(const Value : string);

    function  GetAlbum: string;
    procedure SetAlbum(const Value : string);

    function  GetNumber: string;
    procedure SetNumber(const Value : string);

    function  GetTrack: string;
    procedure SetTrack(const Value : string);

    function  GetRating: string;
    procedure SetRating(const Value : string);

    function  GetGenre: string;
    procedure SetGenre(const Value : string);

    function  GetOnTrack: TMAOnTrack;
    procedure SetOnTrack(const Value : TMAOnTrack);

    // Fire Track Found Event

    procedure DoTrackFound(Track : TMediaBase);

    //--------------------------------------------------------------------------
    // Load Property when loading from Media Db
    //--------------------------------------------------------------------------

    // Set default Properties when creating an Object

    procedure CreateProp; override;

    function  LoadProp(
      const sProp  : string;  // Property Name as String
      const PropId : integer; // Property Name as Id
      const sValue : string)  // Property Value as string
                   : boolean; // True if resolved
                     override;
  public
    // Create Playlist Query object (one for each Playlist)

    constructor Create(
      const Parent : TMediaBase;
      const Artist, Year, Album, Number, Track, Rating, Genre : string;
      MATrackFound : TMAOnTrack); reintroduce;

    destructor Destroy; override;

    //--------------------------------------------------------------------------
    // Refresh
    //--------------------------------------------------------------------------

    procedure RefreshMedia; override;

    procedure CopyQuery(const Query : TMediaQuery);

    function  IsValid: boolean;

    //--------------------------------------------------------------------------
    // Media Property Management
    //--------------------------------------------------------------------------

    // Get Number of Media Properties

    class function MediaPropCount: integer; override;

    // Get Image Id of this Media

    class function MediaImage: integer; override;

    // Get Media Property Name and Justification

    class function MediaPropName(
      var Iter : integer;  // Column Index
      out Name : string;   // Column Name
      out Just : integer)  // Column Adjust
               : boolean;  // True
                 override;

    // Return True if Media Property is Editable by User

    class function MediaPropAllow(const Ind : integer): boolean; override;

    // Validate a Media Property

    class function MediaPropValidate(
      const Ind   : integer; // Property Index
      var   Value : string)  // Property Value (can Change)
                  : boolean; // True if Value is Valid
                    override;

    // Get Media Property Value as a String

    function MediaPropValue(const Ind : integer): string; overload; override;

    // Set Media Property Value from a String

    function MediaPropValue(
      const Ind   : integer; // Property Index
      const Value : string)  // Property Value
                  : boolean; // Value was Used
                    overload; override;

    // Compare Media Properties by Index

    function MediaPropCompare(
      const Media : TMediaBase;
      const Ind   : integer): integer; override;

    //--------------------------------------------------------------------------
    // Saving & Loading
    //--------------------------------------------------------------------------

    // Save Query to Media Db

    procedure SavePropToMediaDb (const TF : TMediaTextFile); override;

    // Save Query to PlayList File

    procedure SaveToPlayList   (const TF : TMediaTextFile);

    // Load Query Property from PlayList File

    function LoadFromPlayList (const Prop : string): boolean;

    // Add Properties to File Object

    class procedure AddPropToFile(const TF : TMediaTextFile);

    //--------------------------------------------------------------------------
    // Debugging
    //--------------------------------------------------------------------------

    procedure LogIt; override;

    //--------------------------------------------------------------------------
    // Properties
    //--------------------------------------------------------------------------

    property pArtist : string  read GetArtist   write SetArtist;
    property pYear   : string  read GetYear     write SetYear;
    property pAlbum  : string  read GetAlbum    write SetAlbum;
    property pNumber : string  read GetNumber   write SetNumber;
    property pTrack  : string  read GetTrack    write SetTrack;
    property pRating : string  read GetRating   write SetRating;
    property pGenre  : string  read GetGenre    write SetGenre;

    // Callback Method Address for Track Found Event

    property OnTrackFound : TMAOnTrack read  GetOnTrack write SetOnTrack;

  end;

const
  // PlayList Query Media Db Properties

  propScArtist = 'ScAr';
  propScYear   = 'ScYe';
  propScAlbum  = 'ScAl';
  propScNumber = 'ScNu';
  propScTrack  = 'ScTr';
  propScRating = 'ScRa';
  propScGenre  = 'ScGe';

  propIdScArtist = 50;
  propIdScYear   = 51;
  propIdScAlbum  = 52;
  propIdScNumber = 53;
  propIdScTrack  = 54;
  propIdScRating = 55;
  propIdScGenre  = 56;

implementation

uses
  TMediaFactoryUnit, // Logging etc.
  TGenClassesUnit;

//------------------------------------------------------------------------------
// Properties used when Saving a Playlist Query to File
//------------------------------------------------------------------------------
const
  propArtist = 'Artist';
  propYear   = 'Year';
  propAlbum  = 'Album';
  propNumber = 'Number';
  propTrack  = 'Track';
  propRating = 'Rating';
  propGenre  = 'Genre';

  propStart  = '#';
  PropEql    = '=';

//------------------------------------------------------------------------------
//
//                                   MEDIA QUERY
//
//------------------------------------------------------------------------------
// Create PlayList Query
//------------------------------------------------------------------------------
constructor TMediaQuery.Create(
      const Parent : TMediaBase;
      const Artist, Year, Album, Number, Track, Rating, Genre : string;
        MATrackFound : TMAOnTrack);
begin
  inherited Create(Parent, '');

  objCS.BeginWrite;

  objArtist := Artist;
  objYear   := Year;
  objAlbum  := Album;
  objNumber := Number;
  objTrack  := Track;
  objRating := Rating;
  objGenre  := Genre;

  objOnTrack := MATrackFound;

  objCS.EndWrite;
end;
//------------------------------------------------------------------------------
// Internal: Set default Properties when creating an Object
//------------------------------------------------------------------------------
procedure TMediaQuery.CreateProp;
begin
  inherited;

  objArtist := '';
  objYear   := '';
  objAlbum  := '';
  objNumber := '';
  objTrack  := '';
  objRating := '';
  objGenre  := '';

  objOnTrack := nil;
end;
//------------------------------------------------------------------------------
// Destroy
//------------------------------------------------------------------------------
destructor TMediaQuery.Destroy;
begin

  inherited;
end;
//------------------------------------------------------------------------------
// Copy Query
//------------------------------------------------------------------------------
procedure TMediaQuery.CopyQuery(const Query : TMediaQuery);
begin
  if Assigned(Query) and (Query is TMediaQuery) then
    begin
      // We do this using properties so the dirty flag will be set
      // And FCS doesnt need to be used

      self.pArtist := Query.pArtist;
      self.pAlbum  := Query.pAlbum;
      self.pYear   := Query.pYear;
      self.pNumber := Query.pNumber;
      self.pTrack  := Query.pTrack;
      self.pRating := Query.pRating;
      self.pGenre  := Query.pGenre;
    end;
end;
//------------------------------------------------------------------------------
//
//                                  GET & SET
//
//------------------------------------------------------------------------------
// Get Media Dirty
//------------------------------------------------------------------------------
function TMediaQuery.GetArtist: string;
begin
  objCS.BeginRead;
  result := objArtist;
  objCS.EndRead;
end;
//------------------------------------------------------------------------------
// Get Media Dirty
//------------------------------------------------------------------------------
procedure TMediaQuery.SetArtist(const Value : string);
begin
  if (self.pArtist <> Value) then
    begin
      objCS.BeginWrite;
      objArtist     := Value;
      objMediaDirty := true;
      objCS.EndWrite;
    end;
end;
//------------------------------------------------------------------------------
// Get Media Dirty
//------------------------------------------------------------------------------
function TMediaQuery.GetYear: string;
begin
  objCS.BeginRead;
  result := objYear;
  objCS.EndRead;
end;
//------------------------------------------------------------------------------
// Get Media Dirty
//------------------------------------------------------------------------------
procedure TMediaQuery.SetYear(const Value : string);
begin
  if (self.pYear <> Value) then
    begin
      objCS.BeginWrite;
      objYear       := Value;
      objMediaDirty := true;
      objCS.EndWrite;
    end;
end;
//------------------------------------------------------------------------------
// Get Media Dirty
//------------------------------------------------------------------------------
function TMediaQuery.GetAlbum: string;
begin
  objCS.BeginRead;
  result := objAlbum;
  objCS.EndRead;
end;
//------------------------------------------------------------------------------
// Get Media Dirty
//------------------------------------------------------------------------------
procedure TMediaQuery.SetAlbum(const Value : string);
begin
  if (Self.pAlbum <> Value) then
    begin
      objCS.BeginWrite;
      objAlbum      := Value;
      objMediaDirty := true;
      objCS.EndWrite;
    end;
end;
//------------------------------------------------------------------------------
// Get Media Dirty
//------------------------------------------------------------------------------
function TMediaQuery.GetNumber: string;
begin
  objCS.BeginRead;
  result := objNumber;
  objCS.EndRead;
end;
//------------------------------------------------------------------------------
// Get Media Dirty
//------------------------------------------------------------------------------
procedure TMediaQuery.SetNumber(const Value : string);
begin
  if (self.pNumber <> Value) then
    begin
      objCS.BeginWrite;
      objNumber     := Value;
      objMediaDirty := true;
      objCS.EndWrite;
    end;
end;
//------------------------------------------------------------------------------
// Get Media Dirty
//------------------------------------------------------------------------------
function TMediaQuery.GetTrack: string;
begin
  objCS.BeginRead;
  result := objTrack;
  objCS.EndRead;
end;
//------------------------------------------------------------------------------
// Get Media Dirty
//------------------------------------------------------------------------------
procedure TMediaQuery.SetTrack(const Value : string);
begin
  if (self.pTrack <> Value) then
    begin
      objCS.BeginWrite;
      objTRack      := Value;
      objMediaDirty := true;
      objCS.EndWrite;
    end;
end;
//------------------------------------------------------------------------------
// Get Media Dirty
//------------------------------------------------------------------------------
function TMediaQuery.GetRating: string;
begin
  objCS.BeginRead;
  result := objRating;
  objCS.EndRead;
end;
//------------------------------------------------------------------------------
// Get Media Dirty
//------------------------------------------------------------------------------
procedure TMediaQuery.SetRating(const Value : string);
begin
  if (self.pRating <> Value) then
    begin
      objCS.BeginWrite;
      objRating     := Value;
      objMediaDirty := true;
      objCS.EndWrite;
    end;
end;
//------------------------------------------------------------------------------
// Get Media Dirty
//------------------------------------------------------------------------------
function TMediaQuery.GetGenre: string;
begin
  objCS.BeginRead;
  result := objGenre;
  objCS.EndRead;
end;
//------------------------------------------------------------------------------
// Get Media Dirty
//------------------------------------------------------------------------------
procedure TMediaQuery.SetGenre(const Value : string);
begin
  if (self.pGenre <> Value) then
    begin
      objCS.BeginWrite;
      objGenre      := Value;
      objMediaDirty := true;
      objCS.EndWrite;
    end;
end;
//------------------------------------------------------------------------------
// Get Media Dirty
//------------------------------------------------------------------------------
function TMediaQuery.GetOnTrack: TMAOnTrack;
begin
  objCS.BeginRead;
  result := objOnTrack;
  objCS.EndRead;
end;
//------------------------------------------------------------------------------
// Get Media Dirty
//------------------------------------------------------------------------------
procedure TMediaQuery.SetOnTrack(const Value : TMAOnTrack);
begin
  objCS.BeginWrite;
  objOnTrack := Value;
  objCS.EndWrite;
end;
//------------------------------------------------------------------------------
// Callback Method used for returning Found Tracks to PlayList
//------------------------------------------------------------------------------
procedure TMediaQuery.DoTrackFound(Track : TMediaBase);
begin
  objCS.BeginRead;

  if Assigned(objOnTrack) then
    objOnTrack(Track);

  objCS.EndRead
end;
//------------------------------------------------------------------------------
// Is Query Valid
//------------------------------------------------------------------------------
function TMediaQuery.IsValid: boolean;
begin
  objCS.BeginRead;

  // At Least one Critera must exist
  
  result := (length(self.objArtist) > 0) or
            (length(self.objYear)   > 0) or
            (length(self.objAlbum)  > 0) or
            (length(self.objNumber) > 0) or
            (length(self.objTrack)  > 0) or
            (length(self.objRating) > 0) or
            (length(self.objGenre)  > 0);

  objCS.EndRead;
end;
//------------------------------------------------------------------------------
//
//                                    REFRESH
//
//------------------------------------------------------------------------------
// Refresh Query
//------------------------------------------------------------------------------
procedure TMediaQuery.RefreshMedia;
begin
  // The Query Reality is saved into the PlayList File, meaning if
  // Playlist is refreshed, that all thats needed. Its ok to call it
  // since it will Load Playlist file only once

  if Assigned(self.pParent) then
    self.pParent.RefreshMedia;
end;
//------------------------------------------------------------------------------
//
//                              PROPERTY MANAGEMENT
//
//------------------------------------------------------------------------------
// Get Next Column
//------------------------------------------------------------------------------
class function TMediaQuery.MediaPropCount: integer;
begin
  result := 7;
end;
//------------------------------------------------------------------------------
// Get Image Id of this Media
//------------------------------------------------------------------------------
class function TMediaQuery.MediaImage: integer;
begin
  result := ImageIdPlaylistProp;
end;
//------------------------------------------------------------------------------
// Get Next Column
//------------------------------------------------------------------------------
class function TMediaQuery.MediaPropName(
      var Iter : integer;  // Column Index
      out Name : string;   // Column Name
      out Just : integer)  // Column Adjust
               : boolean;  // True
begin
  result := true;
  Just   := 0;    // Left

  case Iter of
    0 : Name := propArtist; // Artist
    1 : Name := propYear;   // Album Year
    2 : Name := propAlbum;  // Album Name
    3 : Name := propNumber; // Track Number
    4 : Name := propTrack;  // Track Name
    5 : Name := propRating; // Track Rating
    6 : Name := propGenre;  // Track Genre
  else
    result := false;
  end;

  Inc(Iter);
end;
//------------------------------------------------------------------------------
// Return True if Media Property is Editable by User
//------------------------------------------------------------------------------
class function TMediaQuery.MediaPropAllow(const Ind : integer): boolean;
begin
  Case Ind of
    0..6 : result := true;
  else
    result := false;
  end;
end;
//------------------------------------------------------------------------------
// Validate a Media Property
//------------------------------------------------------------------------------
class function TMediaQuery.MediaPropValidate(
      const Ind   : integer; // Property Index
      var   Value : string)  // Property Value (can Change)
                  : boolean; // True if Value is Valid
begin
  Case Ind of
    0..6 : result := true;
  else
    result := false;
  end;
end;
//------------------------------------------------------------------------------
// Get Property Value
//------------------------------------------------------------------------------
function TMediaQuery.MediaPropValue(const Ind : integer): string;
begin
  result := '';
  case Ind of
    0 : result := self.pArtist;
    1 : result := self.pYear;
    2 : result := self.pAlbum;
    3 : result := self.pNumber;
    4 : result := self.pTrack;
    5 : result := self.pRating;
    6 : result := self.pGenre;
  else
    result := 'Undefined';
  end;
end;
//------------------------------------------------------------------------------
// Set Media Property Value from a String
//------------------------------------------------------------------------------
function TMediaQuery.MediaPropValue(
      const Ind   : integer; // Property Index
      const Value : string)  // Property Value
                  : boolean; // True if Used
begin
  result := true;
  case Ind of
    0 : self.SetArtist (Value);
    1 : self.SetYear   (Value);
    2 : self.SetAlbum  (Value);
    3 : self.SetNumber (Value);
    4 : self.SetTrack  (Value);
    5 : self.SetRating (Value);
    6 : self.SetGenre  (Value);
  else
    result := false;
  end;
end;
//------------------------------------------------------------------------------
// Compare Query to another Media
//------------------------------------------------------------------------------
function TMediaQuery.MediaPropCompare(
      const Media : TMediaBase;
      const Ind   : integer): integer;
var
  Other : TMediaQuery;
begin
  if (Media is TMediaQuery) then
    begin
      Other := Media as TMediaQuery;
      case Ind of
        0 : result := AnsiCompareStr(self.pArtist, Other.pArtist);
        1 : result := AnsiCompareStr(self.pYear,   Other.pYear);
        2 : result := AnsiCompareStr(self.pAlbum,  Other.pAlbum);
        3 : result := AnsiCompareStr(self.pNumber, Other.pNumber);
        4 : result := AnsiCompareStr(self.pTrack,  Other.pTrack);
        5 : result := AnsiCompareStr(self.pRating, Other.pRating);
        6 : result := AnsiCompareStr(self.pGenre,  Other.pGenre);
      else
        result := 0;
      end;
    end
  else
    begin
      result := inherited MediaPropCompare(Media, Ind);
    end;
end;
//------------------------------------------------------------------------------
//
//                                SAVE & LOAD
//
//------------------------------------------------------------------------------
// Internal: Load Media Db Properties
//------------------------------------------------------------------------------
function TMediaQuery.LoadProp(
      const sProp  : string;  // Property Name as String
      const PropId : integer; // Property Name as Id
      const sValue : string)  // Property Value as string
                   : boolean; // True if resolved
begin
  result := true;

  // Already Protected

  case PropId of
    propIdScArtist : objArtist := sValue;
    propIdScYear   : objYear   := sValue;
    propIdScAlbum  : objAlbum  := sValue;
    propIdScNumber : objNumber := sValue;
    propIdScTrack  : objTrack  := sValue;
    propIdScRating : objRating := sValue;
    propIdScGenre  : objGenre  := sValue;
  else
    result := false;
  end;
end;
//------------------------------------------------------------------------------
// Load Properties
//------------------------------------------------------------------------------
class procedure TMediaQuery.AddPropToFile(const TF : TMediaTextFile);
begin
  TF.AddProp(propIdScArtist, propScArtist);
  TF.AddProp(propIdScYear,   propScYear);
  TF.AddProp(propIdScAlbum,  propScAlbum);
  TF.AddProp(propIdScNumber, propScNumber);
  TF.AddProp(propIdScTrack,  propScTrack);
  TF.AddProp(propIdScRating, propScRating);
  TF.AddProp(propIdScGenre,  propScGenre);
end;
//------------------------------------------------------------------------------
// Internal: Save Properties of this Class (Already protected)
//------------------------------------------------------------------------------
procedure TMediaQuery.SavePropToMediaDb(const TF : TMediaTextFile);
begin
  // Write All Properties on this Level

  if (length(objArtist) > 0) then TF.WriteProp(propScArtist,  objArtist);
  if (length(objYear)   > 0) then TF.WriteProp(propScYear,    objYear);
  if (length(objAlbum)  > 0) then TF.WriteProp(propScAlbum,   objAlbum);
  if (length(objNumber) > 0) then TF.WriteProp(propScNumber,  objNumber);
  if (length(objTrack)  > 0) then TF.WriteProp(propScTrack,   objTrack);
  if (length(objRating) > 0) then TF.WriteProp(propScRating,  objRating);
  if (length(objGenre)  > 0) then TF.WriteProp(propScGenre,   objGenre);
end;
//------------------------------------------------------------------------------
// Load PlayList Query from PlayList File
//------------------------------------------------------------------------------
function TMediaQuery.LoadFromPlayList (const Prop : string): boolean;
var
  sTmp : string;
begin
  result := false;

  objCS.BeginWrite;

  if AnsiStartsText(propStart + propArtist + propEql, Prop) then
    begin
      sTmp := AnsiRightStr(Prop, length(Prop) - length(propArtist) - 2);
      if (objArtist <> sTmp) then result := true;
      objArtist := sTmp;
    end
  else if AnsiStartsText(propStart + propYear + propEql, Prop) then
    begin
      sTmp := AnsiRightStr(Prop, length(Prop) - length(propYear) - 2);
      if (objYear <> sTmp) then result := true;
      objYear := sTmp;
    end
  else if AnsiStartsText(propStart + propAlbum + propEql, Prop) then
    begin
      sTmp := AnsiRightStr(Prop, length(Prop) - length(propAlbum) - 2);
      if (objAlbum <> sTmp) then result := true;
      objAlbum := sTmp;
    end
  else if AnsiStartsText(propStart + propNumber + propEql, Prop) then
    begin
      sTmp := AnsiRightStr(Prop, length(Prop) - length(propNumber) - 2);
      if (objNumber <> sTmp) then result := true;
      objNumber := sTmp;
    end
  else if AnsiStartsText(propStart + propTrack + propEql, Prop) then
    begin
      sTmp := AnsiRightStr(Prop, length(Prop) - length(propTrack) - 2);
      if (objTrack <> sTmp) then result := true;
      objTrack := sTmp;
    end
  else if AnsiStartsText(propStart + propRating + propEql, Prop) then
    begin
      sTmp := AnsiRightStr(Prop, length(Prop) - length(propRating) - 2);
      if (objRating <> sTmp) then result := true;
      objRating := sTmp;
    end
  else if AnsiStartsText(propStart + propGenre + propEql, Prop) then
    begin
      sTmp := AnsiRightStr(Prop, length(Prop) - length(propGenre) - 2);
      if (objGenre <> sTmp) then result := true;
      objGenre := sTmp;
    end;

  // When a Query has been Loaded its Dirty Flag is set to false

  objFileDirty  := false;

  objCS.EndWrite
end;
//------------------------------------------------------------------------------
// Save Query Properties to PlayList File
//------------------------------------------------------------------------------
procedure TMediaQuery.SaveToPlayList(const TF : TMediaTextFile);
begin
  objCS.BeginRead;

  // Write All Search Criterias to PlayListFile

  if (length(objArtist) > 0) then
    TF.Write(propStart + propArtist + propEql + objArtist);

  if (length(objYear) > 0) then
    TF.Write(propStart + propYear + propEql + objYear);

  if (length(objAlbum) > 0) then
    TF.Write(propStart + propAlbum + propEql + objAlbum);

  if (length(objNumber) > 0) then
    TF.Write(propStart + propNumber + propEql + objNumber);

  if (length(objTrack) > 0) then
    TF.Write(propStart + propTrack + propEql + objTrack);

  if (length(objRating) > 0) then
    TF.Write(propStart + propRating + propEql + objRating);

  if (length(objGenre) > 0) then
    TF.Write(propStart + propGenre + propEql + objGenre);

  objCS.EndRead;

  // When a Query has been Saved its Dirty Flag is set to false

  self.pFileDirty  := false;
end;
//------------------------------------------------------------------------------
//  Log Object
//------------------------------------------------------------------------------
procedure TMediaQuery.LogIt;
begin
  TheMedia.Log(self.ClassName + ':');
  if Assigned(self.pParent) then
    TheMedia.Log('PlayList: ' + self.pParent.pPath);

  TheMedia.Log('Artist: ' + self.pArtist);
  TheMedia.Log('Year:   ' + self.pYear);
  TheMedia.Log('Album:  ' + self.pAlbum);
  TheMedia.Log('Number: ' + self.pNumber);
  TheMedia.Log('Track:  ' + self.pTrack);
  TheMedia.Log('Rating: ' + self.pRating);
  TheMedia.Log('Genre:  ' + self.pGenre);

end;
//------------------------------------------------------------------------------
//                                    INITIALIZE
//------------------------------------------------------------------------------
initialization
  RegClass(TMediaQuery);
end.
