unit TDocFileUnit;

interface

uses
  Windows, Classes, Contnrs, ComCtrls,

  TGenLogUnit,        // Log Object
  TGenTextFileUnit,   // TextFile Class

  TDocItemUnit,       // Item Base Class
  TTreeItemBaseUnit;  // Item Base Class

//------------------------------------------------------------------------------
//  Docuemnt Item
//------------------------------------------------------------------------------
type TDocFile = Class(TDocItem)
  protected

  public
    class function  GetTypeStr: string; override;

end;

//------------------------------------------------------------------------------
//                                  IMPLEMENTATION
//------------------------------------------------------------------------------
implementation

uses
  SysUtils,
  StrUtils,

  TMediaPlayerResUnit,// Resource Strings
  
  TGenClassesUnit;    // Class Management

//------------------------------------------------------------------------------
// Get Type String with ReadOnly and Hidden Flgs attached
//------------------------------------------------------------------------------
class function TDocFile.GetTypeStr: string;
begin
  result := resDocFileType;
end;
//------------------------------------------------------------------------------
//                                    INITIALIZE
//------------------------------------------------------------------------------
initialization
  RegClass(TDocFile);
end.

