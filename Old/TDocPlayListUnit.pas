unit TDocPlayListUnit;

//{$DEFINE LOGINIT}
//{$DEFINE LOGCMD}

interface

uses
  Windows, Classes, Contnrs, ComCtrls, Menus, Forms,

  TGenLogUnit,        // Log Object
  TGenTReeViewUnit,
  TTreeItemBaseUnit,  // Base Class
  TDocTrackUnit,      // Track Item
  TGenTextFileUnit,
  TMediaFactoryUnit,
  TMediaBaseUnit,
  TMediaTrackUnit,
  TMediaPlayListUnit,
  TMyMessageUnit,
  TDocFolderUnit;     // Item Base Class

//------------------------------------------------------------------------------
//  PlayList Property
//  This Item holds the Playlist Search Criterias (one for criteria Each)
//------------------------------------------------------------------------------
type TDocPlayListProp = Class(TTReeItemBase)
  protected
    objQuery     : TMediaQuery; // Reference to the  Search Query
    objPropIndex : integer;     // Property Index in Search Query
    objPropName  : string;      // Name of the Property

    // The Property Value is stored in the Item Name

    function GetNiceName: string; override;

  public
    // Create a Query Property Item

    constructor Create(
      const pParent : TTreeItemBase;         // Parent Item (PlayList)
      const pQuery  : TMediaQuery;           // Query Reference
      const Index   : integer;               // Query Property Index
      const Name    : string;                // Property Name
      const Value   : string); reintroduce;  // Property Value

    // Return the Children Class of Query property (TDocTrack)

    class function  ChildrenClass: TItemClass; override;

    // Return tru if a Item Column can be edited (true)

    class function  GetPropEdit(const Col : integer): boolean; override;

    // Return true if the Parent is used for ListView population (true)

    class function  ParentListView: boolean; override;

    class function GetImageId: integer; override;

    // Return TReNode to Create for this Item (TGenTreeNodeString)

    function CreateTreeNode(const sCaption : string): TGenTreeNode; override;

    // Add Menu for TreeView

    procedure AddTreeMenuItems(const PopupMenu : TPopupMenu); override;

    // Set a Property Value when user has changed Node Caption

    procedure SetPropValue(const Col : integer; const Txt : string); override;

    // Refresh The Value from the Media Db PlayList Query

    procedure RefreshValue;

    // Compare this Item with another (used for sorting ListView)

    function  Compare(
      const pItem : TTreeItemBase;
      const Col   : integer): integer; override;

end;



//------------------------------------------------------------------------------
//  PlayList
//------------------------------------------------------------------------------
type TDocPlayList = Class(TDocFolder)
  protected
    objDirty  : boolean; // True if Playlist has Changed and need Saving
    objLoaded : boolean; // True if Playlist has been loaded from its File

    // Reference to Media Playlist Object in Media Db

    objMedia : TMediaPlayList;

    //--------------------------------------------------------------------------
    // Properties
    //--------------------------------------------------------------------------

    function GetTrackCount : integer;
    function GetTrackSize  : int64;
    function GetNiceName   : string; override; // Removes [...]

    //--------------------------------------------------------------------------
    // Internal Activities
    //--------------------------------------------------------------------------

    // Find a Media Track on this Playlist Childrens

    function  FindMediaTrack(const pTrack : TMediaTrack): TDocTrack;

    // Tell all Query Properties to Refresh them selves

    procedure RefreshQueryProperties;

    // Refresh all Tracks of the Item Playlist on Media Playlist

    procedure RefreshTracksOnMediaPlaylist;

    // Save this Playlist

    procedure SavePlayList;

  public
    constructor Create(
      const pParent   : TTreeItemBase;
      const Name      : string;
      const nType     : integer;
      const nSize     : int64;
      const tCre      : TFileTime;
      const tMod      : TFIleTime;
      const bReadonly : boolean;
      const bHidden   : boolean); override;

    destructor  Destroy; override;

    //--------------------------------------------------------------------------
    // Ui Properties
    //--------------------------------------------------------------------------

    class function ChildrenClass: TItemClass; override;
    class function GetTypeStr: string;  override;
    class function AutoExpand: boolean; override;

    class function GetImageId: integer; override;

    //--------------------------------------------------------------------------
    // Processes
    //--------------------------------------------------------------------------

    // Create a New Playlist

    procedure ProcNewPlaylist(Sender : TObject);

    // Delete This Playlist

    procedure ProcDeletePlayList (sender : TObject);

    // Run the Playlist Query

    procedure ProcRequeryPlayList (sender : TObject);

    // Refresh Children by reading Media Db

    procedure RefreshChildren;override;

    //--------------------------------------------------------------------------
    // Menus
    //--------------------------------------------------------------------------

    // Add Menu for TreeView

    procedure AddTreeMenuItems(const PopupMenu : TPopupMenu); override;

    //--------------------------------------------------------------------------
    // Property Management for PlayList
    //--------------------------------------------------------------------------

    class function  GetPropCount: integer; override;
    class function  GetPropName (const Col : integer): string; override;
    class function  GetPropEdit (const Col : integer): boolean; override;
          function  GetPropValue(const Col : integer): string;  override;
          procedure SetPropValue(const Col : integer; const Txt : string); override;

    property pTrackCount : integer read GetTrackCount;
    property pTrackSize  : int64   read GetTrackSize;
end;

//------------------------------------------------------------------------------
//                                  IMPLEMENTATION
//------------------------------------------------------------------------------
implementation

uses
  SysUtils,
  StrUtils,
  Math,

  TGenStrUnit,        // String Functions
  TGenMenuUnit,       // TGenMenu
  MyFileTimeUnit,     // TFileTime Function

  TMediaPlayerResUnit,// Resource Strings

  TGenClassesUnit;    // Class Management

resourcestring
  resPlayListRequery = 'Query Playlist';
  resPlayListDelete  = 'Delete Playlist';
  resPlayListNew     = 'New Playlist';

//------------------------------------------------------------------------------
//
//                           QUERY PROPERTY ITEM
//
//------------------------------------------------------------------------------
// Create
//------------------------------------------------------------------------------
constructor TDocPlayListProp.Create(
      const pParent : TTreeItemBase;         // Parent Item (PlayList)
      const pQuery  : TMediaQuery;           // Query Reference
      const Index   : integer;               // Query Property Index
      const Name    : string;                // Property Name
      const Value   : string);               // Property Value
begin
  inherited Create(pParent, Value);

  objQuery     := pQuery;
  objPropIndex := Index;
  objPropName  := Name;
end;
//------------------------------------------------------------------------------
// Return What Class Items Childrens are
//------------------------------------------------------------------------------
class function TDocPlayListProp.ChildrenClass: TItemClass;
begin
  result := TDocTrack;
end;
//------------------------------------------------------------------------------
// Get Type String with ReadOnly and Hidden Flgs attached
//------------------------------------------------------------------------------
class function TDocPlayListProp.GetPropEdit(const Col : integer): boolean;
begin
  result := true;
end;
//------------------------------------------------------------------------------
// Return Class Parent ListView
//------------------------------------------------------------------------------
class function TDocPlayListProp.ParentListView: boolean;
begin
  result := true;
end;
//------------------------------------------------------------------------------
// Return What Class Items Childrens are
//------------------------------------------------------------------------------
class function TDocPlayListProp.GetImageId:integer;
begin
  result := ImageIdQuery;
end;
//------------------------------------------------------------------------------
//  Create a Default TreeNode
//------------------------------------------------------------------------------
function TDocPlayListProp.CreateTreeNode(const sCaption : string): TGenTreeNode;
begin
  result := TGenTreeNodeString.Create;
  TGenTreeNodeString(result).Name := objPropName;
  TGenTreeNodeString(result).Value := sCaption;
end;
//------------------------------------------------------------------------------
// Add Menus accoding to Current Selected Item in TreeView
//------------------------------------------------------------------------------
procedure TDocPlayListProp.AddTreeMenuItems(const PopupMenu : TPopupMenu);
begin
  if Assigned(self.pParent) then self.pParent.AddTreeMenuItems(PopupMenu);
end;
function TDocPlayListProp.GetNiceName: string;
begin
  result := objQuery.GetProp(objPropIndex);
end;
//------------------------------------------------------------------------------
//  Set Text Property depending on Column in ListView
//------------------------------------------------------------------------------
procedure TDocPlayListProp.SetPropValue(const Col : integer; const Txt : string);
begin
  Case Col of
    // Only the First Column (Query Property Value) can Change

    0 : if (not AnsiSameStr(self.pName, Txt)) then
      begin

        // Update Media Db PlayList Query Property

        objQuery.SetProp(objPropIndex, Txt);

        // Update Item Name

        RefreshValue;

        // Tell Its Dirty ??????

        if Assigned(pParent) and (pParent is TDocPlayList) then
          TDocPlayList(pParent).objDirty := true;
      end;
  end;
end;
//------------------------------------------------------------------------------
//  Get a New Value from the Media Db PlayList
//------------------------------------------------------------------------------
procedure TDocPlayListProp.RefreshValue;
begin
  self.pName := objQuery.GetProp(objPropIndex);
end;
//------------------------------------------------------------------------------
// Compare this Item with another (used for sorting ListView)
//------------------------------------------------------------------------------
function TDocPlayListProp.Compare(
      const pItem : TTreeItemBase;
      const Col   : integer): integer;
begin
  // Returns +1 if A > B, -1 if A < B, else 0

  if (pItem is TDocPlayListProp) then
    begin
      result := CompareValue(self.objPropIndex,
                     TDocPlayListProp(pItem).objPropIndex);
    end
  else
    result := inherited Compare(pItem, Col);
end;
//------------------------------------------------------------------------------
//
//                                PLAYLIST ITEM
// 
//------------------------------------------------------------------------------
// Create
//------------------------------------------------------------------------------
constructor TDocPlayList.Create(
      const pParent   : TTreeItemBase;
      const Name      : string;
      const nType     : integer;
      const nSize     : int64;
      const tCre      : TFileTime;
      const tMod      : TFIleTime;
      const bReadonly : boolean;
      const bHidden   : boolean);
var
  Iter     : integer;
  sName    : string;
  sValue   : string;
  PropItem : TDocPlayListProp;
begin
  inherited;

{$IFDEF LOGINIT}
  TheLog.Log('TDocPlayList.Create ' + self.pPath);
{$ENDIF}

  // Find the Media Playlist in Media Db (or create it)
  // and set a reference to it

  objMedia := TheMedia.FindPlayListByPath(self.pPath);

  // Add The Query Name/Value Pairs by iterating Query Properties

  Iter := 0;
  while objMedia.pQuery.IterQuery(Iter, sName, sValue) do
    begin
      PropItem := TDocPlayListProp.Create(self,
                  objMedia.pQuery, Iter - 1, sName, sValue);
      objList.AddItem(PropItem);
    end;

  objDirty  := false;
  objLoaded := false;
end;
//------------------------------------------------------------------------------
// Destroy
//------------------------------------------------------------------------------
destructor TDocPlayList.Destroy;
begin
  if objDirty then SavePlayList;

  inherited;
end;
//------------------------------------------------------------------------------
// Return PlayList Name without Path and Extension
//------------------------------------------------------------------------------
function TDocPlayList.GetNiceName   : string;
begin
  if Assigned(objMedia) then
    result := objMedia.pName
  else
    result := 'Undefined';
end;
//------------------------------------------------------------------------------
// Return What Class Items Childrens are
//------------------------------------------------------------------------------
class function TDocPlayList.ChildrenClass: TItemClass;
begin
  result := TDocTrack;
end;
//------------------------------------------------------------------------------
// Return What Class Items Childrens are
//------------------------------------------------------------------------------
class function TDocPlayList.GetImageId:integer;
begin
  result := ImageIdPlayList;
end;
//------------------------------------------------------------------------------
// Get Type String with ReadOnly and Hidden Flags attached
//------------------------------------------------------------------------------
class function TDocPlayList.GetTypeStr: string;
begin
  result := resPlayListType;
end;
//------------------------------------------------------------------------------
// Return True if This Items Node should Auto Expand
//------------------------------------------------------------------------------
class function TDocPlayList.AutoExpand: boolean;
begin
  result := false;
end;
//------------------------------------------------------------------------------
// Get Track Count
//------------------------------------------------------------------------------
function TDocPlayList.GetTrackCount : integer;
begin
  if Assigned(objMedia) then
    result := objMedia.pTrackCount
  else
    result := 0;
end;
//------------------------------------------------------------------------------
// Get Total Track Size
//------------------------------------------------------------------------------
function TDocPlayList.GetTrackSize : int64;
begin
  if Assigned(objMedia) then
    result := objMedia.pTrackSize
  else
    result := 0;
end;
//------------------------------------------------------------------------------
//
//                                  PROPERTIES
//
//------------------------------------------------------------------------------
// Return Number of Colums in Item Class
//------------------------------------------------------------------------------
class function TDocPlayList.GetPropCount: integer;
begin
  result := 7;
end;
//------------------------------------------------------------------------------
//  Get the Property (text) of a specified Column in ListView
//------------------------------------------------------------------------------
class function TDocPlayList.GetPropName(const Col : integer): string;
begin
  case Col of
    0 : result := resName;
    1 : result := resSize;
    2 : result := resType;
    3 : result := resPlayListTracks;
    4 : result := resPlayListSize;
    5 : result := resCreated;
    6 : result := resModified;
  end;
end;
//------------------------------------------------------------------------------
//  Return true if this Column in ListView is Editable
//------------------------------------------------------------------------------
class function TDocPlayList.GetPropEdit(const Col : integer): boolean;
begin
  // Only first Property (0) the Value can be changed

  case Col of
    0 : result := true;
  else
    result := false;
  end;
end;
//------------------------------------------------------------------------------
//  Get Column Text
//------------------------------------------------------------------------------
function TDocPlayList.GetPropValue(const Col : integer): string;
begin
  result := '';
  Case Col of
    0 : result := self.pNiceName;
    1 : result := SizeToStr(objSize);
    2 : result := self.pTypeStr;
    3 : result := IntToStr(self.pTrackCount);
    4 : result := SizeToStr(self.pTrackSize);
    5 : result := MyFileTimeToStr(objCre);
    6 : result := MyFileTimeToStr(objMod);
  end;
end;
//------------------------------------------------------------------------------
// Set Text Property Value depending on Property Index
//------------------------------------------------------------------------------
procedure TDocPlayList.SetPropValue(const Col : integer; const Txt : string);
begin
  // Only first Property (0) the Value can be changed

  if (Col = 0) and Assigned(objMedia) then
    begin
      // Txt has the Name (no path and no ext). Let Media Db do it

      TheLog.Log('Change Name From: ' + objMedia.pPath + ' To: ' + Txt);

      objMedia.pName := Txt;

      // MediaPlaylist will Change its Name (if possible).
      // The Old File will be Deleted, the new Saved
      // The Playlist Node Text has already Changed
      // No Refresh is Needed
    end;
end;
//------------------------------------------------------------------------------
//
//                                  LOAD AND SAVE
//
//------------------------------------------------------------------------------
//
//                                    MENUS
//
//------------------------------------------------------------------------------
// Add Menus accoding to Current Selected Item in TreeView
//------------------------------------------------------------------------------
procedure TDocPlayList.AddTreeMenuItems(const PopupMenu : TPopupMenu);
var
  pMenu : TGenMenu;
begin

  pMenu := TGenMenu.Create(PopupMenu);
  pMenu.Caption := '-';
  PopupMenu.Items.Add(pMenu);

  // Update Playlist

  pMenu := TGenMenu.Create(PopupMenu);
  pMenu.Caption := resPlayListRequery;
  pMenu.OnClick := ProcRequeryPlayList;
  PopupMenu.Items.Add(pMenu);

  // Remove Playlist

  pMenu := TGenMenu.Create(PopupMenu);
  pMenu.Caption := resPlayListDelete;
  pMenu.OnClick := ProcDeletePlayList;
  PopupMenu.Items.Add(pMenu);

  // Add New Playlist

  pMenu := TGenMenu.Create(PopupMenu);
  pMenu.Caption := resPlayListNew;
  pMenu.OnClick := ProcNewPlaylist;
  PopupMenu.Items.Add(pMenu);
end;
//------------------------------------------------------------------------------
//
//                                   PROCESSES
//
//------------------------------------------------------------------------------
// Perform Create New Playlist
//------------------------------------------------------------------------------
procedure TDocPlayList.ProcNewPlaylist(sender : TObject);
begin
{$IFDEF LOGCMD}
{$ENDIF}
  TheLog.Log('New Playlist in Dir: ' + ExtractFileDir(self.pPath));

  // Process:
  // 1) Create the Playlist File (Done by Media Factory)
  // 2) Refresh its Parent Item Folder (Post Message here)
  // 3) It will be found and an Item will be Created
  // 4) This will Create the Media Playlist
  // 5) File Dirty : False, PlayList Not Loaded
  // 6) Media Dirty: True

  // 1) Create The PlayList File in the same Directory

  objMedia.NewPlayListFile(ExtractFileDir(self.pPath));

  // 2) Refresh Parent Folder

  //PostMessage(Application.Handle,
  //            MSG_TREEVIEW_REFRESH, DWORD(self.pParent), 0);
end;
//------------------------------------------------------------------------------
// Perform Delete Playlist
//------------------------------------------------------------------------------
procedure TDocPlayList.ProcDeletePlayList(sender : TObject);
begin
{$IFDEF LOGCMD}
{$ENDIF}
  TheLog.Log('Delete Playlist: ' + self.pPath);

  // Process:
  //  1) Delete the Physical Playlist and the Media Playlist
  //     Need to set reference objMedia to nil
  //  2) Refresh the Parent Item (Folder).
  //     a) This will detect that the children (this item) is missing
  //        and remove it.
  //     b) Also its TreeNode will be deleted.
  //     c) If it was selected, the Parent Node will be selected

  // 1) Let the Media Db delete the Media Playlist and the Physical File

  TheMedia.DeletePlayList(objMedia);
  objMedia := nil;

  // 2) Refresh Parent TreeNode

  //PostMessage(Application.Handle,
  //            MSG_TREEVIEW_REFRESH, DWORD(self.pParent), 0);
end;
//------------------------------------------------------------------------------
// Perform Update Playlist by Search Criterias
//------------------------------------------------------------------------------
procedure TDocPlayList.ProcRequeryPlayList(sender : TObject);
begin
{$IFDEF LOGCMD}
{$ENDIF}
  TheLog.Log('Requery Playlist ' + objMedia.pPath);

  // The Media Playlist will perform the Query and found Tracks will
  // exists as Children in Media Playlist

  objMedia.Query;

  // Refresh All Tracks from Media Playlist

  RefreshTracksOnMediaPlaylist;

  // Refresh ListView with new Tracks
  
  PostMessage(Application.Handle, MSG_LISTVIEW_REFRESH, 0, 0);
end;
//------------------------------------------------------------------------------
// Process: Refresh Children by reading Media Db
//------------------------------------------------------------------------------
procedure TDocPlayList.RefreshChildren;
begin

  // Refresh Query Data from Media Playlist

  RefreshQueryProperties;

  // Refresh All Tracks

  RefreshTracksOnMediaPlaylist;
end;
//------------------------------------------------------------------------------
//
//                                   ACTIVITIES
//
//------------------------------------------------------------------------------
// Find a Media Track on this Playlist Childrens
//------------------------------------------------------------------------------
function TDocPlayList.FindMediaTrack(const pTrack : TMediaTrack): TDocTrack;
var
  Iter  : integer;
  pItem : TTreeItemBase;
begin
  result := nil;

  Iter := 0;
  while self.GetNextChild(Iter, pItem) do
    if (pItem is TDocTRack) and
       (TDocTRack(pItem).pMediaTrack = pTrack) then
      begin
        result := pItem as TDocTrack;
        BREAK;
      end;
end;
//------------------------------------------------------------------------------
// Activity: Refresh all Tracks of the Item Playlist on Media Playlist
//------------------------------------------------------------------------------
procedure TDocPlayList.RefreshTracksOnMediaPlaylist;
var
  Iter   : integer;
  pTrack : TMediaTrack;
  pItem  : TTreeItemBase;
begin
  if Assigned(objMedia) then
    begin
      // Mark all Current Tracks as false

      self.objList.pMark := false;

      // Walk all Tracks from Media PlayList, Add new and Mark them

      Iter := 0;
      while objMedia.GetNextTrack(Iter, pTrack) do
        begin
          // Find this Track in Childdren Items

          pItem := self.FindMediaTrack(pTrack);
          if (not Assigned(pItem)) then
            begin
              pItem := TDocTrack.CreateFromMedia(self,pTrack.pTrack, pTrack);
              pItem.pMark := true;
              objList.AddItem(pItem);
            end
          else
            begin
              pItem.pMark := true;
            end;
        end;

      // Remove those Tracks still not Marked

      Iter := 0;
      while objList.GetNextItem(Iter, pItem) do
        if (pItem is TDocTrack) and
           (not pItem.pMark) then
          begin
            objList.DelItem(pItem);
            Iter := 0;
          end;
    end;
end;
//------------------------------------------------------------------------------
// Activity: Tell all Query Properties to Refresh them selves
//------------------------------------------------------------------------------
procedure TDocPlayList.RefreshQueryProperties;
var
  Iter  : integer;
  pProp : TTreeItemBase;
begin

  if Assigned(objMedia) then
    begin
      Iter := 0;
      while self.GetNextChild(Iter, pProp) do
        if (pProp is TDocPlayListProp) then
          TDocPlayListProp(pProp).RefreshValue;
    end;
end;
//------------------------------------------------------------------------------
// Activity: Save the Playlist
//------------------------------------------------------------------------------
procedure TDocPlayList.SavePlayList;
begin
  if objDirty and Assigned(objMedia) then
    objMedia.SavePlayList;

  objDirty := false;
end;
//------------------------------------------------------------------------------
//                                 INITIALIZE
//------------------------------------------------------------------------------
initialization
  RegClass(TDocPlayList);
end.

 