unit TProgItemUnit;

interface

uses
  Windows, Classes, Contnrs, ComCtrls, Menus,

  TGenLogUnit,        // Log Object
  TGenTextFileUnit,   // Textfile Class
  TProgBaseUnit,      // Base Class
  TTreeItemBaseUnit;  // Item Base Class

//------------------------------------------------------------------------------
//  Tree Item Base Object
//------------------------------------------------------------------------------
type TProgItem = Class(TProgBase)
  protected
    objFile : string;
    
    procedure SetFile(const Value : string);
    
  public
    constructor Create(
      const pParent : TTreeItemBase;
      const Name    : string); override;

    constructor CreateFromFile(
      const pParent : TTreeItemBase;
      const TF      : TGenTextFile); override;

    destructor  Destroy; override;

    class function  ChildrenClass: TItemClass; override;
    class function  GetTypeStr: string; override;

    procedure SaveToFile(const TF : TGenTextFile); override;

    procedure AddListMenuItems(
      const PopupMenu : TPopupMenu;
      const Column    : integer); override;

    procedure AddTreeMenuItems(const PopupMenu : TPopupMenu); override;
    
    procedure AddNewChild(Sender: TObject); override;

    class function  GetPropCount: integer; override;
    class function  GetPropName (const Col : integer): string; override;
    class function  GetPropEdit (const Col : integer): boolean; override;
          function  GetPropValue(const Col : integer): string;  override;
          procedure SetPropValue(const Col : integer; const Txt : string); override;

    procedure Execute(const Col : integer); override;

    property pFile : string read objFile write SetFile;
end;

//------------------------------------------------------------------------------
// Constants used by other modules
//------------------------------------------------------------------------------
const
  propProgItem = 'ProgItem';

//------------------------------------------------------------------------------
//                                  IMPLEMENTATION
//------------------------------------------------------------------------------
implementation

uses
  SysUtils,
  StrUtils,
  Forms,
  
  TGenStrUnit,        // String Functions
  TGenShellUnit,      // Shell Functions
  TGenOpenDialogUnit, // Open Dialog
  TGenMenuUnit,       // TGenMenu Class

  TMediaPlayerResUnit,// Resource Strings
  TCoupItemUnit,

  TMyMessageUnit,     // App Messages
  TExplorerAppUnit,
  
  TGenClassesUnit;    // Class Management

//------------------------------------------------------------------------------
// Create
//------------------------------------------------------------------------------
constructor TProgItem.Create(
      const pParent : TTreeItemBase;
      const Name    : string);
begin
  inherited Create(pParent, Name);

  objDesc := resProgItemDesc;
  objFile := '';
end;
//------------------------------------------------------------------------------
// Create From TextFile
//------------------------------------------------------------------------------
constructor TProgItem.CreateFromFile(
      const pParent : TTreeItemBase;
      const TF      : TGenTextFile);
var
  sProp, sValue : string;
begin
  inherited Create(pParent, '');

  objDesc := resProgItemDesc;
  objFile := '';

  // Read All Attributes from File

  while TF.ReadProp(sProp, sValue) do
    begin
      // Is this Id

      if AnsiStartsText(PropId, sProp) then
        begin
          self.pUIDLoaded := ToInt(sValue);
        end

      // Is this Name

      else if AnsiStartsText(PropName, sProp) then
        begin
          self.pName := sValue;
        end

      // Is this Description

      else if AnsiStartsText(PropDesc, sProp) then
        begin
          objDesc := sValue;
        end

      // Is this FileName

      else if AnsiStartsText(PropFile, sProp) then
        begin
          objFile := sValue;
        end

      // New Coupling

      else if AnsiSameText(PropCoupItem, sProp) then
        begin
          // Create a New Program Group

          objList.AddItem(TCoupItem.CreateFromFile(self, TF));
        end

      // Is This End Of Group

      else if AnsiSameText(PropProgItem, sProp) then
        begin
          BREAK;
        end;
    end;

  self.pDirty := false;
end;
//------------------------------------------------------------------------------
// Destroy
//------------------------------------------------------------------------------
destructor TProgItem.Destroy;
begin

  inherited;
end;
//------------------------------------------------------------------------------
// Return What Class Items Childrens are
//------------------------------------------------------------------------------
class function TProgItem.ChildrenClass: TItemClass;
begin
  result := TCoupItem;
end;
//------------------------------------------------------------------------------
// Get Type String with ReadOnly and Hidden Flgs attached
//------------------------------------------------------------------------------
class function TProgItem.GetTypeStr: string;
begin
  result := resProgItemType;
end;
//------------------------------------------------------------------------------
// Set a New Name
//------------------------------------------------------------------------------
procedure TProgItem.SetFile(const Value : string);
begin
  if (not AnsiSameStr(objFile, Value)) then
    begin
      self.pDirty := true;
      objFile     := Value;
    end;
end;
//------------------------------------------------------------------------------
// Save This Item
//------------------------------------------------------------------------------
procedure TProgItem.SaveToFile(const TF : TGenTextFile);
begin
  if (TF <> nil) then
    begin
      TF.WriteProp (PropProgItem);
      TF.WriteProp (PropId,   IntToStr(self.pUID));
      TF.WriteProp (PropName, self.pName);
      TF.WriteProp (PropDesc, self.pDesc);
      TF.WriteProp (PropFile, self.pFile);

      objList.SaveToFile(TF);

      TF.WriteProp (PropProgItem);
    end;
end;
//------------------------------------------------------------------------------
//  Get the Property (text) of a specified Column in ListView
//------------------------------------------------------------------------------
class function TProgItem.GetPropName(const Col : integer): string;
begin
  case Col of
    0 : result := TProgItem.GetTypeStr;
    1 : result := resDesc;
    2 : result := resProgItemFile;
  end;
end;
//------------------------------------------------------------------------------
// Return Number of Colums in Item Class
//------------------------------------------------------------------------------
class function TProgItem.GetPropCount: integer;
begin
  result := 3;
end;
//------------------------------------------------------------------------------
//  Get Column Text
//------------------------------------------------------------------------------
function TProgItem.GetPropValue(const Col : integer): string;
begin
  result := '';
  Case Col of
    0 : result := self.pName;
    1 : result := objDesc;
    2 : result := objFile;
  else
    result := self.pName;
  end;
end;
//------------------------------------------------------------------------------
//  Return true if this Column in ListView is Editable
//------------------------------------------------------------------------------
class function TProgItem.GetPropEdit(const Col : integer): boolean;
begin
  case Col of
    0 : result := true;
    1 : result := true;
    2 : result := true;
  else
    result := false;
  end;
end;
//------------------------------------------------------------------------------
//  Update Coumn Text
//------------------------------------------------------------------------------
procedure TProgItem.SetPropValue(const Col : integer; const Txt : string);
begin
  Case Col of
    0 : self.pName := Txt;
    1 : self.pDesc := Txt;
    2 : self.pFile := Txt;
  end;
end;
//------------------------------------------------------------------------------
// Add Menus accoding to Current Selected Item in TreeView
//------------------------------------------------------------------------------
procedure TProgItem.AddTreeMenuItems(const PopupMenu : TPopupMenu);
var
  pMenu : TGenMenu;
begin
  inherited AddTreeMenuItems(PopupMenu);

  // Add New Child

  pMenu := TGenMenu.Create(PopupMenu);
  pMenu.Caption := resAdd + SPC + TCoupItem.GetTypeStr;
  pMenu.OnClick := AddNewChild;
  PopupMenu.Items.Add(pMenu);
end;
//------------------------------------------------------------------------------
// Add Menus accoding to Current Selected Item in ListView
//------------------------------------------------------------------------------
procedure TProgItem.AddListMenuItems(
      const PopupMenu : TPopupMenu;
      const Column    : integer);
var
  pMenu : TGenMenu;
begin
  inherited AddListMenuItems(PopupMenu, Column);

  // Delete My Self

  pMenu := TGenMenu.Create(PopupMenu);
  pMenu.Caption := resDelete + SPC + self.pName;
  pMenu.OnClick := DelMySelf;
  PopupMenu.Items.Add(pMenu);
end;
//------------------------------------------------------------------------------
// Menu: Add a new Program Item
//------------------------------------------------------------------------------
procedure TProgItem.AddNewChild(Sender: TObject);
begin
  objList.AddItem(TCoupItem.Create(self, resNew + SPC + TCoupItem.GetTypeStr));
  self.pDirty := true;

  //PostMessage(Application.Handle, MSG_TREEVIEW_REFRESH, 0, 0);
  //PostMessage(Application.Handle, MSG_LISTVIEW_REFRESH, 0, 0);
end;
//------------------------------------------------------------------------------
//  User Double Clicked
//------------------------------------------------------------------------------
procedure TProgItem.Execute(const Col : integer);
var
  pDlg : TGenOpenDialog;
  sErr : string;
begin
  case Col of
    0, 1 :
      begin
        TheLog.Log('Open Program ' + self.pName);

        if IsFile(self.pFile) then
          begin
            if (not DoShellCmd(self.pFile,'',false, sErr)) then
              TheLog.Log(sErr);
          end
        else
          TheLog.Log('Program dont exist "' + self.pFile + '"');
      end;

    2 :
      begin
        TheLog.Log('Find Executable ' + self.pName);

        pDlg := TGenOpenDialog.Create('FindExecutable');
        pDlg.FileName   := self.pFile;
        pDlg.DefaultExt := 'Exe';
        pDlg.Filter     := 'Executables (*.exe)|*.EXE';

        if pDlg.Execute then
          begin
            self.pFile := pDlg.FileName;
            PostMessage(Application.Handle, MSG_LISTVIEW_REFRESH, 0, 0);
          end;

        pDlg.Free;
      end;
  end;
end;
//------------------------------------------------------------------------------
//                                    INITIALIZE
//------------------------------------------------------------------------------
initialization
  RegClass(TProgItem);
end.

