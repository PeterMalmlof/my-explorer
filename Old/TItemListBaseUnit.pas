unit TItemListBaseUnit;

interface

uses
  Windows, Classes, Contnrs, ComCtrls,

  TGenTextFileUnit,   // Textfile Class
  TTreeItemBaseUnit;  // Item Base Class

//------------------------------------------------------------------------------
//  Tree Item List Object
//------------------------------------------------------------------------------
type TItemListBase = Class(TObject)
  protected
    objList     : TObjectList;
    objTreeView : TTreeView;

    procedure SetMark   (const Value : boolean);
    procedure SetPlayed (const Value : boolean);

    function  GetDirty: boolean;
    function  GetCount: integer;
  public
    constructor Create;
    destructor  Destroy; override;

    // Save all Items to TextFile

    procedure SaveToFile(const TF : TGenTextFile);

    // Add All Items in List as TListItems in ListView

    procedure AddToListView(const ListView : TListView);

    // Add a new Item

    procedure AddItem(const pItem : TTreeItemBase);

    // Delete an Item

    procedure DelItem(const pItem : TTreeItemBase);

    // Return true if Item exists

    function  IsItemRecurse (const pItem : TTreeItemBase):boolean;
    function  IsItemHere    (const pItem : TTreeItemBase):boolean;

    // Find an Item from the Name

    function FindItemHereFromName(const Name : string): TTreeItemBase;

    // Get Item from Its Id (Recursivly)

    function GetItemFromUid      (const Id : Cardinal): TTreeItemBase;
    function GetItemFromUidLoaded(const Id : Cardinal): TTreeItemBase;

    // Iterate all Items in List

    function GetNextItem(
      var Iter  : integer;
      out pItem : TTreeItemBase)
                : boolean;

    procedure RemoveUnmarked;

    procedure RefreshNodes(
      const TReeView : TTreeView;
      const TreeNode : TTreeNode);

    property pMark   : boolean                write SetMark;
    property pPlayed : boolean                write SetPlayed;
    property pDirty  : boolean read GetDirty;
    property pCount  : integer read GetCount;
end;
//------------------------------------------------------------------------------
//                                  IMPLEMENTATION
//------------------------------------------------------------------------------
implementation

uses
  SysUtils,
  StrUtils,

  TGenLogUnit,        // Log Object
  TGenStrUnit,        // String Functions
  TGenTimerUnit,      // TgenTimer Class
  TGenClassesUnit;    // Class Management

//------------------------------------------------------------------------------
// Create
//------------------------------------------------------------------------------
constructor TItemListBase.Create;
begin
  inherited Create;
  
  objList  := TObjectList.Create(true);
end;
//------------------------------------------------------------------------------
// Destroy
//------------------------------------------------------------------------------
destructor TItemListBase.Destroy;
begin
  objList.Free;

  inherited;
end;
//------------------------------------------------------------------------------
// Save all Items to TextFile
//------------------------------------------------------------------------------
procedure TItemListBase.SaveToFile(const TF : TGenTextFile);
var
  Iter  : integer;
  pItem : TTreeItemBase;
begin
  Iter := 0;
  while self.GetNextItem(Iter, pItem) do
    pItem.SaveToFile(TF);
end;
//------------------------------------------------------------------------------
// Add All Items in List as TListItems to a ListView
//------------------------------------------------------------------------------
procedure TItemListBase.AddToListView(const ListView : TListView);
var
  Iter  : integer;
  pItem : TTreeItemBase;
begin
  Iter := 0;
  while self.GetNextItem(Iter, pItem) do
    pItem.AddToListView(ListView);
end;
//------------------------------------------------------------------------------
// Add a New Item
//------------------------------------------------------------------------------
procedure TItemListBase.AddItem(const pItem : TTreeItemBase);
var
  Ind  : integer;
  pCur : TTreeItemBase;
begin
  if (pItem <> nil) then
    begin
      if (objList.Count > 0) then
        begin
          for Ind := 0 to objList.Count - 1 do
            begin
              pCur := objList[Ind] as TTreeItemBase;

              if pCur.Compare(pItem) >= 0 then
                begin
                  objList.Insert(Ind, pItem);
                  EXIT;
                end;
            end;
        end;
    end;
  objList.Add(pItem);
end;
//------------------------------------------------------------------------------
// Return Count
//------------------------------------------------------------------------------
function TItemListBase.GetCount:integer;
begin
  result :=  objList.Count;
end;
//------------------------------------------------------------------------------
// Add a New Item
//------------------------------------------------------------------------------
procedure TItemListBase.DelItem(const pItem : TTreeItemBase);
var
  Ind  : integer;
begin
  if (pItem <> nil) then
    begin
      for Ind := 0 to objList.Count - 1 do
        if (objList[Ind] = pItem) then
          begin
            TheLog.Log('DelItem2 ' + TTreeItemBase(objList[Ind]).pName +
                       ' Ind ' + IntToStr(Ind));
            objList.Delete(Ind);
            break;
          end;
    end;
end;
//------------------------------------------------------------------------------
// Remove all Unmarked Items in List
//------------------------------------------------------------------------------
procedure TItemListBase.RemoveUnmarked;
var
  Ind : integer;
begin
  if objList.Count > 0 then
    for Ind := objList.Count - 1 to 0 do
      if (not TTreeItemBase(objList[Ind]).pMark) then
        objList.Delete(Ind);
end;
//------------------------------------------------------------------------------
// Add a New Item
//------------------------------------------------------------------------------
function TItemListBase.IsItemRecurse(const pItem : TTreeItemBase):boolean;
var
  Ind  : integer;
begin
  result := false;
  if (pItem <> nil) then
    begin
      for Ind := 0 to objList.Count - 1 do
        if (objList[Ind] = pItem) or
           TTreeItemBase(objList[Ind]).IsItem(pItem) then
          begin
            result := true;
            break;
          end
    end;
end;
//------------------------------------------------------------------------------
// Add a New Item
//------------------------------------------------------------------------------
function TItemListBase.IsItemHere(const pItem : TTreeItemBase):boolean;
var
  Ind  : integer;
begin
  result := false;
  if (pItem <> nil) then
    begin
      for Ind := 0 to objList.Count - 1 do
        if (objList[Ind] = pItem) then
          begin
            result := true;
            break;
          end
    end;
end;
//------------------------------------------------------------------------------
// Find an Item from its Name
//------------------------------------------------------------------------------
function TItemListBase.FindItemHereFromName(const Name : string): TTreeItemBase;
var
  Iter  : integer;
  pItem : TTreeItemBase;
begin
  result := nil;

  Iter := 0;
  while self.GetNextItem(Iter, pItem) do
    if AnsiSameStr(pItem.pName, Name) then
      begin
        result := pItem;
        BREAK;
      end;
end;
//------------------------------------------------------------------------------
// Get Item from Its Id (Recursivly)
//------------------------------------------------------------------------------
function TItemListBase.GetItemFromUid(const Id : Cardinal): TTreeItemBase;
var
  Iter  : integer;
  pItem : TTreeItemBase;
  pCur  : TTreeItemBase;
begin
  result := nil;

  Iter := 0;
  while self.GetNextItem(Iter, pItem) do
    if (pItem.pUID = Id) then
      begin
        result := pItem;
        BREAK;
      end
    else
      begin
        pCur := pItem.GetChildFromUid(Id);
        if (pCur <> nil) then
          begin
            result := pCur;
            BREAK;
          end;
      end;
end;
//------------------------------------------------------------------------------
// Get Item from Its Id (Recursivly)
//------------------------------------------------------------------------------
function TItemListBase.GetItemFromUidLoaded(const Id : Cardinal): TTreeItemBase;
var
  Iter  : integer;
  pItem : TTreeItemBase;
  pCur  : TTreeItemBase;
begin
  result := nil;

  Iter := 0;
  while self.GetNextItem(Iter, pItem) do
    if (pItem.pUIDLoaded = Id) then
      begin
        result := pItem;
        BREAK;
      end
    else
      begin
        pCur := pItem.GetChildFromUidLoaded(Id);
        if (pCur <> nil) then
          begin
            result := pCur;
            BREAK;
          end;
      end;
end;
//------------------------------------------------------------------------------
// Iterate the List
//------------------------------------------------------------------------------
function TItemListBase.GetNextItem(
      var Iter  : integer;
      out pItem : TTreeItemBase)
                : boolean;
begin
  result := false;
  pItem  := nil;

  if (Iter >= 0) and (Iter < objList.Count) then
    begin
      result := true;
      pItem := objList[Iter] as TTreeItemBase;
      Inc(Iter);
    end;
end;
//------------------------------------------------------------------------------
// Get Dirty
//------------------------------------------------------------------------------
function TItemListBase.GetDirty : boolean;
var
  Iter  : integer;
  pItem : TTreeItemBase;
begin
  result := false;

  Iter := 0;
  while self.GetNextItem(Iter, pItem) do
    if pItem.pDirty then
      begin
        result := true;
        BREAK;
      end;
end;
//------------------------------------------------------------------------------
// UnMark All Items
//------------------------------------------------------------------------------
procedure TItemListBase.SetMark(const Value : boolean);
var
  Iter  : integer;
  pItem : TTreeItemBase;
begin
  // Walk All Items in List and Set Mark

  Iter := 0;
  while self.GetNextItem(Iter, pItem) do
    pItem.pMark := Value;
end;
//------------------------------------------------------------------------------
// UnMark All Items
//------------------------------------------------------------------------------
procedure TItemListBase.SetPlayed(const Value : boolean);
var
  Iter  : integer;
  pItem : TTreeItemBase;
begin
  // Walk All Items in List and Set Mark

  Iter := 0;
  while self.GetNextItem(Iter, pItem) do
    pItem.pPlayed := Value;
end;
//------------------------------------------------------------------------------
// Refresh All Nodes
//------------------------------------------------------------------------------
procedure TItemListBase.RefreshNodes(
      const TReeView : TTreeView;
      const TreeNode : TTreeNode);
var
  Timer : TGenTimer;
  Iter  : integer;
  pItem : TTreeItemBase;
  pNode : TTreeNode;
  pNext : TTreeNode;
begin
  TreeView.Items.BeginUpdate;

  if (TreeNode <> nil) then
    begin
      self.pMark := false;

      // Get first Child Node

      pNode := TreeNode.getFirstChild;
      while (pNode <> nil) do
        begin
          // Remember Next Sibling (we might delete this Node)

          pNext := pNode.getNextSibling;

          // Does the Item still exists on Item List

          pItem := pNode.Data;
          if self.IsItemHere(pItem)then
            begin
              pItem.pMark := true;

              // Make sure the Name is Correct

              if (not AnsiSameText(pNode.Text, pItem.pName)) then
                pNode.Text := pItem.pName;

              // Now Refresh all its Childrens

              pItem.RefreshNodes(TreeView, pNode);
            end
          else
            begin
              // If not found remove this Tree Node

              pNode.Delete;
            end;
            
          pNode := pNext;
        end;

      // Walk all Unmarked Items and Add then

      Iter := 0;
      while self.GetNextItem(Iter, pItem) do
        begin
          if (not pItem.pMark) and pItem.IsTreeNode then
            begin
              pNode := TreeView.Items.AddChildObject(
                                      TreeNode, pItem.pName, pItem);
              pNode.ImageIndex    := pItem.pImageId;
              pNode.SelectedIndex := pItem.pImageId;
              pNode.Data := pItem;

              pItem.RefreshNodes(TreeView, pNode);
            end;
        end;
    end
  else
    begin
      Timer := TGenTimer.Create;

      self.pMark := false;

      // Get the First TreeNode (its the first Orphan)

      pNode := TreeView.Items.GetFirstNode;
      while (pNode <> nil) do
        begin
          // Get Next Sibling (we might remove this Node)

          pNext := pNode.getNextSibling;

          // Does the Item still exists

          pItem := pNode.Data;
          if self.IsItemHere(pItem)then
            begin
              pItem.pMark := true;

              // Now Refresh all its Childrens

              pItem.RefreshNodes(TreeView, pNode);
            end
          else
            begin
              // If not found remove this Node

              pNode.Delete;
            end;

          pNode := pNext;
        end;

      TheLog.Log('1 ' + ToStr(Timer.Measure));

      // Walk all Unmarked Items and Add then

      Iter := 0;
      while self.GetNextItem(Iter, pItem) do
        begin
          if (not pItem.pMark) and pItem.IsTreeNode then
            begin
              pNode := TreeView.Items.AddObject(nil, pItem.pName, pItem);
              pNode.ImageIndex    := pItem.pImageId;
              pNode.SelectedIndex := pItem.pImageId;
              pNode.Data          := pItem;

              pItem.RefreshNodes(TreeView, pNode);
            end;
        end;
      TheLog.Log('2 ' + ToStr(Timer.Measure));

      Timer.Free;
    end;

  TreeView.Items.EndUpdate;
end;
//------------------------------------------------------------------------------
//                                    INITIALIZE
//------------------------------------------------------------------------------
initialization

  RegClass(TItemListBase);
end.
