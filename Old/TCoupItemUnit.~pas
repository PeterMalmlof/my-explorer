unit TCoupItemUnit;

interface

uses
  Windows, Classes, Contnrs, ComCtrls, Menus,

  TGenLogUnit,        // Log Object
  TGenTextFileUnit,   // Textfile Class
  TTreeItemBaseUnit;  // Item Base Class

//------------------------------------------------------------------------------
//  Tree Item Base Object
//------------------------------------------------------------------------------
type TCoupItem = Class(TTreeItemBase)
  protected
    objVerbItem : TTreeItemBase;
    objExtItem  : TTreeItemBase;
    objDefault  : boolean;

    procedure SetVerbItem(const Value : TTreeItemBase);
    procedure SetExtItem (const Value : TTreeItemBase);
    procedure SetDefault (const Value : boolean);

    procedure SetVerb (Sender: TObject);
    procedure SetExt  (Sender: TObject);
    procedure SetDef  (Sender: TObject);

  public
    constructor Create(
      const pParent : TTreeItemBase;
      const Name    : string); override;

    constructor CreateFromFile(
      const pParent : TTreeItemBase;
      const TF      : TGenTextFile); override;

    destructor  Destroy; override;

    class function IsTreeNode: boolean; override;

    class function ChildrenClass: TItemClass; override;
    class function GetTypeStr: string; override;

    procedure SaveToFile(const TF : TGenTextFile); override;

    class function GetPropCount: integer; override;
    class function GetPropName (const Col : integer): string; override;
          function GetPropValue(const Col : integer): string; override;
          
    procedure AddListMenuItems(
      const PopupMenu : TPopupMenu;
      const Column    : integer); override;

    property pVerbItem : TTreeItemBase read objVerbItem write SetVerbItem;
    property pExtItem  : TTreeItemBase read objExtItem  write SetExtItem;
    property pDefault  : boolean       read objDefault  write SetDefault;
end;

//------------------------------------------------------------------------------
// Constants used by other modules
//------------------------------------------------------------------------------
const
  PropCoupItem    = 'CoupItem';
  PropCoupDefault = 'ProgDef';
  PropCoupVerbId  = 'VerbId';
  PropCoupExtId   = 'ExtId';

//------------------------------------------------------------------------------
//                                  IMPLEMENTATION
//------------------------------------------------------------------------------
implementation

uses
  SysUtils,
  StrUtils,
  Forms,
  
  TGenStrUnit,        // String Functions
  TGenMenuUnit,

  TMediaPlayerResUnit,// Resource Strings
  TProgItemUnit,
  TVerbItemUnit,
  TExtItemUnit,
  
  TItemListUnit,

  TExplorerAppUnit,   // Application Properties
  TGenClassesUnit;    // Class Management

//------------------------------------------------------------------------------
// Create
//------------------------------------------------------------------------------
constructor TCoupItem.Create(
      const pParent : TTreeItemBase;
      const Name    : string);
begin
  inherited Create(pParent, Name);

  objDefault := false;

  objVerbItem := nil;
  objExtItem  := nil;
end;
//------------------------------------------------------------------------------
// Create From TextFile
//------------------------------------------------------------------------------
constructor TCoupItem.CreateFromFile(
      const pParent : TTreeItemBase;
      const TF      : TGenTextFile);
var
  sProp, sValue : string;
begin
  inherited Create(pParent, '');

  objDefault := false;

  objVerbItem := nil;
  objExtItem  := nil;

  // Read All Attributes from File

  while TF.ReadProp(sProp, sValue) do
    begin

      // Is this Default

      if AnsiStartsText(PropCoupDefault, sProp) then
        begin
          objDefault := true;
        end

      // Is this Verb Id

      else if AnsiStartsText(PropCoupVerbId, sProp) then
        begin
          objVerbItem := TheItems.FindVerbFromLoadedUID(ToInt(sValue));
        end

      // Is this Description

      else if AnsiStartsText(PropCoupExtId, sProp) then
        begin
          objExtItem := TheItems.FindExtFromLoadedUID(ToInt(sValue));
        end

      // Is This End Of Group

      else if AnsiSameText(PropCoupItem, sProp) then
        begin
          BREAK;
        end;
    end;
end;
//------------------------------------------------------------------------------
// Destroy
//------------------------------------------------------------------------------
destructor TCoupItem.Destroy;
begin

  inherited;
end;
//------------------------------------------------------------------------------
// Get Type String with ReadOnly and Hidden Flgs attached
//------------------------------------------------------------------------------
class function TCoupItem.GetTypeStr: string;
begin
  result := resCoupItemType;
end;
//------------------------------------------------------------------------------
// Set Verb
//------------------------------------------------------------------------------
procedure TCoupItem.SetVerbItem(const Value : TTreeItemBase);
begin
  if (objVerbItem <> Value) then
    begin
      objVerbItem := Value;
      self.pDirty := true;
    end;
end;
//------------------------------------------------------------------------------
// Set Ext
//------------------------------------------------------------------------------
procedure TCoupItem.SetExtItem (const Value : TTreeItemBase);
begin
  if (objExtItem <> Value) then
    begin
      objExtItem  := Value;
      self.pDirty := true;
    end;
end;
//------------------------------------------------------------------------------
// Set Default
//------------------------------------------------------------------------------
procedure TCoupItem.SetDefault (const Value : boolean);
begin
  if (objDefault <> Value) then
    begin
      objDefault  := Value;
      self.pDirty := true;
    end;
end;
//------------------------------------------------------------------------------
// Return What Class Items Childrens are
//------------------------------------------------------------------------------
class function TCoupItem.ChildrenClass: TItemClass;
begin
  result := TTreeItemBase;
end;
//------------------------------------------------------------------------------
// Should it have TreeNodes
//------------------------------------------------------------------------------
class function TCoupItem.IsTreeNode: boolean;
begin
  result := false;
end;
//------------------------------------------------------------------------------
// Save This Item
//------------------------------------------------------------------------------
procedure TCoupItem.SaveToFile(const TF : TGenTextFile);
begin
  if (TF <> nil) then
    begin
      TF.WriteProp (PropCoupItem);

      if objDefault then
        TF.WriteProp (PropCoupDefault, 'Yes');

      if (objVerbItem <> nil) and (TheItems.IsVerb(objVerbItem)) then
        TF.WriteProp (PropCoupVerbId, IntToStr(objVerbItem.pUID));

      if (objExtItem <> nil) and (TheItems.IsExt(objExtItem)) then
        TF.WriteProp (PropCoupExtId,  IntToStr(objExtItem.pUID));

      // Extensions dont have any Children

      TF.WriteProp (PropCoupItem);
    end;
end;
//------------------------------------------------------------------------------
//  Get the Property (text) of a specified Column in ListView
//------------------------------------------------------------------------------
class function TCoupItem.GetPropName(const Col : integer): string;
begin
  case Col of
    0 : result := resProgItemType;
    1 : result := resDefault;
    2 : result := TVerbItem.GetTypeStr;
    3 : result := resExtItemDesc;
  end;
end;
//------------------------------------------------------------------------------
// Return Number of Colums in Item Class
//------------------------------------------------------------------------------
class function TCoupItem.GetPropCount: integer;
begin
  result := 4;
end;
//------------------------------------------------------------------------------
//  Get the Property (text) of a specified Column in ListView
//------------------------------------------------------------------------------
function TCoupItem.GetPropValue(const Col : integer): string;
begin
  case Col of
    0 : if (self.pParent <> nil) and (TheItems <> nil) and 
           TheItems.IsProg(self.pParent) then
          result := self.pParent.pName
        else
          result := resUndef;

    1 : if objDefault then result := resYes else result := resNo;

    2 : if TheItems.IsVerb(objVerbItem) then
          result := objVerbItem.pName
        else
          result := resUndef;

    3 : if TheItems.IsExt(objExtItem) then
          result := objExtItem.pName
        else
          result := resUndef;
  else
    result := self.pName;
  end;
end;
//------------------------------------------------------------------------------
// Add Menus accoding to Current Selected Item in ListView
//------------------------------------------------------------------------------
procedure TCoupItem.AddListMenuItems(
      const PopupMenu : TPopupMenu;
      const Column    : integer);
var
  Iter  : integer;
  pItem : TTreeItemBase;
  pMenu : TGenMenu;
  Count : integer;
begin
  inherited AddListMenuItems(PopupMenu, Column);

  Count := 0;

  case Column of

    1 : begin
          // Add Default or Not

          if (Count = 0) then
            begin
              pMenu := TGenMenu.Create(PopupMenu);
              pMenu.Caption := '-';
              PopupMenu.Items.Add(pMenu);
            end;

          pMenu := TGenMenu.Create(PopupMenu);
          pMenu.Caption := resYes;
          pMenu.Checked := objDefault;
          pMenu.OnClick := SetDef;
          PopupMenu.Items.Add(pMenu);
          Inc(Count);

          pMenu := TGenMenu.Create(PopupMenu);
          pMenu.Caption := resNo;
          pMenu.Checked := not objDefault;
          pMenu.OnClick := SetDef;
          PopupMenu.Items.Add(pMenu);
          Inc(Count);
        end;
        
    2 : begin
          // Add All Verbs

          Iter := 0;
          while TheItems.GetNextVerb(Iter, pItem) do
            begin
              if (Count = 0) then
                begin
                  pMenu := TGenMenu.Create(PopupMenu);
                  pMenu.Caption := '-';
                  PopupMenu.Items.Add(pMenu);
                end;

              pMenu := TGenMenu.Create(PopupMenu);
              pMenu.Caption := pItem.pName;
              pMenu.Tag     := pItem.pUID;
              pMenu.Checked := objVerbItem = pItem;
              pMenu.OnClick := SetVerb;
              PopupMenu.Items.Add(pMenu);
              Inc(Count);
            end;
        end;

    3 : begin
          // Add All Extensions

          Iter := 0;
          while TheItems.GetNextExt(Iter, pItem) do
            begin
              if (Count = 0) then
                begin
                  pMenu := TGenMenu.Create(PopupMenu);
                  pMenu.Caption := '-';
                  PopupMenu.Items.Add(pMenu);
                end;

              pMenu := TGenMenu.Create(PopupMenu);
              pMenu.Caption := pItem.pName;
              pMenu.Tag     := pItem.pUID;
              pMenu.Checked := objVerbItem = pItem;
              pMenu.OnClick := SetExt;
              PopupMenu.Items.Add(pMenu);
              Inc(Count);
            end;
        end;
  end;

  if (Count > 0) then
    begin
      pMenu := TGenMenu.Create(PopupMenu);
      pMenu.Caption := '-';
      PopupMenu.Items.Add(pMenu);
    end;

  // Delete My Self

  pMenu := TGenMenu.Create(PopupMenu);
  pMenu.Caption := resDelete + SPC + self.pName;
  pMenu.OnClick := DelMySelf;
  PopupMenu.Items.Add(pMenu);
end;
//------------------------------------------------------------------------------
// Map Verb Item
//------------------------------------------------------------------------------
procedure TCoupItem.SetVerb (Sender: TObject);
var
  pItem : TTreeItemBase;
begin
  if (Sender is TMenuItem) then
    begin
      pItem := TheItems.FindVerbFromUID(TMenuItem(Sender).Tag);
      if (pItem <> nil) then
        begin
          objVerbItem := pItem;
          self.pDirty := true;
          PostMessage(Application.Handle, MSG_LISTVIEW_REFRESH, 0, 0);
        end;
    end;
end;
//------------------------------------------------------------------------------
// Map Ext Item
//------------------------------------------------------------------------------
procedure TCoupItem.SetExt  (Sender: TObject);
var
  pItem : TTreeItemBase;
begin
  if (Sender is TMenuItem) then
    begin
      pItem := TheItems.FindExtFromUID(TMenuItem(Sender).Tag);
      if (pItem <> nil) then
        begin
          objExtItem  := pItem;
          self.pDirty := true;
          PostMessage(Application.Handle, MSG_LISTVIEW_REFRESH, 0, 0);
        end;
    end;
end;
//------------------------------------------------------------------------------
// Map Ext Item
//------------------------------------------------------------------------------
procedure TCoupItem.SetDef  (Sender: TObject);
begin
  if (Sender is TMenuItem) then
    begin
      self.pDefault := not self.pDefault;
      PostMessage(Application.Handle, MSG_LISTVIEW_REFRESH, 0, 0);
    end;
end;
//------------------------------------------------------------------------------
//                                    INITIALIZE
//------------------------------------------------------------------------------
initialization
  RegClass(TCoupItem);
end.

