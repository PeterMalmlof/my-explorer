unit MediaBaseUnit;

interface

uses
  Windows, Classes, Contnrs;

//------------------------------------------------------------------------------
//  TMediaBase
//------------------------------------------------------------------------------
type
  TMediaBase = class;                 // Forward Declaration
  TMediaList = class;                 // Forward Declaration

  TMediaClass = class of TMediaBase;  // Item Classes

  TMediaBase = Class(TObject)
  protected
    objUID      : integer;    // Unique Identifier
    objMark     : boolean;    // Marker
    objModified : TFileTime;  // Modified Time of real Object
    objName     : string;     // Name of Object
    objParent   : TMediaBase; // Parent Object
    objChildren : TMediaList; // Children (if Any)

    function  GetName: string; virtual;
    procedure SetName(const Value : string); virtual;

  public
    constructor Create(const Parent : TMediaBase); virtual;
    destructor  Destroy; override;

    // Iterate all Children of this Item

    function GetNextChild(
      var Iter   : integer;     // Iterator
      out pMedia : TMediaBase)  // Returned Chld
                 : boolean;     // True if not EOC

    property pUID      : integer    read objUID       write objUID;
    property pMark     : boolean    read objMark      write objMark;
    property pName     : string     read GetName      write SetName;
    property pParent   : TMediaBase read objParent    write objParent;
    property pModified : TFileTime  read objModified  write objModified;

  end;

  TMediaList = class(TObject)
    protected
      objList : TObjectList;

      function GetCount: integer;

    public
    constructor Create; virtual;
    destructor  Destroy; override;

    // Iterate all Items in List

    function GetNextMedia(
      var Iter   : integer;     // Iterator
      out pMedia : TMediaBase)  // Next Media Object
                 : boolean;     // True if not EOI

    property pCount : integer read GetCount;

  end;

implementation

uses
  SysUtils,
  StrUtils,

  MyFileTimeUnit,

  TGenLogUNit,
  TGenClassesUnit;

var
  ObjectSeq : integer = 0;

//------------------------------------------------------------------------------
//
//                                  TMediaBase
//
//------------------------------------------------------------------------------
// Create
//------------------------------------------------------------------------------
constructor TMediaBase.Create(const Parent : TMediaBase);
begin
  inherited Create;

  objUid := ObjectSeq;
  Inc(ObjectSeq);

  objMark     := false;
  objModified := MyFileTimeZero;
  objName     := '';
  objParent   := Parent;
  objChildren := TMediaList.Create;
end;
//------------------------------------------------------------------------------
// Destroy
//------------------------------------------------------------------------------
destructor TMediaBase.Destroy;
begin

  objChildren.Free;

  inherited;
end;
//------------------------------------------------------------------------------
// Get Name
//------------------------------------------------------------------------------
function TMediaBase.GetName:string;
begin
  result := objName;
end;
//------------------------------------------------------------------------------
// Set Name
//------------------------------------------------------------------------------
procedure TMediaBase.SetName(const Value : string);
begin
  objName := Value;
end;
//------------------------------------------------------------------------------
// Get NextChild
//------------------------------------------------------------------------------
function TMediaBase.GetNextChild(
      var Iter  : integer;      // Iterator
      out pMedia : TMediaBase)  // Returned Chld
                : boolean;      // True if not EOC
begin
  result := objChildren.GetNextMedia(Iter, pMedia);
end;

//------------------------------------------------------------------------------
//
//                                  TMediaList
//
//------------------------------------------------------------------------------
// Create
//------------------------------------------------------------------------------
constructor TMediaList.Create;
begin
  inherited Create;

  objList := TObjectList.Create(true);

end;
//------------------------------------------------------------------------------
// Destroy
//------------------------------------------------------------------------------
destructor TMediaList.Destroy;
begin
  objList.Free;

  inherited;
end;
//------------------------------------------------------------------------------
// Get Count
//------------------------------------------------------------------------------
function TMediaList.GetCount:integer;
begin
  result := objList.Count;
end;
//------------------------------------------------------------------------------
// Iterate all Items in List
//------------------------------------------------------------------------------
function TMediaList.GetNextMedia(
      var Iter   : integer;     // Iterator
      out pMedia : TMediaBase)  // Next Media Object
                 : boolean;     // True if not EOI
begin
  result := false;
  pMedia := nil;

  if (Iter >= 0) and (Iter < objList.Count) then
    begin
      result := true;
      pMedia := objList[Iter] as TMediaBase;
      Inc(Iter);
    end;
end;
//------------------------------------------------------------------------------
//                                    INITIALIZE
//------------------------------------------------------------------------------
initialization
  RegClass(TMediaBase);
end.
 