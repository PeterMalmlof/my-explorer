unit TTestMediaThreadUnit;

interface

uses
  Windows, Classes, Contnrs, ExtCtrls, SysUtils, Forms,

  TMediaBaseUnit,
  TMediaPlayListUnit,
  TMediaQueryUnit,
  TMediaArtistUnit,
  TMediaAlbumUnit,
  TMediaTrackUnit,

  TMediaUtilsUnit,
  TGenThreadUnit;


//------------------------------------------------------------------------------
// Test Media Thread
//------------------------------------------------------------------------------
type TTestMediaThread = class(TGenThread)
  private
    objLinkList  : TObject;      // Media Link List to Test
    objMediaList : TObjectList;  // Media Found needing Refreshing

  protected

    function GetNeedToTerminate : boolean;
  public
    constructor Create(
      const Subsciber : integer;
      const LinkList  : TObject);

    destructor  Destroy; override;

    // CallBack function for retreiving Media to Refresh

    procedure OnMediaToRefresh(Media : TMediaBase);

    procedure Execute; override;

    property NeedToTerminate : boolean read GetNeedToTerminate;
end;

implementation

uses

  TMediaLinkUnit,
  TMediaFactoryUnit,
  TGenTimerUnit;

//------------------------------------------------------------------------------
//
//                              HARD DRIVE THREAD
//
//------------------------------------------------------------------------------
// Create Object
//------------------------------------------------------------------------------
constructor TTestMediaThread.Create(
      const Subsciber : integer;
      const LinkList  : TObject);
begin
  inherited Create(Subsciber);

  // DOnt Kill it when its Finished, we need the result

  self.FreeOnTerminate := false;

  // Remember the reference to the Media Link List

  objLinkList := LinkList;

  // Create the Media List

  objMediaList := TObjectList.Create(false);
  objResult := 0;

  // Now we can run it

  self.Resume;
end;
//------------------------------------------------------------------------------
// Destroy Object
//------------------------------------------------------------------------------
destructor TTestMediaThread.Destroy;
begin
  objMediaList.Destroy;

  inherited;
end;
//------------------------------------------------------------------------------
// CallBack function for retreiving Media to Refresh
//------------------------------------------------------------------------------
procedure TTestMediaThread.OnMediaToRefresh(Media : TMediaBase);
begin
  if Assigned(objMediaList) and Assigned(Media) and (Media is TMediaBase) then
    objMediaList.Add(Media);
end;
//------------------------------------------------------------------------------
// CallBack function for retreiving Media to Refresh
//------------------------------------------------------------------------------
function TTestMediaThread.GetNeedToTerminate : boolean;
begin
  result := self.Terminated;
end;
//------------------------------------------------------------------------------
// Execute Object
//------------------------------------------------------------------------------
procedure TTestMediaThread.Execute;
var
  Timer : TGenTimer;
  Ind   : integer;
  Media : TMediaBase;
begin
  objCS.Acquire;

  // Create the Timer

  Timer := TGenTimer.Create;

  // Start the Testing by iterating the MediaLinkList (it will be Read locked)

  TMediaLinkList(objLinkList).TestMediaBkg(self);

  // Now walk all the Media needing to refresh and do it. The Object List
  // is owned by us, but the Media it references might have been deleted.

  if (self.objMediaList.Count > 0) then
    begin
      objResult := self.objMediaList.Count;
      for Ind := 0 to self.objMediaList.Count -1 do
        begin
          if self.Terminated then BREAK;

          Media := objMediaList[Ind] as TMediaBase;

          // The Media in the List might have been deleted

          if TheMedia.IsMedia(Media) then
            begin
              Media.RefreshMedia;

              PostMessage(Application.Handle, MSG_MEDIA_LOG_BKG,
                  DWORD(Media), 0);
            end;

          sleep(10);
        end;
    end;

  // When finnished we Tell Application about it

  PostMessage(Application.Handle, MSG_MEDIA_TERMINATED,
      self.objSubscriber, round(Timer.Measure/1000));

  Timer.free;

  self.objFinished := true;
  
  objCS.Release;
end;
end.
 