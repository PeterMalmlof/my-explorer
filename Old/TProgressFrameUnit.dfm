object ProgressFrame: TProgressFrame
  Left = 0
  Top = 0
  Width = 224
  Height = 22
  AutoScroll = False
  TabOrder = 0
  OnResize = FrameResize
  object Progress: TGenProgressBar
    Left = 0
    Top = 0
    Width = 209
    Height = 17
    Vertical = False
    ValueMax = 100
    ValueCur = 33
    ValueMin = 0
    ColorFore = clAqua
    ColorHigh = clCream
    TabOrder = 0
    OnDblClick = ProgressDblClick
  end
end
