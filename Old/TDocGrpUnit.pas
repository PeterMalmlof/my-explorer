unit TDocGrpUnit;

interface

uses
  Windows, Classes, Contnrs, ComCtrls, Menus,

  TGenLogUnit,        // Log Object
  TGenTextFileUnit,   // Textfile Class
  TDocFolderUnit,     // Base Class
  TTreeItemBaseUnit;  // Base Item Class

//------------------------------------------------------------------------------
//  Tree Item Base Object
//------------------------------------------------------------------------------
type TDocGrp = Class(TDocFolder)
  protected
    // Folder holds the Folder where this Document Group starts from
    // This can be on any Drive or a Network

    objFolder : string;

    procedure SetFolder(const Value : string);

    function GetPath    : string; override;

  public
    constructor CreateFromFile(
      const pParent : TTreeItemBase;
      const TF      : TGenTextFile); override;

    class function GetTypeStr: string;  override;

    procedure SaveToFile(const TF : TGenTextFile); override;

    class function  GetPropCount: integer; override;
    class function  GetPropName(const Col : integer): string; override;
          function  GetPropValue  (const Col : integer): string; override;
          
    procedure Execute(const Col : integer); override;

    procedure AddListMenuItems(
      const PopupMenu : TPopupMenu;
      const Column    : integer); override;

    procedure LogIt; override;

    property pFolder : string read objFolder write SetFolder;
end;
//------------------------------------------------------------------------------
// Constants used by other modules
//------------------------------------------------------------------------------
const
  PropDocGrp = 'DocGrp';

//------------------------------------------------------------------------------
//                                  IMPLEMENTATION
//------------------------------------------------------------------------------
implementation

uses
  SysUtils,
  StrUtils,
  Forms,

  TGenPickFolderUnit, // Pick a Folder
  TGenMenuUnit,       // TGenMenu Class
  TMyMessageUnit,     // App Messages
  TMediaPlayerResUnit,// Resource Strings
  TDocItemUnit,       // Document Item Class
  TExplorerAppUnit,   // App Properties
  MyFileTimeUnit,     // File Time
  TGenClassesUnit;    // Class Management

//------------------------------------------------------------------------------
// Create From TextFile
//------------------------------------------------------------------------------
constructor TDocGrp.CreateFromFile(
      const pParent : TTreeItemBase;
      const TF      : TGenTextFile);
var
  sProp, sValue : string;
begin
  inherited Create(
    pParent, '', 0, 0, MyFileTimeZero, MyFileTimeZero, false, false);

  // Read All Attributes from File

  while TF.ReadProp(sProp, sValue) do
    begin
      // Is this Name

      if AnsiStartsText(PropName, sProp) then
        begin
          self.pName := sValue;
        end

      // Is this Description

      else if AnsiStartsText(PropDesc, sProp) then
        begin
          //objDesc := sValue;
        end

      // Is this Folder

      else if AnsiStartsText(PropFolder, sProp) then
        begin
          objFolder := sValue;
        end

      // Is This End Of Group

      else if AnsiSameText(PropDocGrp, sProp) then
        begin
          BREAK;
        end;
    end;

  //TheLog.Log('DocGrp Created ' + self.pName + ' Path: ' + self.pPath +
  //            ' Par: ' + self.pParent.pName);
  self.pDirty := false;
end;
//------------------------------------------------------------------------------
// Return Path
//------------------------------------------------------------------------------
function TDocGrp.GetPath:string;
begin
  result := objFolder;
end;
//------------------------------------------------------------------------------
// Get Type String
//------------------------------------------------------------------------------
class function TDocGrp.GetTypeStr: string;
begin
  result := 'Document Link';
end;
//------------------------------------------------------------------------------
// Set Folder
//------------------------------------------------------------------------------
procedure TDocGrp.SetFolder(const Value : string);
begin
  if (not AnsiSameStr(objFolder, Value)) then
    begin
      self.pDirty := true;
      objFolder   := Value;
    end;
end;
//------------------------------------------------------------------------------
// Log Item
//------------------------------------------------------------------------------
procedure TDocGrp.LogIt;
begin
  inherited;
  TheLog.Log(PropFolder + SPC + objFolder);
end;
//------------------------------------------------------------------------------
// Save This Item
//------------------------------------------------------------------------------
procedure TDocGrp.SaveToFile(const TF : TGenTextFile);
begin
  if (TF <> nil) then
    begin
      TF.WriteProp (PropDocGrp);
      TF.WriteProp (PropName,   self.pName);
      TF.WriteProp (PropFolder, objFolder);

      // Note: We dont save its Children (Its Drive Content)

      TF.WriteProp (PropDocGrp);
    end;
end;
//------------------------------------------------------------------------------
//  Get the Property (text) of a specified Column in ListView
//------------------------------------------------------------------------------
class function TDocGrp.GetPropName(const Col : integer): string;
begin
  case Col of
    0 : result := resDocGrpType;
    1 : result := resDesc;
    2 : result := resFolder;
  end;
end;
//------------------------------------------------------------------------------
// Return Number of Colums in Item Class
//------------------------------------------------------------------------------
class function TDocGrp.GetPropCount: integer;
begin
  result := 2;
end;
//------------------------------------------------------------------------------
//  Get the Property (text) of a specified Column in ListView
//------------------------------------------------------------------------------
function TDocGrp.GetPropValue(const Col : integer): string;
begin
  case Col of
    0 : result := self.pName;
    1 : result := objFolder;
  else
    result := self.pName;
  end;
end;
//------------------------------------------------------------------------------
//  User Double Clicked
//------------------------------------------------------------------------------
procedure TDocGrp.Execute(const Col : integer);
var
  sFolder : string;
begin
  case Col of
    2 :
      begin
        sFolder := TGenPickFolder.PickFolder(self.pFolder, resDocGrpPickTitle);

        if length(sFolder) > 0 then
          begin
            self.pFolder := sFolder;
            PostMessage(Application.Handle, MSG_LISTVIEW_REFRESH, 0, 0);
          end;
      end;
  end;
end;
//------------------------------------------------------------------------------
// Add Menus accoding to Current Selected Item in ListView
//------------------------------------------------------------------------------
procedure TDocGrp.AddListMenuItems(
      const PopupMenu : TPopupMenu;
      const Column    : integer);
var
  pMenu : TGenMenu;
begin
  inherited AddListMenuItems(PopupMenu, Column);

  // Delete My Self

  pMenu := TGenMenu.Create(PopupMenu);
  pMenu.Caption := resDelete + SPC + self.pName;
  pMenu.OnClick := DelMySelf;
  PopupMenu.Items.Add(pMenu);
end;
//------------------------------------------------------------------------------
//                                    INITIALIZE
//------------------------------------------------------------------------------
initialization
  RegClass(TDocGrp);
end.

