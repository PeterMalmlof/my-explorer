unit TFullWinUnit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls,

  uRawInput,

  TTreeItemBaseUnit, ImgList, MPlayer;
type
  TFullWin = class(TForm)
    TypeImages: TImageList;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormDblClick(Sender: TObject);
    procedure FormPaint(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    objListBorder   : integer;  // Border to Left, Up and Right
    objBottomBorder : integer;  // Bottom Border
    objFontColor    : TColor;
    objSelectColor  : TColor;
    objBkgColor     : TColor;
    objPenColor     : TColor;

    objCurBaseItem : TTreeItemBase;
    objCurSelected : TTreeItemBase;

    objLastTick : Cardinal;
    objLastCode : string;
    objText : string;

    objItemInfoOn    : boolean;
    objItemInfoLabel : integer;
    objItemInfoStart : integer;

    procedure SetProperties;

    procedure SetBaseItem (const pItem : TTreeItemBase);
    procedure SetSelected (const pItem : TTreeItemBase);

    procedure HandleRemote(
      const Id1 : integer;
      const Id2 : integer);

    procedure PlayDefault;

  public
    { Public declarations }

    procedure HandleWM_INPUTMessage (var Msg : TMessage); message WM_INPUT;

  end;


implementation

{$R *.dfm}

uses
  StrUtils,

  TGenLogUnit,
  TGenStrUnit,
  TGenGraphicsUnit,
  TGenTextFileUnit,
  MyFileTimeUnit,
  TGenShellUnit,
  TItemListBaseUnit,
  TItemListUnit,
  TProgListUnit,
  TDocGrpUnit,
  TDocItemUnit,
  TProgItemUNit,
  TDocFolderUnit,
  
  TExplorerAppUnit,
  TGenClassesUnit;

var
  RawInputDevices : array[0..1] of RAWINPUTDEVICE;
  myRawInput : RAWINPUT;


//------------------------------------------------------------------------------
//  Create
//------------------------------------------------------------------------------
procedure TFullWin.FormCreate(Sender: TObject);
begin

  SetProperties;

  objCurBaseItem := TheItems.pMyDocItem;
  if (objCurBaseItem <> nil) then
    begin
      objCurSelected := objCurBaseItem.GetFirstChild;
    end
  else
    begin
      objCurSelected := nil;
    end;

  // Usage page and collection usage ID...

  RawInputDevices[0].usUsagePage := $FFBC;
  RawInputDevices[0].usUsage := $88;
  RawInputDevices[0].dwFlags := 0;

  // Usage page and collection usage ID...
  RawInputDevices[1].usUsagePage := $C;
  RawInputDevices[1].usUsage := $1;
  RawInputDevices[1].dwFlags := 0;

  // Register for raw input...
  if (not RegisterRawInputDevices (@RawInputDevices,2,SizeOf (RawInputDevices[0]))) then
    begin
      TheLog.Log ('Failed to register RawInputDevices...');
    end;

  objLastTick := Windows.GetTickCount;
  objLastCode := '';
  objText := 'Device Registred';
  objItemInfoOn := true;
end;
//------------------------------------------------------------------------------
//  Handle Raw INput Messages
//------------------------------------------------------------------------------
procedure TFullWin.HandleWM_INPUTMessage (var Msg : TMessage);
var
  dwSize : Cardinal;
  Loop   : Integer;
  ID     : String;
  CurTime : Cardinal;
begin
  dwsize := SizeOf (myRawInput);

  GetRawInputData(Msg.lParam,RID_INPUT,
            @myRawInput,dwSize,SizeOf (RAWINPUTHEADER));

  if (myRawInput.header.dwType = RIM_TYPEHID) then
    begin
      { Collate a unique ID for the remote button - they are constant... }
      ID := '';
      for Loop := 0 to myRawInput.hid.dwSizeHid - 1 do
        begin
          ID := ID + IntToStr (myRawInput.hid.bRawData[Loop]);
          //TheLog.Log('RD ' + IntToStr(myRawInput.hid.bRawData[Loop]));
        end;

      { Display the ID - as that is all we can do at the moment... }

      //TheLog.Log('Message ' + ID);

      CurTime := Windows.GetTickCount - objLastTick;
      if (CurTime < 400) then
        begin

          // There are always two codes

          if IsInt(objLastCode) and IsInt(ID) then
            HandleRemote(ToInt(objLastCode), ToInt(ID));

          objText := objText + ', ' + ID + ' (' + IntToStr(CurTime) + ')';
          objLastCode := '';
        end
      else
        begin
          objText := 'Id ' + ID;
          objLastCode := ID;
        end;

      self.Invalidate;

      objLastTick := Windows.GetTickCount;
    end;
end;

//------------------------------------------------------------------------------
//  Destroy
//------------------------------------------------------------------------------
procedure TFullWin.FormDestroy(Sender: TObject);
begin
  //
  
end;
//------------------------------------------------------------------------------
//  Key
//------------------------------------------------------------------------------
procedure TFullWin.HandleRemote(
      const Id1 : integer;
      const Id2 : integer);
begin
  TheLog.Log('Remote: ' + IntToStr(Id1) + ' / ' + IntToStr(Id2));

  if (Id1 = 292) and (id2 = 200) then
    begin
      objItemInfoOn := not objItemInfoOn;
      self.Invalidate;
    end;
end;
//------------------------------------------------------------------------------
//  Show Information about selected Item
//------------------------------------------------------------------------------
procedure TFullWin.FormKeyPress(Sender: TObject; var Key: Char);
begin
  if (Key = Chr(VK_ESCAPE)) then
    begin
      self.Close;
    end;
end;
//------------------------------------------------------------------------------
//  Key Down
//------------------------------------------------------------------------------
procedure TFullWin.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  pItem : TTreeItemBase;
begin
  TheLog.Log('Key ' + IntToStr(Key));
  ObjText := 'Key ' + IntToStr(Key);
  self.Invalidate;
  
  if (key = VK_UP) then
    begin

      pItem := objCurSelected.GetPrevSibling;
      if (pItem <> nil) then
        SetSelected(pItem);
    end

  else if (key = VK_DOWN) then
    begin
      pItem := objCurSelected.GetNextSibling;
      if (pItem <> nil) then
        SetSelected(pItem);
    end

  else if (key = VK_LEFT) then
    begin
      if (objCurBaseItem <> nil) and
         (objCurBaseItem.pParent <> nil) and
         (not (objCurBaseItem.pParent is TProgList)) then
        SetBaseItem(objCurBaseItem.pParent);
    end

  else if (key = VK_RIGHT) or (key = VK_RETURN) then
    begin
      if (objCurSelected <> nil) and
         (objCurSelected is TDocFolder) then
        SetBaseItem(objCurSelected)
      else if (objCurSelected <> nil) then
        begin
          // Play width Default Program

          PlayDefault;
        end;
    end;
end;
//------------------------------------------------------------------------------
//  Play this with Default Player
//------------------------------------------------------------------------------
procedure TFullWin.PlayDefault;
var
  sExt : string;
  sErr : string;
  sPar : string;
  pProg : TTreeItemBase;
begin

  // Get Extension from FileName

  sExt := SysUtils.ExtractFileExt(objCurSelected.pName);
  if length(sExt) > 0 then
    sExt := AnsiRightStr(sExt, length(sExt) - 1);

  TheLog.Log('Open: ' + objCurSelected.pName + ' Ext: ' + sExt);

  pProg := TheItems.GetDefProg(sExt,'Play');
  if pProg <> nil then
    begin
      sPar := '"' + TDocItem(objCurSelected).pPath + '" ' + pProg.pDesc;
      TheLog.Log('Open: ' + objCurSelected.pName + ' Ext: ' + sExt +
        ' With ' + pProg.pName + ' ' + sPar);

      if not DoShellCmd(TProgItem(pProg).pFile, sPar, false, sErr) then
        TheLog.Log(sErr);
    end; 
end;
//------------------------------------------------------------------------------
//  Double Click
//------------------------------------------------------------------------------
procedure TFullWin.FormDblClick(Sender: TObject);
begin
  self.Close;
end;
//------------------------------------------------------------------------------
//  Reload all Items
//------------------------------------------------------------------------------
procedure TFullWin.SetBaseItem (const pItem : TTreeItemBase);
begin
  // Remember Last Selected Item

  objCurBaseItem.pSelected := objCurSelected;

  // Set New Base Item, and Refresh it

  objCurBaseItem := pItem;
  objCurBaseItem.RefreshChildren;

  // Set New Selected Item

  objCurSelected := objCurBaseItem.pSelected;

  // Redraw all

  self.Invalidate;
end;
//------------------------------------------------------------------------------
//  Reload all Items
//------------------------------------------------------------------------------
procedure TFullWin.SetSelected (const pItem : TTreeItemBase);
begin
  objCurSelected := pItem;

  // Redraw all

  self.Invalidate;
end;
//------------------------------------------------------------------------------
//  Calculate All Paint Settings and Borders
//------------------------------------------------------------------------------
procedure TFullWin.SetProperties;
var
  TextSize : TSize;
begin
  // Avoid Flickering

  self.DoubleBuffered := true;

  // Set Full Screen
  
  with self do
    begin
      BorderStyle := bsNone;
      FormStyle   := fsStayOnTop;
      Left        := 0;
      Top         := 0;
      Height      := Screen.Height;
      Width       := Screen.Width;
      Color       := RGB(0,0,20);
    end;

  objFontColor    := RGB(0, 240, 240);
  objSelectColor  := RGB(0,  70,  70);
  objBkgColor     := RGB(0,  30,  30);
  objPenColor     := RGB(0, 230, 230);

  // Set Text Properties

  self.Font.Name   := 'Verdana';
  self.Font.Color  := objFontColor;
  self.Font.Height := Screen.Height div 25;
  self.Color       := objBkgColor;

  TextSize := self.Canvas.TextExtent('XXXXXXX');

  // Set Borders

  objListBorder   := Screen.Height div 9;
  objBottomBorder := (TextSize.cy + 5) * 3 + 10;

  objItemInfoLabel := objListBorder + 10;
  objItemInfoStart := objListBorder + 10 + TextSize.cx;
end;
//------------------------------------------------------------------------------
//  Paint
//------------------------------------------------------------------------------
procedure TFullWin.FormPaint(Sender: TObject);
var
  Iter  : integer;
  pItem : TTreeItemBase;
  Pos   : TPoint;
  sTmp  : string;
  size  : TSize;
  R     : TRect;
begin
  if (objCurBaseItem <> nil) then
    begin
      // Set Start Position X & Y

      Pos.X := objListBorder;
      Pos.Y := objListBorder;

      // Walk all Children of the Current Item

      Iter := 0;
      while objCurBaseItem.GetNextChild(Iter, pItem) do
        begin
          // Get Name

          sTmp := pItem.pName;
          size := self.Canvas.TextExtent(sTmp);

          // Draw Image depending on Type

          TypeImages.Draw(self.Canvas,
            Pos.X,
            Pos.Y + (size.cy - 32) div 2, pItem.pImageId, true);

          // Set Text Background Color depending on Selection

          if (objCurSelected = pItem) then
            self.Canvas.Brush.Color := objSelectColor
          else
            self.Canvas.Brush.Color := objBkgColor;

          // Draw Selection if selected Item

          if (objCurSelected = pItem) then
            begin
              R.Left   := Pos.X + 42 - 5;
              R.Right  := R.Left + size.cx + 15;
              R.Top    := Pos.Y - 2;
              R.Bottom := Pos.Y + size.cy + 4;

              self.Canvas.Pen.Color := objPenColor;

              self.Canvas.Rectangle(R);
            end;

          // Draw Text

          self.Canvas.TextOut(pos.X + 42, pos.Y, sTmp);

          // Increment Y

          Pos.Y := Pos.Y + size.cy + 5;

          // If No more room, break out

          if ((Pos.Y + size.cy + 5) >
             (Screen.Height - objBottomBorder)) then BREAK;
        end;

      self.Canvas.Brush.Color := RGB(0,0,20);
      self.Canvas.TextOut(20, 20,  objText);

      if objItemInfoOn and (objCurSelected <> nil) then
        begin
          self.Canvas.Pen.Color   := objPenColor;
          self.Canvas.Brush.Color := objSelectColor;

          R.Left   := Pos.X;
          R.Right  := Screen.Width - objListBorder;
          R.Top    := Screen.Height - objBottomBorder;
          R.Bottom := Screen.Height - 10;

          self.Canvas.Rectangle(R);

          Pos.Y := Screen.Height - objBottomBorder + 2;

          sTmp := 'Type';
          self.Canvas.TextOut(objItemInfoLabel, Pos.Y, sTmp);
          sTmp := objCurSelected.pTypeStr;
          self.Canvas.TextOut(objItemInfoStart, Pos.Y, sTmp);

          Pos.Y := Pos.Y + size.cy + 5;

          sTmp := 'Size';
          self.Canvas.TextOut(objItemInfoLabel, Pos.Y, sTmp);
          sTmp := SizeToStr(TDocItem(objCurSelected).pSize);
          self.Canvas.TextOut(objItemInfoStart, Pos.Y, sTmp);

          Pos.Y := Pos.Y + size.cy + 5;

          sTmp := 'Created';
          self.Canvas.TextOut(objItemInfoLabel, Pos.Y, sTmp);
          sTmp := MyFileTimeToStr(TDocItem(objCurSelected).pCre);
          self.Canvas.TextOut(objItemInfoStart, Pos.Y, sTmp);

        end;
    end;
end;
//------------------------------------------------------------------------------
//                              ININTIALIZATION
//------------------------------------------------------------------------------
initialization
  RegClass(TFullWin);
end.
