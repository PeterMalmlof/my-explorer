unit TDocItemUnit;

interface

uses
  Windows, Classes, Contnrs, ComCtrls,

  TGenLogUnit,        // Log Object
  TGenTextFileUnit,   // Textfile Class
  TTreeItemBaseUnit;  // Item Base Class

//------------------------------------------------------------------------------
//  Docuemnt Item
//------------------------------------------------------------------------------
type TDocItem = Class(TTreeItemBase)
  protected
    objSize     : int64;     // Size of File/Folder
    objCre      : TFileTime; // When Item was Created
    objMod      : TFileTime; // When Item was Modified
    objReadonly : boolean;   // Read Only File/Folder
    objHidden   : boolean;   // Hidden File/Folder

    function  GetPath    : string;    override;
    function  GetNiceName: string;    override;
    function  GetSize    : int64;     override;
    function  GetCre     : TFileTime; override;
    function  GetMod     : TFileTime; override;

  public
    // Use this Class Procedure to create Doc Item of the right subclass

    class function CreateItem(
      const pParent   : TTreeItemBase;
      const Name      : string;
      const nType     : integer;
      const nSize     : int64;
      const tCre      : TFileTime;
      const tMod      : TFIleTime;
      const bReadonly : boolean;
      const bHidden   : boolean)
                      : TDocItem; virtual;

    // Constructor

    constructor Create(
      const pParent   : TTreeItemBase;
      const Name      : string;
      const nType     : integer;
      const nSize     : int64;
      const tCre      : TFileTime;
      const tMod      : TFIleTime;
      const bReadonly : boolean;
      const bHidden   : boolean); reintroduce ; virtual;

    destructor  Destroy; override;

    class function ChildrenClass: TItemClass; override;
    class function GetImageId: integer; override;

    procedure Execute(const Col : integer); override;

    class function IsTreeNode: boolean; override;

    function Compare(
      const pItem : TTreeItemBase;
      const Col   : integer): integer; override;

    // Functions for Handling Item Properties

    class function  GetPropCount: integer; override;
    class function  GetPropName (const Col : integer): string; override;
    class function  GetPropEdit (const Col : integer): boolean; override;
          function  GetPropValue(const Col : integer): string;  override;
          procedure SetPropValue(const Col : integer; const Txt : string); override;

    property pPath    : string read GetPath;
end;

//------------------------------------------------------------------------------
//                                  IMPLEMENTATION
//------------------------------------------------------------------------------
implementation

uses
  SysUtils,
  StrUtils,

  TGenStrUnit,        // String Functions
  TGenShellUnit,      // Shell Functions
  MyFileTimeUnit,     // TFileTime Function
  TGenFindFilesUnit,  // FindFile Class

  TMediaPlayerResUnit,// Resource Strings

  TDocGrpUnit,
  TDocFolderUnit,
  TDocTrackUnit,
  TDocVideoUnit,
  TDocFileUnit,
  TItemListUNit,
  TProgItemUnit,
  TDocPlayListUnit,
  TDocTextFileUnit,
  TMediaPlayListUnit,

  TExplorerAppUnit,
  TGenClassesUnit;    // Class Management

//------------------------------------------------------------------------------
// Use this Class Procedure to create Doc Item of the right subclass
//------------------------------------------------------------------------------
class function TDocItem.CreateItem(
      const pParent   : TTreeItemBase;
      const Name      : string;
      const nType     : integer;
      const nSize     : int64;
      const tCre      : TFileTime;
      const tMod      : TFIleTime;
      const bReadonly : boolean;
      const bHidden   : boolean)
                      : TDocItem;
var
  sExt : string;
begin

  //TheLog.Log('TDocItem.CreateItem ' + Name);

  case nType of

    // If its a Folder crate that

    ftFolder   : result := TDocFolder.Create(pParent, Name, nType,
                         nSize, tCre, tMod, bReadonly,bHidden);
  else
    sExt := ExtractFileExt(Name);

    // Is an Video File

    if AnsiSameText(sExt,'.avi') or
       AnsiSameText(sExt,'.mpg') or
       AnsiSameText(sExt,'.mkv') or
       AnsiSameText(sExt,'.mpeg') then
      begin
        result := TDocVideo.Create(pParent, Name, nType,
                         nSize, tCre, tMod, bReadonly,bHidden);
      end

    // Is it a Audio FIle

    else if AnsiSameText(sExt,'.mp3') then
      begin
        result := TDocTrack.Create(pParent, Name, nType,
                         nSize, tCre, tMod, bReadonly,bHidden);
      end

    // Is it a Play List

    else if AnsiSameText(sExt, PlayListExt) then
      begin
        result := TDocPlayList.Create(pParent, Name, nType,
                         nSize, tCre, tMod, bReadonly,bHidden);
      end

    // Is it a TextFile

    else if AnsiSameText(sExt,'.txt') then
      begin
        result := TDocTextFile.Create(pParent, Name, nType,
                         nSize, tCre, tMod, bReadonly,bHidden);
      end

    // Its another kind of file

    else
      begin
        result := TDocFile.Create(pParent, Name, nType,
                         nSize, tCre, tMod, bReadonly,bHidden);
      end;
  end;
end;
//------------------------------------------------------------------------------
// Create
//------------------------------------------------------------------------------
constructor TDocItem.Create(
      const pParent   : TTreeItemBase;
      const Name      : string;
      const nType     : integer;
      const nSize     : int64;
      const tCre      : TFileTime;
      const tMod      : TFIleTime;
      const bReadonly : boolean;
      const bHidden   : boolean);
begin
  //TheLog.Log(self.ClassName + '.Create ' + Name);
  
  inherited Create(pParent, Name);
  
  objSize     := nSize;
  objCre      := tCre;
  objMod      := tMod;
  objReadonly := bReadOnly;
  objHidden   := bHidden;
end;
//------------------------------------------------------------------------------
// Destroy
//------------------------------------------------------------------------------
destructor TDocItem.Destroy;
begin

  inherited;
end;
//------------------------------------------------------------------------------
// Compare two Items (Used for sorting)
//------------------------------------------------------------------------------
function TDocItem.Compare(
      const pItem : TTreeItemBase;
      const Col   : integer): integer;
begin
  if (pItem is TDocFolder) and (Col = 0) then
    result := +1 // Folders are always first
  else
    result := AnsiCompareStr(self.GetPropValue(Col), pItem.GetPropValue(Col));
end;
//------------------------------------------------------------------------------
// Return What Class Items Childrens are
//------------------------------------------------------------------------------
class function TDocItem.ChildrenClass: TItemClass;
begin
  result := TTreeItemBase;
end;
//------------------------------------------------------------------------------
// Should it have TreeNodes. Only Folders Has TreeNodes
//------------------------------------------------------------------------------
class function TDocItem.IsTreeNode: boolean;
begin
  result := false;
end;
//------------------------------------------------------------------------------
// Get Full Path
//------------------------------------------------------------------------------
function TDocItem.GetPath: string;
begin
  if (self.pParent <> nil) then
    result := InDelim(self.pParent.pPath) + self.pName
  else
    result := self.pName;
end;
//------------------------------------------------------------------------------
// Get a Nice Name stripping all text within delimiters
//------------------------------------------------------------------------------
function TDocItem.GetNiceName: string;
begin
  result := ExcludeDelim(StripFileExt(GetFileName(self.pName)), '[',']');
end;
//------------------------------------------------------------------------------
// Get Last Selected
//------------------------------------------------------------------------------
function TDocItem.GetSize: int64;
begin
  result := objSize;
end;
//------------------------------------------------------------------------------
// Get Last Selected
//------------------------------------------------------------------------------
function TDocItem.GetCre : TFileTime;
begin
  result := objCre;
end;
//------------------------------------------------------------------------------
// Get Last Selected
//------------------------------------------------------------------------------
function TDocItem.GetMod : TFileTime; 
begin
  result := objMod;
end;
//------------------------------------------------------------------------------
// Return What Class Items Childrens are
//------------------------------------------------------------------------------
class function TDocItem.GetImageId:integer;
begin
  result := ImageIdDoc;
end;
//------------------------------------------------------------------------------
// Handle wahat happens if an Item is Double clicked on a ViewList Column
//------------------------------------------------------------------------------
procedure TDocItem.Execute(const Col : integer);
var
  sExt : string;
  sErr : string;
  sPar : string;
  pProg : TTreeItemBase;
begin

  // Get Extension from FileName

  sExt := SysUtils.ExtractFileExt(self.pName);
  if length(sExt) > 0 then
    sExt := AnsiRightStr(sExt, length(sExt) - 1);
    
  TheLog.Log('Open: ' + self.pName + ' Ext: ' + sExt);

  pProg := TheItems.GetDefProg(sExt,'Play');
  if pProg <> nil then
    begin
      sPar := '"' + self.GetPath + '" ' + TProgItem(pProg).pDesc;

      TheLog.Log('Open: ' + self.pName  + ' Ext: ' + sExt +
                 ' With ' + pProg.pName + ' ' + sPar);

      if not DoShellCmd(TProgItem(pProg).pFile, sPar, false, sErr) then
        TheLog.Log(sErr);
    end;
end;
//------------------------------------------------------------------------------
//
//                                  PROPERTIES
//
//------------------------------------------------------------------------------
// Return Number of Colums in Item Class
//------------------------------------------------------------------------------
class function TDocItem.GetPropCount: integer;
begin
  result := 5;
end;
//------------------------------------------------------------------------------
//  Get the Property (text) of a specified Column in ListView
//------------------------------------------------------------------------------
class function TDocItem.GetPropName(const Col : integer): string;
begin
  case Col of
    0 : result := resName;
    1 : result := resSize;
    2 : result := resType;
    3 : result := resCreated;
    4 : result := resModified;
  end;
end;
//------------------------------------------------------------------------------
//  Return true if this Column in ListView is Editable
//------------------------------------------------------------------------------
class function TDocItem.GetPropEdit(const Col : integer): boolean;
begin
  case Col of
    0 : result := true;
  else
    result := false;
  end;
end;
//------------------------------------------------------------------------------
//  Get Column Text
//------------------------------------------------------------------------------
function TDocItem.GetPropValue(const Col : integer): string;
begin
  result := '';
  Case Col of
    0 : result := self.pNiceName;
    1 : result := SizeToStr(objSize);
    2 : result := self.pTypeStr;
    3 : result := MyFileTimeToStr(objCre);
    4 : result := MyFileTimeToStr(objMod);
  else
    result := self.pName;
  end;
end;
//------------------------------------------------------------------------------
//  Update Coumn Text
//------------------------------------------------------------------------------
procedure TDocItem.SetPropValue(const Col : integer; const Txt : string);
begin
  Case Col of
    0 :
    begin
      // We must change the name of the Folder or File

      // TODO self.pName := Txt;
    end;
  end;
end;
//------------------------------------------------------------------------------
//                                    INITIALIZE
//------------------------------------------------------------------------------
initialization
  RegClass(TDocItem);
end.

