unit TProgressFrameUnit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, 
  Dialogs, ComCtrls, ExtCtrls,

  TGenLogUnit,
  TMyMessageUnit,
  TWmMsgFactoryUnit,
  TMediaUtilsUnit,
  TGenProgressBarUnit;

type
  TProgressFrame = class(TFrame)
    Progress: TGenProgressBar;
    procedure FrameResize(Sender: TObject);
    procedure ProgressDblClick(Sender: TObject);
    procedure ProgressLog(sLine: String);
  private
    objMe        : TWmMsgSubscriber; // Subscriber
    objMsgPump   : TWmMsgFactory;    // Message Pump

    // Message Pump Process

    procedure MsgPumpProc(var Msg : TMsg);

  public

    procedure StartUp(
      const MsgPump   : TWmMsgFactory;
      const ColorFore : TColor;
      const ColorHigh : TCOlor);

    procedure ShutDown;
  end;

implementation

{$R *.dfm}

const
  BRD = 2;

//------------------------------------------------------------------------------
//  Startup
//------------------------------------------------------------------------------
procedure TProgressFrame.Startup(
      const MsgPump   : TWmMsgFactory;
      const ColorFore : TColor;
      const ColorHigh : TCOlor);
begin
  objMsgPump   := MsgPump;

  Progress.ColorFore := ColorFore;
  Progress.ColorHigh := ColorHigh;

  objMe := objMsgPump.Subscribe(self.ClassName, MsgPumpProc);
  objMe.AddMessage(MSG_STATE_COUNTER_MAX);
  objMe.AddMessage(MSG_STATE_COUNTER_CUR);

  Progress.ValueCur := 0;

  Progress.Color := self.Color;

  TheLog.Log('TProgressFrame.Started');
end;
//------------------------------------------------------------------------------
//  ShutDown
//------------------------------------------------------------------------------
procedure TProgressFrame.ShutDown;
begin

  // Unscribe all messages

  objMsgPump.DeSubscribe(objMe);
  objMe := nil;

  //TheLog.Log('TProgressFrame.ShutDown');
end;
//------------------------------------------------------------------------------
//  Receive Messages Subscribed On
//------------------------------------------------------------------------------
procedure TProgressFrame.MsgPumpProc(var Msg : TMsg);
begin
  //if self.Visible then
  case Msg.message of

  MSG_STATE_COUNTER_MAX :
    begin
      Progress.ValueMax := Msg.wParam;
      //TheLog.Log('TProgressFrame.Max ' + IntToStr(objProgressMax));
    end;

  MSG_STATE_COUNTER_CUR :
    begin
      Progress.ValueCur := Msg.wParam;


      //TheLog.Log('TProgressFrame.Cur ' + IntToStr(ProgressBar.Position) +
      //    ' (' + IntToStr(ProgressBar.Max) + ')');
    end;
  end;
end;
//------------------------------------------------------------------------------
//  Resize Frame
//------------------------------------------------------------------------------
procedure TProgressFrame.FrameResize(Sender: TObject);
begin
  Progress.Left   := BRD;
  Progress.Width  := self.ClientWidth;
  Progress.Top    := BRD;
  Progress.Height := self.ClientHeight - BRD*2;
  
 // Progress.Invalidate;
end;
//------------------------------------------------------------------------------
//  User Double CLicked
//------------------------------------------------------------------------------
procedure TProgressFrame.ProgressDblClick(Sender: TObject);
begin
  PostMessage(Application.Handle, MSG_ESCAPE, 0,0);
end;
//------------------------------------------------------------------------------
//  Logging from Component
//------------------------------------------------------------------------------
procedure TProgressFrame.ProgressLog(sLine: String);
begin
  TheLog.Log(sLine);
end;

end.
