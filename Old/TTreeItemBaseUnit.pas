unit TTreeItemBaseUnit;

interface

//{$DEFINE LOGREFRESH}

uses
  Windows, Classes, Contnrs, ComCtrls, Menus, Messages,

  TGenLogUnit,        // Log Object
  TGenTextFileUnit,   // Textfile Class
  TGenTreeViewUnit;   // TreeView

//------------------------------------------------------------------------------
//  ImageIds
//------------------------------------------------------------------------------
const
  ImageIdUndef    =  0;  // Undefined Item
  ImageIdProgList =  1;  // Program List
  ImageIdProgGrp  =  2;  // Program Group
  ImageIdProg     =  3;  // Program
  ImageIdExtList  =  4;  // Extension List
  ImageIdFolder   =  5;  // Folder
  ImageIdVideo    =  6;  // Video
  ImageIdAudio    =  7;  // Audio
  ImageIdDoc      =  8;  // Document
  ImageIdExtItem  =  9;  // Extension
  ImageIdVerbList = 10;  // Verb List
  ImageIdVerbItem = 11;  // Verb
  ImageIdCoupItem = 12;  // Coupling
  ImageIdPlayList = 13;  // PlayList
  ImageIdQuery    = 14;  // PlayList Query
  
//------------------------------------------------------------------------------
// We need a Generic Base Class, a List of Objects of this Class and a
// Class Of this Base class. These three classes should all know about
// each other (friendlies) so they need to be defined together
//------------------------------------------------------------------------------

type
  TItemListBase = class;                // Forward Declaration
  TTreeItemBase = class;                // Forward Declaration

  TItemClass = class of TTreeItemBase;  // Item Classes

  TItemJust = (
    ijLeft,
    ijCenter,
    ijRight);

//------------------------------------------------------------------------------
//
//  Tree Item Base Object
//
//------------------------------------------------------------------------------
  TTreeItemBase = Class(TObject)
  private
    objUID        : Cardinal;       // Unique Id, set by Create
    objUIDLoaded  : Cardinal;       // Saved Id, used when Loading refrences
    objName       : string;         // Name of the Item
    objMark       : boolean;
    objDirty      : boolean;
    objParent     : TTreeItemBase;
    objSelected   : TTreeItemBase;
    objJust       : TItemJust;

  protected
    objList : TItemListBase;  // Children (Item List)

    //--------------------------------------------------------------------------
    // Property Methods
    //--------------------------------------------------------------------------

    procedure SetMark (const Value : boolean); // Mark (recursive)
    procedure SetName (const Value : string);  // Case sensitive

    function  GetNiceName   : string; virtual; // Removes [...]
    function  GetDirty      : boolean;         // (recursive)
    function  GetChildCount : integer;         // Return nr of Children
    function  GetSelected   : TTreeItemBase;   // Tests if still here

    function  GetPath : string;    virtual;    // Returns ''
    function  GetSize : int64;     virtual;    // Returns 0
    function  GetCre  : TFileTime; virtual;    // Returns Zero FileTime
    function  GetMod  : TFileTime; virtual;    // Returns Zero FileTime

    // Delete My Self from My Parent (it wont hurt much)

    procedure DelMySelf   (Sender: TObject);

    //--------------------------------------------------------------------------
    // Internal User Interface Methods
    //--------------------------------------------------------------------------

    // Add a New Child of this Item (Menu Command)

    procedure AddNewChild (Sender: TObject); virtual;

  public
    constructor Create(
      const pParent : TTreeItemBase;
      const Name    : string); virtual;

    constructor CreateFromFile(
      const pParent : TTreeItemBase;
      const TF      : TGenTextFile); virtual;

    destructor  Destroy; override;

    // Iterate all Children of this Item

    function GetNextChild(
      var Iter  : integer;        // Iterator
      out pItem : TTreeItemBase)  // Returned Chld
                : boolean;        // True if not EOC

    // Find an Item from its UID (recursive)

    function GetChildFromUid (const Id : Cardinal): TTreeItemBase;

    // Find an Item from its Loaded UID (recursive)

    function GetChildFromUidLoaded(const Id : Cardinal): TTreeItemBase;

    // Get the First Child of this Item

    function GetFirstChild  :TTreeItemBase;

    // Get Next Sibling of this Item

    function GetNextSibling :TTreeItemBase;

    // Get Previous Sibling of this Item

    function GetPrevSibling :TTreeItemBase;

    // Return True if pItem exists as children in this Item (recursive)

    function  IsItem(const pItem : TTreeItemBase):boolean;

    // Save this Item (not recursive)

    procedure Save; virtual;

    // Save to an Open File

    procedure SaveToFile(const TF : TGenTextFile); virtual;

    // Load this Item

    procedure Load; virtual;

    // Execute this Item

    procedure Execute(const Col : integer); virtual;

    // Delete all Children

    procedure DelChildren(const pItem : TTreeItemBase);

    // Refresh all Children

    procedure RefreshChildren;virtual;

    // Rebuild Media Content of this Item (recursive)

    procedure MediaDbRebuild
      (var Esc : boolean; var Count, Loop : integer); virtual;

    //--------------------------------------------------------------------------
    // Return the Item Class this class Childrens are of
    // Note: Can be used to Call Class Methods, and constructors only
    //       If no Children allowed it returns Base Class
    //--------------------------------------------------------------------------

    class function ChildrenClass: TItemClass; virtual;

    // Return Class Type String

    class function GetTypeStr: string;  virtual;

    // Return Class Image Id

    class function GetImageId: integer; virtual;

    // Return Class AutoExpand

    class function AutoExpand: boolean; virtual;

    // Return true if Parent Item contains ListView

    class function ParentListView: boolean; virtual;

    //--------------------------------------------------------------------------
    // Menu Management
    //--------------------------------------------------------------------------

    // Add Menus according to Current Selected Item in TreeView

    procedure AddTreeMenuItems(const PopupMenu : TPopupMenu); virtual;

    // Add Menus accoding to Current Selected Item in ListView and Column

    procedure AddListMenuItems(
      const PopupMenu : TPopupMenu;
      const Column    : integer); virtual;

    //--------------------------------------------------------------------------
    // TRee View Management
    //--------------------------------------------------------------------------

    // Create a TreeNode type (Note its still free)

    function CreateTreeNode(const sCaption : string):TGenTreeNode; virtual;

    // Return True if this Item should have a TreeNode

    class function IsTreeNode: boolean; virtual;

    // Refresh the TreeNode with this Items Children

    procedure RefreshNodes(
      const TreeView : TGenTreeView;  // TreeView containing the TreeNode
      const TreeNode : TGenTreeNode;  // TreeNode to Refresh
      const bPref    : boolean);      // Add Preference Nodes also

    //--------------------------------------------------------------------------
    // Property Management (Used by ListView management)
    //--------------------------------------------------------------------------

    // Compare this Item with another (used for sorting ListView)

    function  Compare(
      const pItem : TTreeItemBase;
      const Col   : integer): integer; virtual;

    // Return Number of Properties in Item Class

    class function GetPropCount: integer; virtual;

    // Return true if a specific Property Index is Writable

    class function  GetPropEdit(const Col : integer): boolean; virtual;

    // Get the Property Name (string) of a specified Property Index

    class function  GetPropName(const Col : integer): string; virtual;

    // Get the Property (string) of a specified Property Index

    function  GetPropValue(const Col : integer): string; virtual;

    // Get the Property (string) of a specified Property Index

    class function  GetPropJust(const Col : integer): TItemJust; virtual;

    // Set Text Property Value depending on Property Index

    procedure SetPropValue(const Col : integer; const Txt : string);virtual;

    //--------------------------------------------------------------------------
    // Debugging
    //--------------------------------------------------------------------------

    class function GetDebugCount: integer;

    procedure LogIt; virtual;

    //--------------------------------------------------------------------------
    // Properties
    //--------------------------------------------------------------------------

    property pUID       : Cardinal  read objUID       write objUID;
    property pUIDLoaded : Cardinal  read objUIDLoaded write objUIDLoaded;
    property pMark      : boolean   read objMark      write SetMark;
    property pDirty     : boolean   read GetDirty     write objDirty;
    property pName      : string    read objName      write SetName;

    property pTypeStr    : string    read GetTypeStr;
    property pImageId    : integer   read GetImageId;
    property pAutoExpand : boolean   read AutoExpand;
    property pParentListView : boolean   read ParentListView;

    property pPath    : string    read GetPath;
    property pSize    : int64     read GetSize;
    property pCre     : TFileTime read GetCre;
    property pMod     : TFileTime read GetMod;
    property pNiceName : string read GetNiceName;
    property pJust     : TItemJust read objJust write objJust;

    property pParent : TTreeItemBase read objParent;

    property pChildCount : integer read GetChildCount;

    property pSelected : TTreeItemBase read GetSelected write objSelected;
  end;
//------------------------------------------------------------------------------
//
//  Tree Item List Base Object
//
//------------------------------------------------------------------------------
  TItemListBase = Class(TObject)
  protected
    objList : TObjectList;  // List of All Objects

    procedure SetMark   (const Value : boolean);
    procedure SetPlayed (const Value : boolean);

    function  GetDirty: boolean;
    function  GetCount: integer;

  public
    constructor Create;
    destructor  Destroy; override;

    // Save all Items to open TextFile (recursive)

    procedure SaveToFile(const TF : TGenTextFile);

    // Add a new Item to the List

    procedure AddItem(const pItem : TTreeItemBase);

    // Delete an Item in List (and its children...)

    procedure DelItem(const pItem : TTreeItemBase);

    // Return True if Item Exists in List (recursive)

    function  IsItemRecursive (const pItem : TTreeItemBase):boolean;

    // Return True If Item exists in List (not recursive)

    function  IsItemHere    (const pItem : Pointer):boolean;

    // Find an Item from the Name (not recursive)

    function FindItemHereFromName(const Name : string): TTreeItemBase;

    // Get Item from Its UID (recursive)

    function GetItemFromUid      (const Id : Cardinal): TTreeItemBase;

    // Get Item from Its Loaded UID (recursive)

    function GetItemFromUidLoaded(const Id : Cardinal): TTreeItemBase;

    // Iterate all Items in List

    function GetNextItem(
      var Iter  : integer;        // Iterator
      out pItem : TTreeItemBase)  // Next Item
                : boolean;        // True if not EOI

    // Remove All Unmarked Items (recursive)

    procedure RemoveUnmarked;

    //--------------------------------------------------------------------------
    // User Interface Methods
    //--------------------------------------------------------------------------

    // Refresh All TreeNodes in The Item List (recursive)

    procedure RefreshNodes(
      const TreeView : TGenTreeView;  // TreeView containing the TreeNode
      const TreeNode : TGenTreeNode;  // TreeNode to Refresh
      const bPref    : boolean);      // Add Preference Nodes also

    procedure SortList;

    //--------------------------------------------------------------------------
    // Properties
    //--------------------------------------------------------------------------

    property pMark   : boolean                write SetMark;
    property pPlayed : boolean                write SetPlayed;
    property pDirty  : boolean read GetDirty;
    property pCount  : integer read GetCount;
end;

const
  PropId     = 'Uid';
  PropName   = 'Name';
  PropDesc   = 'Desc';
  PropFolder = 'Folder';
  PropFile   = 'File';
  PropVerb   = 'Verb';


//------------------------------------------------------------------------------
//                                  IMPLEMENTATION
//------------------------------------------------------------------------------
implementation

uses
  SysUtils,
  StrUtils,
  Forms,

  TGenStrUnit,        // String Functions
  MyFileTimeUnit,     // TFileTime Function
  TGenTimerUnit,      // Generic Timer Class

  TMyMessageUnit,     // App Messages
  TMediaPlayerResUnit,// Resource Strings
  TDocMediaUnit,
  TDocListUnit,

  TGenClassesUnit;    // Class Management

var ItemCount : integer = 0;
var ItemSeq   : Cardinal = 0;

//------------------------------------------------------------------------------
//
//                              Tree Item Base Object
//
//------------------------------------------------------------------------------
constructor TTreeItemBase.Create(
      const pParent : TTreeItemBase;
      const Name    : string);
begin
  // Increment Item Debug Counter

  Inc(ItemCount);

  // Call inherited

  inherited Create;

  // Set Unique ID and increment its Counter

  objUID := ItemSeq;
  Inc(ItemSeq);

  // Set its Name

  objName     := Name;

  // Set Default Data properties and create ObjList

  objList     := TItemListBase.Create;
  objDirty    := false;
  objParent   := pParent;
  objSelected := nil;
end;
//------------------------------------------------------------------------------
// Create
//------------------------------------------------------------------------------
constructor TTreeItemBase.CreateFromFile(
      const pParent : TTreeItemBase;
      const TF      : TGenTextFile);
begin
  // Increment Item Debug Counter

  Inc(ItemCount);

  // Call inherited

  inherited Create;

  // Set Unique ID and increment its Counter

  objUID := ItemSeq;
  Inc(ItemSeq);

  // Set Default Data properties and create ObjList

  objName     := '';
  objList     := TItemListBase.Create;
  objDirty    := false;
  objParent   := pParent;
  objSelected := nil;
end;
//------------------------------------------------------------------------------
// Destroy
//------------------------------------------------------------------------------
destructor TTreeItemBase.Destroy;
begin
  // Decrement Debug Count

  Dec(ItemCount);

  // Free the Object List

  objList.Free;

  inherited;
end;
//------------------------------------------------------------------------------
//
//                         INTERNAL PROPERTY METHODS
//
//------------------------------------------------------------------------------
// Set Mark
//------------------------------------------------------------------------------
procedure TTreeItemBase.SetMark(const Value : boolean);
begin
  objMark := Value;

  objList.pMark := Value;
end;
//------------------------------------------------------------------------------
// Set a New Name
//------------------------------------------------------------------------------
procedure TTreeItemBase.SetName(const Value : string);
begin
  if (not AnsiSameStr(objName, Value)) then
    begin
      objDirty := true;
      objName  := Value;
    end;
end;
//------------------------------------------------------------------------------
// Log Item
//------------------------------------------------------------------------------
function TTreeItemBase.GetNiceName:string;
begin
  result := objName;
end;
//------------------------------------------------------------------------------
// Log Item
//------------------------------------------------------------------------------
function TTreeItemBase.GetDirty:boolean;
begin
  if objDirty then
    result := true
  else
    result := objList.pDirty;
end;
//------------------------------------------------------------------------------
// Get Number of Childrens
//------------------------------------------------------------------------------
function TTreeItemBase.GetChildCount: integer;
begin
  result := objList.pCount;
end;
//------------------------------------------------------------------------------
// Get Last Selected
//------------------------------------------------------------------------------
function TTreeItemBase.GetSelected: TTreeItemBase;
begin
  if (objSelected <> nil) and
     objList.IsItemHere(objSelected) then
    result := objSelected
  else
    result := nil;
end;
//------------------------------------------------------------------------------
// Get Path
//------------------------------------------------------------------------------
function TTreeItemBase.GetPath:string;
begin
  result := '';
end;
//------------------------------------------------------------------------------
// Get Last Selected
//------------------------------------------------------------------------------
function TTreeItemBase.GetSize: int64;
begin
  result := 0;
end;
//------------------------------------------------------------------------------
// Get Last Selected
//------------------------------------------------------------------------------
function TTreeItemBase.GetCre : TFileTime;
begin
  result := MyFileTimeZero;
end;
//------------------------------------------------------------------------------
// Get Last Selected
//------------------------------------------------------------------------------
function TTreeItemBase.GetMod : TFileTime;
begin
  result := MyFileTimeZero;
end;
//------------------------------------------------------------------------------
// Return Number of Colums in Item Class
//------------------------------------------------------------------------------
class function TTreeItemBase.GetPropCount: integer;
begin
  result := 1;
end;
//------------------------------------------------------------------------------
// Menu: Delete My Self
//------------------------------------------------------------------------------
procedure TTreeItemBase.DelMySelf(Sender: TObject);
begin
  if (objParent <> nil) then objParent.DelChildren(self);

  //PostMessage(Application.Handle, MSG_TREEVIEW_REFRESH, 0, 0);
  //PostMessage(Application.Handle, MSG_LISTVIEW_REFRESH, 0, 0);
end;
//------------------------------------------------------------------------------
//
//                              EXTERNAL METHODS
//
//------------------------------------------------------------------------------
// Get NextChild
//------------------------------------------------------------------------------
function TTreeItemBase.GetNextChild(
      var Iter  : integer;        // Iterator
      out pItem : TTreeItemBase)  // Returned Chld
                : boolean;        // True if not EOC
begin
  result := objList.GetNextItem(Iter, pItem);
end;
//------------------------------------------------------------------------------
// Get Child From Id (recursivly)
//------------------------------------------------------------------------------
function TTreeItemBase.GetChildFromUid(const Id : Cardinal): TTreeItemBase;
begin
  if (objUID = Id) then
    result := self
  else
    result := objList.GetItemFromUid(Id);
end;
//------------------------------------------------------------------------------
// Get Child From Id (recursivly)
//------------------------------------------------------------------------------
function TTreeItemBase.GetChildFromUidLoaded(const Id : Cardinal): TTreeItemBase;
begin
  if (objUIDLoaded = Id) then
    result := self
  else
    result := objList.GetItemFromUidLoaded(Id);
end;
//------------------------------------------------------------------------------
// Get First Child
//------------------------------------------------------------------------------
function TTreeItemBase.GetFirstChild : TTreeItemBase;
var
  Iter  : integer;
  pItem : TTreeItemBase;
begin
  result := nil;
  Iter := 0;
  if objList.GetNextItem(Iter, pItem) then
    result := pItem;
end;
//------------------------------------------------------------------------------
// Get Next Sibling
//------------------------------------------------------------------------------
function TTreeItemBase.GetNextSibling : TTreeItemBase;
var
  Iter : integer;
  pItem : TTreeItemBase;
begin
  result := nil;
  if (objParent <> nil) then
    begin
      Iter := 0;
      while objParent.GetNextChild(Iter, pItem) do
        if (pItem = self) then
          begin
            if objParent.GetNextChild(Iter, pItem) then
              result := pItem;
            BREAK;
          end;
    end;
end;
//------------------------------------------------------------------------------
// Get Previous Sibling
//------------------------------------------------------------------------------
function TTreeItemBase.GetPrevSibling:TTreeItemBase;
var
  Iter : integer;
  pItem : TTreeItemBase;
  pLast : TTreeItemBase;
begin
  result := nil;
  if (objParent <> nil) then
    begin
      Iter := 0;
      pLast := nil;
      while objParent.GetNextChild(Iter, pItem) do
        begin
          if (pItem = self) then
            begin
              result := pLast;
              BREAK;
            end;
          pLast := pItem;
        end;
    end;
end;
//------------------------------------------------------------------------------
// Add a New Item
//------------------------------------------------------------------------------
function TTreeItemBase.IsItem(const pItem : TTreeItemBase):boolean;
begin
  result := objList.IsItemRecursive(pItem);
end;
//------------------------------------------------------------------------------
// Save This Item
//------------------------------------------------------------------------------
procedure TTreeItemBase.Save;
begin
  // Do Nothing
end;
//------------------------------------------------------------------------------
// Save This Item
//------------------------------------------------------------------------------
procedure TTreeItemBase.SaveToFile(const TF : TGenTextFile);
begin
  // Do Nothing
end;
//------------------------------------------------------------------------------
// Load This Item
//------------------------------------------------------------------------------
procedure TTreeItemBase.Load;
begin
  // Do Nothing
end;
//------------------------------------------------------------------------------
// User Double Clicked on Item and it Should perhaps do something
//------------------------------------------------------------------------------
procedure TTreeItemBase.Execute(const Col : integer);
begin
  TheLog.Log('DblClick on ' + objName + ' Col ' + IntToStr(Col));
end;
//------------------------------------------------------------------------------
// Delete a Children
//------------------------------------------------------------------------------
procedure TTreeItemBase.DelChildren(const pItem : TTreeItemBase);
begin
  objList.DelItem(pItem);
  objDirty := true;
end;
//------------------------------------------------------------------------------
// Refresh All Children
//------------------------------------------------------------------------------
procedure TTreeItemBase.RefreshChildren;
begin
  // Do nothing
end;
//------------------------------------------------------------------------------
// Return What Class Items Childrens are
//------------------------------------------------------------------------------
class function TTreeItemBase.ChildrenClass: TItemClass;
begin
  result := TTreeItemBase;
end;
//------------------------------------------------------------------------------
// Get Type String
//------------------------------------------------------------------------------
class function TTreeItemBase.GetTypeStr: string;
begin
  result := resUndef;
end;
//------------------------------------------------------------------------------
// Return What Class Items Childrens are
//------------------------------------------------------------------------------
class function TTreeItemBase.GetImageId:integer;
begin
  result := ImageIdUndef;
end;
//------------------------------------------------------------------------------
// Return Class AutoExpand
//------------------------------------------------------------------------------
class function TTreeItemBase.AutoExpand: boolean;
begin
  result := true;
end;
//------------------------------------------------------------------------------
// Return Class Parent ListView
//------------------------------------------------------------------------------
class function TTreeItemBase.ParentListView: boolean;
begin
  result := false;
end;
//------------------------------------------------------------------------------
// Log Item
//------------------------------------------------------------------------------
procedure TTreeItemBase.LogIt;
begin
  TheLog.Log('Name   ' + objName + ' (' + self.ClassName + ')');
end;
//------------------------------------------------------------------------------
// Get Debug Count
//------------------------------------------------------------------------------
class function TTreeItemBase.GetDebugCount:integer;
begin
  result := ITemCount;
end;
//------------------------------------------------------------------------------
//
//                                 MENU INTERFACE
//
//------------------------------------------------------------------------------
// Add a New Children (Menu Command)
//------------------------------------------------------------------------------
procedure TTreeItemBase.AddNewChild(Sender: TObject);
begin
  TheLog.Log('ERROR: Abstract AddNewChildren');
end;
//------------------------------------------------------------------------------
// Add Menus accoding to Current Selected Item in TreeView
//------------------------------------------------------------------------------
procedure TTreeItemBase.AddTreeMenuItems(const PopupMenu : TPopupMenu);
begin
  // Dont Do Anything. Subclasses should override this
end;
//------------------------------------------------------------------------------
// Add Menus accoding to Current Selected Item in ListView and Column
//------------------------------------------------------------------------------
procedure TTreeItemBase.AddListMenuItems(
      const PopupMenu : TPopupMenu;
      const Column    : integer);
begin
  // Dont Do Anything. Subclasses should override this
end;
//------------------------------------------------------------------------------
//
//                                 TREEVIEW INTERFACE
//
//------------------------------------------------------------------------------
// Return True if this Item should have a TreeNode
//------------------------------------------------------------------------------
class function TTreeItemBase.IsTreeNode: boolean;
begin
  result := true; // Subclasses Should override this
end;
//------------------------------------------------------------------------------
// Refresh the TreeNode with this Items Children
//------------------------------------------------------------------------------
procedure TTreeItemBase.RefreshNodes(
      const TreeView : TGenTreeView;  // TreeView containing the TreeNode
      const TreeNode : TGenTreeNode;  // TreeNode to Refresh
      const bPref    : boolean);      // Add Preference Nodes also
begin
  objList.RefreshNodes(TReeView, TReeNode, bPref);
end;
//------------------------------------------------------------------------------
//
//                                 LISTVIEW INTERFACE
//
//------------------------------------------------------------------------------
// Compare this Item with another (used for sorting ListView)
//------------------------------------------------------------------------------
function TTreeItemBase.Compare(
      const pItem : TTreeItemBase;
      const Col   : integer): integer;
begin
  // Returns +1 if A > B, -1 if A < B, else 0

  result := AnsiCompareStr(self.GetPropValue(Col), pItem.GetPropValue(Col));
  //result := AnsiCompareStr(self.pName, pItem.pName);
end;
//------------------------------------------------------------------------------
//  Get the Property (text) of a specified Column in ListView
//------------------------------------------------------------------------------
class function TTreeItemBase.GetPropName(const Col : integer): string;
begin
  // Dont care about Column, always return Name

  result := resName;
end;
//------------------------------------------------------------------------------
//  Get the Property (text) of a specified Column in ListView
//------------------------------------------------------------------------------
function TTreeItemBase.GetPropValue(const Col : integer): string;
begin
  // Dont care about Column, always return Name

  result := objName;
end;
//------------------------------------------------------------------------------
//  Get the Property Justification
//------------------------------------------------------------------------------
class function TTreeItemBase.GetPropJust(const Col : integer): TItemJust;
begin
  result := ijLeft;
end;
//------------------------------------------------------------------------------
//  Return true if a specific Column is InlineEditable in ListView
//------------------------------------------------------------------------------
class function TTreeItemBase.GetPropEdit(const Col : integer): boolean;
begin
  result := false; // Must be overriden to be able to Edit Column Texts
end;
//------------------------------------------------------------------------------
//  Set Text Property depending on Column in ListView
//------------------------------------------------------------------------------
procedure TTreeItemBase.SetPropValue(const Col : integer; const Txt : string);
begin
  Case Col of
    0 : self.pName := Txt;
  end;
end;
//------------------------------------------------------------------------------
//  Create a Default TreeNode
//------------------------------------------------------------------------------
function TTreeItemBase.CreateTreeNode(const sCaption : string): TGenTreeNode;
begin
  result := TGenTreeNode.Create;
  result.Caption := sCaption;
end;
//------------------------------------------------------------------------------
//
//                            Tree Item List Base Object
//
//------------------------------------------------------------------------------
// Create
//------------------------------------------------------------------------------
constructor TItemListBase.Create;
begin
  inherited Create;

  // Create the ObjectList

  objList  := TObjectList.Create(true);
end;
//------------------------------------------------------------------------------
// Destroy
//------------------------------------------------------------------------------
destructor TItemListBase.Destroy;
begin
  // Free the Object List (recursive)

  objList.Free;

  inherited;
end;
//------------------------------------------------------------------------------
// Save all Items to open TextFile (recursive)
//------------------------------------------------------------------------------
procedure TItemListBase.SaveToFile(const TF : TGenTextFile);
var
  Iter  : integer;
  pItem : TTreeItemBase;
begin
  Iter := 0;
  while self.GetNextItem(Iter, pItem) do
    pItem.SaveToFile(TF);
end;
//------------------------------------------------------------------------------
// Add a new Item to the List
//------------------------------------------------------------------------------
procedure TItemListBase.AddItem(const pItem : TTreeItemBase);
begin
  if (pItem <> nil) then
    begin
      (*if (objList.Count > 0) then
        begin
          // Walk all Items to find the right place to add it

          for Ind := 0 to objList.Count - 1 do
            begin
              pCur := objList[Ind] as TTreeItemBase;

              if pCur.Compare(pItem, 0) >= 0 then
                begin
                  objList.Insert(Ind, pItem);
                  EXIT;
                end;
            end;
        end; *)

      // Still here, Add it at the End

      objList.Add(pItem);
    end;

end;
//------------------------------------------------------------------------------
// Delete an Item in List (and its children...)
//------------------------------------------------------------------------------
procedure TItemListBase.DelItem(const pItem : TTreeItemBase);
var
  Ind  : integer;
begin
  if (pItem <> nil) then
    begin
      for Ind := 0 to objList.Count - 1 do
        if (objList[Ind] = pItem) then
          begin
            //TheLog.Log('DelItem2 ' + TTreeItemBase(objList[Ind]).pName +
            //           ' Ind ' + IntToStr(Ind));
            objList.Delete(Ind);
            break;
          end;
    end;
end;
//------------------------------------------------------------------------------
// Return True if Item Exists in List (recursive)
//------------------------------------------------------------------------------
function TItemListBase.IsItemRecursive(const pItem : TTreeItemBase):boolean;
var
  Ind  : integer;
begin
  result := false;
  if (pItem <> nil) then
    begin
      for Ind := 0 to objList.Count - 1 do
        if (objList[Ind] = pItem) or
           TTreeItemBase(objList[Ind]).IsItem(pItem) then
          begin
            result := true;
            break;
          end
    end;
end;
//------------------------------------------------------------------------------
// Return True If Item exists in List (not recursive)
//------------------------------------------------------------------------------
function TItemListBase.IsItemHere(const pItem : Pointer):boolean;
var
  Ind  : integer;
begin
  result := false;
  if Assigned(pItem) then
    begin
      for Ind := 0 to objList.Count - 1 do
        if (objList[Ind] = pItem) then
          begin
            result := true;
            break;
          end
    end;
end;
//------------------------------------------------------------------------------
// Find an Item from its Name (not recursive)
//------------------------------------------------------------------------------
function TItemListBase.FindItemHereFromName(const Name : string): TTreeItemBase;
var
  Iter  : integer;
  pItem : TTreeItemBase;
begin
  result := nil;

  Iter := 0;
  while self.GetNextItem(Iter, pItem) do
    if AnsiSameStr(pItem.pName, Name) then
      begin
        result := pItem;
        BREAK;
      end;
end;
//------------------------------------------------------------------------------
// Get Item from Its Id (recursive)
//------------------------------------------------------------------------------
function TItemListBase.GetItemFromUid(const Id : Cardinal): TTreeItemBase;
var
  Iter  : integer;
  pItem : TTreeItemBase;
  pCur  : TTreeItemBase;
begin
  result := nil;

  Iter := 0;
  while self.GetNextItem(Iter, pItem) do
    if (pItem.pUID = Id) then
      begin
        result := pItem;
        BREAK;
      end
    else
      begin
        pCur := pItem.GetChildFromUid(Id);
        if (pCur <> nil) then
          begin
            result := pCur;
            BREAK;
          end;
      end;
end;
//------------------------------------------------------------------------------
// Get Item from Its Loaded UID (recursive)
//------------------------------------------------------------------------------
function TItemListBase.GetItemFromUidLoaded(const Id : Cardinal): TTreeItemBase;
var
  Iter  : integer;
  pItem : TTreeItemBase;
  pCur  : TTreeItemBase;
begin
  result := nil;

  Iter := 0;
  while self.GetNextItem(Iter, pItem) do
    if (pItem.pUIDLoaded = Id) then
      begin
        result := pItem;
        BREAK;
      end
    else
      begin
        pCur := pItem.GetChildFromUidLoaded(Id);
        if (pCur <> nil) then
          begin
            result := pCur;
            BREAK;
          end;
      end;
end;
//------------------------------------------------------------------------------
// Iterate all Items in List
//------------------------------------------------------------------------------
function TItemListBase.GetNextItem(
      var Iter  : integer;        // Iterator
      out pItem : TTreeItemBase)  // Next Item
                : boolean;        // True if not EOI
begin
  result := false;
  pItem  := nil;

  if (Iter >= 0) and (Iter < objList.Count) then
    begin
      result := true;
      pItem  := objList[Iter] as TTreeItemBase;
      Inc(Iter);
    end;
end;
//------------------------------------------------------------------------------
// Remove all Unmarked Items in List (recursive)
//------------------------------------------------------------------------------
procedure TItemListBase.RemoveUnmarked;
var
  Ind : integer;
begin
  if (objList.Count > 0) then
    for Ind := objList.Count - 1 downto 0 do
      if (not TTreeItemBase(objList[Ind]).pMark) then
        begin
{$IFDEF LOGREFRESH}
          TheLog.Log('RemoveUnmarked ' + TTreeItemBase(objList[Ind]).pPath);
{$ENDIF}
          objList.Delete(Ind);
        end;
end;
//------------------------------------------------------------------------------
//
//                                PROPERTY METHODS
//
//------------------------------------------------------------------------------
// Return Number of Items in List
//------------------------------------------------------------------------------
function TItemListBase.GetCount:integer;
begin
  result :=  objList.Count;
end;
//------------------------------------------------------------------------------
// Return True if any Item in The List is Dirty (recursive)
//------------------------------------------------------------------------------
function TItemListBase.GetDirty : boolean;
var
  Iter  : integer;
  pItem : TTreeItemBase;
begin
  result := false;

  Iter := 0;
  while self.GetNextItem(Iter, pItem) do
    if pItem.pDirty then
      begin
        result := true;
        BREAK;
      end;
end;
//------------------------------------------------------------------------------
// Mark/UnMark All Items in List (recursive)
//------------------------------------------------------------------------------
procedure TItemListBase.SetMark(const Value : boolean);
var
  Iter  : integer;
  pItem : TTreeItemBase;
begin
  // Walk All Items in List and Set Mark

  Iter := 0;
  while self.GetNextItem(Iter, pItem) do
    pItem.pMark := Value;
end;
//------------------------------------------------------------------------------
// Mark/UnMark all Items in List as Played (recursive)
//------------------------------------------------------------------------------
procedure TItemListBase.SetPlayed(const Value : boolean);
var
  Iter  : integer;
  pItem : TTreeItemBase;
begin
  // Walk All Items in List and Set Mark

  Iter := 0;
  while self.GetNextItem(Iter, pItem) do
    if (pItem Is TDocMedia) then
      TDocMedia(pItem).pPlayed := Value;
end;
//------------------------------------------------------------------------------
// On Compare
//------------------------------------------------------------------------------
function OnMyCompare (Item1, Item2: Pointer): Integer;
begin
  if (Item1 <> nil) and (Item2 <> nil) then
    result := TTreeItemBase(Item1).Compare(TTreeItemBase(Item2), 0)
  else
    result := 0;
end;
//------------------------------------------------------------------------------
// Sort the List
//------------------------------------------------------------------------------
procedure TItemListBase.SortList;
begin
  objList.Sort(OnMyCompare);
end;
//------------------------------------------------------------------------------
//
//                                USER INTERFACE
//
//------------------------------------------------------------------------------
// Refresh All TreeNodes in The Item List (recursive)
//------------------------------------------------------------------------------
procedure TItemListBase.RefreshNodes(
      const TreeView : TGenTreeView;  // TreeView containing the TreeNode
      const TreeNode : TGenTreeNode;  // TreeNode to Refresh
      const bPref    : boolean);      // Add Preference Nodes also
var
  //Timer : TGenTimer;
  Iter  : integer;
  pItem : TTreeItemBase;
  pNode : TGenTreeNode;
  pNext : TGenTreeNode;
begin

  TreeView.BeginUpdate;

  if Assigned(TreeNode) then
    begin
{$IFDEF LOGREFRESH}
      TheLog.Log('RefreshNodes ' + TreeNode.Caption);
{$ENDIF}

      // Unmark all Items in List (recursivly)

      self.pMark := false;

      // Get first Child Node of This TreeNode

      pNode := TreeNode.FirstChild;
      while (pNode <> nil) do
        begin

          // Remember Next Sibling (we might delete this Node)

          pNext := pNode.NextSibling;

          // Does the Item still exists on Item List

          if self.IsItemHere(pNode.Data) then
            begin
{$IFDEF LOGREFRESH}
              TheLog.Log('IsHere ' + pNode.Caption);
{$ENDIF}
              pItem := pNode.Data as TTreeItemBase;

              pItem.pMark := true;

              // Make sure the Name is Correct

              pNode.Caption := pItem.pNiceName;

              // Now Refresh all its Childrens

              pItem.RefreshNodes(TreeView, pNode, bPref);
            end
          else
            begin
{$IFDEF LOGREFRESH}
              TheLog.Log('NotHere ' + pNode.Caption);
{$ENDIF}
              // If Node is Selected we need to Select its Parent instead

              if (TreeView.Selected = pNode) then
                TreeView.Selected := TreeView.Selected.Parent;

              // If not found remove this Tree Node

              pNode.Delete;
            end;
            
          pNode := pNext;
        end;

      // Walk all Unmarked Items and Add then

      Iter := 0;
      while self.GetNextItem(Iter, pItem) do
        begin
          if (not pItem.pMark) and pItem.IsTreeNode then
            begin
{$IFDEF LOGREFRESH}
              TheLog.Log('Add Child Node: ' + pItem.objName +
               ' Parent: ' + pItem.pParent.objName);
{$ENDIF}

              // Create a Free TreeNode of the right type

              pNode := pItem.CreateTreeNode(pItem.pNiceName);
              TreeNode.AddChild(pNode);

              pNode.Data     := pItem;
              pNode.ImageId  := pItem.pImageId;
              pNode.Expanded := pItem.pAutoExpand;

{$IFDEF LOGREFRESH}
              TheLog.Log('Node ' + pNode.Caption +
                  ' P: ' + pNode.Parent.Caption +
                             ' L: ' + IntToStr(pNode.GetLevel));
{$ENDIF}

              pItem.RefreshNodes(TreeView, pNode, bPref);
            end;
        end;

      TReeNode.Sort;
    end
  else
    begin
{$IFDEF LOGREFRESH}
      TheLog.Log('RefreshNodes = nil');
{$ENDIF}

      // Unmark all Items in List (recursivly)

      self.pMark := false;

      // Get the First TreeNode (its the first Orphan)

      pNode := TreeView.FirstNode;
      while Assigned(pNode) do
        begin
{$IFDEF LOGREFRESH}
          TheLog.Log('Refresh Orphan: ' + pNode.Caption);
{$ENDIF}

          // Get Next Sibling (we might remove this Node)

          pNext := pNode.NextSibling;

          // Does the Item still exists

          if self.IsItemHere(pNode.Data) and
             (bPref or (pItem is TDocList)) then
            begin
              pItem := pNode.Data as TTreeItemBase;

              pItem.pMark := true;

              // Make sure the Name is Correct

              pNode.Caption := pItem.pNiceName;

              // Refresh its Childrens

              pItem.RefreshNodes(TreeView, pNode, bPref);
            end
          else
            begin
{$IFDEF LOGREFRESH}
              TheLog.Log('Node.Dat missing ' + pNode.Caption);
{$ENDIF}
              // If Node is Selected we need to Select its Parent instead

              if (TreeView.Selected = pNode) then
                TreeView.Selected := TreeView.Selected.Parent;
                
              // If not found remove this Node

              pNode.Delete;
            end;

          pNode := pNext;
        end;

      // Walk all Unmarked Items and Add then

      Iter := 0;
      while self.GetNextItem(Iter, pItem) do
        begin
          if (not pItem.pMark) and pItem.IsTreeNode then
            begin
              if bPref or (pItem is TDocList) then
                begin
{$IFDEF LOGREFRESH}
                  TheLog.Log('Add Orphan Node: ' + pItem.objName);
{$ENDIF}

                  // Create a Free TreeNode of the right type

                  pNode := pItem.CreateTreeNode(pItem.pNiceName);
                  TreeView.AddNode(pNode);

                  pNode.Data     := pItem;
                  pNode.ImageId  := pItem.pImageId;
                  pNode.Expanded := pItem.pAutoExpand;

{$IFDEF LOGREFRESH}
                  TheLog.Log('Node ' + pNode.Caption +
                             ' L: ' + IntToStr(pNode.GetLevel));
{$ENDIF}

                  // Refresh All Children

                  pItem.RefreshNodes(TreeView, pNode, bPref);
                end;
            end;
        end;

      TreeView.Sort;
    end;

  TreeView.EndUpdate;
end;
//------------------------------------------------------------------------------
// Rebuild Media Db
//------------------------------------------------------------------------------
procedure TTreeItemBase.MediaDbRebuild
    (var Esc : boolean; var Count, Loop : integer);
begin
  // Must be overridden by its subclasses
end;
//------------------------------------------------------------------------------
//                                    INITIALIZE
//------------------------------------------------------------------------------
initialization

  RegClass(TTreeItemBase);
  RegClass(TItemListBase,'TTreeItemBaseUnit');
end.

