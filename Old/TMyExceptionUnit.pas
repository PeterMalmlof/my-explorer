unit TMyExceptionUnit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls;

type
  TMyException = class(TForm)
    Text1: TLabel;
    Text2: TLabel;
    BtnClose: TButton;
    Text3: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure BtnCloseClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormPaint(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}

uses
  TPmaClassesUnit;

const
  BRD = 10;
  WDT = 500;
  HGT = 200;
  
//------------------------------------------------------------------------------
//  Create Form
//------------------------------------------------------------------------------
procedure TMyException.FormCreate(Sender: TObject);
var
  TextHgt : integer;
begin
  self.Caption := 'Player';

  // Set Windows Position

  self.Width  := WDT;
  self.Height := HGT;
  self.Left   := (Screen.DesktopWidth  - WDT) div 2;
  self.Top    := (Screen.DesktopHeight - HGT) div 2;

  TextHgt := Canvas.TextHeight('XXX');

  Text1.Caption := 'Oooops! The Player has encountered an ' +
                   'unexcpected Error, and must close.';

  Text2.Caption := 'No Media has been lost, but you need to restart ' +
                   'the Player. Sorry for the inconviniance.';

  Text1.Left   := Application.Icon.Width + BRD * 2;
  Text1.Top    := BRD;
  Text1.Width  := self.ClientWidth - Text1.Left - BRD;

  Text2.Left   := BRD;
  Text2.Top    := Application.Icon.Height + BRD * 2;
  Text2.Width  := self.ClientWidth - BRD  * 2;

  Text3.Left   := BRD;
  Text3.Top    := Text2.Top + TextHgt * 2 + BRD;
  Text3.Width  := self.ClientWidth - BRD  * 2;

  BtnClose.Left := (self.ClientWidth - BtnClose.Width) div 2;
  BtnClose.Top  := self.ClientHeight - BtnClose.Height - BRD;
end;
//------------------------------------------------------------------------------
//  Close Button
//------------------------------------------------------------------------------
procedure TMyException.BtnCloseClick(Sender: TObject);
begin
  self.Close;
end;
//------------------------------------------------------------------------------
//  Close Action
//------------------------------------------------------------------------------
procedure TMyException.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := caFree;
end;
//------------------------------------------------------------------------------
//  Paint Icon
//------------------------------------------------------------------------------
procedure TMyException.FormPaint(Sender: TObject);
begin
  // Draw the Application Icon

  self.Canvas.Draw(Brd, BRD, Application.Icon);
end;
//------------------------------------------------------------------------------
//                              ININTIALIZATION
//------------------------------------------------------------------------------
initialization
  TPmaClassFactory.RegClass(TMyException);

end.
