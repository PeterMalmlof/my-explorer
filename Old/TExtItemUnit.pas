unit TExtItemUnit;

interface

uses
  Windows, Classes, Contnrs, ComCtrls, Menus,

  TGenLogUnit,        // Log Object
  TGenTextFileUnit,   // Textfile Class
  TProgBaseUnit,      // Base Class
  TTreeItemBaseUnit;  // Item Base Class

//------------------------------------------------------------------------------
//  Tree Item Base Object
//------------------------------------------------------------------------------
type TExtItem = Class(TProgBase)
  protected

  public
    constructor Create(
      const pParent : TTreeItemBase;
      const Name    : string); override;

    constructor CreateFromFile(
      const pParent : TTreeItemBase;
      const TF      : TGenTextFile); override;

    destructor  Destroy; override;

    class function  ChildrenClass: TItemClass; override;
    class function  GetTypeStr: string; override;

    procedure SaveToFile(const TF : TGenTextFile); override;

    class function GetPropCount: integer; override;
    class function GetPropName(const Col : integer): string; override;
          function GetPropValue(const Col : integer): string; override;

    procedure AddListMenuItems(
      const PopupMenu : TPopupMenu;
      const Column    : integer); override;

end;

//------------------------------------------------------------------------------
// Constants used by other modules
//------------------------------------------------------------------------------
const
  PropExtItem = 'ExtItem';

//------------------------------------------------------------------------------
//                                  IMPLEMENTATION
//------------------------------------------------------------------------------
implementation

uses
  SysUtils,
  StrUtils,
  Forms,
  
  TGenStrUnit,        // String Functions
  TGenMenuUnit,       // TGenMenu Class
  
  TMediaPlayerResUnit,// Resource Strings
  TCoupItemUnit,
  TItemListUnit,
  TExplorerAppUnit,   // Application Properties
  TGenClassesUnit;    // Class Management

//------------------------------------------------------------------------------
// Create
//------------------------------------------------------------------------------
constructor TExtItem.Create(
      const pParent : TTreeItemBase;
      const Name    : string);
begin
  inherited Create(pParent, Name);

  objDesc := resExtItemDesc;
end;
//------------------------------------------------------------------------------
// Create From TextFile
//------------------------------------------------------------------------------
constructor TExtItem.CreateFromFile(
      const pParent : TTreeItemBase;
      const TF      : TGenTextFile);
var
  sProp, sValue : string;
begin
  inherited Create(pParent, '');

  objDesc := resExtItemDesc;

  // Read All Attributes from File

  while TF.ReadProp(sProp, sValue) do
    begin
      // Is this Old Id

      if AnsiStartsText(PropId, sProp) then
        begin
          self.pUIDLoaded := ToInt(sValue);
        end

      // Is this Name

      else if AnsiStartsText(PropName, sProp) then
        begin
          self.pName := sValue;
        end

      // Is this Description

      else if AnsiStartsText(PropDesc, sProp) then
        begin
          objDesc := sValue;
        end

      // Is This End Of Group

      else if AnsiSameText(PropExtItem, sProp) then
        begin
          BREAK;
        end;
    end;

  self.pDirty := false;
  
  //TheLog.Log('TExtItem.Create ' + objName);
end;
//------------------------------------------------------------------------------
// Destroy
//------------------------------------------------------------------------------
destructor TExtItem.Destroy;
begin

  inherited;
end;
//------------------------------------------------------------------------------
// Return What Class Items Childrens are
//------------------------------------------------------------------------------
class function TExtItem.ChildrenClass: TItemClass;
begin
  result := TCoupItem;
end;
//------------------------------------------------------------------------------
// Get Type String with ReadOnly and Hidden Flgs attached
//------------------------------------------------------------------------------
class function TExtItem.GetTypeStr: string;
begin
  result := resExtItemType;
end;
//------------------------------------------------------------------------------
// Save This Item
//------------------------------------------------------------------------------
procedure TExtItem.SaveToFile(const TF : TGenTextFile);
begin
  if (TF <> nil) then
    begin
      TF.WriteProp (PropExtItem);
      TF.WriteProp (PropId,   IntToStr(self.pUID));
      TF.WriteProp (PropName, self.pName);
      TF.WriteProp (PropDesc, self.pDesc);

      // Extensions dont have any Children

      TF.WriteProp (PropExtItem);
    end;
end;
//------------------------------------------------------------------------------
//  Get the Property (text) of a specified Column in ListView
//------------------------------------------------------------------------------
class function TExtItem.GetPropName(const Col : integer): string;
begin
  case Col of
    0 : result := resExtItemType;
    1 : result := resDesc;
  end;
end;
//------------------------------------------------------------------------------
// Return Number of Colums in Item Class
//------------------------------------------------------------------------------
class function TExtItem.GetPropCount: integer;
begin
  result := 2;
end;
//------------------------------------------------------------------------------
//  Get the Property (text) of a specified Column in ListView
//------------------------------------------------------------------------------
function TExtItem.GetPropValue(const Col : integer): string;
begin
  case Col of
    0 : result := self.pName;
    1 : result := self.pDesc;
  else
    result := self.pName;
  end;
end;
//------------------------------------------------------------------------------
// Add Menus accoding to Current Selected Item in ListView
//------------------------------------------------------------------------------
procedure TExtItem.AddListMenuItems(
      const PopupMenu : TPopupMenu;
      const Column    : integer);
var
  pMenu : TGenMenu;
begin
  inherited AddListMenuItems(PopupMenu, Column);

  // Delete My Self

  pMenu := TGenMenu.Create(PopupMenu);
  pMenu.Caption := resDelete + SPC + self.pName;
  pMenu.OnClick := DelMySelf;
  PopupMenu.Items.Add(pMenu);
end;
//------------------------------------------------------------------------------
//                                    INITIALIZE
//------------------------------------------------------------------------------
initialization
  RegClass(TExtItem);
end.

