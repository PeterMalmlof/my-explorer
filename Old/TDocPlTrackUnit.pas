unit TDocPlTrackUnit;

interface

uses
  Windows, Classes, Contnrs, ComCtrls, Math,

  TGenLogUnit,        // Log Object
  TDocTrackUnit,     // Item Base Class
  TTreeItemBaseUnit; // Item Base Class

//------------------------------------------------------------------------------
//  Docuemnt Item
//------------------------------------------------------------------------------
type TDocPlTrack = Class(TDocTrack)
  protected


  public

    // Get the Property (string) of a specified Property Index
    (*
    class function  GetPropJust(const Col : integer): TItemJust; override;

    // Compare this Item with another (used for sorting ListView)

    function  Compare(
      const pItem : TTreeItemBase;
      const Col   : integer): integer; override;

    class function GetPropCount: integer; override;
    class function GetPropName  (const Col : integer): string; override;
          function GetPropValue (const Col : integer): string; override;  *)
end;
//------------------------------------------------------------------------------
//                                  IMPLEMENTATION
//------------------------------------------------------------------------------
implementation

uses
  SysUtils,
  StrUtils,

  TGenStrUnit,
  TMediaPlayerResUnit,// Resource Strings

  TExplorerAppUnit,
  TGenClassesUnit;    // Class Management


//------------------------------------------------------------------------------
//                                    INITIALIZE
//------------------------------------------------------------------------------
initialization
  RegClass(TDocPlTrack);
end.

