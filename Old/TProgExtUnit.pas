unit TProgExtUnit;

interface

uses
  Windows, Classes, Contnrs, ComCtrls, Menus,

  TGenTextFileUnit,  // Text File Class
  TTreeItemBaseUnit; // Item Base Class

//------------------------------------------------------------------------------
//  Tree Item Base Object
//------------------------------------------------------------------------------
type TProgExt = Class(TTreeItemBase)
  protected
    objVerb : string;

    procedure SetVerb(const Value : string);
  public
    constructor Create(
      const pParent : TTreeItemBase;
      const Name    : string);       overload; override;

    constructor Create(
      const pParent : TTreeItemBase;
      const TF      : TGenTextFile); overload; override;

    destructor Destroy; override;

    class function ChildrenClass: TItemClass; override;

    function IsTreeNode: boolean; override;

    procedure SaveToFIle(const TF : TGenTextFile); override;

    class procedure AddListViewColumns(const ListView : TListView); override;
          procedure AddToListView     (const ListView : TListView); override;

    procedure AddListMenuItems(const PopupMenu : TPopupMenu); override;

    function  HasColumnProp(const Col : integer): boolean; override;
    function  GetColumnProp(const Col : integer): string; override;
    procedure SetColumnProp(const Col : integer; const Txt : string); override;

    property pVerb : string read objVerb write SetVerb;
end;

//------------------------------------------------------------------------------
// Constants used by other modules
//------------------------------------------------------------------------------
const
  PropProgExt = 'ProgExt';

//------------------------------------------------------------------------------
// Resources used by other modules
//------------------------------------------------------------------------------
resourcestring
  resProgExt = 'Extension';

//------------------------------------------------------------------------------
//                                  IMPLEMENTATION
//------------------------------------------------------------------------------
implementation

uses
  SysUtils,
  StrUtils,
  Forms,
  
  TGenLogUnit,     // Logging

  TExplorerAppUnit,
  TGenClassesUnit; // Classes

resourcestring
  resDefDesc = 'Extensions this program can open';

//------------------------------------------------------------------------------
// Create
//------------------------------------------------------------------------------
constructor TProgExt.Create(
      const pParent : TTreeItemBase;
      const Name    : string);
begin
  inherited Create(pParent, Name);

  objType := tItemProgExt;
  objDesc := resDefDesc;
  objVerb := '';
end;
//------------------------------------------------------------------------------
// Create From TextFile
//------------------------------------------------------------------------------
constructor TProgExt.Create(
      const pParent : TTreeItemBase;
      const TF      : TGenTextFile);
var
  sProp, sValue : string;
begin
  inherited Create(pParent, '');

  objType := tItemProgExt;
  objDesc := resDefDesc;
  objVerb := '';

  // Read All Attributes from File

  while TF.ReadProp(sProp, sValue) do
    begin
      // Is this Name

      if AnsiStartsText(PropName, sProp) then
        begin
          objName := sValue;
        end

      // Is this Description

      else if AnsiStartsText(PropDesc, sProp) then
        begin
          objDesc := sValue;
        end

      // Is this FileName

      else if AnsiStartsText(PropVerb, sProp) then
        begin
          objVerb := sValue;
        end

      // Is This End Of Ext

      else if AnsiSameText(PropProgExt, sProp) then
        begin
          BREAK;
        end;
    end;
end;
//------------------------------------------------------------------------------
// Destroy
//------------------------------------------------------------------------------
destructor TProgExt.Destroy;
begin

  inherited;
end;
//------------------------------------------------------------------------------
// Return What Class Items Childrens are
//------------------------------------------------------------------------------
class function TProgExt.ChildrenClass: TItemClass;
begin
  result := TTreeItemBase;
end;
//------------------------------------------------------------------------------
// Should it have TreeNodes
//------------------------------------------------------------------------------
function TProgExt.IsTreeNode: boolean;
begin
  result := false;
end;
//------------------------------------------------------------------------------
// Set a New Name
//------------------------------------------------------------------------------
procedure TProgExt.SetVerb(const Value : string);
begin
  if (not AnsiSameStr(objVerb, Value)) then
    begin
      objDirty := true;
      objVerb  := Value;
    end;
end;
//------------------------------------------------------------------------------
// Save This Item
//------------------------------------------------------------------------------
procedure TProgExt.SaveToFile(const TF : TGenTextFile);
begin
  if (TF <> nil) then
    begin
      TF.WriteProp (PropProgExt);
      TF.WriteProp (PropName, objName);
      TF.WriteProp (PropDesc, objDesc);
      TF.WriteProp (PropVerb, objVerb);
      TF.WriteProp (PropProgExt);
    end;
end;
//------------------------------------------------------------------------------
// Add ListView Colums
//------------------------------------------------------------------------------
class procedure TProgExt.AddListViewColumns(const ListView : TListView);
var
  pColumn : TListColumn;
  CurWidth : integer;
begin
  pColumn := ListView.Columns.Add;
  pColumn.Caption := resExt;
  pColumn.Width   := TheApp.GetListViewColWidth(TProgExt, 0, 100);
  CurWidth := pColumn.Width;

  pColumn := ListView.Columns.Add;
  pColumn.Caption := resVerb;
  pColumn.Width   := TheApp.GetListViewColWidth(TProgExt, 1, 100);
  CurWidth := CurWidth + pColumn.Width;

  pColumn := ListView.Columns.Add;
  pColumn.Caption := resDesc;
  pColumn.Width   := ListView.ClientWidth - CurWidth;
end;
//------------------------------------------------------------------------------
// Add Item To ListView
//------------------------------------------------------------------------------
procedure TProgExt.AddToListView(const ListView : TListView);
var
  LI : TListItem;
begin
  LI            := ListView.Items.Add;
  LI.Caption    := objName;
  LI.Data       := self;
  LI.ImageIndex := objType;

  LI.SubItems.Add(objVerb);
  LI.SubItems.Add(objDesc);
end;
//------------------------------------------------------------------------------
//  Return true if this Column in ListView is Editable
//------------------------------------------------------------------------------
function TProgExt.HasColumnProp(const Col : integer): boolean;
begin
  case Col of
    0 : result := true;
    1 : result := true;
    2 : result := true;
  else
    result := false;
  end;
end;
//------------------------------------------------------------------------------
//  Update Coumn Text
//------------------------------------------------------------------------------
procedure TProgExt.SetColumnProp(const Col : integer; const Txt : string);
begin
  Case Col of
    0 : self.pName := Txt;
    1 : self.pVerb := Txt;
    2 : self.pDesc := Txt;
  end;
end;
//------------------------------------------------------------------------------
//  Get Column Text
//------------------------------------------------------------------------------
function TProgExt.GetColumnProp(const Col : integer): string;
begin
  result := '';
  Case Col of
    0 : result := objName;
    1 : result := objVerb;
    2 : result := objDesc;
  end;
end;
//------------------------------------------------------------------------------
// Add Menus accoding to Current Selected Item in ListView
//------------------------------------------------------------------------------
procedure TProgExt.AddListMenuItems(const PopupMenu : TPopupMenu);
var
  pMenu : TMenuItem;
begin
  inherited AddListMenuItems(PopupMenu);

  // Delete My Self

  pMenu := TMenuItem.Create(PopupMenu);
  pMenu.Caption := resDelete + SPC + objName;
  pMenu.OnClick := DelMySelf;
  PopupMenu.Items.Add(pMenu);
end;
//------------------------------------------------------------------------------
//                                    INITIALIZE
//------------------------------------------------------------------------------
initialization
  RegClass(TProgExt);
end.

