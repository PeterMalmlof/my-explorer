//ds----------------------------------------------------------------------------
//  Stores, Reads, and Manages Application properties. Its a singleton object
//  that is started when application loads, and is destroyed when application
//  is terminated.
//
//  Usage:
//    1) Create the Singleton TheApp by calling TMonitorApp.Startup;
//    2) Read and write the properties available during application execution
//    3) Destroy object by calling TMonitorApp.ShutDown.
//de----------------------------------------------------------------------------
unit TExplorerAppUnit;

interface

uses
  Windows, Forms, Graphics, Classes, Messages,

  TWmMsgFactoryUnit,  // Message Factory (Component)
  TGenAppPropUnit;    // Application Properties

//------------------------------------------------------------------------------
//  Messages own by the Application Object
//------------------------------------------------------------------------------
const
  //----------------------------------------------------------------------------
  //  MSG_APP_... (100...) Application Property Messages                 MSG_ID
  //----------------------------------------------------------------------------
  // Set Tracking On/Off
  //  wParam : Message Command (WP_CMD_SET...)
  //  lParam : BOOLEAN On / Off
  //  Sender : Player Frame Shuffle Button
  //  Owner  : Application (TExplorerAppUnit) Stored in IniFile

  MSG_APP_TRACKING   = WM_USER + 101;

//------------------------------------------------------------------------------
//  Application Object
//------------------------------------------------------------------------------
type TExplorerApp = Class(TObject)
  protected
    objSubscriber : TWmMsgSubscriber;

    // Property List

    objPropList : TGenAppPropList;

    //--------------------------------------------------------------------------
    // Main Windows Properties

    objMainRect     : TGenAppPropRect;    // App
    objLeftSplitter : TGenAppPropInt;
    objFullWinRect  : TGenAppPropRect;
    objTextHeight   : TGenAppPropInt;     // App

    //--------------------------------------------------------------------------
    // Preference Properties

    objMainFont   : TGenAppPropFont;      // App
    objBorder     : TGenAppPropBool;      // App
    objBackColor  : TGenAppPropColor;     // App
    objForeColor  : TGenAppPropColor;     // App
    objHighColor  : TGenAppPropColor;     // App
    objTextColor  : TGenAppPropColor;     // App
    objHint       : TGenAppPropBool;      // App

    objConfirm    : TGenAppPropBool;  // Recycler Bin Confirmation
    objTracking   : TGenAppPropBool;  // Track Playing

    objBaseDir : TGenAppPropString;   // Base String

  public
    constructor Create;
    destructor  Destroy; override;

    function    AddProp(const pProp : TGenAppPropBase): TGenAppPropBase;
    procedure   DelProp(const pProp : TGenAppPropBase);

    procedure  PostData;

    procedure  ProcessMsg(var Msg : TMsg);

    class procedure StartUp(const MsgPump : TWmMsgFactory);
    class procedure ShutDown;

    //--------------------------------------------------------------------------
    // Properties
    //--------------------------------------------------------------------------

    property pMainRect     : TGenAppPropRect  read objMainRect;      // MainForm
    property pLeftSplitter : TGenAppPropInt   read objLeftSplitter;  // MainForm
    property pFullWinRect  : TGenAppPropRect  read objFullWinRect;   // Remove
    property pTextHeight   : TGenAppPropInt   read objTextHeight;    // Remove

    property pMainFont     : TGenAppPropFont  read objMainFont;      // MainForm
    property pBorder       : TGenAppPropBool  read objBorder;        // ????
    property pBackColor    : TGenAppPropColor read objBackColor;     // MainForm
    property pForeColor    : TGenAppPropColor read objForeColor;     // MainForm
    property pHighColor    : TGenAppPropColor read objHighColor;     // MainForm
    property pTextColor    : TGenAppPropColor read objTextColor;     // MainForm
    property pHint         : TGenAppPropBool  read objHint;          // Remove
    property pConfirm      : TGenAppPropBool  read objConfirm;       // Remove
    property pTracking     : TGenAppPropBool  read objTracking;      // MainForm

    property pBaseDir : TGenAppPropString  read objBaseDir;          // MainForm
end;
//------------------------------------------------------------------------------
// Singleton: Use Startup and Shutdown for creating and destroying it
//------------------------------------------------------------------------------
var TheApp : TExplorerApp = nil;

implementation

uses
  SysUtils,

  TGenClassesUnit;    // Class Management

//------------------------------------------------------------------------------
// Create
//------------------------------------------------------------------------------
constructor TExplorerApp.Create;
begin
  inherited Create;

  objPropList := TGenAppPropList.Create;

  //----------------------------------------------------------------------------
  // Main Windows
  //----------------------------------------------------------------------------

  objMainRect := objPropList.AddProp(TGenAppPropRect.Create(
        'Main','Rect', Rect(100,100,800,500))) as TGenAppPropRect;

  objLeftSplitter := objPropList.AddProp(TGenAppPropInt.Create(
        'Main','LeftSplitter', 200)) as TGenAppPropInt;

  objFullWinRect := objPropList.AddProp(TGenAppPropRect.Create(
        'FullWin','Rect', Rect(100,100,800,500))) as TGenAppPropRect;

  objTextHeight := objPropList.AddProp(TGenAppPropInt.Create(
        'Main','TextHeight', 25)) as TGenAppPropInt;

  //----------------------------------------------------------------------------
  // Preferences
  //----------------------------------------------------------------------------

  objMainFont := TGenAppPropFont.Create(
                  'Preferences','MainFont', 'Ariel',
                  -15, false, false, RGB(0,0,0));
  objPropList.AddProp(objMainFont);

  objBorder := objPropList.AddProp(TGenAppPropBool.Create(
        'Preferences','Border', true)) as TGenAppPropBool;

  objBackColor := objPropList.AddProp(TGenAppPropColor.Create(
        'Preferences','BackColor', clBtnFace)) as TGenAppPropColor;

  objForeColor := objPropList.AddProp(TGenAppPropColor.Create(
        'Preferences','ForeColor', clSilver)) as TGenAppPropColor;

  objHighColor := objPropList.AddProp(TGenAppPropColor.Create(
        'Preferences','HighColor', clCream)) as TGenAppPropColor;

  objTextColor := objPropList.AddProp(TGenAppPropColor.Create(
        'Preferences','TextColor', RGB(0, 0, 0))) as TGenAppPropColor;

  objHint := objPropList.AddProp(TGenAppPropBool.Create(
        'Preferences','Hint', true)) as TGenAppPropBool;

  objConfirm := objPropList.AddProp(TGenAppPropBool.Create(
        'Preferences','Confirm', true)) as TGenAppPropBool;

  objTracking := objPropList.AddProp(TGenAppPropBool.Create(
        'Preferences','Tracking', true)) as TGenAppPropBool;

  //----------------------------------------------------------------------------
  // Base Directory
  //----------------------------------------------------------------------------

  objBaseDir := objPropList.AddProp(TGenAppPropString.Create(
        'Preferences','BaseDir', '')) as TGenAppPropString;

  //----------------------------------------------------------------------------
  // Read All Values
  //----------------------------------------------------------------------------

  objPropList.ReadValues;

  //----------------------------------------------------------------------------
  // Set Up Message Subscribtion
  //----------------------------------------------------------------------------

  objSubscriber := MsgFactory.Subscribe(self.ClassName, ProcessMsg);
  objSubscriber.AddMessage(MSG_APP_TRACKING);

  objSubscriber.AddMessage(MSG_UI_BACKCOLOR);
  objSubscriber.AddMessage(MSG_UI_FORECOLOR);
  objSubscriber.AddMessage(MSG_UI_HIGHCOLOR);
  objSubscriber.AddMessage(MSG_UI_BORDER);

end;
//------------------------------------------------------------------------------
// Destroy
//------------------------------------------------------------------------------
destructor TExplorerApp.Destroy;
begin
  objPropList.WriteValues;
  objPropList.Free;

  // Close Subscribtion

  MsgFactory.DeSubscribe(objSubscriber);
  inherited;
end;
//------------------------------------------------------------------------------
//  Post All Owned Data
//------------------------------------------------------------------------------
procedure TExplorerApp.PostData;
begin
  // Send Initial Values to all

  PostMsg(MSG_APP_TRACKING, WP_CMD_CHANGED, objTracking.pBool);

  PostMsg(MSG_UI_BACKCOLOR, WP_CMD_CHANGED, DWORD(self.objBackColor.pColor));
  PostMsg(MSG_UI_FORECOLOR, WP_CMD_CHANGED, DWORD(self.objForeColor.pColor));
  PostMsg(MSG_UI_HIGHCOLOR, WP_CMD_CHANGED, DWORD(self.objHighColor.pColor));

  PostMsg(MSG_UI_BORDER, WP_CMD_CHANGED, DWORD(self.objBorder.pBool));
end;
//------------------------------------------------------------------------------
//  Process Messages
//------------------------------------------------------------------------------
procedure TExplorerApp.ProcessMsg(var Msg : TMsg);
begin
  case Msg.message of

    MSG_APP_TRACKING :
      begin
        if Msg.WParam = WP_CMD_SET then
          begin
            objTracking.pBool := Dw2Bool(Msg.LParam);
            PostMsg(MSG_APP_TRACKING, WP_CMD_CHANGED, objTracking.pBool);
          end
        else if Msg.WParam = WP_CMD_TOGGLE then
          begin
            objTracking.pBool := not objTracking.pBool;
            PostMsg(MSG_APP_TRACKING, WP_CMD_CHANGED, objTracking.pBool);
          end
        else if Msg.WParam = WP_CMD_UPDATE then
          PostMsg(MSG_APP_TRACKING, WP_CMD_CHANGED, objTracking.pBool);

      end;

    //--------------------------------------------------------------------------
    // UI Color
    //--------------------------------------------------------------------------

    MSG_UI_BACKCOLOR :
      begin
        if Msg.WParam = WP_CMD_SET then
          begin
            self.objBackColor.pColor := TColor(Msg.LParam);
            PostMsg(MSG_UI_BACKCOLOR, WP_CMD_CHANGED,
                      DWORD(self.objBackColor.pColor));
          end
        else if Msg.WParam = WP_CMD_UPDATE then
          PostMsg(MSG_UI_BACKCOLOR, WP_CMD_CHANGED,
                      DWORD(self.objBackColor.pColor));
      end;

    MSG_UI_FORECOLOR :
      begin
        if Msg.WParam = WP_CMD_SET then
          begin
            self.objForeColor.pColor := TColor(Msg.LParam);
            PostMsg(MSG_UI_FORECOLOR, WP_CMD_CHANGED,
                      DWORD(self.objForeColor.pColor));
          end
        else if Msg.WParam = WP_CMD_UPDATE then
          PostMsg(MSG_UI_FORECOLOR, WP_CMD_CHANGED,
                      DWORD(self.objForeColor.pColor));
      end;

    MSG_UI_HIGHCOLOR :
      begin
        if Msg.WParam = WP_CMD_SET then
          begin
            self.objHighColor.pColor := TColor(Msg.LParam);
            PostMsg(MSG_UI_HIGHCOLOR, WP_CMD_CHANGED,
                      DWORD(self.objHighColor.pColor));
          end
        else if Msg.WParam = WP_CMD_UPDATE then
          PostMsg(MSG_UI_HIGHCOLOR, WP_CMD_CHANGED,
                      DWORD(self.objHighColor.pColor));
      end;

    MSG_UI_BORDER :
      begin
        if Msg.WParam = WP_CMD_SET then
          begin
            self.objBorder.pBool := BOOLEAN(Msg.LParam);
            PostMsg(MSG_UI_BORDER, WP_CMD_CHANGED,
                      DWORD(self.objBorder.pBool));
          end
        else if Msg.WParam = WP_CMD_TOGGLE then
          begin
            self.objBorder.pBool := not self.objBorder.pBool;
            PostMsg(MSG_UI_BORDER, WP_CMD_CHANGED, DWORD(self.objBorder.pBool));
          end
        else if Msg.WParam = WP_CMD_UPDATE then
          PostMsg(MSG_UI_BORDER, WP_CMD_CHANGED,
                      DWORD(self.objBorder.pBool));
      end;

  end;
end;
//------------------------------------------------------------------------------
//  Add a Property
//------------------------------------------------------------------------------
function TExplorerApp.AddProp(const pProp : TGenAppPropBase): TGenAppPropBase;
begin
  objPropList.AddProp(pProp);

  // Read it also

  pProp.ForceRead;

  result := pProp;
end;
//------------------------------------------------------------------------------
//  Delete a Property
//------------------------------------------------------------------------------
procedure TExplorerApp.DelProp(const pProp : TGenAppPropBase);
begin
  // If Property is Dirty then Force Write it before removing it

  if (pProp <> nil) and (pProp is TGenAppPropBase) then
    begin
      if pProp.pDirty then
        begin
          pProp.ForceWrite;
        end;

      objPropList.DelProp(pProp);
    end;
end;
//------------------------------------------------------------------------------
//
//                                STARTUP & SHUTDOWN
//
//------------------------------------------------------------------------------
// Startup
//------------------------------------------------------------------------------
class procedure TExplorerApp.StartUp(const MsgPump : TWmMsgFactory);
begin
  if (TheApp = nil) then
    begin
      TheApp := TExplorerApp.Create;
    end;
end;
//------------------------------------------------------------------------------
// ShutDown
//------------------------------------------------------------------------------
class procedure TExplorerApp.ShutDown;
begin
  if (TheApp <> nil) then
    begin
      TheApp.Free;
      TheApp := nil;

      TGenAppPropBase.GetDebugCount;
    end;
end;
//------------------------------------------------------------------------------
//                                    INITIALIZE
//------------------------------------------------------------------------------
initialization

  RegClass(TExplorerApp);
end.
