unit TProgGrpUnit;

interface

uses
  Windows, Classes, Contnrs, ComCtrls, Menus,

  TGenLogUnit,        // Log Object
  TGenTextFileUnit,   // Textfile Class
  TProgBaseUnit,      // Base Class
  TTreeItemBaseUnit;  // Item Base Class

//------------------------------------------------------------------------------
//  Tree Item Base Object
//------------------------------------------------------------------------------
type TProgGrp = Class(TProgBase)
  protected

    procedure AddNewChild (Sender: TObject); override;

  public
    constructor Create(
      const pParent : TTreeItemBase;
      const Name    : string); override;

    constructor CreateFromFile(
      const pParent : TTreeItemBase;
      const TF      : TGenTextFile); override;

    destructor  Destroy; override;

    class function  ChildrenClass: TItemClass; override;
    class function  GetTypeStr: string; override;

    procedure SaveToFile(const TF : TGenTextFile); override;

    class function  GetPropName(const Col : integer): string; override;

    procedure AddTreeMenuItems(const PopupMenu : TPopupMenu); override;
    procedure AddListMenuItems(
      const PopupMenu : TPopupMenu;
      const Column    : integer); override;
end;

//------------------------------------------------------------------------------
// Constants used by other modules
//------------------------------------------------------------------------------
const
  PropPrgGrp = 'ProgGrp';

//------------------------------------------------------------------------------
//                                  IMPLEMENTATION
//------------------------------------------------------------------------------
implementation

uses
  SysUtils,
  StrUtils,
  Forms,

  TGenStrUnit,        // String Functions
  TGenMenuUnit,       // TGenMenu Class

  TMediaPlayerResUnit,// Resource Strings
  TProgItemUnit,      // Program Item Class
  TExplorerAppUnit,
  TMyMessageUnit,     // App Messages
  
  TGenClassesUnit;    // Class Management

//------------------------------------------------------------------------------
// Create
//------------------------------------------------------------------------------
constructor TProgGrp.Create(
      const pParent : TTreeItemBase;
      const Name    : string);
begin
  inherited Create(pParent, Name);

  objDesc := resProgGrpDesc;
end;
//------------------------------------------------------------------------------
// Create From TextFile
//------------------------------------------------------------------------------
constructor TProgGrp.CreateFromFile(
      const pParent : TTreeItemBase;
      const TF      : TGenTextFile);
var
  sProp, sValue : string;
begin
  inherited Create(pParent, '');

  objDesc := resProgGrpDesc;

  // Read All Attributes from File

  while TF.ReadProp(sProp, sValue) do
    begin
      // Is this Old Id

      if AnsiStartsText(PropId, sProp) then
        begin
          self.pUIDLoaded := ToInt(sValue);
        end

      // Is this Name

      else if AnsiStartsText(PropName, sProp) then
        begin
          self.pName := sValue;
        end

      // Is this Description

      else if AnsiStartsText(PropDesc, sProp) then
        begin
          objDesc := sValue;
        end

      // New Program

      else if AnsiSameText(PropProgItem, sProp) then
        begin
          // Create a New Program Group

          objList.AddItem(TProgItem.CreateFromFile(self, TF));
        end

      // Is This End Of Group

      else if AnsiSameText(PropPrgGrp, sProp) then
        begin
          BREAK;
        end;
    end;
  self.pDirty := false;
end;
//------------------------------------------------------------------------------
// Destroy
//------------------------------------------------------------------------------
destructor TProgGrp.Destroy;
begin

  inherited;
end;
//------------------------------------------------------------------------------
// Return What Class Items Childrens are
//------------------------------------------------------------------------------
class function TProgGrp.ChildrenClass: TItemClass;
begin
  result := TProgItem;
end;
//------------------------------------------------------------------------------
// Get Type String with ReadOnly and Hidden Flgs attached
//------------------------------------------------------------------------------
class function TProgGrp.GetTypeStr: string;
begin
  result := resProgGrpType;
end;
//------------------------------------------------------------------------------
// Save This Item
//------------------------------------------------------------------------------
procedure TProgGrp.SaveToFile(const TF : TGenTextFile);
begin
  if (TF <> nil) then
    begin
      TF.WriteProp (PropPrgGrp);
      TF.WriteProp (PropId,   IntToStr(self.pUID));
      TF.WriteProp (PropName, self.pName);
      TF.WriteProp (PropDesc, self.pDesc);

      objList.SaveToFile(TF);

      TF.WriteProp (PropPrgGrp);
    end;
end;
//------------------------------------------------------------------------------
//  Get the Property (text) of a specified Column in ListView
//------------------------------------------------------------------------------
class function TProgGrp.GetPropName(const Col : integer): string;
begin
  case Col of
    0 : result := resProgGrpType;
    1 : result := resDesc;
  end;
end;
//------------------------------------------------------------------------------
// Add Menus accoding to Current Selected Item in TreeView
//------------------------------------------------------------------------------
procedure TProgGrp.AddTreeMenuItems(const PopupMenu : TPopupMenu);
var
  pMenu : TGenMenu;
begin
  inherited AddTreeMenuItems(PopupMenu);

  // Add New Child

  pMenu := TGenMenu.Create(PopupMenu);
  pMenu.Caption := resAdd + SPC + TProgItem.GetTypeStr;
  pMenu.OnClick := AddNewChild;
  PopupMenu.Items.Add(pMenu);
end;
//------------------------------------------------------------------------------
// Add Menus accoding to Current Selected Item in ListView
//------------------------------------------------------------------------------
procedure TProgGrp.AddListMenuItems(
      const PopupMenu : TPopupMenu;
      const Column    : integer);
var
  pMenu : TGenMenu;
begin
  inherited AddListMenuItems(PopupMenu, Column);

  // Delete My Self

  pMenu := TGenMenu.Create(PopupMenu);
  pMenu.Caption := resDelete + SPC + self.pName;
  pMenu.OnClick := DelMySelf;
  PopupMenu.Items.Add(pMenu);
end;
//------------------------------------------------------------------------------
// Menu: Add a new Program Item
//------------------------------------------------------------------------------
procedure TProgGrp.AddNewChild(Sender: TObject);
begin
  objList.AddItem(TProgItem.Create(self, resNew + SPC + TProgItem.GetTypeStr));
  self.pDirty := true;

  //PostMessage(Application.Handle, MSG_TREEVIEW_REFRESH, 0, 0);
  //PostMessage(Application.Handle, MSG_LISTVIEW_REFRESH, 0, 0);
end;
//------------------------------------------------------------------------------
//                                    INITIALIZE
//------------------------------------------------------------------------------
initialization
  RegClass(TProgGrp);
end.

