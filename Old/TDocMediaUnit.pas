unit TDocMediaUnit;

interface

uses
  Windows, Classes, Contnrs, ComCtrls,

  TGenLogUnit,        // Log Object
  TDocItemUnit,      // Item Base Class
  TTreeItemBaseUnit; // Item Base Class

//------------------------------------------------------------------------------
//  Docuemnt Item
//------------------------------------------------------------------------------
type TDocMedia = Class(TDocItem)
  protected
    objPlayed  : boolean;        // Item was Played (used for Shuffle Play)

    // Generic mark used for Tracks only

    function  GetPlayed : boolean;               virtual;
    procedure SetPlayed (const Value : boolean); virtual;

  public

    property pPlayed  : boolean   read GetPlayed    write SetPlayed;
end;

//------------------------------------------------------------------------------
//                                  IMPLEMENTATION
//------------------------------------------------------------------------------
implementation

uses
  SysUtils,
  StrUtils,

  TGenStrUnit,        // String Functions
  TGenShellUnit,      // Shell Functions

  TMediaPlayerResUnit,// Resource Strings
  TItemListUnit,
  TProgItemUnit,

  TGenClassesUnit;    // Class Management


//------------------------------------------------------------------------------
// Get if Track was Played
//------------------------------------------------------------------------------
function TDocMedia.GetPlayed: boolean;
begin
  result := objPlayed;
end;
//------------------------------------------------------------------------------
// Set Track is Played
//------------------------------------------------------------------------------
procedure TDocMedia.SetPlayed(const Value : boolean);
begin
  objPlayed := Value;
end;
//------------------------------------------------------------------------------
//                                    INITIALIZE
//------------------------------------------------------------------------------
initialization
  RegClass(TDocMedia);
end.

