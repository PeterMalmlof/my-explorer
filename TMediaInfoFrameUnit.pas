unit TMediaInfoFrameUnit;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Math,

  TGenAppPropUnit,    // App Properties          (Component)
  TWmMsgFactoryUnit,  // Message Factory         (Component)

  TGenButtonUnit,     // Button                  (Component)
  TGenMemoUnit,       // Memo                    (Component)

  TBaseObjectUnit,    // Base Object
  TMediaBaseUnit,     // Media Base
  TMediaFactoryUnit;  // Media Factory           (Component)

//------------------------------------------------------------------------------
// Messages
//------------------------------------------------------------------------------
const
  //----------------------------------------------------------------------------
  //  MSG_MIF_... (50) Media Information Frame Messages                  MSG_ID
  //----------------------------------------------------------------------------
  // Show Media Information (Movie MediaInfo, Track Tags etc.)
  //  wParam : 0
  //  lParam : Media (Pointer) to Show Info about
  //  Owner  : TMediaInfoFrameUnit
  //  Sender : App when a Media is Selected in TreeView or ListView

  MSG_MIF_SHOW_MEDIAINFO = WM_USER + 51; // lParam

//------------------------------------------------------------------------------
// Media Info Frame
//------------------------------------------------------------------------------
type
  TMediaInfoFrame = class(TFrame)
    WinLabel    : TLabel;
    WinVisible  : TGenButton;
    VideoInfo   : TGenMemo;
    AudioInfo   : TGenMemo;
    GeneralInfo : TGenMemo;
    TagInfo     : TGenMemo;
    procedure FrameResize(Sender: TObject);
    procedure WinVisibleClick(Sender: TObject);
  private
    objState      : integer;           // Frame State
    objSubscriber : TWmMsgSubscriber;  // Subscriber

    // This is a Reference Object that holds ONE reference to a Media Object

    objCurMedia   : TBaseReference;

    // Application Properties

    objVisible    : TGenAppPropBool;
    objVisHeight  : TGenAppPropInt;

  protected

    function  GetVisible   : boolean;
    function  GetVisHeight : integer;
    function  GetMinHeight : integer;
    procedure SetVisHeight(const Value : integer);

    // Message Process called by Message Pump

    procedure ProcessMsg(var Msg : TMsg);

    procedure ShowMediaInfo;

    // Log to LogFile

    procedure Log(const Line : string);

    // Frame Message Process

    procedure WndProc(var Message: TMessage); override;

    // CallBack for The Media Reference

    procedure OnReferenceChg(Sender : TBaseObject; Event : integer);

  public
    constructor Create(AOwner: TComponent); override;

    procedure   StartUp;
    procedure   ShutDown;

    //procedure   MediaRemoved(const Media : TMediaBase);

    property    pVisHeight : integer read GetVisHeight write SetVisHeight;
    property    pMinHeight : integer read GetMinHeight;
    property    pVisible   : boolean read GetVisible;

    // Reintroduced Properties

    property TabOrder;
    property TabStop;
  end;

implementation

{$R *.dfm}

uses

  TGenGraphicsUnit,
  TPmaFormUtils,     // Form Utils
  TPmaLogUnit,
  TGenStrUnit,
  TMainFormUnit,     // Message Id
  TMediaInfoUnit,
  TMediaTaggerUnit,
  TMediaTrackUnit,

  TPmaClassesUnit;

type TMediaInfoFrameReference = class (TBaseReference);

const
  BRD       = 4;    // Border Size
  BTNSIZE   = 12;   // Button Size
  DEFHEIGHT = 150;  // Default Window Height

  StateOpening = 0;
  StateRunning = 1;
  StateClosing = 2;

  prefVisible = 'On';
  prefHeight  = 'Height';

resourcestring
  resHintFrame     = 'Media Information';
  resHintFrameDis  = 'Media Information Hidden';
  resHintVisible   = 'Click to Hide Media Information';
  resHintInVisible = 'Click to Show Media Information';
  resHintGenInfo   = 'General Media Info';
  resHintVideoInfo = 'Video Stream Information';
  resHintAudioInfo = 'Audio Stream Information';
  resHintTagInfo   = 'Mp3 Tags (v2) if available';

//------------------------------------------------------------------------------
//  Frame Message Pump
//------------------------------------------------------------------------------
procedure TMediaInfoFrame.WndProc(var Message: TMessage);
begin
  case Message.Msg of
    MSG_STARTUP :
      begin
        if BOOLEAN(Message.LParam) then
          self.StartUp
        else
          self.ShutDown;
      end;
  else
    inherited;
  end;
end;
//------------------------------------------------------------------------------
//  Create
//------------------------------------------------------------------------------
constructor TMediaInfoFrame.Create(AOwner: TComponent);
begin
  inherited;
  objState := StateOpening;

  // Make sure some settings is ok

  self.AutoSize              := false;
  self.AutoScroll            := false;
  self.VertScrollBar.Visible := false;
  self.HorzScrollBar.Visible := false;

  // Read Window Properties

  objSubscriber := nil;

  objCurMedia  := nil;
  objVisible   := nil;
  objVisHeight := nil;
end;
//------------------------------------------------------------------------------
//  StartUp
//------------------------------------------------------------------------------
procedure TMediaInfoFrame.StartUp;
begin
  Log('  ' + self.ClassName + '.StartUp (' + self.Name + ')');

  // Create the Media Reference object and set its CallBack Function

  objCurMedia := TMediaInfoFrameReference.Create(nil);
  objCurMedia.pOnEvent := OnReferenceChg;

  objState := StateRunning;

  // Read Window Properties

  objVisible   := App.CreatePropBool(self.Name, prefVisible, true);
  objVisHeight := App.CreatePropInt( self.Name, prefHeight, DEFHEIGHT);

  objVisHeight.pInt := DEFHEIGHT;

  WinVisible.Checked := objVisible.pBool;

  if Assigned(MsgFactory) then
    begin
      // Setup the Message Pump Processing

      objSubscriber := MsgFactory.Subscribe(
        self.ClassName + '.' + self.Name, ProcessMsg);
    
      objSubscriber.AddMessage(MSG_MIF_SHOW_MEDIAINFO);
    end;

  WinLabel.Caption := 'Media Info';

  GeneralInfo.UseTabs := true;
  GeneralInfo.Tabs    := 20;
  GeneralInfo.StartUp;

  VideoInfo.UseTabs := true;
  VideoInfo.Tabs    := 20;
  VideoInfo.StartUp;

  AudioInfo.UseTabs := true;
  AudioInfo.Tabs    := 20;
  AudioInfo.StartUp;

  TagInfo.UseTabs := true;
  TagInfo.Tabs    := 12;
  TagInfo.StartUp; 

  if objVisible.pBool then
    begin
      self.Hint       := resHintFrame;
      WinVisible.Hint := resHintVisible;
    end
  else
    begin
      self.Hint       := resHintFrameDis;
      WinVisible.Hint := resHintInVisible;
    end;

  GeneralInfo.Hint := resHintGenInfo;
  VideoInfo.Hint   := resHintVideoInfo;
  AudioInfo.Hint   := resHintAudioInfo;
  TagInfo.Hint     := resHintTagInfo;
end;
//------------------------------------------------------------------------------
//  ShutDown
//------------------------------------------------------------------------------
procedure TMediaInfoFrame.ShutDown;
begin
  Log('  ' + self.ClassName + '.ShutDown (' + self.Name + ')');

  objState := StateClosing;

  ReferenceFree(objCurMedia);

  // Unsubscribe all Messages in Message Pump

  if Assigned(objSubscriber) then
    MsgFactory.DeSubscribe(objSubscriber);

  // ShutDown all Components

  StartupAllComps(self, false);

end;
//------------------------------------------------------------------------------
//  Log
//------------------------------------------------------------------------------
procedure TMediaInfoFrame.Log(const Line : string);
begin
  if Assigned(PmaLog) then
    PmaLog.Log(Line);
end;
//------------------------------------------------------------------------------
//  Current Media has Changed or Destroyed
//------------------------------------------------------------------------------
procedure TMediaInfoFrame.OnReferenceChg (Sender : TBaseObject; Event : integer);
begin

  case Event of
    reDestroyed :
      begin
        // Release the Reference

        SetNewReference(objCurMedia, nil);
      end;
  end;

  ShowMediaInfo;
end;
//------------------------------------------------------------------------------
//  Show Media Info
//------------------------------------------------------------------------------
procedure TMediaInfoFrame.ShowMediaInfo;
var
  MI     : TMediaInfo;
  Count  : integer;
  Ind    : integer;
  Iter   : integer;
  Name   : string;
  Value  : string;
  Tags   : TTagRec;
  Media  : TMediaBase;
begin
  // Reset All Information to Default

  WinLabel.Caption := 'Media Info';
  GeneralInfo.Clear;
  VideoInfo.Clear;
  AudioInfo.Clear;
  TagInfo.Clear;

  // Get The Media Info from the Reference Object. The Returned Media is
  // a validated Media (but not Locked). Before using it we readlock it

  Media := MediaReferenceReturn(objCurMedia);
  if MediaBeginRead(Media) then
    begin
      WinLabel.Caption := 'Media Info: ' + Media.pPath;

      if GeneralInfo.Visible then
        begin
          // Open The Media Info

          MI := TMediaInfo.Create;
          if MI.OpenFile(Media.pPath) then
            begin

              //----------------------------------------------------------------
              // General
              //----------------------------------------------------------------

              GeneralInfo.AddNameValue('General','');
              Iter := 0;
              while MI.IterGeneral(Iter, Name, Value) do
                GeneralInfo.AddNameValue(Name, Value);

              //----------------------------------------------------------------
              // Video
              //----------------------------------------------------------------

              VideoInfo.AddNameValue('Video','');
              Count := MI.pVideoCount;
              if (Count = 1) then
                begin
                  Iter := 0;
                  while MI.IterVideo(Iter, 0, Name, Value) do
                    VideoInfo.AddNameValue(Name, Value);
                end
              else if (Count > 1) then
                for Ind := 0 to Count - 1 do
                  begin
                    Iter := 0;
                    while MI.IterVideo(Iter, Ind, Name, Value) do
                      VideoInfo.AddNameValue(Name, Value +
                         ' (' + IntToStr(Ind+1) + ')');
                  end;

              //----------------------------------------------------------------
              // Audio
              //----------------------------------------------------------------

              AudioInfo.AddNameValue('Audio','');
              Count := MI.pAudioCount;
              if (Count = 1) then
                begin
                  Iter := 0;
                  while MI.IterAudio(Iter, 0, Name, Value) do
                    AudioInfo.AddNameValue(Name, Value);
                end
              else if (Count > 1) then
                for Ind := 0 to Count - 1 do
                  begin
                    Iter := 0;
                    while MI.IterAudio(Iter, Ind, Name, Value) do
                      AudioInfo.AddNameValue(Name, Value +
                         ' (' + IntToStr(Ind+1) + ')');
                  end;
            end;

          // CLose Media Info

          MI.Free;

          if (Media is TMediaTrack) and
             FileExists(Media.pPath) then
            begin
              TagInfo.AddNameValue('Tags','');

              Tags := TheTagger.GetTagsFromFile(Media.pPath);

              if (length(Tags.Artist) > 0) then
                TagInfo.AddNameValue('Artist',      Tags.Artist);
              
              if (Tags.Year > 0) then
                TagInfo.AddNameValue('Year',        IntToStr(Tags.Year));
             
              if (length(Tags.Album) > 0) then
                TagInfo.AddNameValue('Album',        Tags.Album);

              if (Tags.Num > 0) then
                TagInfo.AddNameValue('Track',       IntToStr(Tags.Num));
              
              if (length(Tags.Track) > 0) then
                TagInfo.AddNameValue('Title',       Tags.Track);
              
              if (length(Tags.Genre) > 0) then
                TagInfo.AddNameValue('Genre',       Tags.Genre);
              
              if (length(Tags.Comment) > 0) then
                TagInfo.AddNameValue('Comment',     Tags.Comment);
              
              if (length(Tags.Composer) > 0) then
                TagInfo.AddNameValue('Composer',    Tags.Composer);
              
              if (length(Tags.Encoder) > 0) then
                TagInfo.AddNameValue('Encoder',     Tags.Encoder);
              
              if (length(Tags.Copyright) > 0) then
                TagInfo.AddNameValue('Copyright',   Tags.Copyright);
              
              if (length(Tags.Language) > 0) then
                TagInfo.AddNameValue('Language',    Tags.Language);
              
              if (length(Tags.Link) > 0) then
                TagInfo.AddNameValue('Link',        Tags.Link);
              
              if (length(Tags.OrigArtist) > 0) then
                TagInfo.AddNameValue('OrigArtist',  Tags.OrigArtist);
            end;
        end;
      Media.EndRead;
  end;
end;
//------------------------------------------------------------------------------
//  Message Pump Processing
//------------------------------------------------------------------------------
procedure TMediaInfoFrame.ProcessMsg(var Msg : TMsg);
begin
 if (objState = StateRunning) then
    begin
      // Manage Player State Changes

      case Msg.message of

        MSG_MIF_SHOW_MEDIAINFO :
          begin
            if (Msg.lParam <> 0) then
              begin
                SetNewReference(objCurMedia, TBaseObject(Msg.lParam));

                ShowMediaInfo;
              end;
          end;
      end;{case msg}
  end;{running}
end;
//------------------------------------------------------------------------------
//  Resize
//------------------------------------------------------------------------------
procedure TMediaInfoFrame.FrameResize(Sender: TObject);
var
  LabelHgt : integer;
  Pos : integer;
begin
  // Get the Height of Current Label in pixels (must be 2 + CheckBox Height)

  LabelHgt := Max(WinLabel.Height, BTNSIZE + 2);

  // Set Label Text

  SetCtrlPos(WinLabel,
      BTNSIZE + BRD*2, (LabelHgt - WinLabel.Height) div 2);

  // Set Checkbox position

  Pos := (LabelHgt - BTNSIZE + 1)  div 2;
  SetCtrlSize(WinVisible, Pos, Pos, BTNSIZE, BTNSIZE);

  if Assigned(objVisible) and objVisible.pBool then
    begin
      // Frame is Visible

      if (objVisHeight.pInt <> self.Height) then
        self.Height := objVisHeight.pInt;

      GeneralInfo.Visible := true;

      GeneralInfo.Left   := 0;
      GeneralInfo.Width  := self.ClientWidth div 4;
      GeneralInfo.Top    := WinLabel.Top + WinLabel.Height + BRD;
      GeneralInfo.Height := self.ClientHeight - VideoInfo.Top - BRD;

      VideoInfo.Visible := true;

      VideoInfo.Left   := GeneralInfo.Left + GeneralInfo.Width + BRD;
      VideoInfo.Width  := self.ClientWidth div 4;
      VideoInfo.Top    := GeneralInfo.Top;
      VideoInfo.Height := GeneralInfo.Height;

      AudioInfo.Visible := true;

      AudioInfo.Left   := VideoInfo.Left + VideoInfo.Width + BRD;
      AudioInfo.Width  := self.ClientWidth div 4;
      AudioInfo.Top    := GeneralInfo.Top;
      AudioInfo.Height := GeneralInfo.Height;

      TagInfo.Visible := true;

      TagInfo.Left   := AudioInfo.Left + AudioInfo.Width + BRD;
      TagInfo.Width  := self.ClientWidth - TagInfo.Left;
      TagInfo.Top    := GeneralInfo.Top;
      TagInfo.Height := GeneralInfo.Height;

      if (objState = StateRunning) then
        ShowMediaInfo;
    end
  else
    begin
      if (self.Height <> self.pMinHeight) then
        self.Height := self.pMinHeight;

      GeneralInfo.Visible := false;
      VideoInfo.Visible   := false;
      AudioInfo.Visible   := false;
      TagInfo.Visible     := false;
    end;
end;

//------------------------------------------------------------------------------
//  Get Visible
//------------------------------------------------------------------------------
function  TMediaInfoFrame.GetVisible : boolean;
begin
  result := Assigned(objVisible) and objVisible.pBool;
end;
//------------------------------------------------------------------------------
//  Get real Height
//------------------------------------------------------------------------------
function  TMediaInfoFrame.GetVisHeight: integer;
begin
  if Assigned(objVisible) and objVisible.pBool then
    result := DEFHEIGHT // objVisHeight.pInt
  else
    result := self.pMinHeight;
end;
//------------------------------------------------------------------------------
//  Get real Height
//------------------------------------------------------------------------------
procedure  TMediaInfoFrame.SetVisHeight(const Value : integer);
begin
  if Assigned(objVisible) and objVisible.pBool then
    objVisHeight.pInt := Value;
end;
//------------------------------------------------------------------------------
//  Return Minimum Height
//------------------------------------------------------------------------------
function TMediaInfoFrame.GetMinHeight: integer;
begin
  result := BRD + WinLabel.Height + 2;
end;
//------------------------------------------------------------------------------
//  Resize Frame
//------------------------------------------------------------------------------
procedure TMediaInfoFrame.WinVisibleClick(Sender: TObject);
begin
  if (objState = StateRunning) then
    begin
      objVisible.Toggle;

      WinVisible.Checked := objVisible.pBool;
      PostMessage(Application.Handle, MSG_APP_RESIZE_RIGHTPANEL, 0,0);

      if objVisible.pBool then
        begin
          self.Hint       := resHintFrame;
          WinVisible.Hint := resHintVisible;
        end
      else
        begin
          self.Hint       := resHintFrameDis;
          WinVisible.Hint := resHintInVisible;
        end;
    end;
end;
//------------------------------------------------------------------------------
//                              ININTIALIZATION
//------------------------------------------------------------------------------
initialization
  TPmaClassFactory.RegClass(TMediaInfoFrame);
end.
