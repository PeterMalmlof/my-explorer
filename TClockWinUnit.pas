unit TClockWinUnit;

interface
uses
  Windows, ExtCtrls, Graphics,

  TGenRectWinUnit,    // Base RectWin Class
  TGenTimeUnit;       // Time Functions

//------------------------------------------------------------------------------
//  Object
//------------------------------------------------------------------------------
type TClockWin = class(TGenRectWin)
  protected
    objTimer : TTimer;
    objClock : TGenTime;

    objDestRect : TRect;
    objSrcRect  : TRect;

    function  GetUid: string; override;

    procedure SetRect    (const Value : TRect);   override;
    procedure SetEnabled (const Value : boolean); override;
    procedure SetState   (const Value : integer); override;

    procedure OnClock (Sender: TObject);

    function  GetString: string;

    class function GetDefaultRect: TRect; override;
  public
    constructor Create(const C : TCanvas); override;

    destructor  Destroy; override;

    procedure RefreshUi; override;
    procedure Paint;     override;

    function GetBackBitmap: TBitmap; override;

    property  pString : string read GetString;
end;

implementation

uses
  SysUtils,
  Math,
  Types,

  TGenStrUnit,        // String Functions
  TGenLogUnit,        // Log Object
  TGenClassesUnit;    // Class Management

//------------------------------------------------------------------------------
//  Constructor
//------------------------------------------------------------------------------
constructor TClockWin.Create(const C : TCanvas);
begin
  inherited Create(C);

  objTimer          := TTimer.Create(nil);
  objTimer.OnTimer  := OnClock;
  objTimer.Interval := 10 * 1000;
  objTimer.Enabled  := objEnabled;

  objClock := TGenTime.Create(false);
  objClock.Now;
end;
//------------------------------------------------------------------------------
//  Destructor
//------------------------------------------------------------------------------
destructor TClockWin.Destroy;
begin

  objTimer.Free;
  objClock.Free;

  inherited;
end;
//------------------------------------------------------------------------------
// Get Default Window Position, must be overridden by all Windows
//------------------------------------------------------------------------------
function TClockWin.GetUid:string;
begin
  result := 'ClockWin';
end;
//------------------------------------------------------------------------------
// Get Default Window Position, must be overridden by all Windows
//------------------------------------------------------------------------------
class function TClockWin.GetDefaultRect: TRect;
begin
  result := Rect(
    20,
    20,
    80,
    80);
end;
//------------------------------------------------------------------------------
//  Set Rect
//------------------------------------------------------------------------------
procedure TClockWin.OnClock (Sender: TObject);
begin
  if objEnabled then
    begin
      objClock.Now;

      // Set Interval just after next minute
      // Note: not Zero, that will disable the Timer

      objTimer.Interval := Max(1, 1000 * (60 - objClock.pSecond));

      // TheLog.Log('Clock ' + objClock.DateTimeStr +
      //            ' Interval ' + IntToStr(objTimer.Interval));
      self.Paint;
    end;
end;
//------------------------------------------------------------------------------
//  Set State (Override for special management)
//------------------------------------------------------------------------------
procedure TClockWin.SetState(const Value : integer);
begin
  if (Value = StateView) or (Value = StatePlayAudio) then
    begin
      objEnabled       := true;
      objVisible       := true;
      objTimer.Enabled := true;
    end
  else
    begin
      objEnabled       := false;
      objVisible       := false;
      objTimer.Enabled := false;
    end;
end;
//------------------------------------------------------------------------------
//  Set Rect
//------------------------------------------------------------------------------
procedure TClockWin.SetEnabled (const Value : boolean);
begin
  objEnabled := Value;
  objTimer.Enabled := objEnabled;
end;
//------------------------------------------------------------------------------
//  Set Rect
//------------------------------------------------------------------------------
function TClockWin.GetString :string;
begin
  result := objClock.DateStr + ' ' +
            objClock.HourStr + ':' +
            objClock.MinuteStr;
end;
//------------------------------------------------------------------------------
//  Refresh User Inter
//------------------------------------------------------------------------------
procedure TClockWin.SetRect (const Value : TRect);
begin
  objRect.Left   := Value.Left;
  objRect.Top    := Value.Top;

  RefreshUi;
end;
//------------------------------------------------------------------------------
//  Refresh Ui
//------------------------------------------------------------------------------
procedure TClockWin.RefreshUi;
var
  S : TSize;
begin
  S := objCanvas.TextExtent('YYYY-MM-DD HH:MM');

  objRect.Right  := objRect.Left + S.cx + 6;
  objRect.Bottom := objRect.Top  + S.cy + 4;

  // TheLog.Log('Clockwin  Rect ' + ToStr(objRect));

  objMainRect.pRect := objRect;

  // The Drawing Bitmap must be the same size as Window

  objDrawBitmap.Width  := objRect.Right - objRect.Left;
  objDrawBitmap.Height := objRect.Bottom - objRect.Top;

  // Destination Rect is 1 less in all directions

  objDestRect := Rect(objRect.Left  + 1, objRect.Top    + 1,
                      objRect.Right - 1, objRect.Bottom - 1);

  // Source Rect is the Same Size as Destination Rect but on Drawing Bitmap

  objSrcRect := Rect(1, 1, objDrawBitmap.Width - 1, objDrawBitmap.Height - 1);

  // Set same Font Size size as Screen
  
  objDrawBitmap.canvas.Font       := objCanvas.Font;
  objDrawBitmap.Canvas.Font.Color := objForeColor;
end;
//------------------------------------------------------------------------------
//  Paint
//------------------------------------------------------------------------------
procedure TClockWin.Paint;
begin
  inherited;

  if objVisible and objEnabled then
    begin
      // Draw the Background Color first without Pen

      objDrawBitmap.Canvas.Brush.Color := objBackColor;
      objDrawBitmap.Canvas.Brush.Style := bsSolid;
      objDrawBitmap.Canvas.Pen.Style   := psClear;

      objDrawBitmap.Canvas.Rectangle(
        Rect(0, 0, objDrawBitmap.Width, objDrawBitmap.Height));

      // Draw the Text with Foreground Color 1 Pixels Left and Top

      objDrawBitmap.Canvas.TextOut(1, 1, GetString);

      // Copy Draw Bitmap to Screen

      objCanvas.CopyRect(objDestRect, objDrawBitmap.Canvas, objSrcRect);
    end;
end;
//------------------------------------------------------------------------------
//  Return The Draw Bitmap
//------------------------------------------------------------------------------
function TClockWin.GetBackBitmap: TBitmap;
begin
  result := objDrawBitmap;
end;
//------------------------------------------------------------------------------
//                                 INITALIZE
//------------------------------------------------------------------------------
initialization

  FindClass(TClockWin);
end.

