unit TPlayerKeyBoardUnit;

interface
uses
  Windows,  Forms, Graphics, Classes, Contnrs, Controls, Menus, Messages,
  StrUtils, SysUtils,

  TWmMsgFactoryUnit,  // Message Factory
  TMediaBaseUnit;     // Media Base

//------------------------------------------------------------------------------
// Special Key Codes returned from the Remote Controler through Virtual Codes
// You need to Manage these hooking the WM_KEYDOWN message.
// This object will also return them as Codes (That you should not act on)
// This will make it transparent if Keys comes from Keyboard or Remote
// Note: Return, Left, Right, Up, Down, numbers (0..9), and Clear will not
// be returned by Remote handler at all, they will be sent directly as normal
// virtual KeyCodes by Windows. (VK_RETURN etc.) You need to handle them.
//------------------------------------------------------------------------------
const
  VK_VOLUME_MUTE          = 173; // (AD) Volume Mute key       Kb
  VK_VOLUME_DOWN          = 174; // (AE) Volume Down key       Kb
  VK_VOLUME_UP            = 175; // (AF) Volume Up key         Kb
  VK_MEDIA_NEXT_TRACK     = 176; // (B0) Next Track key        Kb
  VK_MEDIA_PREV_TRACK     = 177; // (B1) Previous Track key    Kb
  VK_MEDIA_STOP           = 178; // (B2) Stop Media key        Kb
  VK_MEDIA_PLAY_PAUSE     = 179; // (B3) Play/Pause Media key  Kb
  VK_LAUNCH_MEDIA_SELECT  = 181; // (B5)                       Kb
  VK_BROWSER_BACK         = 166; // (A6) Back, Escape button   Rem


  //----------------------------------------------------------------------------
  //  MSG_HOOK_... (600) KeyBoard Hook Messages                          MSG_ID
  //----------------------------------------------------------------------------
  // Message from KeyBoard Hook
  //  wParam : Command, Key etc
  //  lParam : 0 or debug info
  //  Sender : Sent By Keyboard Hook Dll
  //  Owner  : App will translate command and forward inwards

  MSG_HOOK_MSG = WM_USER + 600;         // Returned MessageId

//------------------------------------------------------------------------------
//  Player KeyBoard Manager
//------------------------------------------------------------------------------
type TPlayerKeyBoard = Class(TObject)
  private
    objSubscriber   : TWmMsgSubscriber; // Message Subscriber
  protected

    // Subscriber Message Process

    procedure MsgProcess(var Msg : TMsg);

    procedure Init;
    procedure Close;

  public
    constructor Create;
    destructor  Destroy; override;

    class procedure StartUp;
    class procedure ShutDown;
end;

//------------------------------------------------------------------------------
//  HOOK Definitions
//------------------------------------------------------------------------------
const
  MyExplorerHook = 'MyExplorerHook.dll';  // Dll Hook

//------------------------------------------------------------------------------
//  External Hook.ddl functions
//------------------------------------------------------------------------------
//  Set Hook
//------------------------------------------------------------------------------
function SetHook(WinHandle: HWND; MsgToSend: Integer): integer; stdcall;
  external MyExplorerHook;
//------------------------------------------------------------------------------
//  Free Hook
//------------------------------------------------------------------------------
function FreeHook: integer; stdcall; external MyExplorerHook;

//------------------------------------------------------------------------------
// Singleton: Player KeyBoard Manager
//------------------------------------------------------------------------------
var TheKeyBoard : TPlayerKeyBoard = nil;

implementation

uses
  TPmaLogUnit,      // Log (Component)
  TPmaProcessUtils,
  TBassPlayerUnit,  // Bass Player (Component)
  TMasterMixerUnit, // Master Mixer
  TMediaUtilsUnit,  // Media Messages
  TMainFormUnit,    // Main Form
  TPmaClassesUnit;


//------------------------------------------------------------------------------
//  Log
//------------------------------------------------------------------------------
procedure Log(const Line : string);
begin
  if Assigned(PmaLog) then
    PmaLog.Log(Line);
end;
//------------------------------------------------------------------------------
//  Create
//------------------------------------------------------------------------------
constructor TPlayerKeyBoard.Create;
begin
  inherited;

  objSubscriber := nil;
end;
//------------------------------------------------------------------------------
//  Startup Deskband
//------------------------------------------------------------------------------
procedure TPlayerKeyBoard.Init;
var
  res : integer;
begin

  // Set Subscribtion on Messages I own

  if Assigned(MsgFactory) then
    begin
      objSubscriber := MsgFactory.Subscribe(self.ClassName, MsgProcess);

      // KeyBoard Messages

      objSubscriber.AddMessage(WM_KEYDOWN);

      // KeyBoard Hook Messages

      objSubscriber.AddMessage(MSG_HOOK_MSG);
    end;

  // Set Hook and give it the Message Id for returning messages
   
  res := SetHook(Application.Handle, MSG_HOOK_MSG);
  case res of
    0 : Log('  Hook Set Sucessfully');
    1 : Log('  Error: MapFile is already Mapped');
    2 : Log('  Error: MapFile Already exists');
    3 : Log('  Error: Could Not Create MapFile');
    4 : Log('  Error: Could Not Create MapFile for other reason');
    5 : Log('  Error: Couldnt Map to Address Space');
  else
    Log('  SetHook Error: ' + IntToStr(res));
  end;

  if (res <> 0) then
    Log('  Last Error: ' + GetErrorString(GetLastError));

end;
//------------------------------------------------------------------------------
//  ShutDown DeskBand
//------------------------------------------------------------------------------
procedure TPlayerKeyBoard.Close;
var
  res : integer;
begin

  // Unsubscribe all Messages

  if Assigned(objSubscriber) then
    MsgFactory.DeSubscribe(objSubscriber);

  //----------------------------------------------------------------------------
  // Free Hook
  //----------------------------------------------------------------------------

  res := FreeHook;
  case res of
    0 : Log('  Hook Freed Sucessfully');
    1 : Log('  Error: MapFile not Mapped');
    2 : Log('  Error: Could not UnHook Hook');
    3 : Log('  Error: Couldnt UnMap the MapFile');
    4 : Log('  Error: Couldnt Close the MapFile');
  else
    Log('  FreeHook Error: ' + IntToStr(res));
  end;

  if (res <> 0) then
    Log('  Last Error: ' + GetErrorString(GetLastError));

end;
//------------------------------------------------------------------------------
//  Create
//------------------------------------------------------------------------------
destructor TPlayerKeyBoard.Destroy;
begin

  inherited;
end;
//------------------------------------------------------------------------------
// Subscriber Message Process
//------------------------------------------------------------------------------
procedure TPlayerKeyBoard.MsgProcess(var Msg : TMsg);
begin
  case Msg.message of

    WM_KEYDOWN :
      begin
        // TheLog.Log('Key Down ' + IntToStr(Msg.wParam));

        case Msg.wParam of

          // VK_MEDIA_NEXT_TRACK :
          //  PostMsg(MSG_PLAY_NEXT, 0, 0);

          //VK_MEDIA_PREV_TRACK :
          //  PostMsg(MSG_PLAY_PREV, 0, 0);

          //VK_MEDIA_STOP :
          //  PostMsg(MSG_PLAYER_TRACK_STOP, WP_CMD_SET,0);

          VK_MEDIA_PLAY_PAUSE :
            begin
              PostMsg(MSG_BASS_TRACK_PLAYPAUSE, 0, 0);
            end;

          VK_RETURN : ;

          VK_BROWSER_BACK : PostMsg(MSG_APP_ESCAPE, 0,0);
          VK_VOLUME_UP    : ;
          VK_VOLUME_DOWN  : ;
          VK_LEFT         : ;
          VK_RIGHT        : ;

          VK_ESCAPE : PostMsg(MSG_APP_ESCAPE, 0,0);

        end;{case wParam}
      end;{keydown}

    //--------------------------------------------------------------------------
    // Hook Message
    //--------------------------------------------------------------------------

    MSG_HOOK_MSG :
      begin
        case Msg.wParam of
          0 : Log('Hook: Installed');

          VK_VOLUME_MUTE         : PostMsg(MSG_MIX_MASTER_MUTE,WP_CMD_TOGGLE,0);
          VK_VOLUME_DOWN         : Log('Hook VK: Volume Down key');
          VK_VOLUME_UP           : Log('Hook VK: Volume Up key');
          VK_MEDIA_NEXT_TRACK    : PostMsg(MSG_MP_PLAY_NEXT,0,0);
          VK_MEDIA_PREV_TRACK    : PostMsg(MSG_MP_PLAY_PREV,0,0);
          VK_MEDIA_STOP          : PostMsg(MSG_BASS_TRACK_STOP,0,0);
          VK_BROWSER_BACK        : Log('Hook VK: Back, Escape button');
          VK_LAUNCH_MEDIA_SELECT : Log('Hook VK: Media Select');

          VK_MEDIA_PLAY_PAUSE : PostMsg(MSG_BASS_TRACK_PLAYPAUSE, 0, 0);

        else
          Log('Hook: ' + IntToStr(Msg.wParam) + '/' + IntToStr(Msg.lParam));
        end;

      end;
  end;
end;
//------------------------------------------------------------------------------
//  Startup
//------------------------------------------------------------------------------
class procedure TPlayerKeyBoard.StartUp;
begin
  if (not Assigned(TheKeyBoard)) then
    begin
      Log('  TPlayerKeyBoard.StartUp');
      TheKeyBoard := TPlayerKeyBoard.Create;
      TheKeyBoard.Init;
    end;
end;
//------------------------------------------------------------------------------
//  ShutDown
//------------------------------------------------------------------------------
class procedure TPlayerKeyBoard.ShutDown;
begin
  if Assigned(TheKeyBoard) then
    begin
      Log('  TPlayerKeyBoard.ShutDown');
      TheKeyBoard.Close;
      TheKeyBoard.Free;
      TheKeyBoard := nil;
    end;
end;
//------------------------------------------------------------------------------
//                              ININTIALIZATION
//------------------------------------------------------------------------------
initialization
  TPmaClassFactory.RegClass(TPlayerKeyBoard);
end.
 