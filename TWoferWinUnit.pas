//ds----------------------------------------------------------------------------
//  Window showing a big Wofer
//
//  Its drawn using Images from an ImageList with Wofers. The Number of
//  images should be around six to give a nice action. It can be less or more.
//  The Image size should be around 120 pixels of height and width. It can be
//  less or more. The Images can be mirrored on X to give left and a right
//  Wofers. The WIndow can be resized to smaller or bigger than Image size
//  Too big will cost much CPU power drawing, and it will look bad.
//de----------------------------------------------------------------------------
unit TWoferWinUnit;

interface
uses
  Windows, ExtCtrls, Graphics, Controls, Types, ImgList,

  TGenRectWinUnit,    // Base RectWin Class
  TGenTimeUnit;       // Time Functions

//------------------------------------------------------------------------------
//  Object
//------------------------------------------------------------------------------
type TWoferWin = class(TGenRectWin)
  protected
    objBitmaps : array of TBitmap; // Array of Wofer Bitmaps
    objSize    : TSize;            // Size of Bitmaps
    objNum     : integer;          // Number of Wofers

    objGain    : single;           // Current Gain
    objLevel   : single;           // Current Level
    objIndex   : integer;          // Index of Wofer Drawn Current
    objLeft    : boolean;          // True if Wofer is mirrored

    objDestRect : TRect; // Destination Rect on Draw Canvas
    objSrcRect  : TRect; // Source Rect of the Wofer Bitmaps

    function  GetUid : string; override;
    procedure SetRect    (const Value : TRect); override;
    procedure SetState   (const Value : integer); override;

    // Set Wofer Volume Level and Paint Wofer
    
    procedure SetLevel   (const Value : single);

    class function GetDefaultRect: TRect;   override;
    class function IsSizeable:     boolean; override;
  public
    constructor Create(
      const C    : TCanvas;     // Canvas to Draw Wofer on  (MUST)
      const Img  : TImageList;  // Image List with 6 Wofers (MUST)
      const Mirr : boolean);    // Mirror Images
                   reintroduce;

    destructor  Destroy; override;

    procedure Paint;     override;

    property pLevel : single  write SetLevel;
end;

implementation

uses
  SysUtils,
  Math,
  Forms,

  TGenGraphicsUnit,   // Graphic Functions
  TGenLogUnit,        // Log Object
  TGenClassesUnit;    // Class Management

const
  DefWoferSize = 120; // Default Wofer Bitmap Size

//------------------------------------------------------------------------------
//  Constructor
//------------------------------------------------------------------------------
constructor TWoferWin.Create(
      const C    : TCanvas;     // Canvas to Draw Wofer on  (MUST)
      const Img  : TImageList;  // Image List with  Wofers  (MUST)
      const Mirr : boolean);    // Mirror Images
var
  Ind : integer;
begin
  objLeft := Mirr; 

  inherited Create(C);

  objSize.cx  := Img.Width;
  objSize.cy  := Img.Height;
  objNum      := Img.Count;

  objGain  := 0.5;
  objLevel := 0.0;

  SetLength(objBitmaps, objNum);

  for Ind := 0 to objNum - 1 do
    begin
      objBitmaps[Ind] := TBitmap.Create;

      Img.GetBitmap(Ind, objBitmaps[Ind]);

      if Mirr then BitMapMirror(objBitmaps[Ind], false);
    end;

  objSrcRect := Rect(0, 0, objSize.cx, objSize.cy);

  objIndex := 0;

  objVisible := false;
  objEnabled := false;
end;
//------------------------------------------------------------------------------
//  Destructor
//------------------------------------------------------------------------------
destructor TWoferWin.Destroy;
var
  Ind : integer;
begin
  // Free all Bitmaps

  for Ind := 0 to objNum - 1 do
    begin
      objBitmaps[Ind].Free;
    end;

  // Free Bitmap array

  SetLength(objBitmaps, 0);

  inherited;
end; 
//------------------------------------------------------------------------------
// Get Default Window Position, must be overridden by all Windows
//------------------------------------------------------------------------------
function TWoferWin.GetUid:string;
begin
  if objLeft then
    result := 'WoferLeftWin'
  else
    result := 'WoferRightWin'
end;
//------------------------------------------------------------------------------
// Get Default Window Position, must be overridden by all Windows
//------------------------------------------------------------------------------
class function TWoferWin.GetDefaultRect: TRect;
const
  BRD = 20;
begin
  // To the Left at Bottom Corner using default Bitmap size

  result := Rect(
    BRD,
    Screen.Height - DefWoferSize - BRD,
    BRD + DefWoferSize,
    Screen.Height - BRD);
end;
//------------------------------------------------------------------------------
// Return true if Window can be resized
//------------------------------------------------------------------------------
class function TWoferWin.IsSizeable: boolean;
begin
  result := true;
end;
//------------------------------------------------------------------------------
//  Set State (Override for special management)
//------------------------------------------------------------------------------
procedure TWoferWin.SetState(const Value : integer);
begin
  if (Value = StatePlayAudio) then
    begin
      objEnabled := true;
      objVisible := true;
      //objTimer.Enabled := true;
    end
  else
    begin
      objEnabled := false;
      objVisible := false;
      //objTimer.Enabled := false;
    end;
end;
//------------------------------------------------------------------------------
//  Set Rect and update Dest and Src Rect
//------------------------------------------------------------------------------
procedure TWoferWin.SetRect (const Value : TRect);
begin
  inherited;

  // Destination Rect is one smaller in all directions

  objDestRect := Rect(objRect.Left  + 1, objRect.Top    + 1,
                      objRect.Right - 1, objRect.Bottom - 1);
end;
//------------------------------------------------------------------------------
//  Set Wofer Volume Level and Paint Wofer
//------------------------------------------------------------------------------
procedure TWoferWin.SetLevel   (const Value : single);
var
  oldIndex : integer;
begin
  oldIndex := objIndex;

  // Set Max Gain depending on incoming level

  if      Value > 0.45 then objGain := 1.0
  else if Value > 0.30 then objGain := Min( objGain, 1.5)
  else if Value > 0.15 then objGain := Min( objGain, 2.2)
  else                      objGain := Min( objGain, 4.5);

  // Multiply Level by current Gain

  objLevel := Value * objGain;

  // Calculate which Wofer to make visible

  objIndex := Max(0, Min(objNum - 1 , round(objNum * objLevel)));

  // If level is below 30000 increase Gain next time

  if Value < 0.915 then objGain := objGain * 1.008;

  // Gain Max is 6.0

  if objGain > 6.0 then objGain := 6.0;

  // Paint if Wofer Bitmap Changed

  if (oldIndex <> objIndex) then Paint;
end;
//------------------------------------------------------------------------------
//  Paint
//------------------------------------------------------------------------------
procedure TWoferWin.Paint;
begin
  inherited;
  
  if objVisible and (objIndex >= 0) and (objIndex < objNum) then
    begin
      objCanvas.CopyMode := cmSrcCopy;

      // StretchCopy Wofer on Window - 1 pixel all around
      //    Destination Rect, Canvas to copy from, Source Rect

      objCanvas.CopyRect(
        objDestRect, objBitmaps[objIndex].Canvas, objSrcRect);
    end;
end;
//------------------------------------------------------------------------------
//                                 INITALIZE
//------------------------------------------------------------------------------
initialization
  FindClass(TWoferWin);
end.

 