unit DetailFrameUnit;

  // TODO: Copy Pictures and Paste Pictures
  
interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, 
  Dialogs, StdCtrls, Math,     jpeg,     ExtCtrls,

  TGenAppPropUnit,     // App Properties        (Component)
  TWmMsgFactoryUnit,   // Message Factory       (Component)

  TGenButtonUnit,      // Button                (Component)
  TGenMemoUnit,        // Memo                  (Component)

  TBaseObjectUnit,     // Base Object
  TMediaBaseUnit,      // Media Base Class
  TMediaArtistUnit,    // Media Artist
  TMediaAlbumUnit,     // Media Album
  TMediaFactoryUnit;   // Media Factory         (Component)

//------------------------------------------------------------------------------
// Message Definitions
//------------------------------------------------------------------------------
const
  //----------------------------------------------------------------------------
  // MSG_MDL... (60) Artist/Album Information Frame Messages             MSG_ID
  //----------------------------------------------------------------------------

  //----------------------------------------------------------------------------
  // Show Artist and Album Media Information on a Track
  //  wParam : 0
  //  lParam : Media (Pointer) to show information on
  //  Owner  : DetailFrameUnit (this Unit)
  //  Sender : Application when a Track is Selected

  MSG_MDL_SHOW_MEDIA_TRACK_INFO = WM_USER + 61;

type TDetailFrameReference = class (TBaseReference);

//------------------------------------------------------------------------------
// Detailed Frame
//------------------------------------------------------------------------------
type
  TDetailFrame = class(TFrame)
    WinVisible : TGenButton;
    WinLabel   : TLabel;
    MediaImage : TImage;
    MediaDesc: TGenMemo;
    procedure FrameResize(Sender: TObject);
    procedure WinVisibleClick(Sender: TObject);
    procedure MediaDescLog(sLine: String);
  private
    objState      : integer;           // Frame State
    objSubscriber : TWmMsgSubscriber;  // Subscriber

    objVisible    : TGenAppPropBool;
    objVisHeight  : TGenAppPropInt;

    // Reference to Current Media

    objMediaReference : TBaseReference;

    // If Artist Details, else Album Details

    objArtist   : boolean;

    objCurPicture   : integer;  // Current Picture Index Shown
    objPictureTimer : TTimer;   // Picture Timer

    objTitle : string;
  protected
    function  GetVisible   : boolean;
    function  GetVisHeight : integer;
    function  GetMinHeight : integer;
    procedure SetVisHeight(const Value : integer);

    procedure OnRefRelease (Sender : TBaseObject; Event : integer);

    // Message Process called by Message Pump

    procedure ProcessMsg(var Msg : TMsg);

    procedure CalcMedia(const P : Pointer);

    procedure ShowMedia;

    procedure OnPictureTimer (Sender : TObject);

    // Log to LogFile

    procedure Log(const Line : string);

    // Frame Message Process

    procedure WndProc(var Message: TMessage); override;

  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;

    procedure   StartUp;
    procedure   ShutDown;

    property    pVisHeight : integer read GetVisHeight write SetVisHeight;
    property    pMinHeight : integer read GetMinHeight;
    property    pArtist    : boolean read objArtist write objArtist;
    property    pVisible   : boolean read GetVisible;

    // Reintroduced Properties

    property TabOrder;
    property TabStop;

  end;

implementation

{$R *.dfm}

uses

  TGenGraphicsUnit,
  TPmaLogUnit,
  TPmaFormUtils,     // Form Utils
  TMediaTrackUnit,
  TMainFormUnit,

  TPmaExceptionUNit,
  TPmaClassesUnit;

const
  BRD       = 4;    // Border Size
  BTNSIZE   = 12;   // Button Size
  DEFHEIGHT = 200;  // Default Window Height

  prefArtist  = 'ArtistWindow';
  prefAlbum   = 'AlbumWindow';
  prefVisible = 'On';
  prefHeight  = 'Height';

  StateOpening = 0;
  StateRunning = 1;
  StateClosing = 2;

resourcestring
  resHintFrame     = 'Detailed Information on ';
  resHintFrameDis  = 'Detailed Information on ';
  resHintVisible   = 'Click to Hide ';
  resHintInVisible = 'Click to Show ';
  resHintInfo      = ' Information';
  resHintDesc      = ' Text Description';
  resHintPicture   = ' Pictures';

//------------------------------------------------------------------------------
//  Frame Message Pump
//------------------------------------------------------------------------------
procedure TDetailFrame.WndProc(var Message: TMessage);
begin
  case Message.Msg of
    MSG_STARTUP :
      begin
        if BOOLEAN(Message.LParam) then
          self.StartUp
        else
          self.ShutDown;
      end;
  else
    inherited;
  end;
end;
//------------------------------------------------------------------------------
//  Create
//------------------------------------------------------------------------------
constructor TDetailFrame.Create(AOwner: TComponent);
begin
  inherited;
  objState := StateOpening;

  // Make sure some settings is ok

  self.AutoSize              := false;
  self.AutoScroll            := false;
  self.VertScrollBar.Visible := false;
  self.HorzScrollBar.Visible := false;

  // Read Window Properties

  objSubscriber := nil;

  objMediaReference := nil;
  objArtist         := true;
  objCurPicture     := 0;

  objPictureTimer := TTimer.Create(nil);
  objPictureTimer.Interval := 10000;
  objPictureTimer.OnTimer  := OnPictureTimer;
  objPictureTimer.Enabled  := false;

  objVisible   := nil;
  objVisHeight := nil;

end;
//------------------------------------------------------------------------------
//  StartUp
//------------------------------------------------------------------------------
procedure TDetailFrame.StartUp;
begin
  Log('  ' + self.ClassName + '.StartUp (' + self.Name + ')');

  objState := StateRunning;

  // Create the Reference object

  objMediaReference := TDetailFrameReference.Create(nil);
  objMediaReference.pOnEvent := OnRefRelease;

  // Read Window Properties

  if objArtist then
    begin
      objVisible   := App.CreatePropBool(
                        prefArtist, prefVisible, true);
      objVisHeight := App.CreatePropInt(
                        prefArtist, prefHeight, DEFHEIGHT);

      objTitle := 'Artist';
    end
  else
    begin
      objVisible   := App.CreatePropBool(
                        prefAlbum, prefVisible, true);
      objVisHeight := App.CreatePropInt(
                        prefAlbum, prefHeight, DEFHEIGHT);

      objTitle := 'Album';
    end;

  WinVisible.Checked := objVisible.pBool;

  // Setup the Message Pump Processing

  if Assigned(MsgFactory) then
    begin
      objSubscriber := MsgFactory.Subscribe(
        self.ClassName + '.' + self.Name, ProcessMsg);

      // Add Owner Messages

      objSubscriber.AddMessage(MSG_MDL_SHOW_MEDIA_TRACK_INFO);
    end;

  WinLabel.Caption := objTitle + ' Details';

  MediaDesc.StartUp;

  if objVisible.pBool then
    begin
      self.Hint       := resHintFrame + objTitle;
      WinVisible.Hint := resHintVisible + objTitle + resHintInfo;
    end
  else
    begin
      self.Hint       := resHintFrameDis + objTitle;
      WinVisible.Hint := resHintInVisible + objTitle + resHintInfo;
    end;

  MediaDesc.Hint  := objTitle + resHintDesc;
  MediaImage.Hint := objTitle + resHintPicture;
end;
//------------------------------------------------------------------------------
//  ShutDown
//------------------------------------------------------------------------------
procedure TDetailFrame.ShutDown;
begin
  Log('  ' + self.ClassName + '.ShutDown (' + self.Name + ')');

  objState := StateClosing;

  // Disable the Picture Timer

  objPictureTimer.Enabled := false;

  // Remove the Reference Object

  ReferenceFree(objMediaReference);

  // Unsubscribe all Messages in Message Pump

  if Assigned(objSubscriber) then
    MsgFactory.DeSubscribe(objSubscriber);

  // Save any Dirty Description

  if (length(MediaDesc.FileName) > 0) and
      MediaDesc.Dirty then
    begin
      MediaDesc.SaveToFile;
      self.MediaDescLog('Saved ' + MediaDesc.FileName);
    end;

  MediaDesc.ShutDown;
end;
//------------------------------------------------------------------------------
//  Destroy
//------------------------------------------------------------------------------
destructor TDetailFrame.Destroy;
begin

  objPictureTimer.Free;

  inherited;
end;
//------------------------------------------------------------------------------
//  Log
//------------------------------------------------------------------------------
procedure TDetailFrame.Log(const Line : string);
begin
  if Assigned(PmaLog) then
    PmaLog.Log(Line);
end;
//------------------------------------------------------------------------------
//  Media Reference has Changed or was Destroyed
//------------------------------------------------------------------------------
procedure TDetailFrame.OnRefRelease(Sender : TBaseObject; Event : integer);
begin
  case Event of

    // We only care for Destroy

    reDestroyed :
      begin
        // Release Media Reference

        SetNewReference(objMediaReference, nil);
      end;
  end;
  ShowMedia;
end;
//------------------------------------------------------------------------------
//  Message Pump Processing
//------------------------------------------------------------------------------
procedure TDetailFrame.ProcessMsg(var Msg : TMsg);
begin
 if (objState = StateRunning) then
    begin

      case Msg.message of

        MSG_MDL_SHOW_MEDIA_TRACK_INFO : CalcMedia(Pointer(Msg.lParam));

      end;{case msg}
  end;{running}
end;
//------------------------------------------------------------------------------
//  Calculate Media to Show depending on input
//------------------------------------------------------------------------------
procedure TDetailFrame.CalcMedia(const P : Pointer);
var
  Media    : TMediaBase;
  NewMedia : TMediaBase;
  OldMedia : TMediaBase;
begin
  // Read Lock the Media. This means its Validated and we have a Read Lock

  if MediaBeginRead(P) then
    begin
      Media := TMediaBase(P);

      // Translate This Media to a New Media

      NewMedia := nil;
      if objArtist then
        begin
          // Walk Media to see if there are any Artist in the Path

          if (Media is TMediaArtist) then
            begin
              // We already have the right media and Read Lock

              NewMedia := Media;
            end
          else if (Media is TMediaAlbum) then
            begin
              if Assigned(Media.pParent) then
                begin
                  // We got the Artist (not Validated)

                  NewMedia := Media.pParent;
                end;
            end
          else if (Media is TMediaTrack) then
            begin
              if Assigned(Media.pParent) and
                 Assigned(Media.pParent.pParent) then
                begin
                  // We got the Artist (not Validated)

                  NewMedia := Media.pParent.pParent;
                end;
            end;
        end
      else
        begin
          // Walk Media to see if there are any Album

          if (Media is TMediaAlbum) then
            NewMedia := Media
          else if (Media is TMediaTrack) then
            begin
              if Assigned(Media.pParent) then
                NewMedia := Media.pParent;
            end;
        end;

      // Release the Read Lock of Media

      Media.EndRead;

      // Get Current Media Reference and Change only if its different

      OldMedia := MediaReferenceReturn(objMediaReference);
      if (OldMedia <> NewMedia) then
        begin
          SetNewReference(objMediaReference, NewMedia);

          if MediaImage.Visible then
            begin
              ShowMedia;
            end;
        end;
   end;
end;
//------------------------------------------------------------------------------
//  Show
//------------------------------------------------------------------------------
procedure TDetailFrame.ShowMedia;
var
  Count    : integer;
  Media    : TMediaBase;
begin
  // Save any Dirty Description

  if (length(MediaDesc.FileName) > 0) and
      MediaDesc.Dirty then
    begin
      MediaDesc.SaveToFile;
      self.MediaDescLog('Saved ' + MediaDesc.FileName);
    end;

  // Clear Current Text and Picture

  MediaDesc.Clear;
  MediaDesc.FileName := '';
  MediaDesc.WordWrap := true;
  
  MediaImage.Picture := nil;

  if objArtist then
    WinLabel.Caption := 'Artist Info'
  else
    WinLabel.Caption := 'Album Info';

  // Stop any Picture Timer

  objPictureTimer.Enabled := false;

  // Get the Media from Media Reference

  Media := MediaReferenceReturn(objMediaReference);
  if Assigned(Media) and MediaBeginRead(Media) then
    begin
      // First Load the Media Description if it exists

      MediaDesc.LoadFromFile(Media.pMediaDescFile);

      // Gent Number of Pictures of the Media

      Count := Media.pPictureCount;

      WinLabel.Caption := Media.pTitle + ' (' +
                    IntToStr(Media.pPictureCount) + ')';

      Media.EndRead;

      // Set Current Picture to 0

      objCurPicture := 0;

      // If there was at least 1 Picture, Load it

      if (Count > 0) then OnPictureTimer(nil);

      // If there are more than 1 Picture, Start the Picture Change Timer

      if (Count > 1) then
        objPictureTimer.Enabled := true;
    end;
end;
//------------------------------------------------------------------------------
//  Picture Timer
//------------------------------------------------------------------------------
procedure TDetailFrame.OnPictureTimer (Sender : TObject);
var
  FileName : string;
  Jpeg     : TJpegImage;
  Media    : TMediaBase;
begin
  // Get the Media from Media Reference and Lock it

  Media := MediaReferenceReturn(objMediaReference);
  if Assigned(Media) and MediaBeginRead(Media) then
    begin
      // Is it time to reset the CurPicture to zero

      if (objCurPicture >= Media.pPictureCount) then
        objCurPicture := 0;

      // Get the Next Picture that exists

      while Media.GetNextPicture(objCurPicture, FileName) do
        begin
          // Try to Load the Picture

          if SysUtils.FileExists(FileName) then
            begin
              MediaImage.Center := true;
              MediaImage.Proportional := true;
              MediaImage.Stretch := true;

              Jpeg := TJpegImage.Create;
              Jpeg.LoadFromFile(FileName);

              MediaImage.Picture.Assign(Jpeg);
              Jpeg.Free;

              BREAK;
            end;
        end;

      Media.EndRead;
    end;
end;
//------------------------------------------------------------------------------
//  Resize Frame
//------------------------------------------------------------------------------
procedure TDetailFrame.FrameResize(Sender: TObject);
var
  LabelHgt : integer;
  Pos      : integer;
begin
  // Get the Height of Current Label in pixels (must be 2 + CheckBox Height)

  LabelHgt := Max(WinLabel.Height, BTNSIZE + 2);

  // Set Label Text

  SetCtrlPos(WinLabel,
      BTNSIZE + BRD*2, (LabelHgt - WinLabel.Height) div 2);

  // Set Checkbox position

  Pos := (LabelHgt - BTNSIZE + 1)  div 2;
  SetCtrlSize(WinVisible, Pos, Pos, BTNSIZE, BTNSIZE);

  if Assigned(objVisible) and objVisible.pBool then
    begin
      // Frame is Visible

      if (objVisHeight.pInt <> self.Height) then
        self.Height := objVisHeight.pInt;

      MediaDesc.Visible := true;

      MediaDesc.Left   := self.ClientWidth div 4;
      MediaDesc.Width  := self.ClientWidth - MediaDesc.Left - BRD;
      MediaDesc.Top    := WinLabel.Top + WinLabel.Height + BRD;
      MediaDesc.Height := self.ClientHeight - MediaDesc.Top - BRD;

      MediaImage.Visible := true;

      MediaImage.Left   := BRD;
      MediaImage.Width  := MediaDesc.Left - BRD;
      MediaImage.Top    := WinLabel.Top + WinLabel.Height + BRD;
      MediaImage.Height := self.ClientHeight - MediaDesc.Top - BRD;

      if (objState = StateRunning) and MediaImage.Visible then
        ShowMedia;
    end
  else
    begin
      if (self.Height <> self.pMinHeight) then
        self.Height := self.pMinHeight;

      MediaDesc.Visible  := false;
      MediaImage.Visible := false;
    end;
end;
//------------------------------------------------------------------------------
//  Get Visible
//------------------------------------------------------------------------------
function  TDetailFrame.GetVisible : boolean;
begin
  result := Assigned(objVisible) and objVisible.pBool;
end;
//------------------------------------------------------------------------------
//  Get real Height
//------------------------------------------------------------------------------
function  TDetailFrame.GetVisHeight: integer;
begin
  if Assigned(objVisible) and objVisible.pBool then
    result := objVisHeight.pInt
  else
    result := self.pMinHeight;
end;
//------------------------------------------------------------------------------
//  Get real Height
//------------------------------------------------------------------------------
procedure  TDetailFrame.SetVisHeight(const Value : integer);
begin
  if Assigned(objVisHeight) then
    objVisHeight.pInt := Value;
end;
//------------------------------------------------------------------------------
//  Return Minimum Height
//------------------------------------------------------------------------------
function TDetailFrame.GetMinHeight: integer;
begin
  result := BRD + WinLabel.Height + 2;
end;
//------------------------------------------------------------------------------
//  Resize Frame
//------------------------------------------------------------------------------
procedure TDetailFrame.WinVisibleClick(Sender: TObject);
begin
  if (objState = StateRunning) then
    begin
      objVisible.Toggle;

      WinVisible.Checked := objVisible.pBool;
      PostMessage(Application.Handle, MSG_APP_RESIZE_RIGHTPANEL, 0,0);

      if objVisible.pBool then
        begin
          self.Hint       := resHintFrame + objTitle;
          WinVisible.Hint := resHintVisible + objTitle + resHintInfo;
        end
      else
        begin
          self.Hint       := resHintFrameDis + objTitle;
          WinVisible.Hint := resHintInVisible + objTitle + resHintInfo;
        end;
    end;
end;
//------------------------------------------------------------------------------
//  �og
//------------------------------------------------------------------------------
procedure TDetailFrame.MediaDescLog(sLine: String);
begin
  PlayerMainForm.MainLog(sLine);
end;
//------------------------------------------------------------------------------
//                              ININTIALIZATION
//------------------------------------------------------------------------------
initialization
  TPmaClassFactory.RegClass(TDetailFrame,'DetailFrameUnit');
end.
