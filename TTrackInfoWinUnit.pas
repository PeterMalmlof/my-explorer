unit TTrackInfoWinUnit;

interface
uses
  Windows, ExtCtrls, Graphics, Types, Messages,

  TGenRectWinUnit,    // Base RectWin Class
  TTreeItemBaseUnit;
  
const
  WM_TRACK_ENDED = WM_USER + 219; // A new Item was Selected

//------------------------------------------------------------------------------
//  Object
//------------------------------------------------------------------------------
type TTrackInfoWin = class(TGenRectWin)
  protected
    objItem   : TTreeItemBase;

    objTimer  : TTimer;
    objPos    : single;
    objPaused : boolean;

    objTxtSize  : TSize; // Temp

    procedure OnClock (Sender: TObject);

    function  GetUid: string; override;

    procedure SetRect    (const Value : TRect);         override;
    procedure SetState   (const Value : integer);       override;
    procedure SetItem    (const Value : TTreeItemBase); override;
    function  GetVisible : boolean; override;

    class function GetDefaultRect: TRect; override;

    class function IsSizeable: boolean; override;
  public
    constructor Create(const C : TCanvas); override;

    destructor  Destroy; override;

    procedure RefreshUi; override;
    procedure Paint;     override;
end;

implementation

uses
  SysUtils,
  Math,
  Forms,

  TGenGraphicsUnit,   // Grapfics Functions
  TGenStrUnit,        // String Functions
  MyFileTimeUnit,     // TFileTime Function

  TDocTrackUnit,      // TDocTrack Class
  TMediaPlayerResUnit,// Resource Strings

  TBassPlayerUnit,    // Bass Player Object
  TGenLogUnit,        // Log Object
  TGenClassesUnit;    // Class Management

//------------------------------------------------------------------------------
//  Constructor
//------------------------------------------------------------------------------
constructor TTrackInfoWin.Create(const C : TCanvas);
begin
  inherited Create(C);

  objItem := nil;

  objTimer          := TTimer.Create(nil);
  objTimer.OnTimer  := OnClock;
  objTimer.Interval := 1000;
  objTimer.Enabled  := false;
end;
//------------------------------------------------------------------------------
//  Destructor
//------------------------------------------------------------------------------
destructor TTrackInfoWin.Destroy;
begin
  objTimer.Free;

  inherited;
end;
//------------------------------------------------------------------------------
// Get Default Window Position, must be overridden by all Windows
//------------------------------------------------------------------------------
function TTrackInfoWin.GetUid:string;
begin
  result := 'TrackInfoWin';
end;
//------------------------------------------------------------------------------
// Get Default Window Position, must be overridden by all Windows
//------------------------------------------------------------------------------
class function TTrackInfoWin.GetDefaultRect: TRect;
begin
  result := Rect(
    Screen.Width  div 8,
    Screen.Height - 120,
    Screen.Width - (Screen.Width div 8),
    Screen.Height - 20);

  //TheLog.Log('TrackWin Rect ' + ToStr(result));
end;
//------------------------------------------------------------------------------
// Return true if Window can be resized
//------------------------------------------------------------------------------
class function TTrackInfoWin.IsSizeable: boolean;
begin
  result := true;
end;
//------------------------------------------------------------------------------
//  Set State (Override for special management)
//------------------------------------------------------------------------------
function TTrackInfoWin.GetVisible : boolean;
begin
  result := objEnabled and objVisible;
end;
//------------------------------------------------------------------------------
//  Set State (Override for special management)
//------------------------------------------------------------------------------
procedure TTrackInfoWin.SetState(const Value : integer);
begin
  if (Value = StatePlayAudio) then
    begin
      objVisible := true;
      objEnabled := true;
      objTimer.Enabled := objEnabled;
    end
  else
    begin
      objVisible := false;
      objEnabled := false;
      objTimer.Enabled := objEnabled;
    end;
end;
//------------------------------------------------------------------------------
//  Set Rect
//------------------------------------------------------------------------------
procedure TTrackInfoWin.SetItem    (const Value : TTreeItemBase);
begin
  if (Value <> nil) and (Value IS TDocTrack) then
    begin
      objItem := Value;
      if objVisible then Paint;
      objPos := 0;
    end
  else
    begin
      objItem := nil;
      objPos := 0;
    end;
end;
//------------------------------------------------------------------------------
//  Refresh User Inter
//------------------------------------------------------------------------------
procedure TTrackInfoWin.SetRect (const Value : TRect);
begin
  objRect := Value;

  RefreshUi;
end;
//------------------------------------------------------------------------------
//  Set Rect
//------------------------------------------------------------------------------
procedure TTrackInfoWin.OnClock (Sender: TObject);
begin
  if objEnabled then
    begin
      if ThePlayer.pTrackEnded then
        begin
          PostMessage(Application.Handle, WM_TRACK_ENDED, 0, 0);
        end
      else if (objItem <> nil) and (objItem is TDocTrack) and
          AnsiSameText(objItem.pPath, ThePlayer.pTrackFile) then
        begin
          objPos    := ThePlayer.pTrackPos;
          objPaused := ThePlayer.pPaused;
          self.Paint;
        end
      else
        begin
          objPos    := 0;
          objPaused := false;
        end;
    end;
end;
//------------------------------------------------------------------------------
//  Refresh Ui
//------------------------------------------------------------------------------
procedure TTrackInfoWin.RefreshUi;
begin
  // Set same Font Size size as Screen

  objDrawBitmap.canvas.Font       := objCanvas.Font;
  objDrawBitmap.Canvas.Font.Color := objForeColor;

  // Get the Textsize of the largest Label

  objTxtSize := objDrawBitmap.Canvas.TextExtent('Created X');

  // Set the Top to Bottom + 5 rows

  objRect.Top := objRect.Bottom - ((4 * objTxtSize.cy) + 4);

  // Remember new Window Placement

  objMainRect.pRect := objRect;

  // The Drawing Bitmap must be the same size as Window

  objDrawBitmap.Width  := objRect.Right - objRect.Left;
  objDrawBitmap.Height := objRect.Bottom - objRect.Top;

end;
//------------------------------------------------------------------------------
//  Paint
//------------------------------------------------------------------------------
procedure TTrackInfoWin.Paint;
const
  LEFTBRD = 10;
var
  LabelAPos : integer;
  TextAPos  : integer;
  LabelBPos : integer;
  TextBPos  : integer;

  Pos     : TPoint;

  Resolution : DWORD;
  TrackLen   : double;
  BitRate    : DWORD;
  sTmp : string;
begin
  inherited;

  if GetVisible and (objItem <> nil) and (objItem is TDocTrack) then
    begin
      // Calculate Label and Text X positions

      LabelAPos := 7;
      TextAPos  := objTxtSize.cx + 2;
      TextBPos  := (objRect.Right - objRect.Left) - objTxtSize.cx - 10;
      LabelBPos := TextBPos - objTxtSize.cx - 4;

      // Draw the Background Color first without Pen

      objDrawBitmap.Canvas.Brush.Color := objHighColor;
      objDrawBitmap.Canvas.Brush.Style := bsSolid;
      objDrawBitmap.Canvas.Pen.Color   := objForeColor;
      objDrawBitmap.Canvas.Pen.Style   := psSolid;

      objDrawBitmap.Canvas.Rectangle(
        Rect(0, 0, objDrawBitmap.Width, objDrawBitmap.Height));

      // Get some Track Data

      ThePlayer.GetTrackData(objItem.pPath, Resolution, TrackLen, BitRate);

      //------------------------------------------------------------------------
      // Draw Artist and Genre
      //------------------------------------------------------------------------
      Pos.Y := 2;

      objDrawBitmap.Canvas.TextOut(LabelAPos, Pos.Y, resDocTrackArtist);

      DrawTextEll(objDrawBitmap.Canvas,
      Rect(LabelAPos, 0, LabelBPos - 5, Pos.Y + objTxtSize.cy),
      Point(TextAPos, Pos.Y), false, TDocTrack(objItem).pArtist);

      objDrawBitmap.Canvas.TextOut(LabelBPos, Pos.Y, resDocTrackGenre);

      DrawTextEll(objDrawBitmap.Canvas,
      Rect(LabelBPos, 0, objDrawBitmap.Width - 2, Pos.Y + objTxtSize.cy),
      Point(TextBPos, Pos.Y), false, TDocTrack(objItem).pGenre);

      //------------------------------------------------------------------------
      // Draw Album and BitRate
      //------------------------------------------------------------------------
      Inc(Pos.Y, objTxtSize.cy);

      objDrawBitmap.Canvas.TextOut(LabelAPos, Pos.Y, resDocTrackAlbum);

      DrawTextEll(objDrawBitmap.Canvas,
      Rect(LabelAPos,0, LabelBPos - 5, Pos.Y + objTxtSize.cy),
      Point(TextAPos, Pos.Y), false, TDocTrack(objItem).pAlbum);

      objDrawBitmap.Canvas.TextOut(LabelBPos, Pos.Y, resDocTrackBitrate);

      DrawTextEll(objDrawBitmap.Canvas,
      Rect(LabelBPos, 0, objRect.Right - 5, Pos.Y + objTxtSize.cy),
      Point(TextBPos, Pos.Y), false,
      IntToStr(TDocTrack(objItem).pBitRate) + resDocTrackKbS);

      //------------------------------------------------------------------------
      // Draw Track Name and Number
      //------------------------------------------------------------------------
      Inc(Pos.Y, objTxtSize.cy);

      objDrawBitmap.Canvas.TextOut(LabelAPos, Pos.Y, resDocTrackTrack);

      DrawTextEll(objDrawBitmap.Canvas,
      Rect(LabelAPos,0, LabelBPos - 5, Pos.Y + objTxtSize.cy),
      Point(TextAPos, Pos.Y), false, TDocTrack(objItem).pTrack);

      objDrawBitmap.Canvas.TextOut(LabelBPos, Pos.Y, resDocTrackNo);

      DrawTextEll(objDrawBitmap.Canvas,
      Rect(LabelBPos, 0, objRect.Right - 5, Pos.Y + objTxtSize.cy),
      Point(TextBPos, Pos.Y), false, TDocTrack(objItem).pTrackNo);

      //------------------------------------------------------------------------
      // Draw Position and Length
      //------------------------------------------------------------------------
      Inc(Pos.Y, objTxtSize.cy);

      objDrawBitmap.Canvas.TextOut(LabelAPos, Pos.Y, resDocTrackPos);

      sTmp := ToTime(round(objPos));
      if objPaused then sTmp := sTmp + ' ' + resDocTrackPaused;

      DrawTextEll(objDrawBitmap.Canvas,
      Rect(LabelAPos,0, LabelBPos - 5, Pos.Y + objTxtSize.cy),
      Point(TextAPos, Pos.Y), false, sTmp);

      objDrawBitmap.Canvas.TextOut(LabelBPos, Pos.Y, resDocTrackLength);

      DrawTextEll(objDrawBitmap.Canvas,
      Rect(LabelBPos, 0, objRect.Right - 5, Pos.Y + objTxtSize.cy),
      Point(TextBPos, Pos.Y), false, ToTime(round(TDocTrack(objItem).pLength)));

      //------------------------------------------------------------------------
      // Draw Created and Size
      //------------------------------------------------------------------------
      (*Inc(Pos.Y, objTxtSize.cy);

      objDrawBitmap.Canvas.TextOut(LabelAPos, Pos.Y, 'Created');

      DrawTextEll(objDrawBitmap.Canvas,
      Rect(LabelAPos,0, LabelBPos - 5, Pos.Y + objTxtSize.cy),
      Point(TextAPos, Pos.Y), false, MyFileTimeToStr(objItem.pCre));

      objDrawBitmap.Canvas.TextOut(LabelBPos, Pos.Y, 'Size');

      DrawTextEll(objDrawBitmap.Canvas,
      Rect(LabelBPos, 0, objRect.Right - 5, Pos.Y + objTxtSize.cy),
      Point(TextBPos, Pos.Y), false, SizeToStr(objItem.pSize));   *)

      //------------------------------------------------------------------------
      // Copy Draw Bitmap to Screen
      //------------------------------------------------------------------------

      objCanvas.CopyRect(objRect, objDrawBitmap.Canvas,
        Rect (0,0,objDrawBitmap.Width,objDrawBitmap.Height));

    end;
end;
//------------------------------------------------------------------------------
//                                 INITALIZE
//------------------------------------------------------------------------------
initialization

  FindClass(TTrackInfoWin);
end.

