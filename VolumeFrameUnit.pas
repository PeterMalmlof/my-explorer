//------------------------------------------------------------------------------
// Volume Frame
//------------------------------------------------------------------------------
unit VolumeFrameUnit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, 
  Dialogs, tiaonImageButton, ExtCtrls, Math, StdCtrls;

//------------------------------------------------------------------------------
// TVolumeFrame
//------------------------------------------------------------------------------
type
  TVolumeFrame = class(TFrame)
    VolumeBackground: TImage;
    VolumeKnob: TtiaonImageButton;
    VolumeProg: TLabel;
    procedure FrameResize(Sender: TObject);
    procedure VolumeKnobMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure VolumeKnobMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure VolumeKnobMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
  private

    objPos : double;  // Volume Position (0..objMax)
    objMax : double;  // Volume Max Position for 100% Volume

    objLastPos : double;
    objVolumeColor : TColor;

    objKnobMoving : boolean; // Volume Knob is moving
    objKnobStart  : integer; // Start Position

    procedure SetKnobPos(); // Set Volume Knob position
    procedure SetHInt();    // Set Volume HInt

    procedure SetMax(dMax : double);  // Set Volume Position
    function  GetMax(): double;       // Get Volume Position

    procedure SetPos(dPos : double);  // Set Max Volume
    function  GetPos(): double;       // Get Max Volume

    function  GetMinHgt(): integer;
    function  GetMinWdt(): integer;
    procedure SetBackground(bDark : boolean);
  public
    constructor Create (AOwner : TComponent); override;

    procedure ResizeAll(); // Resize the Volume Frame

    property pMinHgt : integer read GetMinHgt;
    property pMinWdt : integer read GetMinWdt;
    property pUiDark : boolean write SetBackground;

    property pPos : double read GetPos write SetPos;
    property pMax : double read GetMax write SetMax;
  end;

implementation

{$R *.dfm}

uses
  TGenStrUnit,            // Generic Functions
  TGenGraphicsUnit,       // Graphics Functions
  TGenLogUnit,             // Logging
  //TWorkLoadTrackUnit,   // Track Tasks
  //TWorkLoadPlayerUnit,  // Player Tasks
  //WorkFrameUnit;        // OnIdle Work Unit

  TGenClassesUnit;

const
  BackgroundHgt = 24; // Background normal height in pixels
  BackgroundWdt = 91; // Background normal width  in pixels
  KnobHgt       = 18; // Knob height in pixels
  KnobWdt       = 11; // Knob width  in pixels
  KnobBrd       =  3; // Border

  ResStrVolume = 'Volume';

//------------------------------------------------------------------------------
// Constructor
//------------------------------------------------------------------------------
constructor TVolumeFrame.Create (AOwner : TComponent);
begin
  inherited Create(AOwner);

  objMax := 1.0;
  objPos := 0;

  objKnobMoving := false;

  VolumeBackground.Transparent := true;
  VolumeBackground.Picture.Bitmap.TransparentColor := self.Color;
  VolumeBackground.Picture.Bitmap.TransparentMode  := tmFixed;

  objVolumeColor := self.Color;
end;
//------------------------------------------------------------------------------
// Internal: Set Max Position
//------------------------------------------------------------------------------
procedure TVolumeFrame.SetMax(dMax : double);
begin
  objMax := dMax;          // Set Max Position
  objMax := Max(objMax,0); // Make sure its > 0
  SetPos(objPos);          // Secure objPosition
end;
//------------------------------------------------------------------------------
// Internal: Get Max Position
//------------------------------------------------------------------------------
function TVolumeFrame.GetMax():double;
begin
  result := objMax;
end;
//------------------------------------------------------------------------------
// Internal: Set Position
//------------------------------------------------------------------------------
procedure TVolumeFrame.SetPos(dPos : double);
begin
  objPos := dPos;                // Set Position
  objPos := Min(objPos, objMax); // Make sure its =< Max
  objPos := Max(objPos, 0);      // Make sure its >= 0
  SetKnobPos();                  // Set Knob
end;
//------------------------------------------------------------------------------
// Internal: Get Position
//------------------------------------------------------------------------------
function TVolumeFrame.GetPos():double;
begin
  result := objPos;
end;
//------------------------------------------------------------------------------
// Internal: Set Hint Text
//------------------------------------------------------------------------------
procedure TVolumeFrame.SetHint();
begin
  if (objMax > 0.0) then
    VolumeBackground.Hint := ResStrVolume + SPC +
                             IntToStr(round(100 * objPos/objMax)) + '(%)'
  else
    VolumeBackground.Hint := ResStrVolume + SPC + '0(%)';

  VolumeKnob.Hint := VolumeBackground.hint;
end;
//------------------------------------------------------------------------------
// Internal: Set Volume Knob Position
//------------------------------------------------------------------------------
procedure TVolumeFrame.SetKnobPos();
begin
  if (objMax > 0) then
    SetCtrlSize(VolumeKnob,
      round((BackgroundWdt - KnobWdt - KnobBrd*2) * objPos / objMax), KnobWdt,
      (BackgroundHgt - KnobHgt) div 2, KnobHgt)
  else
    SetCtrlSize(VolumeKnob, KnobBrd, KnobWdt,
     (BackgroundHgt - KnobHgt) div 2, KnobHgt);

  SetCtrlSize(VolumeProg,0, VolumeKnob.Left + 2,2, KnobHgt);
  SetHint;
end;
//------------------------------------------------------------------------------
// Return Minimum Heigth of Frame
//------------------------------------------------------------------------------
function TVolumeFrame.GetMinHgt(): integer;
begin
  result := BackgroundHgt;
end;
//------------------------------------------------------------------------------
// Return Minimum Width of Frame
//------------------------------------------------------------------------------
function TVolumeFrame.GetMinWdt(): integer;
begin
  result := BackGroundWdt;
end;
//------------------------------------------------------------------------------
// Event: Frame is resizing
//------------------------------------------------------------------------------
procedure TVolumeFrame.FrameResize(Sender: TObject);
begin
  ResizeAll();
end;
//------------------------------------------------------------------------------
// Resize all controls in the Frame
//------------------------------------------------------------------------------
procedure TVolumeFrame.ResizeAll();
var
  bRes : boolean;
begin

  // Set Size of Background

  bRes := SetCtrlSize(VolumeBackground, 0, BackgroundWdt, 0, BackgroundHgt);

  // Adjust Volume Knob Position

  SetKnobPos();

  TheLog.Log(self.ClassName + 'ResizeAll ' + ToStr(bRes));
end;
//------------------------------------------------------------------------------
// Set Background Color
//------------------------------------------------------------------------------
procedure TVolumeFrame.SetBackground(bDark : boolean);
begin


  if bDark then
    begin
      self.color     := ColorBkgDark;
      objVolumeColor := ColorToSepia(ColorProgressBar,20);
    end
  else
    begin
      self.color     := ColorBkgLight;
      objVolumeColor := ColorProgressBar;
    end;

  VolumeProg.Color := objVolumeColor;

  VolumeBackground.Transparent := true;
  VolumeBackground.Picture.Bitmap.TransparentColor := ColorTransparent;
  VolumeBackground.Picture.Bitmap.TransparentMode  := tmFixed;
  repaint;
end;
//------------------------------------------------------------------------------
// Knob has started to move
//------------------------------------------------------------------------------
procedure TVolumeFrame.VolumeKnobMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  objKnobMoving := true;
  objKnobStart  := X;
  objLastPos    := objPos;
end;
//------------------------------------------------------------------------------
// Knob is moving
//------------------------------------------------------------------------------
procedure TVolumeFrame.VolumeKnobMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
  if objKnobMoving then
    begin
      // Get new position of the Knob, and secure limits

      VolumeKnob.Left := SetMinMax(VolumeKnob.Left + (X - objKnobStart),
                                   KnobBrd,
                                   (BackgroundWdt - KnobWdt - KnobBrd));

      VolumeProg.Width := VolumeKnob.Left + 2;
      
      // Convert this to real Volume (0..objMax)

      if ((BackgroundWdt - KnobWdt - KnobBrd * 2) > 0) then
        objPos := SetMinMax( ( (VolumeKnob.Left - KnobBrd)* objMax) /
                               (BackgroundWdt - KnobWdt - KnobBrd * 2),
                               0, objMax)
      else
        objPos := 0.0;

      if (objLastPos <> objPos) then
        begin
          // Set Player Volume

          objTask.AddOnIdleTask(TWorkPlayerSetVolume.Create(objPos));

          // Set Volume Hint

          SetHint();

          Application.ActivateHint(Mouse.CursorPos);

          objLastPos := objPos;
        end;
    end;
end;
//------------------------------------------------------------------------------
// Knob has stop to move
//------------------------------------------------------------------------------
procedure TVolumeFrame.VolumeKnobMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  objKnobMoving := false;
end;

end.
