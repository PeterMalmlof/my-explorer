unit TSaverWinUnit;

interface
uses
  Windows, ExtCtrls, Graphics, Messages,

  TGenRectWinUnit,    // Base RectWin Class
  TGenTimeUnit;       // Time Functions

//------------------------------------------------------------------------------
//  MESSAGE IDS
//------------------------------------------------------------------------------
const
  WM_SAVER_TIME    = WM_USER + 224; // Screen Saver is Activated
  WM_SAVER_REPAINT = WM_USER + 225; // Need to repaint all

//------------------------------------------------------------------------------
//  Object
//------------------------------------------------------------------------------
type TSaverWin = class(TGenRectWin)
  protected
    objTimer : TTimer;
    objClock : TGenTime;

    function  GetUid: string; override;

    procedure SetEnabled (const Value : boolean); override;
    procedure SetState   (const Value : integer); override;

    procedure OnClock (Sender: TObject);

    function  GetString: string;

  public
    constructor Create(const C : TCanvas); override;

    destructor  Destroy; override;

    procedure Reset;
    procedure Paint;     override;

    property  pString : string read GetString;
end;

implementation

uses
  SysUtils,
  Math,
  Types,
  Forms,
  
  TGenStrUnit,        // String Functions
  TGenLogUnit,        // Log Object
  TGenClassesUnit;    // Class Management

const
  SaverTime = 2 * 60 * 1000; // ScreenSaver on 2 Minutes
  ClockTime =      2 * 1000; // Initial only

//------------------------------------------------------------------------------
//  Constructor
//------------------------------------------------------------------------------
constructor TSaverWin.Create(const C : TCanvas);
begin
  inherited Create(C);

  objTimer          := TTimer.Create(nil);
  objTimer.OnTimer  := OnClock;
  objTimer.Interval := SaverTime;
  objTimer.Enabled  := false;

  objClock := TGenTime.Create(false);
  objClock.Now;
end;
//------------------------------------------------------------------------------
//  Destructor
//------------------------------------------------------------------------------
destructor TSaverWin.Destroy;
begin

  objTimer.Free;
  objClock.Free;

  inherited;
end;
//------------------------------------------------------------------------------
// Get Default Window Position, must be overridden by all Windows
//------------------------------------------------------------------------------
function TSaverWin.GetUid:string;
begin
  result := 'SaverWin';
end;
//------------------------------------------------------------------------------
//  Set State (Override for special management)
//------------------------------------------------------------------------------
procedure TSaverWin.SetState(const Value : integer);
begin
  if (Value = StateSaver) then
    begin
      objEnabled := true;
      objVisible := true;
      objTimer.Interval := ClockTime;
      objTimer.Enabled := true;
    end
  else if (Value = StateView) then
    begin
      objEnabled := true;
      objVisible := false;
      objTimer.Interval := SaverTime;
      objTimer.Enabled := true;
    end
  else
    begin
      objEnabled := false;
      objVisible := false;
      objTimer.Enabled := false;
    end;
end;
//------------------------------------------------------------------------------
//  Set Rect
//------------------------------------------------------------------------------
procedure TSaverWin.OnClock (Sender: TObject);
begin
  if objEnabled then
    begin
      // Set Interval just after next minute
      // Note: not Zero, that will disable the Timer

      if objVisible then
        PostMessage(Application.Handle, WM_SAVER_REPAINT, 0, 0)
      else
        PostMessage(Application.Handle, WM_SAVER_TIME, 0, 0);
    end;
end;
//------------------------------------------------------------------------------
//  Reset Timer
//------------------------------------------------------------------------------
procedure TSaverWin.Reset;
begin
  if objEnabled then
    begin
      objTimer.Enabled  := false;
      objTimer.Interval := SaverTime;
      objTimer.Enabled  := true;
    end;
end;
//------------------------------------------------------------------------------
//  Set Rect
//------------------------------------------------------------------------------
procedure TSaverWin.SetEnabled (const Value : boolean);
begin
  objEnabled := Value;
  objTimer.Enabled := objEnabled;
end;
//------------------------------------------------------------------------------
//  Set Rect
//------------------------------------------------------------------------------
function TSaverWin.GetString :string;
begin
  result := objClock.DateStr + ' ' +
            objClock.HourStr + ':' +
            objClock.MinuteStr;
end;
//------------------------------------------------------------------------------
//  Paint
//------------------------------------------------------------------------------
procedure TSaverWin.Paint;
var
  sTmp : string;
  TextSize : TSize;
begin
  inherited;

  if objVisible then
    begin
      objClock.Now;

      sTmp := self.GetString;

      // Get Text Size

      TextSize := objCanvas.TextExtent(sTmp);

      // Set Colors

      objCanvas.Brush.Color := clBlack;
      objCanvas.Brush.Style := bsSolid;
      objCanvas.Font.Color  := objForeColor;

      objCanvas.TextOut(
            20 + Random(screen.Width  - TextSize.cx - 40),
            20 + Random(screen.Height - TextSize.cy - 40),
            sTmp);
    end;
end;
//------------------------------------------------------------------------------
//                                 INITALIZE
//------------------------------------------------------------------------------
initialization

  FindClass(TSaverWin);
end.

